import {by, element, ElementFinder} from 'protractor';

export class TopNav {
    static getLogOutButton(): ElementFinder {
        return element(by.tagName('button'));
    }
}
