import { browser, element, by } from 'protractor';

export class DpmUiPage {
  static navigateTo() {
    return browser.get('/');
  }

  static getParagraphText() {
    return element(by.css('dpm-root p')).getText();
  }
}
