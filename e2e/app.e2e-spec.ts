import {DpmUiPage} from './app.po';
import {LoginPage} from './login/login.po';
import {Credentials} from '../src/app/core/auth/models/credentials';
import {browser} from 'protractor';
import {TopNav} from './top-nav/top-nav.po';

const credentials: Credentials = {
    username: 'alioscia@studiostorti.com',
    password: 'alioscia'
};

describe('DPM Home page', function () {


    it('should display message saying dashboard works', () => {

        LoginPage.attemptLogin(credentials).then(() => {

            browser.wait(function () {
                    return DpmUiPage.getParagraphText();
                }, 5000)
                .then(() => {
                    expect<any>(DpmUiPage.getParagraphText()).toEqual('dashboard works!');

                    TopNav.getLogOutButton().click();
                });
        });
    });
});
