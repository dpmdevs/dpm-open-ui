import {LoginPage} from './login.po';
import {browser} from 'protractor';

describe('DPM login page', function () {

    beforeAll(() => {
        browser.restart();
    });

    it('should display username field', () => {
        LoginPage.navigateTo().then(() => {
            expect(LoginPage.getUsernameInput().isPresent()).toBeTruthy();
        });
    });

    it('should display password field', () => {
        LoginPage.navigateTo().then(() => {
            expect(LoginPage.getPasswordInput().isPresent()).toBeTruthy();
        });
    });

    it('should display login button', () => {
        LoginPage.navigateTo().then(() => {
            expect(LoginPage.getLoginButton().isPresent()).toBeTruthy();
        });
    });
});
