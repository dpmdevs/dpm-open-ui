import {browser, element, by, ElementFinder} from 'protractor';
import * as webdriver from 'selenium-webdriver';
import {Credentials} from '../../src/app/core/auth/models/credentials';
import Promise = webdriver.promise.Promise;

export class LoginPage {
    static navigateTo(): Promise<any> {
        return browser.get('/login');
    }

    static getUsernameInput(): ElementFinder {
        return element(by.id('username'));
    }

    static getPasswordInput(): ElementFinder {
        return element(by.id('password'));
    }

    static getLoginButton(): ElementFinder {
        return element(by.css('button[type="button"]'));
    }

    static attemptLogin(credentials: Credentials): Promise<any> {

        return this.navigateTo().then(() => {

            this.getUsernameInput().sendKeys(credentials.username);
            this.getPasswordInput().sendKeys(credentials.password);

            this.getLoginButton().click();
        });

    }
}
