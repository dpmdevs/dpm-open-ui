/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Action } from '@ngrx/store';

import { DPO } from '../../../../shared/models';


export interface DPOAction extends Action {
    payload?: DPO | DPO[] | number;
}

export class DPOsActions {
    static ADD_DPO = '[DPOs] Adding DPO';
    static REMOVE_DPO = '[DPOs] Removing DPO';
    static SET_DPOS = '[DPOs] Setting DPOs';
    static RESET_DPOS = '[DPOs] Resetting DPOs';

    static addDPO(data: DPO): DPOAction {
        return {
            type: DPOsActions.ADD_DPO,
            payload: data
        };
    }

    static removeDPO(id: number): DPOAction {
        return {
            type: DPOsActions.REMOVE_DPO,
            payload: id
        };
    }

    static setDPOs(data: DPO[]): DPOAction {
        return {
            type: DPOsActions.SET_DPOS,
            payload: data
        };
    }

    static resetDPOs(): DPOAction {
        return { type: DPOsActions.RESET_DPOS };
    }
}
