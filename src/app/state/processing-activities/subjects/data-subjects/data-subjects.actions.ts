/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Action } from '@ngrx/store';

import { DataSubject } from '../../../../shared/models';


export interface DSAction extends Action {
    payload?: DataSubject | DataSubject[] | number;
}

export class DataSubjectsActions {
    static ADD_DATA_SUBJECT = '[Data Subjects] Adding Data Subject';
    static REMOVE_DATA_SUBJECT = '[Data Subjects] Removing Data Subject';
    static SET_DATA_SUBJECTS = '[Data Subjects] Setting Data Subjects';
    static RESET_DATA_SUBJECTS = '[Data Subjects] Resetting Data Subjects';

    static addDataSubject(data: DataSubject): DSAction {
        return {
            type: DataSubjectsActions.ADD_DATA_SUBJECT,
            payload: data
        }
    }

    static removeDataSubject(id: number): DSAction {
        return {
            type: DataSubjectsActions.REMOVE_DATA_SUBJECT,
            payload: id
        }
    }

    static setDataSubjects(data: DataSubject[]): DSAction {
        return {
            type: DataSubjectsActions.SET_DATA_SUBJECTS,
            payload: data
        }
    }

    static resetDataSubjects(): DSAction {
        return { type: DataSubjectsActions.RESET_DATA_SUBJECTS };
    }
}
