/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { DataSubject } from '../../../../shared/models';
import { DataSubjectsActions, DSAction } from './data-subjects.actions';


export function dataSubjectsReducers(state: DataSubject[] = [], action: DSAction): DataSubject[] {
    switch (action.type) {
        case DataSubjectsActions.ADD_DATA_SUBJECT:
            return [...state, <DataSubject>action.payload];

        case DataSubjectsActions.REMOVE_DATA_SUBJECT:
            return state.filter(ds => ds.id !== action.payload);

        case DataSubjectsActions.SET_DATA_SUBJECTS:
            return <DataSubject[]>action.payload;

        case DataSubjectsActions.RESET_DATA_SUBJECTS:
            return [];

        default:
            return state;
    }
}
