/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Action } from '@ngrx/store';

import { ThirdParties } from '../../../../shared/models';


export interface ExternalProcessorsAction extends Action {
    payload?: ThirdParties | ThirdParties[] | number;
}

export class ExternalProcessorsActions {
    static ADD_PROCESSOR = '[External Processors] Adding External Processor';
    static REMOVE_PROCESSOR = '[External Processors] Removing External Processor';
    static SET_PROCESSORS = '[External Processors] Setting External Processors';
    static RESET_PROCESSORS = '[External Processors] Removing External Processors';

    static addProcessor(data: ThirdParties): ExternalProcessorsAction {
        return {
            type: ExternalProcessorsActions.ADD_PROCESSOR,
            payload: data
        };
    }

    static removeProcessor(id: number): ExternalProcessorsAction {
        return {
            type: ExternalProcessorsActions.REMOVE_PROCESSOR,
            payload: id
        };
    }

    static setProcessors(data: ThirdParties[]): ExternalProcessorsAction {
        return {
            type: ExternalProcessorsActions.SET_PROCESSORS,
            payload: data
        };
    }

    static resetProcessors(): ExternalProcessorsAction {
        return { type: ExternalProcessorsActions.RESET_PROCESSORS };
    }
}
