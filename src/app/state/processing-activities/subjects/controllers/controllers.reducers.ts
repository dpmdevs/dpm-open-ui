/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Controller } from '@dpm-models';
import { ControllerAction, ControllersActions } from './controllers.actions';


export function controllersReducers(state: Controller[] = [], action: ControllerAction): Controller[] {
    switch (action.type) {
        case ControllersActions.ADD_CONTROLLER:
            return [...state, <Controller>action.payload];

        case ControllersActions.REMOVE_CONTROLLER:
            return state.filter(c => c.id !== action.payload);

        case ControllersActions.SET_CONTROLLERS:
            return <Controller[]>action.payload;

        case ControllersActions.RESET_CONTROLLERS:
            return [];

        default:
            return state;
    }
}
