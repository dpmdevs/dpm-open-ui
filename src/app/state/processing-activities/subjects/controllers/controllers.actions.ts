/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Action } from '@ngrx/store';

import { Controller } from '@dpm-models';


export interface ControllerAction extends Action {
    payload?: Controller | Controller[] | number;
}

export class ControllersActions {
    static ADD_CONTROLLER = '[Controllers] Adding Controller';
    static REMOVE_CONTROLLER = '[Controllers] Removing Controller';
    static SET_CONTROLLERS = '[Controllers] Setting Controllers';
    static RESET_CONTROLLERS = '[Controllers] Resetting Controllers';

    static addController(data: Controller): ControllerAction {
        return {
            type: ControllersActions.ADD_CONTROLLER,
            payload: data
        };
    }

    static removeController(id: number): ControllerAction {
        return {
            type: ControllersActions.REMOVE_CONTROLLER,
            payload: id
        };
    }

    static setControllers(data: Controller[]): ControllerAction {
        return {
            type: ControllersActions.SET_CONTROLLERS,
            payload: data
        };
    }

    static resetControllers(): ControllerAction {
        return { type: ControllersActions.RESET_CONTROLLERS };
    }
}
