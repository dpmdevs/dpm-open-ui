/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { ActionReducer, combineReducers, compose } from '@ngrx/store';
import { storeFreeze } from 'ngrx-store-freeze';

import { ISubjects } from './subjects';
import { environment } from 'environments/environment';
import { dataSubjectsReducers } from './data-subjects/data-subjects.reducers';
import { controllersReducers } from './controllers/controllers.reducers';
import { internalProcessorsReducers } from './internal-processors/internal-processors.reducers';
import { externalProcessorsReducers } from './external-processors/external-processors.reducers';
import { dposReducers } from './DPOs/dpos.reducers';


export const reducers = {
    data_subjects: dataSubjectsReducers,
    controllers: controllersReducers,
    internal_processors: internalProcessorsReducers,
    external_processors: externalProcessorsReducers,
    dpos: dposReducers,
};

// see here: https://github.com/ngrx/store/issues/190#issuecomment-247074138
const developmentReducer: ActionReducer<any> = compose(storeFreeze, combineReducers)(reducers);
const productionReducer: ActionReducer<ISubjects> = combineReducers(reducers);

export function subjectsReducers(state: any, action: any) {
    return environment.production
        ? productionReducer(state, action)
        : developmentReducer(state, action)
        ;
}
