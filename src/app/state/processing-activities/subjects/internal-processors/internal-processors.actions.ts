/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Action } from '@ngrx/store';

import { ProcessorTableData } from '../../../../authenticated/processing-activities/wizard-steps/subjects-step/internal-processors-selector/internal-processors';


export interface ProcessorAction extends Action {
    payload?: ProcessorTableData | ProcessorTableData[] | number;
}

export class InternalProcessorsActions {
    static ADD_PROCESSOR = '[Internal Processors] Adding Internal Processor';
    static REMOVE_PROCESSOR = '[Internal Processors] Removing Internal Processor';
    static SET_PROCESSORS = '[Internal Processors] Setting Internal Processors';
    static RESET_PROCESSORS = '[Internal Processors] Removing Internal Processors';

    static addProcessor(data: ProcessorTableData): ProcessorAction {
        return {
            type: InternalProcessorsActions.ADD_PROCESSOR,
            payload: data
        };
    }

    static removeProcessor(id: number): ProcessorAction {
        return {
            type: InternalProcessorsActions.REMOVE_PROCESSOR,
            payload: id
        };
    }

    static setProcessors(data: ProcessorTableData[]): ProcessorAction {
        return {
            type: InternalProcessorsActions.SET_PROCESSORS,
            payload: data
        };
    }

    static resetProcessors(): ProcessorAction {
        return { type: InternalProcessorsActions.RESET_PROCESSORS };
    }
}
