/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { ProcessorTableData } from '../../../../authenticated/processing-activities/wizard-steps/subjects-step/internal-processors-selector/internal-processors';
import { ProcessorAction, InternalProcessorsActions } from './internal-processors.actions';

export function internalProcessorsReducers(state: ProcessorTableData[] = [], action: ProcessorAction): ProcessorTableData[] {
    switch (action.type) {
        case InternalProcessorsActions.ADD_PROCESSOR:
            return [...state, <ProcessorTableData>action.payload];

        case InternalProcessorsActions.REMOVE_PROCESSOR:
            return state.filter(p => p.id !== action.payload);

        case InternalProcessorsActions.SET_PROCESSORS:
            return <ProcessorTableData[]>action.payload;

        case InternalProcessorsActions.RESET_PROCESSORS:
            return [];

        default:
            return state;
    }
}
