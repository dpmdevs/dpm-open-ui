/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Action } from '@ngrx/store';


export interface SecurityMeasuresAction extends Action {
    payload?: number[];
}

export class SecurityMeasuresActions {
    static SET_SECURITY_MEASURE = '[SecurityMeasures] Setting Security Measures';
    static RESET_SECURITY_MEASURE = '[SecurityMeasures] Resetting Security Measures';

    static setSecurityMeasures(data: number[]): SecurityMeasuresAction {
        return {
            type: SecurityMeasuresActions.SET_SECURITY_MEASURE,
            payload: data
        };
    }

    static resetSecurityMeasures(): SecurityMeasuresAction {
        return {
            type: SecurityMeasuresActions.RESET_SECURITY_MEASURE
        };
    }
}
