/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { ActionReducer, combineReducers, compose } from '@ngrx/store';
import { storeFreeze } from 'ngrx-store-freeze';

import { environment } from 'environments/environment';
import { DisclosureSubject, StepState } from '@dpm-models';
import { IBasicData, initialBasicDataState, basicDataReducers } from './basic-data';
import { ISubjects, initialSubjectsState, subjectsReducers } from './subjects';
import { disclosureSubjectsReducers } from './disclosure-subjects/disclosure-subjects.reducers';
import { IOtherData, initialOtherDataState, otherDataReducers } from './other-data';
import { selectedSecurityMeasuresIds } from './security-measures/security-measures.reducers';
import { initialPaCompletionState } from './pa-completion-state/pa-completion-state';
import { paCompletionStateReducers } from './pa-completion-state/pa-completion-state.reducers';
import { currentPAIdReducer } from './current-pa-id/current-pa-id.reducers';


export interface IProcessingActivities {
    currentProcessingActivityId: number;
    paCompletionState: StepState[];
    basicData: IBasicData;
    subjects: ISubjects;
    disclosureSubjects: DisclosureSubject[];
    otherData: IOtherData;
    selectedSecurityMeasuresIds: number[];
}

export const initialProcessingActivitiesState: IProcessingActivities = {
    currentProcessingActivityId: null,
    paCompletionState: initialPaCompletionState,
    basicData: initialBasicDataState,
    subjects: initialSubjectsState,
    disclosureSubjects: [],
    otherData: initialOtherDataState,
    selectedSecurityMeasuresIds: []
};

const reducers = {
    currentProcessingActivityId: currentPAIdReducer,
    paCompletionState: paCompletionStateReducers,
    basicData: basicDataReducers,
    subjects: subjectsReducers,
    disclosureSubjects: disclosureSubjectsReducers,
    otherData: otherDataReducers,
    selectedSecurityMeasuresIds: selectedSecurityMeasuresIds
};

// see here: https://github.com/ngrx/store/issues/190#issuecomment-247074138
const developmentReducer: ActionReducer<any> = compose(storeFreeze, combineReducers)(reducers);
const productionReducer: ActionReducer<IProcessingActivities> = combineReducers(reducers);

export function processingActivitiesReducers(state: any, action: any) {
    return environment.production
        ? productionReducer(state, action)
        : developmentReducer(state, action)
    ;
}
