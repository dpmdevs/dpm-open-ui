/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Action } from '@ngrx/store';

import { ThirdParties } from '@dpm-models';


export interface DataSourcesAction extends Action {
    payload?: ThirdParties | ThirdParties[] | number;
}

export class DataSourcesActions {
    static ADD_DATA_SOURCE = '[Data Sources] Adding Data Source';
    static REMOVE_DATA_SOURCE = '[Data Sources] Removing Data Source';
    static SET_DATA_SOURCES = '[Data Sources] Setting Data Sources';
    static RESET_DATA_SOURCES = '[Data Sources] Resetting Data Sources';

    static addDataSource(data: ThirdParties): DataSourcesAction {
        return {
            type: DataSourcesActions.ADD_DATA_SOURCE,
            payload: data
        }
    }

    static removeDataSource(id: number): DataSourcesAction {
        return {
            type: DataSourcesActions.REMOVE_DATA_SOURCE,
            payload: id
        }
    }

    static setDataSources(data: ThirdParties[]): DataSourcesAction {
        return {
            type: DataSourcesActions.SET_DATA_SOURCES,
            payload: data
        }
    }

    static resetDataSources(): DataSourcesAction {
        return { type: DataSourcesActions.RESET_DATA_SOURCES };
    }
}
