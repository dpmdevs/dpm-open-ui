/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { IBasicData, initialBasicDataState } from './basic-data';
import { BasicDataAction, BasicDataActions } from './basic-data.actions';
import { legalBasisReducers } from './legal-basis/legal-basis.reducers';
import { LegalBasisAction, LegalBasisActions } from './legal-basis/legal-basis.actions';
import { DataSourceTypesAction, DataSourceTypesActions } from './data-source-types/data-source-types.actions';
import { DataSourcesAction, DataSourcesActions } from './data-sources/data-sources.actions';
import { dataSourcesReducers } from './data-sources/data-sources.reducers';
import { dataSourceTypesReducers } from './data-source-types/data-source-types.reducer';


export function basicDataReducers(state: IBasicData = initialBasicDataState,
                                  action: BasicDataAction | LegalBasisAction | DataSourcesAction | DataSourceTypesAction): IBasicData {
    switch (action.type) {
        case BasicDataActions.SET_DATA:
            return Object.assign({}, initialBasicDataState, action.payload);

        case BasicDataActions.RESET_DATA:
            return initialBasicDataState;

        case LegalBasisActions.ADD_LEGAL_BASIS:
        case LegalBasisActions.REMOVE_LEGAL_BASIS:
        case LegalBasisActions.SET_LEGAL_BASIS:
        case LegalBasisActions.RESET_LEGAL_BASIS:
            const legal_basis = legalBasisReducers(state.legal_basis, <LegalBasisAction>action);
            return Object.assign({}, state, { legal_basis });

        case DataSourceTypesActions.ADD_DATA_SOURCE_TYPE:
        case DataSourceTypesActions.REMOVE_DATA_SOURCE_TYPE:
        case DataSourceTypesActions.SET_DATA_SOURCE_TYPES:
        case DataSourceTypesActions.RESET_DATA_SOURCE_TYPES:
            const data_source_types = dataSourceTypesReducers(state.data_source_types, <DataSourceTypesAction>action);
            return Object.assign({}, state, { data_source_types });

        case DataSourcesActions.ADD_DATA_SOURCE:
        case DataSourcesActions.REMOVE_DATA_SOURCE:
        case DataSourcesActions.SET_DATA_SOURCES:
        case DataSourcesActions.RESET_DATA_SOURCES:
            const data_sources = dataSourcesReducers(state.data_sources, <DataSourcesAction>action);
            return Object.assign({}, state, { data_sources });

        default:
            return state;
    }
}
