/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Action } from '@ngrx/store';

export interface DataSourceTypesAction extends Action {
    payload?: number | number[];
}

export class DataSourceTypesActions {
    static ADD_DATA_SOURCE_TYPE = '[Data Source Types] Adding Data Source Type';
    static REMOVE_DATA_SOURCE_TYPE = '[Data Source Types] Removing Data Source Type';
    static SET_DATA_SOURCE_TYPES = '[Data Source Types] Setting Data Source Types';
    static RESET_DATA_SOURCE_TYPES = '[Data Source Types] Resetting Data Source Types';

    static addDataSubjectType(type: number): DataSourceTypesAction {
        return {
            type: DataSourceTypesActions.ADD_DATA_SOURCE_TYPE,
            payload: type
        }
    }

    static removeDataSubjectType(type: number): DataSourceTypesAction {
        return {
            type: DataSourceTypesActions.REMOVE_DATA_SOURCE_TYPE,
            payload: type
        }
    }

    static setDataSourceTypes(data: number[]): DataSourceTypesAction {
        return {
            type: DataSourceTypesActions.SET_DATA_SOURCE_TYPES,
            payload: data
        };
    }

    static resetDataSourceTypes(): DataSourceTypesAction {
        return { type: DataSourceTypesActions.RESET_DATA_SOURCE_TYPES };
    }
}
