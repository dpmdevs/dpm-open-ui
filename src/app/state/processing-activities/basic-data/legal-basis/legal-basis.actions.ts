/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Action } from '@ngrx/store';

import { LegalBasis } from '@dpm-models';


export interface LegalBasisAction extends Action {
    payload?: LegalBasis | LegalBasis[] | number;
}

export class LegalBasisActions {
    static ADD_LEGAL_BASIS = '[Legal Basis] Adding Legal Basis';
    static REMOVE_LEGAL_BASIS = '[Legal Basis] Removing Legal Basis';
    static SET_LEGAL_BASIS = '[Legal Basis] Setting Legal Basis';
    static RESET_LEGAL_BASIS = '[Legal Basis] Resetting Legal Basis';

    static addLegalBasis(data: LegalBasis): LegalBasisAction {
        return {
            type: LegalBasisActions.ADD_LEGAL_BASIS,
            payload: data
        };
    }

    static removeLegalBasis(id: number): LegalBasisAction {
        return {
            type: LegalBasisActions.REMOVE_LEGAL_BASIS,
            payload: id
        };
    }

    static setLegalBasis(data: LegalBasis[]): LegalBasisAction {
        return {
            type: LegalBasisActions.SET_LEGAL_BASIS,
            payload: data
        };
    }

    static resetLegalBasis(): LegalBasisAction {
        return { type: LegalBasisActions.RESET_LEGAL_BASIS };
    }
}
