/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { DataCategory, LegalBasis, ProcessType, ThirdParties } from '@dpm-models';


export interface IBasicData {
    identification_code: string;
    name: string;
    description: string;
    process_types: ProcessType[];
    data_categories: DataCategory[];
    purpose: string;
    legal_references: string;
    legal_basis: LegalBasis[];
    data_sources: ThirdParties[];
    data_source_types: number[];
    is_active: boolean;
}

export const initialBasicDataState: IBasicData = {
    identification_code: '',
    name: '',
    description: '',
    process_types: [],
    data_categories: [],
    purpose: '',
    legal_references: '',
    legal_basis: [],
    data_sources: [],
    data_source_types: [],
    is_active: true,
};
