/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Action } from '@ngrx/store';

import { StepState } from '../../../shared/models';


export interface StepAction extends Action {
    payload?: StepState | StepState[];
}

export class PaCompletionStateActions {
    static UPDATE_STEP_STATE = '[PA Step State] Updating Step';
    static SET_STEPS_STATE = '[PA Step State] Setting Steps';
    static RESET_STEPS_STATE = '[PA Step State] Resetting Steps';

    static updateStepState(newStepState: StepState): StepAction {
        return {
            type: PaCompletionStateActions.UPDATE_STEP_STATE,
            payload: newStepState
        };
    }

    static setStepsState(data: StepState[]): StepAction {
        return {
            type: PaCompletionStateActions.SET_STEPS_STATE,
            payload: data
        };
    }

    static resetStepsState(): StepAction {
        return { type: PaCompletionStateActions.RESET_STEPS_STATE };
    }
}
