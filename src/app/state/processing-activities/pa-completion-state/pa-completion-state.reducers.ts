/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { StepState } from '../../../shared/models';
import { initialPaCompletionState } from './pa-completion-state';
import { PaCompletionStateActions, StepAction } from './pa-completion-state.actions';


export function paCompletionStateReducers(state: StepState[] = initialPaCompletionState, action: StepAction): StepState[] {

    switch (action.type) {
        case PaCompletionStateActions.UPDATE_STEP_STATE:
            return state.map((step: StepState) => {
                const payload = <StepState>action.payload;

                if (step.id !== payload.id)
                    return step;

                return <StepState>Object.assign({}, step, payload);
            });

        case PaCompletionStateActions.SET_STEPS_STATE:
            return <StepState[]>action.payload;

        case PaCompletionStateActions.RESET_STEPS_STATE:
            return initialPaCompletionState;

        default:
            return state;
    }

}
