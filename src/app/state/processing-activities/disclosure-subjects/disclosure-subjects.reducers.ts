/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { DisclosureSubject } from '../../../shared/models';
import { DisclosureSubjectAction, DisclosureSubjectsActions } from './disclosure-subjects.actions';


export function disclosureSubjectsReducers(state: DisclosureSubject[] = [],
                                           action: DisclosureSubjectAction): DisclosureSubject[] {
    switch (action.type) {
        case DisclosureSubjectsActions.ADD_DISCLOSURE_SUBJECT:
            return [...state, <DisclosureSubject>action.payload];

        case DisclosureSubjectsActions.REMOVE_DISCLOSURE_SUBJECT:
            return state.filter(ds => ds.id !== action.payload);

        case DisclosureSubjectsActions.UPDATE_DISCLOSURE_SUBJECT: {
            const disclosureSubject = <DisclosureSubject>action.payload;
            const disclosureSubjects = [...state];
            const index = state.findIndex(ds => ds.id === disclosureSubject.id);
            disclosureSubjects.splice(index, 1, disclosureSubject);
            return disclosureSubjects;
        }

        case DisclosureSubjectsActions.SET_DISCLOSURE_SUBJECTS:
            return <DisclosureSubject[]>action.payload;

        case DisclosureSubjectsActions.RESET_DISCLOSURE_SUBJECTS:
            return [];

        default:
            return state;
    }
}
