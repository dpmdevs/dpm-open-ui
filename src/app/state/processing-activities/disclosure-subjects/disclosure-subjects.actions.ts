/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Action } from '@ngrx/store';

import { DisclosureSubject } from '../../../shared/models';


export interface DisclosureSubjectAction extends Action {
    payload?: DisclosureSubject | DisclosureSubject[] | number;
}

export class DisclosureSubjectsActions {
    static ADD_DISCLOSURE_SUBJECT = '[Disclosure Subjects] Adding Disclosure Subject';
    static REMOVE_DISCLOSURE_SUBJECT = '[Disclosure Subjects] Removing Disclosure Subject';
    static UPDATE_DISCLOSURE_SUBJECT = '[Disclosure Subjects] Updating Disclosure Subject';
    static SET_DISCLOSURE_SUBJECTS = '[Disclosure Subjects] Setting Disclosure Subjects';
    static RESET_DISCLOSURE_SUBJECTS = '[Disclosure Subjects] Resetting Disclosure Subjects';

    static addDisclosureSubject(data: DisclosureSubject): DisclosureSubjectAction {
        return {
            type: DisclosureSubjectsActions.ADD_DISCLOSURE_SUBJECT,
            payload: data
        };
    }

    static removeDisclosureSubject(id: number): DisclosureSubjectAction {
        return {
            type: DisclosureSubjectsActions.REMOVE_DISCLOSURE_SUBJECT,
            payload: id
        };
    }

    static updateDisclosureSubject(data: DisclosureSubject): DisclosureSubjectAction {
        return {
            type: DisclosureSubjectsActions.UPDATE_DISCLOSURE_SUBJECT,
            payload: data
        };
    }

    static setDisclosureSubjects(data: DisclosureSubject[]): DisclosureSubjectAction {
        return {
            type: DisclosureSubjectsActions.SET_DISCLOSURE_SUBJECTS,
            payload: data
        };
    }

    static resetDisclosuresubjects(): DisclosureSubjectAction {
        return { type: DisclosureSubjectsActions.RESET_DISCLOSURE_SUBJECTS };
    }
}
