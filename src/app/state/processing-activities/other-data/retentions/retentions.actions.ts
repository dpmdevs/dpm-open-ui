/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Action } from '@ngrx/store';

import { Retention } from '../../../../shared/models';


export interface RetentionAction extends Action {
    payload?: Retention | Retention[] | number;
}

export class RetentionsActions {
    static ADD_RETENTION = '[Retentions] Adding Retention';
    static REMOVE_RETENTION = '[Retentions] Removing Retention';
    static SET_RETENTIONS = '[Retentions] Setting Retentions';
    static RESET_RETENTIONS = '[Retentions] Resetting Retentions';

    static addRetention(data: Retention): RetentionAction {
        return {
            type: RetentionsActions.ADD_RETENTION,
            payload: data
        };
    }

    static removeRetention(id: number): RetentionAction {
        return {
            type: RetentionsActions.REMOVE_RETENTION,
            payload: id
        };
    }

    static setRetentions(data: Retention[]): RetentionAction {
        return {
            type: RetentionsActions.SET_RETENTIONS,
            payload: data
        };
    }

    static resetRetentions(): RetentionAction {
        return { type: RetentionsActions.RESET_RETENTIONS };
    }
}

