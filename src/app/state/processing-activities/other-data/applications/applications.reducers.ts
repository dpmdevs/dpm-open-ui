/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { ApplicationAction, ApplicationsActions } from './applications.actions';

import { Application } from '../../../../shared/models';


export function applicationsReducers(state: Application[] = [], action: ApplicationAction): Application[] {
    switch (action.type) {
        case ApplicationsActions.ADD_APPLICATION:
            return [...state, <Application>action.payload];

        case ApplicationsActions.REMOVE_APPLICATION:
            return state.filter(app => app.id !== action.payload);

        case ApplicationsActions.SET_APPLICATIONS:
            return <Application[]>action.payload;

        case ApplicationsActions.RESET_APPLICATIONS:
            return [];

        default:
            return state;
    }
}
