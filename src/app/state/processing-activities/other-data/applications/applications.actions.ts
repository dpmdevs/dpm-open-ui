/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Action } from '@ngrx/store';

import { Application } from '../../../../shared/models';


export interface ApplicationAction extends Action {
    payload?: Application | Application[] | number;
}

export class ApplicationsActions {
    static ADD_APPLICATION = '[Applications] Adding Application';
    static REMOVE_APPLICATION = '[Applications] Removing Application';
    static SET_APPLICATIONS = '[Applications] Setting Applications';
    static RESET_APPLICATIONS = '[Applications] Resetting Applications';

    static addApplication(data: Application): ApplicationAction {
        return {
            type: ApplicationsActions.ADD_APPLICATION,
            payload: data
        };
    }

    static removeApplication(id: number): ApplicationAction {
        return {
            type: ApplicationsActions.REMOVE_APPLICATION,
            payload: id
        };
    }

    static setApplications(data: Application[]): ApplicationAction {
        return {
            type: ApplicationsActions.SET_APPLICATIONS,
            payload: data
        };
    }

    static resetApplications(): ApplicationAction {
        return { type: ApplicationsActions.RESET_APPLICATIONS };
    }
}
