/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { initialOtherDataState, IOtherData } from './other-data';
import { OtherDataAction, OtherDataActions } from './other-data.actions';
import { ApplicationAction, ApplicationsActions } from './applications/applications.actions';
import { applicationsReducers } from './applications/applications.reducers';
import { RetentionAction, RetentionsActions } from './retentions/retentions.actions';
import { retentionsReducers } from './retentions/retentions.reducers';

export function otherDataReducers(state: IOtherData = initialOtherDataState, action: OtherDataAction | ApplicationAction): IOtherData {

    switch (action.type) {
        case OtherDataActions.SET_DATA:
            return <IOtherData>Object.assign({}, initialOtherDataState, action.payload);

        case OtherDataActions.RESET_DATA:
            return initialOtherDataState;

        case ApplicationsActions.ADD_APPLICATION:
        case ApplicationsActions.REMOVE_APPLICATION:
        case ApplicationsActions.SET_APPLICATIONS:
        case ApplicationsActions.RESET_APPLICATIONS:
            const applications = applicationsReducers(state.applications, <ApplicationAction>action);
            return Object.assign({}, state, { applications });

        case RetentionsActions.ADD_RETENTION:
        case RetentionsActions.REMOVE_RETENTION:
        case RetentionsActions.SET_RETENTIONS:
        case RetentionsActions.RESET_RETENTIONS:
            const retentions = retentionsReducers(state.retentions, <RetentionAction>action);
            return Object.assign({}, state, { retentions });

        default:
            return state;
    }

}
