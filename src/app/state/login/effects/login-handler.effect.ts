/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Action } from '@ngrx/store';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { LoginHandlerAction, LoginHandlerActions } from '../actions/login-handler.action';
import { Credentials } from '../../../core/auth/models/credentials';
import { AuthService } from '../../../core/auth';
import { UserActions } from '../../user/user.actions';
import { ConfigActions } from '../../config/config.actions';
import { StateActions } from '../../state.actions';


@Injectable()
export class LoginHandlerEffects {

    constructor(private actions$: Actions, private authService: AuthService) {
    }

    @Effect()
    attemptLogin$: Observable<Action> = this.actions$
        .ofType(LoginHandlerActions.ATTEMPT_LOGIN)
        .switchMap((action: LoginHandlerAction) => this.authService.attemptLogin(<Credentials>action.payload)
            .do(response => this.authService.saveTokenToLocalStorage(response.token))
            .switchMap(response => [
                LoginHandlerActions.loginSuccess(),
                UserActions.setAuthToken(response.token)
            ])
            .catch((error: HttpErrorResponse) => of(LoginHandlerActions.loginFail(error)))
        );


    @Effect()
    loadUserAfterLoginSuccess$: Observable<Action> = this.actions$
        .ofType(LoginHandlerActions.LOGIN_SUCCESS)
        .switchMap(() => [
            UserActions.loadUser(),
            ConfigActions.loadConfig()
        ]);

    @Effect()
    checkUserAuthentication$: Observable<Action> = this.actions$
        .ofType(LoginHandlerActions.CHECK_USER_AUTHENTICATION)
        .map((action: LoginHandlerAction) => action.payload)
        .switchMap(token => this.authService.checkUserAuthentication()
            .switchMap(response => response === true
                ? [UserActions.setAuthToken(<string>token), LoginHandlerActions.loginSuccess()]
                : of(StateActions.clearSession())
            )
            .catch(() => of(StateActions.clearSession()))
        );

}
