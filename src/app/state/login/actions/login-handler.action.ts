/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Action } from '@ngrx/store';

import { Credentials } from '../../../core/auth/models/credentials';


export interface LoginHandlerAction extends Action {
    payload?: Credentials | string | HttpErrorResponse;
}

@Injectable()
export class LoginHandlerActions {

    static ATTEMPT_LOGIN = '[Login Handler] Attempting login';
    static LOGIN_SUCCESS = '[Login Handler] Login success';
    static LOGIN_FAIL = '[Login Handler] Login fail';
    static CHECK_USER_AUTHENTICATION = '[Login Handler] Check user authentication';


    static attemptLogin(credentials: Credentials): LoginHandlerAction {
        return {
            type: LoginHandlerActions.ATTEMPT_LOGIN,
            payload: credentials
        };
    }

    static loginSuccess(): LoginHandlerAction {
        return {
            type: LoginHandlerActions.LOGIN_SUCCESS
        };
    }

    static loginFail(errors: HttpErrorResponse): LoginHandlerAction {
        return {
            type: LoginHandlerActions.LOGIN_FAIL,
            payload: errors
        };
    }

    static checkUserAuthentication(token: string): LoginHandlerAction {
        return {
            type: LoginHandlerActions.CHECK_USER_AUTHENTICATION,
            payload: token
        };
    }

}
