/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { HttpErrorResponse } from '@angular/common/http';

import { StateBase } from '../../state.model';
import { LoginHandlerAction, LoginHandlerActions } from '../actions/login-handler.action';


export interface LoginHandlerState extends StateBase {
    login_error_messages: HttpErrorResponse;
}

export const initialLoginState: LoginHandlerState = {
    loading: false,
    login_error_messages: null,
};

export function loginHandlerReducer(state = initialLoginState, action: LoginHandlerAction): LoginHandlerState {

    switch (action.type) {
        case LoginHandlerActions.ATTEMPT_LOGIN:
            return {
                ...state,
                loading: true,
                login_error_messages: null
            };

        case LoginHandlerActions.LOGIN_SUCCESS:
            return {
                ...state,
                loading: false,
            };

        case LoginHandlerActions.LOGIN_FAIL:
            return {
                ...state,
                loading: false,
                login_error_messages: <HttpErrorResponse>action.payload,
            };

        default:
            return state;
    }

}

export const getLoginHandlerLoading = (state: LoginHandlerState) => state.loading;
export const getLoginHandlerErrorMessages = (state: LoginHandlerState) => state.login_error_messages;
