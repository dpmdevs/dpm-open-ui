/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';

import { User } from '@dpm-models';


export interface UserAction extends Action {
    payload?: User | string;
}

@Injectable()
export class UserActions {

    static LOG_OUT = '[User] Logging out';
    static SESSION_EXPIRED = '[User] Session expired';
    static SET_AUTH_TOKEN = '[User] Setting Auth Token';
    static LOAD_USER = '[User] Loading user';
    static LOAD_USER_SUCCESS = '[User] User load success';

    static logOut(): UserAction {
        return {
            type: UserActions.LOG_OUT
        };
    }

    static setAuthToken(token: string): UserAction {
        return {
            type: UserActions.SET_AUTH_TOKEN,
            payload: token
        };
    }

    static loadUser(): UserAction {
        return {
            type: UserActions.LOAD_USER
        };
    }

    static loadUserSuccess(user: User): UserAction {
        return {
            type: UserActions.LOAD_USER_SUCCESS,
            payload: user
        }
    }

    static sessionExpired(): UserAction {
        return {
            type: UserActions.SESSION_EXPIRED
        };
    }

}
