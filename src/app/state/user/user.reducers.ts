/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { UserAction, UserActions } from './user.actions';
import { initialProfileState, IProfile } from './user';


export function userReducers(state: IProfile = initialProfileState, action: UserAction): IProfile {

    switch (action.type) {
        case UserActions.LOAD_USER_SUCCESS:
            return Object.assign({}, state, { user: action.payload });

        case UserActions.SET_AUTH_TOKEN:
            return Object.assign({}, state, { access_token: action.payload });

        case UserActions.SESSION_EXPIRED:
            return Object.assign({}, state, { session_expired: true });

        case UserActions.LOG_OUT:
            return Object.assign({}, state, initialProfileState);

        default:
            return state;
    }

}
