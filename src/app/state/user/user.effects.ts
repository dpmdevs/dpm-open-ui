/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Action } from '@ngrx/store';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';

import { UserAction, UserActions } from './user.actions';
import { UserService } from '../../authenticated/user/user.service';
import { ConfigService } from '../../authenticated/config/config.service';
import { AuthService } from '../../core/auth';
import { StateActions } from '../state.actions';
import { CommonActions } from '../common/common.actions';
import { User } from '@dpm-models';


@Injectable()
export class UserEffects {

    @Effect()
    loadUser$: Observable<Action> = this.actions$
        .ofType(UserActions.LOAD_USER)
        .switchMap(() => this.userService.getLoggedUser()
            .map(user => UserActions.loadUserSuccess(user))
        );

    @Effect({ dispatch: false })
    redirectUserToHomepage$: Observable<Action> = this.actions$
        .ofType(UserActions.LOAD_USER_SUCCESS)
        .do(({ payload: user }: UserAction) => {
            if (location.pathname === '/')
                this._router.navigate([(<User>user).homepage]).then();
        });

    @Effect()
    resetStoreOnLogout$: Observable<Action> = this.actions$
        .ofType(UserActions.LOG_OUT)
        .switchMap(() => this.authService.logout()
            .catch(() => {
                console.warn('Session expired');
                return Observable.of();
            })
            .map(() => StateActions.clearSession())
        );

    @Effect()
    closeClientErrorOnSessionExpired$: Observable<Action> = this.actions$
        .ofType(UserActions.SESSION_EXPIRED)
        .switchMap(() => [CommonActions.unsetClientError(), StateActions.clearSession()]);

    @Effect()
    clearSessionOnSessionExpired$: Observable<Action> = this.actions$
        .ofType(StateActions.CLEAR_SESSION)
        .do(() => this.authService.deleteTokenFromLocalStorage())
        .do(() => this._router.navigate(['/login'], {}).then())
        .map(() => StateActions.clearState());


    constructor(private actions$: Actions,
                private userService: UserService,
                private configService: ConfigService,
                private authService: AuthService,
                private _router: Router) {
    }
}
