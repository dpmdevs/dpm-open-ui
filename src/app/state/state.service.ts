/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { IState } from './state.model';
import { DisclosureSubject, QueryParams, QuestionnaireAnswer, StepState, User } from '@dpm-models';
import { Config } from 'app/state/config/config';
import { IBasicData } from './processing-activities/basic-data';
import { ISubjects } from './processing-activities/subjects';
import { IOtherData } from './processing-activities/other-data';

import { PaCompletionStateActions } from './processing-activities/pa-completion-state/pa-completion-state.actions';
import { CurrentPaIdActions } from './processing-activities/current-pa-id/current-pa-id.actions';
import { BasicDataActions } from './processing-activities/basic-data/basic-data.actions';
import { DataSubjectsActions } from './processing-activities/subjects/data-subjects/data-subjects.actions';
import { ControllersActions } from './processing-activities/subjects/controllers/controllers.actions';
import { InternalProcessorsActions } from './processing-activities/subjects/internal-processors/internal-processors.actions';
import { ExternalProcessorsActions } from './processing-activities/subjects/external-processors/external-processors.actions';
import { DPOsActions } from './processing-activities/subjects/DPOs/dpos.actions';
import { DisclosureSubjectsActions } from './processing-activities/disclosure-subjects/disclosure-subjects.actions';
import { OtherDataActions } from './processing-activities/other-data/other-data.actions';
import { SecurityMeasuresActions } from './processing-activities/security-measures/security-measures.actions';
import { Section } from './config/section';
import { InstanceConfig } from './config/instanceConfig';
import { IBasicDpiaData } from './dpia-projects/basic-dpia-data/basic-dpia-data';
import { CurrentDpiaProjectIdActions } from './dpia-projects/current-dpia-project-id/current-dpia-project-id.actions';
import { BasicDpiaDataActions } from './dpia-projects/basic-dpia-data/basic-dpia-data.actions';
import { DpiaPreAssessmentActions } from './dpia-projects/dpia-pre-assessment-questionnaire/dpia-pre-assessment.actions';
import { DataSourcesActions } from './processing-activities/basic-data/data-sources/data-sources.actions';
import { DataSourceTypesActions } from './processing-activities/basic-data/data-source-types/data-source-types.actions';
import { DpiaPrinciplesActions } from './dpia-projects/dpia-principles/dpia-principles.actions';


export interface GenericAction extends Action {
    payload?: any;
}


@Injectable()
export class StateService {

    constructor(private _store: Store<IState>) {
    }

    getUser(): Observable<User> {
        return this._store.select('profile', 'user');
    }

    getConfig(): Observable<Config> {
        return this._store.select('config');
    }

    getSections(): Observable<Section[]> {
        return this._store.select('config', 'sections');
    }

    getInstanceConfig(): Observable<InstanceConfig> {
        return this._store.select('config', 'instanceConfig');
    }

    getQueryParams(): Observable<QueryParams> {
        return this._store.select('queryParams');
    }

    getPACompletionState(): Observable<StepState[]> {
        return this._store.select('processingActivities', 'paCompletionState');
    }

    getCurrentProcessingActivityId(): Observable<number> {
        return this._store.select('processingActivities', 'currentProcessingActivityId');
    }

    getBasicData(): Observable<IBasicData> {
        return this._store.select('processingActivities', 'basicData');
    }

    getSubjects(): Observable<ISubjects> {
        return this._store.select('processingActivities', 'subjects');
    }

    getDisclosure(): Observable<DisclosureSubject[]> {
        return this._store.select('processingActivities', 'disclosureSubjects');
    }

    getOtherData(): Observable<IOtherData> {
        return this._store.select('processingActivities', 'otherData');
    }

    getSelectedSecurityMeasuresIds(): Observable<number[]> {
        return this._store.select('processingActivities', 'selectedSecurityMeasuresIds');
    }

    getCurrentDpiaProjectId(): Observable<number> {
        return this._store.select('dpiaProjects', 'currentDpiaProjectId')
            .filter(id => id !== null);
    }

    getBasicDpiaData(): Observable<IBasicDpiaData> {
        return this._store.select('dpiaProjects', 'basicDpiaData')
            .filter(basicData => basicData.processing_activity_id !== null);
    }

    getDpiaPreAssessmentQuestionnaireAnswers(): Observable<QuestionnaireAnswer[]> {
        return this._store.select('dpiaProjects', 'preAssessmentQuestionnaire');
    }

    getDpiaPrinciplesAnswers(): Observable<QuestionnaireAnswer[]> {
        return this._store.select('dpiaProjects', 'principles');
    }

    isUserSignedIn(): Observable<boolean> {
        return this._store.select(state => state.profile.access_token !== '');
    }

    isSessionExpired(): Observable<boolean> {
        return this._store.select(state => state.profile.session_expired === true);
    }

    hasServerError(): Observable<boolean> {
        return this._store.select(state => state.common.hasServerError === true);
    }

    hasClientError(): Observable<boolean> {
        return this._store.select(state => state.common.hasClientError === true);
    }

    resetWizardForms(): void {
        this.dispatch(PaCompletionStateActions.resetStepsState());
        this.dispatch(CurrentPaIdActions.resetId());

        this.dispatch(BasicDataActions.resetBasicData());
        this.dispatch(DataSourcesActions.resetDataSources());
        this.dispatch(DataSourceTypesActions.resetDataSourceTypes());

        // Subjects step
        this.dispatch(DataSubjectsActions.resetDataSubjects());
        this.dispatch(ControllersActions.resetControllers());
        this.dispatch(InternalProcessorsActions.resetProcessors());
        this.dispatch(ExternalProcessorsActions.resetProcessors());
        this.dispatch(DPOsActions.resetDPOs());

        this.dispatch(DisclosureSubjectsActions.resetDisclosuresubjects());

        this.dispatch(OtherDataActions.resetOtherData());

        this.dispatch(SecurityMeasuresActions.resetSecurityMeasures());
    }

    resetDpiaProjectWizardForms(): void {
        this.dispatch(CurrentDpiaProjectIdActions.resetId());
        this.dispatch(BasicDpiaDataActions.resetBasicDpiaData());
        this.dispatch(DpiaPreAssessmentActions.resetPreAssessmentAnswers());
        this.dispatch(DpiaPrinciplesActions.resetPrinciplesAnswers());
    }

    dispatch(action: GenericAction) {
        return this._store.dispatch(action);
    }
}
