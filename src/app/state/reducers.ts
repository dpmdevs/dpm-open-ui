/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { ActionReducerMap } from '@ngrx/store';

import { IState } from './state.model';
import { userReducers } from './user/user.reducers';
import { commonReducers } from './common/common.reducers';
import { processingActivitiesReducers } from './processing-activities/processing-activities';
import { configReducers } from './config/config.reducers';
import { dpiaProjectsReducers } from './dpia-projects/dpia-projects';
import { queryParamsReducers } from './query-params/query-params.reducers';


export const reducers: ActionReducerMap<IState> = {
    queryParams: queryParamsReducers,
    profile: userReducers,
    config: configReducers,
    common: commonReducers,
    processingActivities: processingActivitiesReducers,
    dpiaProjects: dpiaProjectsReducers,
};
