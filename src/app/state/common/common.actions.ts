/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Action } from '@ngrx/store';


export class CommonActions {

    static SET_SERVER_ERROR = '[Common] Has server error';
    static SET_CLIENT_ERROR = '[Common] Has client error';
    static UNSET_SERVER_ERROR = '[Common] Unset server error';
    static UNSET_CLIENT_ERROR = '[Common] Unset client error';

    static setServerError(): Action {
        return {
            type: CommonActions.SET_SERVER_ERROR
        };
    }

    static unsetServerError(): Action {
        return {
            type: CommonActions.UNSET_SERVER_ERROR
        };
    }

    static setClientError(): Action {
        return {
            type: CommonActions.SET_CLIENT_ERROR
        };
    }

    static unsetClientError(): Action {
        return {
            type: CommonActions.UNSET_CLIENT_ERROR
        };
    }
}
