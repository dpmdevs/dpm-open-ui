/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Action } from '@ngrx/store';

import { CommonActions } from './common.actions';
import { ICommon, initialCommonState } from './common';


export function commonReducers(state: ICommon = initialCommonState, action: Action): ICommon {

    switch (action.type) {

        case CommonActions.SET_SERVER_ERROR:
            return Object.assign({}, state, <ICommon>{ hasServerError: true });

        case CommonActions.UNSET_SERVER_ERROR:
            return Object.assign({}, state, <ICommon>{ hasServerError: false });

        case CommonActions.SET_CLIENT_ERROR:
            return Object.assign({}, state, <ICommon>{ hasClientError: true });

        default:
            return state;
    }
}
