/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { QueryParams } from '@dpm-models';
import { initialQueryParams } from './query-params';
import { QueryParamsAction, QueryParamsActions } from './query-params.actions';


export function queryParamsReducers(state: QueryParams = initialQueryParams, action: QueryParamsAction): QueryParams {

    switch (action.type) {
        case QueryParamsActions.SET_QUERY_PARAMS:
            return Object.assign({}, action.payload) as QueryParams;

        case QueryParamsActions.UPDATE_QUERY_PARAMS:
            return Object.assign({}, state, action.payload);

        case QueryParamsActions.RESET_QUERY_PARAMS:
            return Object.assign({}, initialQueryParams);

        default:
            return state;
    }

}
