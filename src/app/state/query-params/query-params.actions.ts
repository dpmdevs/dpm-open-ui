/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Action } from '@ngrx/store';

import { QueryParams } from '@dpm-models';


export interface QueryParamsAction extends Action {
    payload?: QueryParams | Partial<QueryParams>;
}

export class QueryParamsActions {

    static SET_QUERY_PARAMS = '[Query Params] Setting query params';
    static UPDATE_QUERY_PARAMS = '[Query Params] Updating query params';
    static RESET_QUERY_PARAMS = '[Query Params] Resetting query params';

    static setQueryParams(queryParams: QueryParams): QueryParamsAction {
        return {
            type: QueryParamsActions.SET_QUERY_PARAMS,
            payload: queryParams
        };
    }

    static updateQueryParams(queryParams: QueryParams | Partial<QueryParams>): QueryParamsAction {
        return {
            type: QueryParamsActions.UPDATE_QUERY_PARAMS,
            payload: queryParams
        };
    }

    static resetQueryParams(): QueryParamsAction {
        return { type: QueryParamsActions.RESET_QUERY_PARAMS };
    }

}
