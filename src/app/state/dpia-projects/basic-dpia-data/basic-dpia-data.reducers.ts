/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { IBasicDpiaData, initialBasicDpiaDataState } from './basic-dpia-data';
import { BasicDpiaDataAction, BasicDpiaDataActions } from './basic-dpia-data.actions';
import {
    DpiaProcessingActivitiesAction,
    DpiaProcessingActivityActions
} from './dpia-processing-activities/dpia-processing-activities.actions';
import { dpiaProcessingActivitiesReducer } from './dpia-processing-activities/dpia-processing-activities.reducers';


export function basicDpiaDataReducer(state: IBasicDpiaData = initialBasicDpiaDataState, action: BasicDpiaDataAction | DpiaProcessingActivitiesAction): IBasicDpiaData {

    switch (action.type) {
        case BasicDpiaDataActions.SET_DATA:
            return Object.assign({}, initialBasicDpiaDataState, action.payload);

        case BasicDpiaDataActions.RESET_DATA:
            return initialBasicDpiaDataState;

        case DpiaProcessingActivityActions.SET_PROCESSING_ACTIVITY:
        case DpiaProcessingActivityActions.RESET_PROCESSING_ACTIVITY:
            const processing_activity_id = dpiaProcessingActivitiesReducer(state.processing_activity, <DpiaProcessingActivitiesAction>action);
            return Object.assign({}, state, { processing_activity_id });

        default:
            return state;
    }

}
