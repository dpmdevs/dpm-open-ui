/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { ActionReducer, combineReducers, compose } from '@ngrx/store';
import { storeFreeze } from 'ngrx-store-freeze';

import { environment } from 'environments/environment';
import { QuestionnaireAnswer } from '@dpm-models';
import { IBasicDpiaData, initialBasicDpiaDataState } from './basic-dpia-data/basic-dpia-data';
import { currentDpiaProjectIdReducer } from './current-dpia-project-id/current-dpia-project-id.reducer';
import { basicDpiaDataReducer } from './basic-dpia-data/basic-dpia-data.reducers';
import { preAssessmentQuestionnaireReducer } from './dpia-pre-assessment-questionnaire/dpia-pre-assessment.reducers';
import { principlesReducer } from './dpia-principles/dpia-principles.reducers';


export interface IDpiaProjects {
    currentDpiaProjectId: number;
    basicDpiaData: IBasicDpiaData;
    preAssessmentQuestionnaire: QuestionnaireAnswer[];
    principles: QuestionnaireAnswer[];
}

export const initialDpiaProjectsState: IDpiaProjects = {
    currentDpiaProjectId: null,
    basicDpiaData: initialBasicDpiaDataState,
    preAssessmentQuestionnaire: [],
    principles: [],
};

const reducers = {
    currentDpiaProjectId: currentDpiaProjectIdReducer,
    basicDpiaData: basicDpiaDataReducer,
    preAssessmentQuestionnaire: preAssessmentQuestionnaireReducer,
    principles: principlesReducer,
};

// see here: https://github.com/ngrx/store/issues/190#issuecomment-247074138
const developmentReducer: ActionReducer<any> = compose(storeFreeze, combineReducers)(reducers);
const productionReducer: ActionReducer<IDpiaProjects> = combineReducers(reducers);

export function dpiaProjectsReducers(state: any, action: any) {
    return environment.production
        ? productionReducer(state, action)
        : developmentReducer(state, action)
    ;
}

