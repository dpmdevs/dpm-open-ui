/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';

import { Config } from 'app/state/config/config';

export interface ConfigAction extends Action {
    payload?: Config;
}

@Injectable()
export class ConfigActions {

    static LOAD_CONFIG = '[Config] Loading config';
    static LOAD_CONFIG_SUCCESS = '[Config] Load config success';
    static UPDATE_CONFIG = '[Config] Updating config';

    static loadConfig(): ConfigAction {
        return {
            type: ConfigActions.LOAD_CONFIG
        }
    }

    static loadConfigSuccess(config: Config): ConfigAction {
        return {
            type: ConfigActions.LOAD_CONFIG_SUCCESS,
            payload: config
        }
    }

    static updateConfig(data: Config): ConfigAction {
        return {
            type: ConfigActions.UPDATE_CONFIG,
            payload: data
        };
    }

}
