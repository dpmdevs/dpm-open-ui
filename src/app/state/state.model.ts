/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { ICommon, initialCommonState } from './common/common';
import { initialProfileState, IProfile } from './user/user';
import { initialProcessingActivitiesState, IProcessingActivities } from './processing-activities/processing-activities';
import { Config, initialConfigState } from './config/config';
import { IDpiaProjects, initialDpiaProjectsState } from './dpia-projects/dpia-projects';
import { initialQueryParams } from './query-params/query-params';
import { QueryParams } from '@dpm-models';


export interface StateBase {
    loading?: boolean;
}

export interface IState {
    queryParams: QueryParams
    profile: IProfile;
    config: Config;
    common: ICommon;
    processingActivities: IProcessingActivities;
    dpiaProjects: IDpiaProjects;
}

export const initialState: IState = {
    queryParams: null,
    profile: initialProfileState,
    config: initialConfigState,
    common: initialCommonState,
    processingActivities: initialProcessingActivitiesState,
    dpiaProjects: initialDpiaProjectsState,
};
