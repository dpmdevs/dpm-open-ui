/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { NgModule } from '@angular/core';
import { Route, RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './core/auth';
import { LoginGuard } from './public/login/login.guard';
import { PageNotFoundComponent } from './public/page-not-found/page-not-found.component';


const publicRoutes: Routes = [
    { path: 'login', loadChildren: 'app/public/login/login.module#LoginModule', canActivate: [LoginGuard] },
    { path: 'about', loadChildren: 'app/public/about/about.module#AboutModule' }
];

const authenticatedRoutes: Routes = [
    { path: '', canActivate: [AuthGuard], loadChildren: 'app/authenticated/authenticated.module#AuthenticatedModule' }
];

const fallbackRoute: Route = { path: '**', component: PageNotFoundComponent };

/**
 * Defines base routing and lazy loaded modules
 */

const routes: Routes = [
    ...publicRoutes,
    ...authenticatedRoutes,
    fallbackRoute
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
    providers: [
        LoginGuard
    ]
})
export class AppRoutingModule {
}
