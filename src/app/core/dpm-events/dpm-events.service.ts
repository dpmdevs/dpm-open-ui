/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { environment } from 'environments/environment';
import { BaseService } from '../base-service';
import { StateService } from '../../state/state.service';
import { DPMEvent } from './dpm-event';


@Injectable()
export class DPMEventsService extends BaseService {
    private apiUrl: string;

    constructor(private _http: HttpClient, protected _ss: StateService) {
        super();
        this.apiUrl = environment.apiUrl + 'events';
    }

    protected get stateService(): StateService {
        return this._ss;
    }

    getLastEvent(): Observable<DPMEvent> {
        return this._http.get<DPMEvent>(this.apiUrl + '/last');
    }
}
