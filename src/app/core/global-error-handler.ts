/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { ErrorHandler, Injectable } from '@angular/core';
import { isNullOrUndefined } from 'util';

import { StateService } from '../state/state.service';
import { CommonActions } from '../state/common/common.actions';
import { NotificationsInterceptor } from './notifications-interceptor';


@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
    constructor(private _stateService: StateService) {
    }

    handleError(error) {

        if (!isNullOrUndefined(error.rejection) && !isNullOrUndefined(error.rejection.error) &&
            error.rejection.error.error === NotificationsInterceptor.TOKEN_NOT_PROVIDED) {
            return;
        }

        if (!this.isHandledByNotificationError(error.status)) {
            this._stateService.dispatch(CommonActions.setClientError());
            console.error('GlobalErrorHandler', error);
        }
    }

    private isHandledByNotificationError(status) {
        switch (status) {
            case NotificationsInterceptor.UNAUTHORIZED:
            case NotificationsInterceptor.INTERNAL_SERVER_ERROR:
            case NotificationsInterceptor.SERVER_DOWN:
            case NotificationsInterceptor.UNPROCESSABLE_ENTRY:
            case NotificationsInterceptor.BAD_REQUEST:
                return true;
            default:
                return false;
        }
    }
}
