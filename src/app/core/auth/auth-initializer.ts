/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';

import { AuthService } from './auth.service';
import { JWTToken } from './models/jwt-token';
import { IState } from '../../state/state.model';
import { Store } from '@ngrx/store';
import { LoginHandlerActions } from '../../state/login/actions/login-handler.action';

/**
 * APP_INITIALIZER provider for local storage auth token validation (on reload)
 * Will dispatch an UserActions.setAuthToken with localStorage saved token
 * to preserve logged profile session
 */

@Injectable()
export class AuthInitializer {

    private isLoggedIn(token: string) {

        if (token === null) return false;

        const parsedToken: JWTToken = this.parseJwt(token);

        if (this.tokenExpired(parsedToken.exp)) {
            console.warn('JWT Token is expired');
            return false;
        }

        if (this.tokenNotAccepted(parsedToken.nbf)) {
            console.warn('JWT Token is not accepted');
            return false;
        }

        return true;
    }

    constructor(private store: Store<IState>) {
    }

    init(): void {
        const token = localStorage.getItem(AuthService.AUTH_TOKEN_NAME);

        if (this.isLoggedIn(token))
            this.store.dispatch(LoginHandlerActions.checkUserAuthentication(token));
    }

    private parseJwt(token: string): JWTToken {
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        return <JWTToken>JSON.parse(window.atob(base64));
    };

    private tokenExpired(expirationTime: number): boolean {
        return Date.now() > expirationTime * 1000;
    }

    private tokenNotAccepted(nbf: number) {
        return Date.now() < nbf * 1000;
    }
}
