/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';

import { environment } from 'environments/environment';
import { Credentials } from './models/credentials';
import { AuthResponse } from './models/auth-response';
import { Store } from '@ngrx/store';
import { IState } from '../../state/state.model';


@Injectable()
export class AuthService {

    static AUTH_TOKEN_NAME = 'id_token';

    private authUrl = environment.authUrl;

    constructor(private http: HttpClient, private store: Store<IState>) {
    }

    attemptLogin(credentials: Credentials): Observable<AuthResponse> {
        return this.http.post<AuthResponse>(`${this.authUrl}login`, credentials);
    }

    saveTokenToLocalStorage(token: string): void {
        localStorage.setItem(AuthService.AUTH_TOKEN_NAME, token);
    }

    deleteTokenFromLocalStorage(): void {
        localStorage.removeItem(AuthService.AUTH_TOKEN_NAME);
    }

    logout(): Observable<boolean> {
        return this.http.get<boolean>(`${this.authUrl}logout`);
    }

    checkUserAuthentication(): Observable<boolean> {
        return this.http.get(`${this.authUrl}check-user-authentication`)
            .map((response: { is_user_authenticated: boolean }) => response.is_user_authenticated === true);
    }

}
