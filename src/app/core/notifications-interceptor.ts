/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { isNullOrUndefined } from 'util';
import { Observable } from 'rxjs/Observable';

import { StateService } from '../state/state.service';
import { CommonActions } from '../state/common/common.actions';
import { UserActions } from '../state/user/user.actions';


@Injectable()
export class NotificationsInterceptor implements HttpInterceptor {

    static SERVER_DOWN = 0;
    static BAD_REQUEST = 400;
    static UNAUTHORIZED = 401;
    static UNPROCESSABLE_ENTRY = 422;
    static INTERNAL_SERVER_ERROR = 500;
    static TOKEN_NOT_PROVIDED = 'token_not_provided';

    constructor(private _stateService: StateService) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        return next
            .handle(req)
            .catch(error => {

                if (!isNullOrUndefined(error.error) && error.error.error === NotificationsInterceptor.TOKEN_NOT_PROVIDED) {
                    console.error('TOKEN_NOT_PROVIDED Exception', error);
                    this._stateService.dispatch(UserActions.sessionExpired());
                } else if (error instanceof HttpErrorResponse) {

                    console.error('NotificationsInterceptor HttpErrorResponse:', error);

                    switch (error.status) {
                        case  NotificationsInterceptor.UNPROCESSABLE_ENTRY:
                            break;

                        case NotificationsInterceptor.SERVER_DOWN:
                            this._stateService.dispatch(UserActions.sessionExpired());
                            break;

                        case NotificationsInterceptor.UNAUTHORIZED:
                            this._stateService.dispatch(UserActions.sessionExpired());
                            break;

                        case NotificationsInterceptor.BAD_REQUEST:
                            this._stateService.dispatch(CommonActions.setServerError());
                            break;

                        case  NotificationsInterceptor.INTERNAL_SERVER_ERROR:
                            this._stateService.dispatch(CommonActions.setServerError());
                            break;
                    }
                }
                return Observable.throw(error);
            });
    }
}

