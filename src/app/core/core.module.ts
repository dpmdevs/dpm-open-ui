/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { APP_INITIALIZER, ErrorHandler, NgModule, Optional, SkipSelf } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';

import { environment } from 'environments/environment';
import { reducers } from '../state/reducers';
import { metaReducers } from '../state/state.reducers';
import { StateService } from '../state/state.service';
import { AuthInitializer, AuthModule, JWTRequestInterceptor } from './auth';
import { DPMEventsService } from './dpm-events/dpm-events.service';
import { GlobalErrorHandler } from './global-error-handler';
import { NotificationsInterceptor } from './notifications-interceptor';
import { UserEffects } from '../state/user/user.effects';
import { UserService } from '../authenticated/user/user.service';
import { ConfigEffects } from '../state/config/config.effects';
import { ConfigService } from '../authenticated/config/config.service';
import { LoginHandlerEffects } from '../state/login/effects';
import { loginReducers } from '../state/login/reducers';

/**
 * CoreModule with providers for the singleton services loaded when the application starts.
 * Provider will be registered with the app root injector, making a singleton instance
 * Import CoreModule in the root AppModule only. Never import CoreModule in any module other than the root AppModule.
 *
 * This should be a pure services module with no declarations.
 */

export function getUserFactory(authInit: AuthInitializer): () => void {
    return () => authInit.init();
}

export const GlobalErrorHandlerProvider = {
    provide: ErrorHandler,
    useClass: GlobalErrorHandler
};


@NgModule({
    imports: [
        BrowserAnimationsModule,
        HttpClientModule,
        StoreModule.forRoot(reducers, { metaReducers }),
        // Note that you must instrument after importing StoreModule
        !environment.production
            ? StoreDevtoolsModule.instrument({ maxAge: 50 })
            : [],
        AuthModule,
        StoreModule.forFeature('login', loginReducers),
        EffectsModule.forRoot([UserEffects, ConfigEffects, LoginHandlerEffects])
    ],
    providers: [
        StateService,
        {
            provide: APP_INITIALIZER,
            useFactory: getUserFactory,
            deps: [AuthInitializer],
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JWTRequestInterceptor,
            multi: true,
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: NotificationsInterceptor,
            multi: true,
        },
        GlobalErrorHandlerProvider,
        DPMEventsService,
        UserService,
        ConfigService
    ]
})
export class CoreModule {
    /**
     * Constructor to prevent unwanted CoreModule will be loaded in child modules (lazy loaded)
     *
     * @param parentModule
     */
    constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
        if (parentModule)
            throw new Error('CoreModule is already loaded. Import it in the AppModule only');
    }
}
