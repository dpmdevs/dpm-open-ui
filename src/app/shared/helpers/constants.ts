/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

export class DpmConstants {
    static readonly DATA_SOURCE_SUBJECT = 1;
    static readonly DATA_SOURCE_THIRD_PARTY = 2;
    static readonly PROCESS_TYPE_PAPER = { id: 1, name: 'cartaceo' };
    static readonly PROCESS_TYPE_IT = { id: 2, name: 'informatizzato' };
    static readonly USER_ICON_PATH = 'assets/img/icon-user.svg';
}
