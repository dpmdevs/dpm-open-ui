/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { AbstractControl, FormControl, ValidatorFn, Validators } from '@angular/forms';


export class DPMValidators extends Validators {

    public static required({ value }: FormControl) {
        return (!value || typeof value === 'string' && !value.trim())
            ? { required: true }
            : null;
    }

    public static email({ value }: FormControl) {
        return ((!value || typeof value === 'string' && !value.trim()) || value.indexOf('@') !== -1)
            ? null : { email: true };
    }

    public static password({ value }: FormControl) {
        return compliantPasswordRegExp.test(value) ? null : { password: true };
    }

}

/**
 * RegExp for compliant password
 * 1+ lowercase: (?=.*[a-z])
 * 1+ uppercase: (?=.*[A-Z])
 * 1+ numbers: (?=.*[0-9])
 * 1+ special characters: (?=.[!@#\$%\^&\*])
 * 8+ characters: (?=.{8,})
 */
export const compliantPasswordRegExp = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\\$%\\^&\\*])(?=.{8,})', 'g');

export function compliantPasswordValidator(): ValidatorFn {
    return ({ value }: AbstractControl): { [key: string]: any } => {
        if (value === '')
            return null;

        return !compliantPasswordRegExp.test(value) ? {
            'patternInvalid': { compliantPasswordRegExp },
            password: true
        } : null;
    };
}

export function fiscalCodeValidator(): ValidatorFn {
    /* http://blog.marketto.it/2016/01/regex-validazione-codice-fiscale-con-omocodia */
    const fiscalCodeRegExp = new RegExp('^(?:(?:[B-DF-HJ-NP-TV-Z]|[AEIOU])[AEIOU][AEIOUX]|[B-DF-HJ-NP-TV-Z]{2}[A-Z]){2}[\\dLMNP-V]{2}(?:[A-EHLMPR-T](?:[04LQ][1-9MNP-V]|[1256LMRS][\\dLMNP-V])|[DHPS][37PT][0L]|[ACELMRT][37PT][01LM])(?:[A-MZ][1-9MNP-V][\\dLMNP-V]{2}|[A-M][0L](?:[1-9MNP-V][\\dLMNP-V]|[0L][1-9MNP-V]))[A-Z]$', 'gi');

    return ({ value }: AbstractControl): { [key: string]: any } => {
        if (value === '')
            return null;

        return !fiscalCodeRegExp.test(value) ? { 'patternInvalid': { fiscalCodeRegExp }, fiscal_code: true } : null;
    };
}


export const IPAddressPattern = '^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$';

// https://stackoverflow.com/questions/35474991/angular-2-form-validating-for-repeat-password
export function matchOtherValidator(otherControlName: string) {
    let thisControl: FormControl;
    let otherControl: FormControl;

    return function matchOtherValidate(control: FormControl) {
        if (!control.parent) {
            return null;
        }

        if (!thisControl) {
            thisControl = control;
            otherControl = control.parent.get(otherControlName) as FormControl;
            if (!otherControl)
                throw new Error('matchOtherValidator(): other control is not found in parent group');

            otherControl.valueChanges.subscribe(() => thisControl.updateValueAndValidity());
        }

        if (!otherControl)
            return null;

        if (otherControl.value !== thisControl.value)
            return { matchOther: true };

        return null;
    };
}
