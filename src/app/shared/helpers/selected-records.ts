/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { isNull } from 'util';

import { BaseRecord } from '../models';


export class SelectedRecords<T extends BaseRecord> {
    private records: T[] = [];

    get length() {
        return this.records.length;
    }

    setRecords(records: T[]): void {
        this.records = records;
    }

    addRecords(...records: T[]): void {
        if (records.length === 0)
            return;

        if (this.records.length > 0 && !this.inputsHaveSameTypeAsSavedRecords(records))
            this.records = [];

        records.forEach(r => {
            if (!this.isRecordSelected(r))
                this.records.push(r);
        });
    }

    getAllRecords(): T[] {
        return this.records;
    }

    removeRecords(...filterRecords: T[]): void {
        this.records = this.records.filter(r => !filterRecords.some(f => f.id === r.id));
    }

    removeAllRecords(): void {
        this.records = [];
    }

    isRecordSelected(record: T): boolean {
        return this.records.some(r => {
            try {
                return r.id === record.id;
            } catch (e) {
                return false;
            }
        });
    }

    toggleRecord(record: T): void {
        if (this.isRecordSelected(record))
            this.removeRecords(record);
        else
            this.addRecords(record);
    }

    private inputsHaveSameTypeAsSavedRecords(inputRecords): boolean {
        return this.areAllSavedRecordsActive() && isNull(inputRecords[0].deleted_at)
            || this.areAllSavedRecordsTrashed() && !isNull(inputRecords[0].deleted_at);
    }

    private areAllSavedRecordsActive(): boolean {
        return this.records.every(r => r.deleted_at === null);
    }

    private areAllSavedRecordsTrashed(): boolean {
        return this.records.every(r => r.deleted_at !== null);
    }
}
