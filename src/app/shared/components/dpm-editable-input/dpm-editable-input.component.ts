/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';


@Component({
    selector: 'dpm-editable-input',
    templateUrl: './dpm-editable-input.component.html',
    styleUrls: ['./dpm-editable-input.component.scss']
})
export class DpmEditableInputComponent implements OnInit {
    @ViewChild('editableInput') private inputRef: ElementRef;

    @Input() type: 'text' | 'number' | 'email';
    @Input() autocomplete: 'on' | 'off' = 'off';
    @Input() initialValue: string;
    @Input() disabled: boolean;
    @Input() customClasses: string;
    @Input() minLength: number = 0;
    @Input() maxLength = Number.MAX_SAFE_INTEGER;

    @Input('doneEditing')
    set doneEditing(doneEditing: boolean) {
        if (this.isInputActive === !doneEditing)
            return;

        this.currentValue = this.initialValue;
        this.isInputActive = !doneEditing;

        if (doneEditing)
            this.editComplete.emit();
    }

    @Output() editComplete = new EventEmitter();
    @Output() editStart = new EventEmitter();

    isInputActive = false;
    currentValue = '';

    constructor() {
    }

    ngOnInit() {
        this.currentValue = this.initialValue;
    }

    enableInput(): void {
        this.isInputActive = true;
        this.inputRef.nativeElement.focus();
        this.editStart.emit();
    }

    submit(): void {
        this.isInputActive = false;

        this.editComplete.emit(this.currentValue);
    }

    cancel(): void {
        this.currentValue = this.initialValue;
        this.isInputActive = false;

        this.editComplete.emit(this.initialValue);
    }
}
