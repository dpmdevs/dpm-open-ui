/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, EventEmitter, HostListener, Input, Output } from '@angular/core';


@Component({
    selector: 'dpm-toggle-switch',
    templateUrl: './dpm-toggle-switch.component.html',
    styleUrls: ['./dpm-toggle-switch.component.scss']
})
export class DpmToggleSwitchComponent {

    @HostListener('click', ['$event.target']) onClick() {
        this.toggle.emit();
        return false;
    }

    @Input() defaultValue: boolean = false;
    @Input() currentValue: boolean = this.defaultValue;
    @Output() toggle = new EventEmitter<boolean>();

}
