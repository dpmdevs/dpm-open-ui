/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { TableBaseService } from '../dpm-crud-page/table-base-service';
import { environment } from '../../../../environments/environment';
import { Unit } from '../../models';


@Injectable()
export class UnitsEntitiesPairingService extends TableBaseService {
    get baseUrl() {
        return environment.apiUrl;
    }

    get table() {
        return '';
    }

    getAssociatedUnits(table: string, entityId: number): Observable<Unit[]> {
        return this.http.get<Unit[]>(`${this.baseUrl}${table}/${entityId}/working-units`);
    }

    toggleUnit(table: string, entityId: number, unitIds: number[]): Observable<Response> {
        return this.http.put<Response>(`${this.baseUrl}${table}/${entityId}/working-units`, { unitIds });
    }
}
