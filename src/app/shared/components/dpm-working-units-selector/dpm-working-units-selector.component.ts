/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { TreeComponent, TreeNode } from 'angular-tree-component';
import { Observable } from 'rxjs/Observable';
import { isNullOrUndefined, isUndefined } from 'util';

import { Tag } from '../../models';
import { UnitsEntitiesPairingService } from './units-entities-pairing.service';
import { AnimationProvider } from '../../providers';
import { DpmTreeOptions } from '../dpm-tree-selector/dpm-tree-selector.component';
import { UnitsService } from '../../services/units.service';


@Component({
    selector: 'dpm-working-units-selector',
    templateUrl: './dpm-working-units-selector.component.html',
    styleUrls: ['./dpm-working-units-selector.component.scss'],
    animations: [AnimationProvider.getSlideInTop()],
    providers: [UnitsEntitiesPairingService]
})
export class WorkingUnitsSelectorComponent implements OnInit, OnChanges {

    @Input() title: string;
    @Input() referenceId: number;
    @Input() referenceTable: string;
    @Output() nodeSelected = new EventEmitter();
    units$: Observable<any>;
    selectedUnitsTags: Tag[] = [];

    options: DpmTreeOptions = {
        animateExpand: true,
        animateAcceleration: 2.2,
        allowDrag: true,
        levelPadding: 40,
        searchable: true,
        displayCheckboxes: true
    };

    constructor(protected _unitsService: UnitsService,
                protected _unitsEntitiesPairingService: UnitsEntitiesPairingService) {
    }

    ngOnInit() {
        this.onUpdateTree();
    }

    ngOnChanges() {
        this.onUpdateTree();
    }

    protected onUpdateTree(): void {
        this.units$ = !isNullOrUndefined(this.referenceId)
            ? Observable
                .forkJoin(
                    this._unitsService.getUnits(),
                    this._unitsEntitiesPairingService.getAssociatedUnits(this.referenceTable, this.referenceId)
                        .do(units => this.selectedUnitsTags = units
                            .map(({ id, name, description: title }) => ({ id, name, title }))
                        )
                        .map(units => units.map(u => u.id))
                )
                .map(data => this.toggleSelectedNodes(data))
            : this._unitsService.getUnits();
    }

    private toggleSelectedNodes(data) {
        const [units, selectedUnitsIds] = data;

        if (isUndefined(this.referenceId))
            return units;

        (function addIsSelectedPropertyRecursively(nodes) {
            nodes.forEach(node => {
                node.isSelected = selectedUnitsIds.includes(node.id);
                if (node.children)
                    addIsSelectedPropertyRecursively(node.children);
            });
        }).bind(this)(units);

        return units;
    }

    filterNodes(filter: string, tree: TreeComponent): void {
        const searchNameOrCode = (node: TreeNode): boolean => {
            return node.data.name.toLowerCase().indexOf(filter.toLowerCase()) !== -1
                || node.data.code.toLowerCase().indexOf(filter.toLowerCase()) !== -1;
        };

        // https://angular2-tree.readme.io/docs/filtering#filter-by-function
        tree.treeModel.filterNodes(searchNameOrCode);
    }

    onNodeSelected(id): void {
        this._unitsEntitiesPairingService.toggleUnit(this.referenceTable, this.referenceId, [id])
            .subscribe({
                next: this.onUpdateTree.bind(this),
                complete: () => this.nodeSelected.emit()
            });
    }

}
