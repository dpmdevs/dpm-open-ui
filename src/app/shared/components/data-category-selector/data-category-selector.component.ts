/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { DataCategory, DataType } from '@dpm-models';
import { AnimationProvider } from '@dpm-providers';


@Component({
    selector: 'dpm-data-category-selector',
    templateUrl: './data-category-selector.component.html',
    styleUrls: ['./data-category-selector.component.scss'],
    animations: [AnimationProvider.getToggleUpDown()]
})
export class DataCategorySelectorComponent {

    @Input() selectedDataCategories: DataCategory[] = [];
    @Input() selectedDataTypes: DataType[] = [];
    @Input() personalDataCategories: DataCategory[] = [];
    @Input() sensitiveDataCategories: DataCategory[] = [];

    @Output() dataCategoriesSelectionChange = new EventEmitter();
    @Output() dataCategoryToggled = new EventEmitter<DataCategory>();
    @Output() dataTypeToggled = new EventEmitter();

    accordionsVisibility = ['unSelected', 'unSelected'];

    toggleDataCategory(dataCategory: DataCategory): void {
        const dataCategoryWithoutDataTypes = Object.assign({}, dataCategory, { data_types: [] });
        this.dataCategoryToggled.emit(dataCategoryWithoutDataTypes);
        this.dataCategoriesSelectionChange.emit();
    }

    toggleDataType(dataType: DataType, parentDataCategory: DataCategory): void {
        const parentDCWithoutDataTypes = Object.assign({}, parentDataCategory, { data_types: [] });
        if (!(this.isDataCategorySelected(parentDCWithoutDataTypes) || this.isAnyChildChecked(parentDCWithoutDataTypes)))
            this.dataCategoryToggled.emit(parentDCWithoutDataTypes);

        this.dataTypeToggled.emit([dataType, parentDCWithoutDataTypes]);
        this.dataCategoriesSelectionChange.emit();
    }

    private isAnyChildChecked(dataCategory: DataCategory): boolean {
        return dataCategory.data_types.some(dt => this.isDataTypeSelected(dt));
    }

    isDataCategorySelected(dataCategory: DataCategory): boolean {
        return this.selectedDataCategories.some(dc => dc.id === dataCategory.id);
    }

    isDataTypeSelected(dataType: DataType): boolean {
        return this.selectedDataTypes.some(dt => dt.id === dataType.id);
    }

    toggleAccordion(index): void {
        this.accordionsVisibility[index] = this.accordionsVisibility[index] === 'selected' ? 'unSelected' : 'selected';
    }

}
