/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, EventEmitter, HostListener, Input, Output, TemplateRef } from '@angular/core';

import { AnimationProvider } from '@dpm-providers';


@Component({
    selector: 'dpm-card',
    templateUrl: './dpm-card.component.html',
    animations: [AnimationProvider.getFadeIn()],
})
export class DpmCardComponent {

    @Input() cardColor: 'primary' | 'success' | 'warning' | 'danger' | 'info' = 'info';
    @Input() isSelected = false;
    @Input() isClickable = true;

    @Output() select = new EventEmitter();

    @HostListener('click', ['$event']) onClick() {
        this.select.emit();
        return false;
    }

    constructor() {
    }

}
