/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { TreeComponent, TreeNode } from 'angular-tree-component';
import { Observable } from 'rxjs/Observable';
import { isNullOrUndefined, isUndefined } from 'util';

import { AnimationProvider } from '@dpm-providers';
import { ProcessingActivity, Tag } from '@dpm-models';
import { UnitsService } from '@dpm-services';
import { DpmTreeOptions } from '../dpm-tree-selector/dpm-tree-selector.component';
import { ProcessingActivitiesService } from '../../../authenticated/processing-activities/services/processing-activities.service';
import { UserService } from '../../../authenticated/user/user.service';


@Component({
    selector: 'dpm-pa-units-selector',
    templateUrl: './pa-units-selector.component.html',
    styleUrls: ['./pa-units-selector.component.scss'],
    animations: [AnimationProvider.getSlideInTop()],
    providers: [UnitsService]
})
export class PaUnitsSelectorComponent implements OnInit, OnChanges {

    @Input() processingActivityId: number;
    @Output() nodeSelected = new EventEmitter();

    selectedUnitsTags: Tag[] = [];
    units$: Observable<any>;
    options: DpmTreeOptions = {
        animateExpand: true,
        animateAcceleration: 2.2,
        allowDrag: true,
        levelPadding: 40,
        searchable: true,
        displayCheckboxes: true
    };

    constructor(private _paService: ProcessingActivitiesService,
                private _userService: UserService) {
    }

    ngOnInit() {
        this.onUpdateTree();
    }

    ngOnChanges() {
        this.onUpdateTree();
    }

    private onUpdateTree(): void {
        this.units$ = !isNullOrUndefined(this.processingActivityId)
            ? Observable
                .forkJoin(
                    this._userService.getDirectedUnits(),
                    this._paService.getEntity(this.processingActivityId)
                        .map((pa: ProcessingActivity) => pa.units)
                        .do(units => this.selectedUnitsTags = units
                            .map(({ id, name, description: title }) => ({ id, name, title }))
                        )
                        .map(units => units.map(u => u.id))
                )
                .map(data => this.toggleSelectedUnits(data))
            : this._userService.getDirectedUnits();
    }

    private toggleSelectedUnits(data) {
        const [units, selectedUnitsIds] = data;

        if (isUndefined(this.processingActivityId))
            return units;

        (function addIsSelectedPropertyRecursively(nodes) {
            nodes.forEach(node => {
                node.isSelected = selectedUnitsIds.includes(node.id);
                if (node.children)
                    addIsSelectedPropertyRecursively(node.children);
            });
        }).bind(this)(units);

        return units;
    }

    filterNodes(filter: string, tree: TreeComponent): void {
        const searchNameOrCode = (node: TreeNode): boolean => {
            return node.data.name.toLowerCase().indexOf(filter.toLowerCase()) !== -1
                || node.data.code.toLowerCase().indexOf(filter.toLowerCase()) !== -1;
        };

        // https://angular2-tree.readme.io/docs/filtering#filter-by-function
        tree.treeModel.filterNodes(searchNameOrCode);
    }

    onNodeSelected(id: number): void {
        this._paService.toggleUnit(this.processingActivityId, id)
            .subscribe({
                next: () => this.onUpdateTree(),
                complete: () => this.nodeSelected.emit()
            });
    }
}
