/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { EventEmitter, Output } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { BaseSelectorService } from './base-selector.service';
import { BaseRecord, Tag } from '../../models';


export abstract class BaseSelector<T extends BaseRecord> {
    @Output() selectionChange = new EventEmitter();

    selectedTags: Tag[] = [];
    lastKeyword: string;
    availableTags$: Observable<Tag[]>;
    availableItems: T[];
    availableItems$: Observable<T[]>;
    showingResults = true;
    labels: { selectedTags?: string; placeholder?: string; description?: string; };

    constructor(protected _selectorService: BaseSelectorService<T>) {
        this.loadTags();
    }

    protected loadTags(): void {
        this._selectorService.getSelectedTags()
            .map(this.mapItemsToTags)
            .subscribe(tags => {
                this.selectedTags = tags;
            });
    }

    protected abstract mapItemsToTags(items: T[]): Tag[];

    protected reloadAvailableTags(): void {
        this.availableItems$ = this._selectorService.searchItems(this.lastKeyword)
            .map(this.filterSelectedItems.bind(this));

        this.subscribeToAvailableItems();
    }

    protected subscribeToAvailableItems(): void {
        this.availableItems$.subscribe(items => {
            this.availableItems = items;
            this.availableTags$ = this.availableItems$.map(this.mapItemsToTags);
        });
    }

    protected filterSelectedItems(items: T[]): T[] {
        const selectedTagsIds = this.selectedTags.map(tag => tag.id);
        return items.filter(item => !selectedTagsIds.includes(item.id));
    }

    onSearchTags(keyword: string): void {
        this.showingResults = true;

        if (!this.showingResults && keyword === this.lastKeyword)
            return;

        this.lastKeyword = keyword;
        this.reloadAvailableTags();
    }

    onSelectTag(selection: Tag): void {
        const item = this.availableItems.find(i => i.id === selection.id);
        this._selectorService.addItem(item);
        this.selectionChange.emit();
        this.reloadAvailableTags();
    }

    protected onDeleteTag(selection: Tag): void {
        this._selectorService.removeItem(selection.id);
        this.selectionChange.emit();
        this.reloadAvailableTags();
    }
}
