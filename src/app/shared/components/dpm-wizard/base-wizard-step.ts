/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastsManager } from 'ng2-toastr';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import { StepState } from '@dpm-models';
import { ValidationMessages } from '@dpm-components/dpm-crud-page/base-details-page/validation-messages';


export abstract class BaseWizardStep implements OnInit, OnDestroy {
    @Input() stepState: StepState;
    @Output() previousStep = new EventEmitter();
    @Output() nextStep = new EventEmitter();

    formChanges$ = new Subject();
    stepForm: FormGroup;
    validationMessages: ValidationMessages = {
        errors: {},
        message: ''
    };

    protected stateSubscriptions: Subscription[] = [];
    protected abstract formConfig;

    abstract get toastr(): ToastsManager;

    constructor(protected _formBuilder: FormBuilder) {
    }

    ngOnInit() {
        this.stepForm = this._formBuilder.group(this.formConfig);
    }

    ngOnDestroy() {
        this.stateSubscriptions.forEach(s => s.unsubscribe());
    }

    abstract onNextStep(): void;

    protected catchError(error: HttpErrorResponse): Observable<HttpErrorResponse> {
        if (error.status === 422) {
            this.validationMessages = error.error;

            for (const err in this.validationMessages.errors)
                this.toastr.error(this.validationMessages.errors[err].toString(), '', { dismiss: 'click' }).then();

            return Observable.throw(error);
        }

        return Observable.of(error);
    }

    resetValidationErrors(): void {
        this.validationMessages = {
            errors: {},
            message: ''
        };
    }

}
