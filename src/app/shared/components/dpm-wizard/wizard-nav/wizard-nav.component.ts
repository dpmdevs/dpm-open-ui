/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, EventEmitter, Input, Output } from '@angular/core';
import 'rxjs/add/operator/mergeMap';

import { StepState } from '../../../models';


@Component({
    selector: 'dpm-wizard-nav',
    templateUrl: './wizard-nav.component.html',
    styleUrls: ['./wizard-nav.component.scss']
})
export class WizardNavComponent {
    @Input() currentStepId = 'step-0';
    @Input() steps: StepState[];
    @Input() stepsLabels: string[];
    @Input() canExitFirstStep = false;
    @Output() goToStep = new EventEmitter<string>();

    selectStep(id: string): void {
        if (!this.canExitFirstStep)
            return;

        this.goToStep.emit(id);
    }
}
