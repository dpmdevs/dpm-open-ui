/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { DpmButtonComponent } from './dpm-button/dpm-button.component';
import { DpmCardComponent } from './dpm-card/dpm-card.component';
import { DpmUserCardComponent } from './dpm-user-card/dpm-user-card.component';
import { DpmProcessingActivityCardComponent } from './dpm-processing-activity-card/dpm-processing-activity-card.component';
import { DpmEditableInputComponent } from './dpm-editable-input/dpm-editable-input.component';
import { DpmSearchBoxComponent } from './dpm-search-box/dpm-search-box.component';
import { DpmSelectComponent } from './dpm-select/dpm-select.component';
import { DpmTagComponent } from './dpm-tag/dpm-tag.component';
import { DpmTagCouplerComponent } from './dpm-tag-coupler/dpm-tag-coupler.component';
import { DpmToggleSwitchComponent } from './dpm-toggle-switch/dpm-toggle-switch.component';
import { DpmTreeSelectorComponent } from './dpm-tree-selector/dpm-tree-selector.component';
import { WorkingUnitsSelectorComponent } from './dpm-working-units-selector/dpm-working-units-selector.component';
import { PaUnitsSelectorComponent } from './pa-units-selector/pa-units-selector.component';
import { PersonalDataDisclaimerComponent } from './personal-data-disclaimer/personal-data-disclaimer.component';
import { PreLoaderComponent } from './pre-loader/pre-loader.component';
import { WizardNavComponent } from './dpm-wizard/wizard-nav/wizard-nav.component';
import { WizardStepComponent } from './dpm-wizard/wizard-step/wizard-step.component';
import { ControlMessagesComponent } from './dpm-crud-page/base-details-page/control-messages/control-messages.component';
import { RequiredIconIndicatorComponent } from './required-icon-indicator/required-icon-indicator.component';
import { DataCategorySelectorComponent } from './data-category-selector/data-category-selector.component';

import { TableBaseService } from './dpm-crud-page/table-base-service';
import { BaseDetailsPage } from './dpm-crud-page/base-details-page/base-details-page';
import { GridDecorator } from './dpm-crud-page/grid.decorator';
import { BaseSelector } from './dpm-base-selector/base-selector';
import { BaseSelectorService } from './dpm-base-selector/base-selector.service';
import { BaseCrudPageComponent } from './dpm-crud-page/base-crud-page';
import { ValidationMessageComponent } from './dpm-crud-page/base-details-page/validation-message/validation-message.component';


export { TableBaseService, BaseDetailsPage, GridDecorator, BaseSelector, BaseSelectorService, BaseCrudPageComponent };
export * from './dpm-wizard/base-wizard-step';

export const sharedComponents = [
    DpmButtonComponent,
    DpmCardComponent,
    DpmUserCardComponent,
    DpmProcessingActivityCardComponent,
    DpmEditableInputComponent,
    DpmSearchBoxComponent,
    DpmSelectComponent,
    DpmTagComponent,
    DpmTagCouplerComponent,
    DpmToggleSwitchComponent,
    DpmTreeSelectorComponent,
    WorkingUnitsSelectorComponent,
    PaUnitsSelectorComponent,
    PersonalDataDisclaimerComponent,
    PreLoaderComponent,
    WizardNavComponent,
    WizardStepComponent,
    ControlMessagesComponent,
    ValidationMessageComponent,
    RequiredIconIndicatorComponent,
    DataCategorySelectorComponent
];
