/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, EventEmitter, Input, Output } from '@angular/core';

import { AnimationProvider } from '../../providers';


export interface DpmSelectOption {
    name: string;
    description?: string;
    value?: any;
    customClasses?: string;
}

@Component({
    selector: 'dpm-select',
    templateUrl: './dpm-select.component.html',
    styleUrls: ['./dpm-select.component.scss'],
    animations: [AnimationProvider.getToggleUpDownFlex()]
})
export class DpmSelectComponent {

    @Input() title: string;
    @Input() defaultValue: DpmSelectOption;
    @Input() options: DpmSelectOption[];
    @Output() selection = new EventEmitter();

    optionsVisible = 'unSelected';
    currentSelection: string;

    toggleOptions(): void {
        this.optionsVisible = this.optionsVisible === 'selected' ? 'unSelected' : 'selected';
    }

    select(item): void {
        this.currentSelection = item.name;
        this.selection.emit(item);
    }

}
