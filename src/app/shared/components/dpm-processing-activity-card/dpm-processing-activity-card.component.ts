/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, Input, TemplateRef } from '@angular/core';

import { DpmCardComponent } from '../dpm-card/dpm-card.component';
import { ProcessingActivity, Unit } from '@dpm-models';
import { RendererProvider } from '@dpm-providers';


@Component({
    selector: 'dpm-processing-activity-card',
    templateUrl: './dpm-processing-activity-card.component.html',
})
export class DpmProcessingActivityCardComponent extends DpmCardComponent {

    @Input() processingActivity: ProcessingActivity;
    @Input() units: Unit[] = [];
    @Input() dataTemplate: TemplateRef<any>;

    public RendererProvider = RendererProvider;

}
