/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, ElementRef, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import { Tag } from '../../models';


@Component({
    selector: 'dpm-search-box',
    templateUrl: './dpm-search-box.component.html',
    styleUrls: ['./dpm-search-box.component.scss'],
})
export class DpmSearchBoxComponent {
    @Input() placeholder: string;
    @Input() availableTags: Tag[];
    @Input() showTags = true;

    @Output() selectTag = new EventEmitter<any>();
    @Output() searchTags = new EventEmitter<string>();
    @Output() hideResults = new EventEmitter();

    keyword = '';
    searchTerm$ = new Subject<string>();
    currentHoveringTagIndex = -1;

    @HostListener('document:click', ['$event.target']) onClick(target) {
        const isClickOutsideOfThisComponent =
            !this._elementRef.nativeElement.contains(target)
            && !target.classList.contains('available-tag');

        if (isClickOutsideOfThisComponent)
            this.hideResults.emit();
    }

    constructor(private _elementRef: ElementRef) {
        this.searchTerm$
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(keyword => {
                this.keyword = keyword;
                this.filter();
            });
    }

    select(item): void {
        this.selectTag.emit(item);
    }

    filter(keyword = this.keyword): void {
        this.searchTags.emit(keyword);
    }

    resetSearch(): void {
        this.keyword = '';
        this.searchTags.emit('');
    }

    selectCurrentHoveringTag(): void {
        this.select(this.availableTags[this.currentHoveringTagIndex]);
    }

    incrementHoveringTagIndex(): void {
        if (this.currentHoveringTagIndex < this.availableTags.length - 1)
            this.currentHoveringTagIndex++;
        else
            this.currentHoveringTagIndex = 0;
    }

    decrementHoveringTagIndex(): void {
        if (this.currentHoveringTagIndex > 0)
            this.currentHoveringTagIndex--;
        else
            this.currentHoveringTagIndex = this.availableTags.length - 1;
    }

}
