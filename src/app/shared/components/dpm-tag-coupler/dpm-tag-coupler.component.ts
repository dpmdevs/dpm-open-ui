/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, EventEmitter, Input, Output, TemplateRef, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';

import { AnimationProvider } from '../../providers';
import { DpmTagCouplerLabels, Tag } from '../../models';


@Component({
    selector: 'dpm-tag-coupler',
    templateUrl: './dpm-tag-coupler.component.html',
    styleUrls: ['./dpm-tag-coupler.component.scss'],
    animations: [AnimationProvider.getSlideInTop(), AnimationProvider.getFadeIn()]
})
export class DpmTagCouplerComponent {

    @Input() areTagsDeletable: boolean = true;
    @Input() showInsertionPrompt: boolean = true;
    @Input() showDeletionPrompt: boolean = true;
    @Input() isDeleted: boolean = false;
    @Input() labels: DpmTagCouplerLabels;
    @Input() assignedTags$: Observable<Tag[]>;
    @Input() availableTags$: Observable<Tag[]>;
    @Input() displayToggleButton = true;
    @Input() deletionModalBody: TemplateRef<any>;
    @Input() additionModalBody: TemplateRef<any>;

    @Output() toggleTag = new EventEmitter<Tag>();
    @Output() searchTag = new EventEmitter<string>();

    @ViewChild('deletionModalTemplate') deletionModalTemplate: TemplateRef<any>;
    @ViewChild('additionModalTemplate') additionModalTemplate: TemplateRef<any>;

    modalRef: BsModalRef;
    showingResults = true;
    isSearchFieldVisible = false;
    protected lastKeyword = '';
    currentTag: Tag;

    constructor(private _modalService: BsModalService) {
    }

    onSearchTags(keyword): void {
        this.showingResults = true;

        if (!this.showingResults && keyword === this.lastKeyword)
            return;

        this.lastKeyword = keyword;
        this.searchTag.emit(keyword);
    }

    addTag(tag: Tag): void {
        if (!this.showInsertionPrompt) {
            this.toggleTag.emit(tag);
            return;
        }

        this.currentTag = tag;
        this.modalRef = this._modalService.show(this.additionModalTemplate);
    }

    removeTag(tag: Tag): void {
        if (!this.showDeletionPrompt) {
            this.toggleTag.emit(tag);
            return;
        }

        this.currentTag = tag;
        this.modalRef = this._modalService.show(this.deletionModalTemplate);
    }

}
