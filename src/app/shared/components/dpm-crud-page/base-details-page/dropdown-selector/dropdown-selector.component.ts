/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, HostListener, Input, OnDestroy, OnInit } from '@angular/core';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subscription } from 'rxjs/Subscription';


@Component({
    selector: '[dpmDropdownSelector]',
    templateUrl: './dropdown-selector.component.html',
    styleUrls: ['./dropdown-selector.component.scss']
})
export class DropdownSelectorComponent implements OnInit, OnDestroy {
    @Input() values: string[];
    @Input() activeTab$: BehaviorSubject<string>;

    private subscription: Subscription;
    private activeTab = 'dettagli';

    @HostListener('change', ['$event.target.value']) onTabChange(tab) {
        this.activeTab$.next(tab);
    }

    constructor() {
    }

    ngOnInit() {
        this.subscription = this.activeTab$
            .subscribe(tab => this.activeTab = tab);
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
