/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { EventEmitter, Input, OnDestroy, OnInit, Output, TemplateRef } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { ToastsManager } from 'ng2-toastr';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';

import { SelectedRecords } from '@dpm-helpers';
import { TableBaseService } from '../table-base-service';
import { BaseRecord } from '@dpm-models';
import { ValidationMessages } from './validation-messages';


export abstract class BaseDetailsPage implements OnInit, OnDestroy {

    modalRef: BsModalRef;

    protected abstract get modalService(): BsModalService;

    deletionModalLabels = {
        title: 'Conferma eliminazione',
        htmlText: `<p>Sei sicuro di voler eliminare i record selezionati?</p>
            <p>Tutte le relazioni collegate all’oggetto verranno eliminate.</p>
            <div role="alert" class="alert alert-warning">
                <strong>Attenzione:</strong> Questa operazione è irreversibile.
            </div>`,
        confirmText: 'Elimina i record',
        cancelText: 'Annulla'
    };

    @Input() selectedRecords: SelectedRecords<BaseRecord>;
    @Input() activeTab$: BehaviorSubject<string>;
    @Input() defaultTab: string;
    @Input() dataService: TableBaseService;
    @Input() toastr: ToastsManager;

    @Output() updateTable = new EventEmitter();

    activeTab: string;
    dropdownOptions?: string[] = [];
    readonly singleRecordOptions?: string[] = [];
    readonly multipleRecordOptions?: string[] = [];

    protected activeTabSubscription: Subscription;
    protected validationMessages: ValidationMessages = {
        errors: {},
        message: ''
    };

    protected abstract initValidationErrors(): void;

    get records(): BaseRecord[] {
        return this.selectedRecords.getAllRecords();
    }

    constructor() {
        this.initValidationErrors();
    }

    ngOnInit() {
        this.activeTabSubscription = this.activeTab$
            .subscribe(t => this.activeTab = t);
    }

    ngOnDestroy() {
        this.activeTabSubscription.unsubscribe();
    }

    openModal(template: TemplateRef<any>): void {
        this.modalRef = this.modalService.show(template);
    }

    protected catchError(error: HttpErrorResponse): Observable<HttpErrorResponse> {
        if (error.status === 422) {
            this.validationMessages = error.error;

            this.toastr.error(this.validationMessages.message).then();

            for (const err in this.validationMessages.errors)
                this.toastr.error(this.validationMessages.errors[err].toString()).then();

            return Observable.throw(error);
        }

        return Observable.of(error);
    }

    onAddEntity(entity: BaseRecord): void {
        this.dataService.addEntity(entity)
            .subscribe({
                next: (() => {
                    this.refreshComponents();
                    this.initValidationErrors();
                    this.toastr.success('Record inserito correttamente').then();
                }),
                error: this.catchError.bind(this)
            });
    }

    onUpdateEntity(entity: BaseRecord): void {
        this.dataService.updateEntity(entity)
            .subscribe({
                next: () => {
                    this.refreshComponents();
                    this.initValidationErrors();
                    this.toastr.info('Record aggiornato correttamente').then();
                },
                error: this.catchError.bind(this)
            });
    }

    onDeleteEntities(): void {
        this.records.forEach(entity => {
            this.dataService.deleteEntity(entity)
                .subscribe({
                    next: () => {
                        this.refreshComponents();
                        this.toastr.warning('Record eliminato').then();
                    },
                    error: this.catchError.bind(this)
                });
        });
    }

    onDisableEntity(): void {
        this.records.forEach(entity => {
            this.dataService.disableEntity(entity)
                .subscribe({
                    next: (() => {
                        this.refreshComponents();
                        this.toastr.warning('Record disattivato').then();
                    })
                });
        });
    }

    onRestoreEntity(): void {
        this.records.forEach(entity => {
            this.dataService.restoreEntity(entity)
                .subscribe({
                    next: (() => {
                        this.refreshComponents();
                        this.toastr.custom('Record riattivato correttamente').then();
                    }),
                    error: this.catchError.bind(this)
                });
        });
    }

    private refreshComponents() {
        this.updateTable.emit();
        this.clearSelection();
        this.restoreDefaultTab();
    }

    clearSelection(): void {
        this.selectedRecords.removeAllRecords();
    }

    restoreDefaultTab() {
        this.activeTab$.next(this.defaultTab);
    }

}
