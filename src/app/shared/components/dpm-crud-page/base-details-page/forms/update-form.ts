/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { AfterContentInit, EventEmitter, Input, Output } from '@angular/core';

import { BaseRecord } from '@dpm-models';
import { BaseForm } from './base-form';
import { ValidationMessages } from '../validation-messages';


export abstract class UpdateForm<T extends BaseRecord> extends BaseForm implements AfterContentInit {
    @Input() record: T;
    @Input() validationMessages: ValidationMessages;

    @Output() updateEntity = new EventEmitter<T>();
    @Output() cancelUpdate = new EventEmitter();

    ngOnInit() {
        this.setControlsConfig(this.record);
        super.ngOnInit();
    }

    ngAfterContentInit() {
        Object.keys(this.recordInfo.controls).forEach(key => {
            const control = this.recordInfo.get(key);

            if (control.value !== '')
                control.markAsTouched();
        });
    }

    onSubmit(entity): void {
        entity.id = this.record.id;
        this.updateEntity.emit(entity);
    }

    cancelRecordUpdate(): void {
        this.cancelUpdate.emit();
    }
}
