/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { BaseRecord, QueryParams, TableData } from '../../models';
import { StateService } from '../../../state/state.service';
import { initialQueryParams } from '../../../state/query-params/query-params';


@Injectable()
export abstract class TableBaseService {

    abstract get baseUrl(): string;

    abstract get table(): string;

    constructor(protected http: HttpClient, protected _stateService: StateService) {
    }

    getTableData(queryParams?: Partial<QueryParams>): Observable<TableData> {
        return this.http.get<TableData>(this.makeUrl(queryParams))
            .catch(() => {
                return Observable.of(<TableData>{ data: [], metadata: { totalRecords: 0 } });
            })
            .shareReplay(2);
    }

    getEntity(id: number): Observable<BaseRecord> {
        return this.http.get<BaseRecord>(`${this.baseUrl}${this.table}/${id}`);
    }

    addEntity<T>(entity: BaseRecord): Observable<T> {
        return this.http.post<T>(`${this.baseUrl}${this.table}`, entity);
    }

    updateEntity(entity: BaseRecord): Observable<Response> {
        return this.http.put<Response>(`${this.baseUrl}${this.table}/${entity.id}`, entity);
    }

    deleteEntity(entity: BaseRecord): Observable<Response> {
        return this.http.delete<Response>(`${this.baseUrl}${this.table}/${entity.id}/force`);
    }

    disableEntity(entity: BaseRecord): Observable<Response> {
        return this.http.delete<Response>(`${this.baseUrl}${this.table}/${entity.id}`);
    }

    restoreEntity(entity: BaseRecord): Observable<Response> {
        return this.http.put<Response>(`${this.baseUrl}${this.table}/${entity.id}/restore`, null);
    }

    protected makeUrl(queryParams: Partial<QueryParams>): string {
        const {
            sortField = 'id',
            sortDirection = 'desc',
            start = 0,
            limit = 15,
            trashed = false,
            searchCriteria = [],
            searchOrCriteria = [],
        } = Object.assign({}, initialQueryParams, queryParams);

        return `${this.baseUrl}${this.table}`
            + `?sortField=${sortField}`
            + `&sortDirection=${sortDirection}`
            + `&start=${start}`
            + `&limit=${limit}`
            + `&trashed=${trashed ? 1 : 0}`
            + (searchCriteria.length > 0 ? `&searchCriteria=${JSON.stringify(searchCriteria)}` : '')
            + (searchOrCriteria.length > 0 ? `&searchOrCriteria=${JSON.stringify(searchOrCriteria)}` : '');
    }

}
