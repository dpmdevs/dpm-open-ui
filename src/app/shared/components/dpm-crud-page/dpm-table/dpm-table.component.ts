/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { isUndefined } from 'util';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';

import { BaseRecord, SearchCriteria } from '@dpm-models';
import { SelectedRecords } from '@dpm-helpers';
import { RendererProvider } from '@dpm-providers';
import { Header } from '../table-header/Header';


export interface HeaderUpdateParams {
    sortField: string,
    sortDirection: string,
    searchCriteria: SearchCriteria[]
}

@Component({
    selector: 'dpm-table',
    templateUrl: './dpm-table.component.html',
    styleUrls: ['./dpm-table.component.scss']
})
export class DpmTableComponent implements OnInit, OnDestroy, OnChanges {

    @Input() records: BaseRecord[] = [];
    @Input() headers: Header[] = [];
    @Input() trashed: boolean = false;

    @Output() headerUpdate = new EventEmitter<HeaderUpdateParams>();
    @Output() selectRecords = new EventEmitter<SelectedRecords<BaseRecord>>();

    public RendererProvider = RendererProvider;
    private searchCriteriaSubject = new Subject<SearchCriteria>();
    private searchCriteriaSubscription: Subscription;
    headerUpdateParams: HeaderUpdateParams;

    protected selectedRecords = new SelectedRecords<BaseRecord>();

    constructor() {
        this.headerUpdateParams = {
            sortField: 'id',
            sortDirection: 'desc',
            searchCriteria: [],
        };
    }

    private areAllSame(): boolean {
        const checkboxesValues = this.records.map(r => this.selectedRecords.isRecordSelected(r));

        for (const value of checkboxesValues)
            if (value !== checkboxesValues[0])
                return false;

        return true;
    }

    private deactivateOtherHeaders(header: Header): void {
        this.headers.forEach(h => h.isActive = h.columnName === header.columnName);
    }

    private updateSearchCriteriaWith({ columnName, keyword }: SearchCriteria): void {
        const searchCriteria = this.headerUpdateParams.searchCriteria
            .filter(sc => sc.columnName !== columnName);

        if (keyword !== '')
            searchCriteria.push({ columnName, keyword });

        this.headerUpdateParams.searchCriteria = searchCriteria;
    }

    private emitUpdateTableData() {
        this.headerUpdate.emit(<HeaderUpdateParams> this.headerUpdateParams);
    }

    private toggleDeletedAtColumn(value) {
        const hasDeletedAtHeader = this.headers.map(h => h.columnName).indexOf('deleted_at') > -1;

        if (value) {
            if (!hasDeletedAtHeader)
                this.headers.push(new Header({ displayName: 'Data di disattivazione', columnName: 'deleted_at' }));
        } else
            this.headers = this.headers.filter(h => h.columnName !== 'deleted_at');
    }

    protected onHeaderUpdate(header: Header): void {

        this.headerUpdateParams = Object.assign(
            {},
            this.headerUpdateParams,
            { sortDirection: header.sortDirection, sortField: header.columnName }
        );

        this.deactivateOtherHeaders(header);
        this.emitUpdateTableData();
    }


    protected onSearchUpdate(searchCriteria: SearchCriteria): void {
        this.searchCriteriaSubject.next(searchCriteria);
    }

    ngOnInit() {

        this.searchCriteriaSubscription = this.searchCriteriaSubject
            .debounceTime(300)
            .distinctUntilChanged(
                null,
                ({ columnName, keyword }: SearchCriteria) => columnName + keyword
            )
            .map(criteria => {
                this.updateSearchCriteriaWith(criteria);
            })
            .switchMap(() => {
                this.emitUpdateTableData();
                return Observable.of([]);
            })
            .subscribe();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (!isUndefined(changes.trashed))
            this.toggleDeletedAtColumn(changes.trashed.currentValue);
    }

    areAllChecked(): boolean {
        return this.areAllSame() && this.selectedRecords.isRecordSelected(this.records[0]);
    }

    toggleRecord(record: BaseRecord): void {
        this.selectedRecords.toggleRecord(record);
        this.selectRecords.emit(this.selectedRecords);
    }

    toggleAllRecords(): void {
        if (this.areAllChecked())
            this.selectedRecords.removeAllRecords();
        else
            this.records.forEach(r => this.selectedRecords.addRecords(r));

        this.selectRecords.emit(this.selectedRecords);
    }

    selectSingleRecord(record: BaseRecord) {
        const isTheOnlySelectedRecord =
            this.selectedRecords.getAllRecords().length === 1 &&
            this.selectedRecords.isRecordSelected(record);

        this.selectedRecords.removeAllRecords();

        if (!isTheOnlySelectedRecord)
            this.selectedRecords.addRecords(record);

        this.selectRecords.emit(this.selectedRecords);
    }

    ngOnDestroy() {
        this.searchCriteriaSubscription.unsubscribe();
    }

}
