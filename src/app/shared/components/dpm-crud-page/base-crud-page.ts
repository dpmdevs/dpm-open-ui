/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import {
    AfterContentInit, ComponentFactory, ComponentFactoryResolver, OnDestroy, OnInit, ViewChild, ViewContainerRef
} from '@angular/core';
import { isNull, isNullOrUndefined } from 'util';
import { ToastsManager } from 'ng2-toastr';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';

import { Headers } from './table-header/Header';
import { BaseRecord, QueryParams, TableData } from '@dpm-models';
import { TableBaseService } from '@dpm-components';
import { SelectedRecords } from '@dpm-helpers';
import { PaginationParams } from './pagination/pagination.component';
import { HeaderUpdateParams } from './dpm-table/dpm-table.component';
import { StateService } from '../../../state/state.service';
import { QueryParamsActions } from '../../../state/query-params/query-params.actions';
import { initialQueryParams } from '../../../state/query-params/query-params';


export abstract class BaseCrudPageComponent implements OnInit, OnDestroy, AfterContentInit {

    static detailPage = 'dettagli';

    @ViewChild('detailsComponent', { read: ViewContainerRef }) detailsComponent;

    title: string;
    description: string;
    headers: Headers;
    hasDetailPage: boolean;
    hasNewButton: boolean;
    isLoading: boolean;
    records: BaseRecord[] = [];
    totalRecords = 0;

    queryParams$: Observable<QueryParams>;
    paginationParams$: Observable<PaginationParams>;
    tableSubscription: Subscription;

    protected abstract get stateService(): StateService;

    protected abstract get defaultTab(): string;

    protected abstract get dataService(): TableBaseService;

    protected abstract get resolver(): ComponentFactoryResolver;

    protected abstract get viewChildComponent(): any | null;

    protected abstract get viewContainerRef(): ViewContainerRef;

    protected abstract get toastr(): ToastsManager;

    protected detailsActiveTab$: BehaviorSubject<string>;
    protected selectedRecords = new SelectedRecords<BaseRecord>();
    protected detailsComponentRef: any;
    protected updateTableSubscription: Subscription;

    protected createDetailsComponent(): void {
        const factory: ComponentFactory<any> = this.resolver.resolveComponentFactory(this.viewChildComponent);

        this.detailsComponent.clear();

        if (!isNullOrUndefined(this.updateTableSubscription))
            this.updateTableSubscription.unsubscribe();

        this.detailsComponentRef = this.detailsComponent.createComponent(factory);
        this.detailsComponentRef.instance.selectedRecords = this.selectedRecords;
        this.detailsComponentRef.instance.defaultTab = this.defaultTab;
        this.detailsComponentRef.instance.activeTab$ = this.detailsActiveTab$;
        this.detailsComponentRef.instance.dataService = this.dataService;
        this.detailsComponentRef.instance.toastr = this.toastr;

        this.updateTableSubscription = this.detailsComponentRef.instance.updateTable
            .subscribe(this.subscribeOnTableData.bind(this));
    }

    protected subscribeOnTableData(): void {
        this.tableSubscription = this.queryParams$
            .filter(params => !isNullOrUndefined(params))
            .mergeMap(queryParams => this.dataService.getTableData(queryParams))
            .distinctUntilChanged()
            .subscribe((tableData: TableData) => {
                this.isLoading = false;
                this.totalRecords = tableData.metadata.totalRecords;
                this.records = tableData.data;
            });
    }

    ngOnInit() {
        this.detailsActiveTab$ = new BehaviorSubject(this.defaultTab);
        this.queryParams$ = this.stateService.getQueryParams();
        this.paginationParams$ = this.queryParams$
            .map((queryParams: QueryParams) => isNull(queryParams) ? initialQueryParams : queryParams)
            .map(({ start, limit, trashed }) => ({ start, limit, trashed }));
        this.toastr.setRootViewContainerRef(this.viewContainerRef);
        this.subscribeOnTableData();
    }

    ngAfterContentInit() {
        if (this.hasDetailPage)
            this.createDetailsComponent();
    }

    ngOnDestroy() {
        this.detailsComponentRef.destroy();
        if (!isNullOrUndefined(this.tableSubscription))
            this.tableSubscription.unsubscribe();
    }

    onSelectRecords(selectedRecords: SelectedRecords<BaseRecord>) {
        this.detailsComponentRef.instance.selectedRecords = selectedRecords;
        this.detailsActiveTab$.next(BaseCrudPageComponent.detailPage);
    }

    setActiveTab(tabName: string): void {
        this.detailsActiveTab$.next(tabName);
    }

    onHeaderUpdate({ sortField, sortDirection, searchCriteria }: HeaderUpdateParams) {
        this.isLoading = true;
        this.stateService.dispatch(QueryParamsActions.updateQueryParams({
            sortField,
            sortDirection,
            searchCriteria,
            start: 0
        } as Partial<QueryParams>));
    }

    onPaginationUpdate(paginationParams: Partial<PaginationParams>): void {
        this.isLoading = true;
        this.stateService.dispatch(QueryParamsActions.updateQueryParams(paginationParams as Partial<QueryParams>));
    }

}
