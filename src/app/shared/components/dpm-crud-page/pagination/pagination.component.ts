/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, EventEmitter, HostListener, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

import { initialQueryParams } from '../../../../state/query-params/query-params';


interface PageItem {
    pageNumber: number;
    displayText: string;
    isActive: boolean;
}

export interface PaginationParams {
    start: number;
    limit: number;
    trashed: boolean;
}


@Component({
    selector: 'dpm-pagination',
    templateUrl: './pagination.component.html',
    styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit, OnChanges {

    @Input() set paginationParams(paginationParams: PaginationParams) {
        this.paginationParameters = Object.assign({}, this.paginationParameters, paginationParams);
    };

    @Input() totalRecords: number;
    @Input() isLoading = true;
    @Output() paginationUpdate = new EventEmitter<Partial<PaginationParams>>();

    lastDisplayedRecordIndex = 0;
    pageItems: PageItem[] = [];

    paginationParameters: PaginationParams = {
        start: initialQueryParams.start,
        limit: initialQueryParams.limit,
        trashed: initialQueryParams.trashed
    };

    @HostListener('click', ['$event']) onClick() {
        return false;
    }

    constructor() {
    }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges) {
        const { start, limit } = this.paginationParameters;
        this.lastDisplayedRecordIndex = start + Number(limit);
        this.pageItems = [];

        const numberOfPages = Math.ceil(this.totalRecords / limit);
        const currentPage = Math.ceil(start / limit) + 1;

        const firstPageItem = { pageNumber: 1, displayText: '«', isActive: false, };
        const lastPageItem = { pageNumber: numberOfPages, displayText: '»', isActive: false };

        this.pageItems.push(firstPageItem);

        for (let i = currentPage - 5; i < currentPage + 5; i++)
            if (i > 0 && i <= numberOfPages)
                this.pageItems.push({
                    pageNumber: i,
                    displayText: `${i}`,
                    isActive: i === currentPage
                });

        this.pageItems.push(lastPageItem);
    }

    onPageItemClick(pageNumber: number): void {
        const start = this.paginationParameters.limit * (pageNumber - 1);

        if (start < 0 || start > this.totalRecords) return;

        this.paginationUpdate.emit({ start });
    }

    updatePagination({ start }): void {
        this.paginationUpdate.emit(Object.assign({}, this.paginationParameters, { start }));
    }

}
