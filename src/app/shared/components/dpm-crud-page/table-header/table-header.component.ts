/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';

import { Header } from './Header';
import { SearchCriteria } from '../../../models';


@Component({
    selector: '[dpmTableHeader]',
    templateUrl: './table-header.component.html',
    styleUrls: ['./table-header.component.scss']
})
export class TableHeaderComponent implements OnInit {
    private searchTerm = '';

    @Input() header: Header;
    @Output() headerUpdate = new EventEmitter<Header>();
    @Output() searchUpdate = new EventEmitter<SearchCriteria>();

    @HostListener('click', ['$event']) onClick(event) {
        if (!this.header.sortable || !this.isSearchToggled(event.target))
            return false;

        this.header.invertSort();
        this.header.isActive = true;
        this.headerUpdate.emit(this.header);
        return false;
    }

    private isSearchToggled(target): string {
        return target.classList.contains('clickable')
            || target.classList.contains('fa-sort')
            || target.classList.contains('fa-sort-amount-asc')
            || target.classList.contains('fa-sort-amount-desc');
    }

    constructor() {
    }

    ngOnInit() {
    }

    getCSSCaretClass(): string {
        if (this.header.isActive)
            return this.header.isDirectionASC() ? 'fa-sort-amount-asc' : 'fa-sort-amount-desc';

        return 'fa-sort';
    }

    search(): void {
        this.searchUpdate.emit(<SearchCriteria> {
            columnName: this.header.searchColumn,
            keyword: this.searchTerm
        });
    }
}
