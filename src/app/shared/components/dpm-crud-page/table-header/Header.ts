/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { isUndefined } from 'util';

export interface IHeader {
    displayName: string;
    columnName?: string;
    sortable?: boolean;
    searchable?: boolean;
    searchColumn?: string;
    isActive?: boolean;
    sortDirection?: 'asc' | 'desc';
    isArray?: boolean;
    listFieldName?: string;
    renderer?: (f: any) => string;
    customClass?: string;
}

export declare type Headers = IHeader[];

export class Header implements IHeader {
    displayName: string;
    columnName: string;
    sortable: boolean;
    searchable: boolean;
    searchColumn: string;
    isActive: boolean;
    sortDirection: 'asc' | 'desc';
    listFieldName: string;
    renderer: (f: any) => string;
    customClass: string;

    constructor({
                    displayName,
                    columnName,
                    sortable,
                    searchable,
                    searchColumn,
                    isActive,
                    sortDirection,
                    listFieldName,
                    renderer,
                    customClass,
                }: IHeader) {
        this.displayName = displayName;
        this.columnName = isUndefined(columnName) ? displayName : columnName;
        this.sortable = isUndefined(sortable) ? true : sortable;
        this.searchable = isUndefined(searchable) ? true : searchable;
        this.searchColumn = isUndefined(searchColumn) ? columnName : searchColumn;
        this.isActive = isUndefined(isActive) ? false : isActive;
        this.sortDirection = isUndefined(sortDirection) ? 'asc' : sortDirection;
        this.listFieldName = isUndefined(listFieldName) ? '' : listFieldName;
        this.renderer = isUndefined(renderer) ? field => field : renderer;
        this.customClass = isUndefined(customClass) ? '' : customClass
    }

    isDirectionASC() {
        return this.sortDirection === 'asc';
    }

    invertSort() {
        this.sortDirection = this.sortDirection === 'asc' ? 'desc' : 'asc';
    }
}
