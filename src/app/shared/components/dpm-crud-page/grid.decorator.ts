/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Header, Headers } from './table-header/Header';

export interface GridConfig {
    title: string;
    description: string;
    headers: Headers;
    paginationLimit: number;
    hasDetailPage: boolean;
    hasNewButton?: boolean;
}

export function GridDecorator(gridConfig: GridConfig) {
    return function (target): void {

        const { paginationLimit, title, description, hasDetailPage, hasNewButton = true } = gridConfig;

        const decoratingClass = Object.assign(
            target.prototype,
            {
                headers: <Headers>[],
                paginationLimit,
                title,
                description,
                hasDetailPage,
                hasNewButton
            }
        );

        gridConfig.headers.forEach((header: Header) => {
            decoratingClass.headers.push(new Header(header));
        });
    };
}
