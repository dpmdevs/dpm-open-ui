/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, EventEmitter, Input, OnInit, Output, TemplateRef } from '@angular/core';
import { ITreeOptions, TreeComponent, TreeNode } from 'angular-tree-component';
import { isNull } from 'util';

import { AnimationProvider } from '@dpm-providers';


export interface DpmTreeOptions extends ITreeOptions {
    searchable: boolean;
    displayCheckboxes: boolean;
}

@Component({
    selector: 'dpm-tree-selector',
    templateUrl: './dpm-tree-selector.component.html',
    styleUrls: ['./dpm-tree-selector.component.scss'],
    animations: [AnimationProvider.getSlideIn()]
})
export class DpmTreeSelectorComponent implements OnInit {

    @Input() nodes: any[] = [];
    @Input() options: DpmTreeOptions = {
        animateExpand: true,
        animateAcceleration: 2.2,
        allowDrag: true,
        levelPadding: 40,
        searchable: true,
        displayCheckboxes: true
    };
    @Input() selectedNodesIds: number[] = [];
    @Input() filterFunction = (filter: string, tree: TreeComponent): void => {
        const searchNameOrCode = (node: TreeNode): boolean => {
            return node.data.name.toLowerCase().indexOf(filter.toLowerCase()) !== -1;
        };

        // https://angular2-tree.readme.io/docs/filtering#filter-by-function
        tree.treeModel.filterNodes(searchNameOrCode);
    };
    @Input() nodeTemplate: TemplateRef<any>;
    @Input() nodeNameTemplate: TemplateRef<any>;

    @Output() nodeSelected = new EventEmitter<TreeNode>();

    constructor() {
    }

    ngOnInit() {
    }

    isSelected(node: TreeNode): boolean {
        return (!isNull(node.data.isSelected) && node.data.isSelected)
            || this.selectedNodesIds.includes(node.data.id);
    }

}
