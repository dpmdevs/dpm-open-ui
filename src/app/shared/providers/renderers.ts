/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { HttpErrorResponse } from '@angular/common/http';
import { isNull, isNullOrUndefined, isUndefined } from 'util';
import * as moment from 'moment';

import {
    DataCategory, OperatingSystem, ProcessingActivity, Qualification, ThirdParties, Unit, User
} from '@dpm-models';
import { DpmConstants } from '../helpers';


export class RendererProvider {

    static unitRenderer(units: Unit[]): string {
        if (isNullOrUndefined(units) || units.length === 0)
            return 'Nessuna unità';

        return units.length === 1 ? units[0].name : 'Condiviso';
    }

    static assignmentUnitsRenderer(units: Unit[]): string {
        if (isNullOrUndefined(units) || units.length === 0)
            return 'Nessuna unità';

        return units.map(u => u.name).join(', ');
    }

    static processTypeRenderer(processTypes: any[]): string {
        return processTypes.map(p => p.name).join(', ');
    }

    static riskEstimateRenderer(riskEstimate: number) {
        let labelClass = 'label-default';
        let riskEstimateLabel = 'non classificato';

        switch (riskEstimate) {
            case 0:
                labelClass = 'label-success';
                riskEstimateLabel = 'basso';
                break;

            case 1:
                labelClass = 'label-warning';
                riskEstimateLabel = 'medio';
                break;

            case 2:
                labelClass = 'label-danger';
                riskEstimateLabel = 'alto';
                break;
        }

        return `<h5 class="text-uppercase"><span class="label ${labelClass}">${riskEstimateLabel}</span></h5>`;
    }

    static userRenderer(user: User): string {
        return user ? `${user.firstname} ${user.lastname}` : 'Nessuno';
    }

    static dpiaStatusRenderer(status: string): string {
        let labelClass = 'label-default';

        switch (status) {
            case 'completo':
                labelClass = 'label-success';
                break;

            case 'in corso':
                labelClass = 'label-warning';
                break;

            case 'chiuso':
            case 'in attesa di revisione':
                labelClass = 'label-info';
                break;
        }

        return `<h5 class="text-uppercase"><span class="label ${labelClass}">${status}</span></h5>`;
    }

    static dataCategoriesRenderer(dataCategories: DataCategory[] = []): string {
        return dataCategories.length > 0
            ? dataCategories.map(dc => dc.name).join(', ')
            : 'Nessuna categoria';
    }

    static qualificationsRenderer(qualifications: Qualification[]): string {
        return (qualifications && qualifications.length > 0)
            ? qualifications.map(q => q.name).join(', ')
            : 'Nessuna qualifica';
    }

    static dataSourceTypesRenderer(dataSourceTypes: number[]): string {
        let result;

        if (dataSourceTypes.includes(DpmConstants.DATA_SOURCE_SUBJECT)) {
            result = 'Raccolti presso l’interessato';
            if (dataSourceTypes.includes(DpmConstants.DATA_SOURCE_THIRD_PARTY))
                result += ' e comunicati da terzi';
        } else if (dataSourceTypes.includes(DpmConstants.DATA_SOURCE_THIRD_PARTY))
            result = 'Comunicati da terzi';
        else
            result = 'Non specificata';

        return result;
    }

    static thirdPartiesRenderer(thirdParties: ThirdParties[]): string {
        return (thirdParties.length > 0)
            ? thirdParties.map(t => t.company_name).join(', ')
            : 'Nessuna associazione';
    }

    static operatingSystemRenderer(operatingSystem: OperatingSystem): string {
        return !isNull(operatingSystem) ? operatingSystem.name : 'Nessuno impostato';
    }

    static processingActivitiesRenderer(p: ProcessingActivity): string {
        if (isNull(p))
            return `Nessun trattamento assegnato`;

        let string;
        if (!isNullOrUndefined(p.identification_code && p.identification_code !== ''))
            string = `${p.identification_code}: ${p.name}`;
        else
            string = p.name;

        if (p.deleted_at !== null)
            string = `${string} <i class="fa fa-warning text-warning" title="Trattamento eliminato"></i>`;

        return string;
    }

    static httpErrorResponseRenderer(httpError: HttpErrorResponse): string[] {
        if (httpError.status === 0)
            return ['Il server non risponde. Contattare l’assistenza tecnica'];

        const parsedErrors = !isUndefined(httpError.error.errors) ? httpError.error.errors : httpError.error;
        return Object.keys(parsedErrors).map(key => parsedErrors[key]);
    }

    static userIconCssClassRenderer(base64image: string): string {
        return RendererProvider.isImageInvalid(base64image) ? 'bg-gray-lighter' : '';
    }

    static userIconSrcRenderer(base64image: string): string {
        return RendererProvider.isImageInvalid(base64image) ? DpmConstants.USER_ICON_PATH : base64image;
    }

    static isImageInvalid(base64image: string): boolean {
        const deprecatedTransparentPixel = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==';
        const deprecatedTransparentPixel2 = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';
        return isNullOrUndefined(base64image) || base64image === '' || base64image === deprecatedTransparentPixel || base64image === deprecatedTransparentPixel2;
    }

    static identificationCodeRenderer(identificationCode: string): string {
        return isNullOrUndefined(identificationCode) ? 'Nessun identificativo' : identificationCode;
    }

    static dateRenderer(date: Date | string, format = 'L'): string {
        return isNullOrUndefined(date) ? '' : moment(date).format(format);
    }

    static truncateText({ maxStringLength = 500 }): (text: string) => string {
        return text => {
            if (text.length <= maxStringLength)
                return text;

            const truncatedTextArray = text.substr(0, maxStringLength).split(' ');
            return `${truncatedTextArray.splice(0, truncatedTextArray.length - 1).join(' ')} [...]`;
        };
    }

}
