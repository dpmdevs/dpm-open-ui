/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { animate, state, style, transition, trigger } from '@angular/animations';

export class AnimationProvider {

    static getSlideIn() {
        return trigger('SlideIn', [
            transition(':enter', [
                style({
                    opacity: 0,
                    transform: 'translateX(-100%)'
                }),
                animate('0.2s ease-in')
            ])
        ]);
    }

    static getFadeIn() {
        return trigger('FadeIn', [
            transition(':enter', [
                style({
                    opacity: 0
                }),
                animate('0.4s ease-in')
            ])
        ]);
    }

    static getSlideInFadeInRight() {
        return trigger('SlideInFadeInRight', [
            state('hide', style({
                opacity: 0,
                position: 'relative',
                right: '-100%',
            })),
            state('show', style({
                opacity: 1,
                right: '0',
            })),
            transition('show <=> hide', [animate('0.2s ease-in')])
        ]);
    }

    static getDelayedSlideInBottom() {
        return trigger('DelayedSlideInBottom', [
            transition(':enter', [
                style({ opacity: '0', position: 'relative', top: '5rem' }),
                animate('0.2s 1.2s ease-in', style({ opacity: '1', position: 'relative', top: '0' }))
            ])
        ]);
    }

    static getSlideInTop() {
        return trigger('SlideInTop', [
            transition(':enter', [
                style({ opacity: '0', position: 'relative', top: '-.5rem' }),
                animate('0.2s ease-in', style({ opacity: '1', position: 'relative', top: '0' }))
            ])
        ]);
    }

    static getToggleUpDown() {
        return trigger('ToggleUpDown', [
            state('selected', style({ opacity: 1, display: 'block' })),
            state('unSelected', style({ opacity: 0, height: 0, display: 'none' })),
            transition('selected <=> unSelected', [
                animate('0.1s ease-in')
            ])
        ]);
    }

    static getToggleUpDownFlex() {
        return trigger('ToggleUpDownFlex', [
            state('selected', style({ opacity: 1, display: 'flex' })),
            state('unSelected', style({ opacity: 0, height: 0, display: 'none' })),
            transition('selected <=> unSelected', [
                animate('0.1s ease-in')
            ])
        ]);
    }
}
