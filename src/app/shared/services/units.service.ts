/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { environment } from 'environments/environment';
import { TableBaseService } from '@dpm-components/dpm-crud-page';
import { TableData, Unit, User } from '../models';


@Injectable()
export class UnitsService extends TableBaseService {

    get baseUrl(): string {
        return environment.apiUrl;
    }

    get table(): string {
        return 'units';
    }

    getUnits(): Observable<Unit[]> {
        return this.http.get(`${this.baseUrl}${this.table}`)
            .map((response: TableData) => <Unit[]>response.data);
    }

    getAssignedDirectors(unitId: number): Observable<User[]> {
        return this.http.get<User[]>(`${this.baseUrl}${this.table}/${unitId}/directors`);
    }

    getAvailableDirectors(unitId: number, keyword?: string): Observable<User[]> {
        const url = `${this.baseUrl}${this.table}/${unitId}/available-directors?sortField=user_number&sortDirection=asc`
            + (
                keyword !== ''
                    ? `&searchOrCriteria=${ JSON.stringify([
                        { columnName: 'firstname', keyword },
                        { columnName: 'lastname', keyword },
                        { columnName: 'user_number', keyword }
                    ]) }`
                    : ''
            );

        return this.http.get<User[]>(url);
    }

    toggleDirector(unitId: number, directorId: number): Observable<Response> {
        return this.http.put<Response>(`${this.baseUrl}${this.table}/${unitId}/directors`, { directors: [directorId] });
    }

}
