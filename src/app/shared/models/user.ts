/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { BaseRecord } from './table-data';
import { Unit } from './unit';
import { Qualification } from './qualification';
import { Role } from './role';

export interface User extends BaseRecord {
    created_at: string;
    username: string;
    firstname: string;
    lastname: string;
    email: string;
    password?: string;
    user_number: string;
    fiscal_code: string;
    sex: string;
    dismissal_date: Date;
    avatar: string;
    binding_field: string;
    working_units: Unit[];
    in_charge_units: Unit[];
    delegated_units: Unit[];
    directed_units: Unit[];
    qualifications: Qualification[];
    homepage: string;
    roles?: Role[];
}
