/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Moment } from 'moment';

import { BaseRecord } from './table-data';
import { ProcessingActivity } from './processing-activity';


export interface DpiaProject extends BaseRecord {
    name: string;
    goal: string;
    status: string;
    proportionality_evaluation: boolean;
    proportionality_reason: string;
    user_id: number;
    processing_activity_id: number;
    notes?: string;
    review_date?: Moment;
    processing_activity: ProcessingActivity;
}
