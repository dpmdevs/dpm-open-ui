/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastModule, ToastOptions } from 'ng2-toastr';
import { TreeModule } from 'angular-tree-component';
import { AlertModule, TooltipModule } from 'ngx-bootstrap';

import { CustomToastOption } from './helpers';
import { sharedDirectives } from './directives';
import { sharedPipes } from './pipes';
import { sharedComponents } from './components';

/**
 * SharedModule to hold the common components, directives, and pipes and share them with the modules that need them.
 *
 * Do not specify app-wide singleton providers in a shared module.
 * A lazy loaded module that imports that shared module will make its own copy of the service
 *
 * This module should consist entirely of declarations most of them exported.
 * It should not have providers.
 *
 */

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        TreeModule,
        ToastModule.forRoot(),
        AlertModule.forRoot(),
        TooltipModule.forRoot(),
    ],
    declarations: [
        ...sharedPipes,
        ...sharedComponents,
        ...sharedDirectives,
    ],
    exports: [
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        TreeModule,
        AlertModule,
        ...sharedPipes,
        ...sharedComponents,
        ...sharedDirectives,
    ],
    providers: [
        { provide: ToastOptions, useClass: CustomToastOption },
    ],
})
export class SharedModule {
}
