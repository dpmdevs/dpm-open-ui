/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { AfterViewInit, Directive, ElementRef, Renderer } from '@angular/core';


@Directive({
    selector: '[dpmAutofocus]'
})
export class AutofocusDirective implements AfterViewInit {

    constructor(private _el: ElementRef, private _renderer: Renderer) {
    }

    ngAfterViewInit() {
        const { nativeElement: inputElement } = this._el;

        this._renderer.invokeElementMethod(inputElement, 'focus', []);
    }

}
