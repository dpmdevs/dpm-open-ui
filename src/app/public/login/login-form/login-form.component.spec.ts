/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LoginFormComponent } from './login-form.component';
import { ReactiveFormsModule, AbstractControl } from '@angular/forms';
import { DpmButtonComponent } from '../../../shared/components/dpm-button/dpm-button.component';


describe('LoginFormComponent', () => {
    let component: LoginFormComponent;
    let fixture: ComponentFixture<LoginFormComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [LoginFormComponent, DpmButtonComponent],
            imports: [
                ReactiveFormsModule
            ],
            providers: []
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LoginFormComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create LoginFormComponent', () => {
        expect(component).toBeTruthy();
    });

    it('should have username empty', () => {

        const compiled = fixture.debugElement.nativeElement;

        const username = compiled.querySelector('input[id=username]').textContent;

        expect(username).toBe('');
    });

    it('should have password empty', () => {

        const compiled = fixture.debugElement.nativeElement;

        const password = compiled.querySelector('input[id=password]').textContent;

        expect(password).toBe('');
    });

    it('should have login form not valid when username is empty', () => {

        const usernameFormControl: AbstractControl = component.loginForm.get('username');

        expect(usernameFormControl.value).toBe('');
        expect(usernameFormControl.valid).toBeFalsy();
        expect(component.loginForm.valid).toBeFalsy();
    });

    it('should have login form not valid when password is empty', () => {

        const passwordFormControl: AbstractControl = component.loginForm.get('password');

        expect(passwordFormControl.value).toBe('');
        expect(passwordFormControl.valid).toBeFalsy();
        expect(component.loginForm.valid).toBeFalsy();
    });

    it('should be valid when valid username and password', () => {

        const loginForm = component.loginForm;

        loginForm.setValue({
            username: 'test-username',
            password: 'validPassword'
        });

        expect(component.loginForm.get('username').value).toBe('test-username');
        expect(component.loginForm.get('username').valid).toBeTruthy();

        expect(component.loginForm.get('password').value).toBe('validPassword');
        expect(component.loginForm.get('password').valid).toBeTruthy();

        expect(component.loginForm.valid).toBeTruthy();
    });
});
