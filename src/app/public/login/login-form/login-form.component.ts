/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Credentials } from '../../../core/auth/models/credentials';
import { DPMValidators } from '@dpm-helpers';


@Component({
    selector: 'dpm-login-form',
    templateUrl: 'login-form.component.html',
    styleUrls: ['login-form.component.scss']
})
export class LoginFormComponent implements OnInit {

    @Input() loading = false;
    @Output() submitLoginEvent = new EventEmitter<Credentials>();

    loginForm: FormGroup;

    constructor(private _formBuilder: FormBuilder) {
    }

    ngOnInit(): void {
        this.loginForm = this._formBuilder.group({
            username: ['', [DPMValidators.required, Validators.minLength(4)]],
            password: ['', [DPMValidators.required, Validators.minLength(4)]]
        });
    }

    submitLogin() {
        const credentials: Credentials = {
            username: this.loginForm.get('username').value,
            password: this.loginForm.get('password').value
        };

        this.submitLoginEvent.emit(credentials);
    }
}
