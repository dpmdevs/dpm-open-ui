/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { isNull } from 'util';

import { LoginFormComponent } from './login-form/login-form.component';
import { AuthService } from '../../core/auth/auth.service';
import { Credentials } from '../../core/auth/models/credentials';
import { StateService } from '../../state/state.service';
import { getLoginErrorMessages, getLoginLoading, LoginState } from '../../state/login/reducers';
import { LoginHandlerActions } from '../../state/login/actions/login-handler.action';
import { RendererProvider } from '@dpm-providers';


@Component({
    selector: 'dpm-login',
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.scss'],
    providers: [LoginFormComponent]
})
export class LoginComponent implements OnInit, OnDestroy {

    loginErrorMessages: string[] = [];
    loading$: Observable<boolean>;
    userHomepageSubscription: Subscription;

    constructor(private authService: AuthService,
                private router: Router,
                private stateService: StateService,
                private store: Store<LoginState>) {
    }

    ngOnInit() {
        this.loading$ = this.store.select(getLoginLoading);
        this.store.select(getLoginErrorMessages)
            .filter(error => !isNull(error))
            .subscribe((httpError: HttpErrorResponse) =>
                this.loginErrorMessages = RendererProvider.httpErrorResponseRenderer(httpError)
            );

        this.userHomepageSubscription = this.stateService.getUser()
            .map(user => user.homepage)
            .filter(homepage => homepage !== '')
            .subscribe(homepage => this.router.navigate([homepage]).then());
    }

    ngOnDestroy() {
        this.userHomepageSubscription.unsubscribe();
    }

    attemptLogin(credentials: Credentials) {
        this.store.dispatch(LoginHandlerActions.attemptLogin(credentials));
    }

}
