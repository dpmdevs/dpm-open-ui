/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { APP_BASE_HREF } from '@angular/common';
import { CoreModule } from './core/core.module';

describe('AppComponent', () => {
    beforeEach(() => {

        // TestBed creates an Angular testing module—an @NgModule class to produce the module environment for
        // the class you want to test

        TestBed.configureTestingModule({
            imports: [
                CoreModule,
                RouterTestingModule
            ],
            declarations: [
                AppComponent
            ],
            providers: [{ provide: APP_BASE_HREF, useValue: '/' }]
        });
        TestBed.compileComponents();
    });

    it('should create the app', async(() => {

        // the createComponent method returns a ComponentFixture, a handle on the test environment surrounding
        // the created component. The fixture provides access to the component instance itself and to the DebugElement,
        // which is a handle on the component's DOM element.

        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));

    // it('should render title in a h1 tag', async(() => {
    //     const fixture = TestBed.createComponent(AppComponent);
    //     fixture.detectChanges();
    //     const compiled = fixture.debugElement.nativeElement;
    //     expect(compiled.querySelector('h1').textContent).toContain('app works!');
    // }));
});

