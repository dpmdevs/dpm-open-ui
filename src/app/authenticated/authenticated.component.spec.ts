/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable } from 'rxjs/Observable';

import { User } from '@dpm-models';
import { AuthenticatedComponent } from './authenticated.component';
import { CoreModule } from '../core/core.module';
import { SideNavComponent } from './side-nav/side-nav.component';
import { TopNavComponent } from './top-nav/top-nav.component';
import { SharedModule } from '../shared/shared.module';
import { ConfigService } from './config/config.service';
import { UserService } from './user/user.service';
import { Config, initialConfigState } from '../state/config/config';
import { MenuComponent } from './side-nav/menu/menu.component';
import { UserStub } from '../shared/test-stubs/user.stub';
import { LoginComponent } from '../public/login/login.component';
import { LoginModule } from '../public/login/login.module';


const testConfig: Config = initialConfigState;

const userStub = UserStub.get();

const testConfig$: Observable<Config> = Observable.of(testConfig);

const testUser$: Observable<User> = Observable.of(userStub);

describe('AuthenticatedComponent', () => {
    let component: AuthenticatedComponent;
    let fixture: ComponentFixture<AuthenticatedComponent>;

    let userServiceSpy;
    let configServiceSpy;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AuthenticatedComponent, SideNavComponent, TopNavComponent, MenuComponent],
            imports: [
                CoreModule,
                SharedModule,
                LoginModule,
                RouterTestingModule.withRoutes([{ path: 'login', component: LoginComponent }])
            ],
            providers: [
                UserService,
                ConfigService
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AuthenticatedComponent);
        component = fixture.componentInstance;

// UserService actually injected into the component
        const userService = fixture.debugElement.injector.get(UserService);

        // Setup spy on the `getLoggedUser` method
        userServiceSpy = spyOn(userService, 'getLoggedUser')
            .and.returnValue(testUser$);

        const configService = fixture.debugElement.injector.get(ConfigService);

        // Setup spy on the `getQuote` method
        configServiceSpy = spyOn(configService, 'getUserConfig')
            .and.returnValue(testConfig$);
    });

    it('should create authenticated before on getUserFactory', () => {
        expect(component).toBeTruthy();
    });

    it('should have userStub data after on getUserFactory', async(() => {
        fixture.detectChanges();

        fixture.whenStable().then(() => {
            fixture.detectChanges();

            component.user$.do((user) => {
                expect(user).toBe(userStub);
            });
        });
    }));
});
