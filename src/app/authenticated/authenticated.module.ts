/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { NgModule } from '@angular/core';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TooltipModule } from 'ngx-bootstrap';

import { AuthenticatedRoutingModule } from './authenticated-routing.module';
import { SharedModule } from '../shared/shared.module';
import { UserModule } from './user/user.module';
import { AssetsModule } from './assets/assets.module';

import { AuthenticatedComponent } from './authenticated.component';
import { SideNavComponent } from './side-nav/side-nav.component';
import { TopNavComponent } from './top-nav/top-nav.component';
import { MenuComponent } from './side-nav/menu/menu.component';
import { MenuCollapsedComponent } from './side-nav/menu-collapsed/menu-collapsed.component';
import { SideMenuComponent } from './side-nav/menu-collapsed/side-menu/side-menu.component';
import { MenuSearchComponent } from './top-nav/menu-search/menu-search.component';


@NgModule({
    imports: [
        SharedModule,
        ModalModule.forRoot(),
        AuthenticatedRoutingModule,
        UserModule,
        AssetsModule,
        TooltipModule.forRoot(),
    ],
    declarations: [
        AuthenticatedComponent,
        SideNavComponent,
        MenuComponent,
        MenuCollapsedComponent,
        SideMenuComponent,
        TopNavComponent,
        MenuSearchComponent
    ],
    exports: []
})
export class AuthenticatedModule {
}
