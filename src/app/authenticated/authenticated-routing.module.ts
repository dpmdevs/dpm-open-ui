/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthenticatedComponent } from './authenticated.component';
import { AuthGuard, PermissionGuard } from '../core/auth';


const routes: Routes = [
    {
        path: '',
        component: AuthenticatedComponent,
        children: [
            {
                path: 'user',
                canActivate: [AuthGuard],
                loadChildren: 'app/authenticated/user/user.module#UserModule'
            },
            {
                path: 'dashboard',
                canActivate: [AuthGuard, PermissionGuard],
                loadChildren: 'app/authenticated/dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'p-activities',
                canActivate: [AuthGuard, PermissionGuard],
                loadChildren: 'app/authenticated/processing-activities/processing-activities.module#ProcessingActivitiesModule'
            },
            {
                path: 'assets',
                canActivate: [AuthGuard, PermissionGuard],
                loadChildren: 'app/authenticated/assets/assets.module#AssetsModule'
            },
            {
                path: 'subjects',
                canActivate: [AuthGuard, PermissionGuard],
                loadChildren: 'app/authenticated/subjects/subjects.module#SubjectsModule'
            },
            {
                path: 'config',
                canActivate: [AuthGuard, PermissionGuard],
                loadChildren: 'app/authenticated/config/config.module#ConfigModule'
            },
            {
                path: 'dpia',
                canActivate: [AuthGuard, PermissionGuard],
                loadChildren: 'app/authenticated/dpia-projects/dpia-projects.module#DpiaModule'
            },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AuthenticatedRoutingModule {
}
