/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { Tag } from '@dpm-models';
import { AnimationProvider } from '@dpm-providers';
import { StateService } from '../../../state/state.service';
import { Section } from '../../../state/config/section';
import { QueryParamsActions } from '../../../state/query-params/query-params.actions';


@Component({
    selector: 'dpm-menu-search',
    templateUrl: './menu-search.component.html',
    styleUrls: ['./menu-search.component.scss'],
    animations: [AnimationProvider.getSlideInFadeInRight()]
})
export class MenuSearchComponent {

    availableTagsSubscription: Subscription;
    availableTags: Tag[] = [];
    showingResults = false;
    keyword = '';
    placeholder = 'Ricerca menu';
    showMenuSearch = 'hide';
    lastKeyword = '';

    constructor(private stateService: StateService,
                private router: Router) {
        this.availableTagsSubscription = stateService.getSections()
            .map(this.flattenMenus)
            .map(this.excludePathlessSections)
            .map(this.mapMenuSectionsToTags)
            .map(this.sortAlphabetically)
            .subscribe(tags => this.availableTags = tags);
    }

    private excludePathlessSections(sections: Section[]): Section[] {
        return sections.filter(s => s.path !== 'assets' && s.path !== 'subjects');
    }

    private flattenMenus(sections: Section[]) {
        return sections
            .map(({ iconClass, children, path, name }) => {
                return children ? [{ name, path, iconClass }, ...children] : { name, path, iconClass };
            })
            .reduce((arr: any, cur: any | any[]) => {
                if (cur.length)
                    return [...arr, ...cur];
                return [...arr, cur];
            }, []);
    }

    private mapMenuSectionsToTags(sections: Section[]): Tag[] {
        return sections.map(({ name, path }) => ({ id: 0, name, title: path }));
    }

    private sortAlphabetically(tags: Tag[]): Tag[] {
        return tags.sort((a, b) => a.name.localeCompare(b.name));
    }

    toggleMenuSearchVisibility(): void {
        this.showMenuSearch = this.showMenuSearch === 'show' ? 'hide' : 'show';
    }

    onSearch(keyword: string): void {
        this.showingResults = true;

        if (keyword === this.lastKeyword)
            return;

        this.lastKeyword = keyword;
        this.availableTagsSubscription.unsubscribe();
        this.availableTagsSubscription = this.stateService.getSections()
            .map(this.flattenMenus)
            .map(this.mapMenuSectionsToTags)
            .map(menus => menus.filter(m => new RegExp(keyword, 'gi').test(m.name)))
            .subscribe(tags => this.availableTags = tags);
    }

    onSelectTag(tag: Tag): void {
        const availableTagsIds = this.availableTags.map(t => t.id);
        if (!tag || !availableTagsIds.includes(tag.id))
            return;

        this.stateService.dispatch(QueryParamsActions.resetQueryParams());
        const path = tag.title;
        this.router.navigate([path]).then();
        this.showingResults = false;
    }

}
