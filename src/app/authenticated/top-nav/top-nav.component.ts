/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { environment } from 'environments/environment';
import { DPMEventsService } from '../../core/dpm-events/dpm-events.service';
import { UserActions } from '../../state/user/user.actions';
import { StateService } from '../../state/state.service';


@Component({
    selector: 'dpm-top-nav',
    templateUrl: 'top-nav.component.html',
    styleUrls: ['top-nav.component.scss']
})
export class TopNavComponent implements OnInit {

    @Output() toggleEvent = new EventEmitter<void>();

    lastEvent$: Observable<number>;
    customerLogo: string;
    today: number;
    waitingForResponse = false;

    constructor(private _eventsService: DPMEventsService, private _stateService: StateService) {
        this.customerLogo = environment.customerLogo;
    }

    ngOnInit(): void {
        this.today = Date.now();
        this.lastEvent$ = this._eventsService.getLastEvent().map(event => event.timestamp * 1000);
    }

    doLogout(): void {
        this.waitingForResponse = true;

        this._stateService.dispatch(UserActions.logOut());
    }

    emitToggleSideNavbar(): void {
        this.toggleEvent.emit();
    }
}
