/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, ElementRef, Input, OnChanges, ViewChild } from '@angular/core';

import { AnimationProvider } from '@dpm-providers';
import { Section } from '../../../state/config/section';
import { Menu } from './menu.class';
import { MenuItem } from './menu-item.class';
import { StateService } from '../../../state/state.service';
import { QueryParamsActions } from '../../../state/query-params/query-params.actions';


@Component({
    selector: 'dpm-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss'],
    animations: [
        AnimationProvider.getSlideIn()
    ]
})
export class MenuComponent implements OnChanges {
    @ViewChild('menuElement') menuElementRef: ElementRef;
    @Input() sections: Section[];
    @Input() sideNavState: 'opened' | 'collapsed';

    menu: Menu;

    constructor(private stateService: StateService) {
    }

    ngOnChanges() {
        this.loadInitialMenu();
        this.selectSubMenuBasedOnLocation();
    }

    private selectSubMenuBasedOnLocation() {
        const hashArray = location.hash.split('/');
        if (hashArray.length > 2)
            this.selectMenuItem(this.menu.findByPath(hashArray[1]));
    }

    loadInitialMenu(): void {
        this.menu = new Menu(this.sections);
    }

    selectMenuItem(menuItem: MenuItem) {
        if (menuItem.children)
            this.menu = new Menu(menuItem.children);

        this.menu.setSelected(menuItem);
    }

    resetQueryParams(): void {
        this.stateService.dispatch(QueryParamsActions.resetQueryParams());
    }

}
