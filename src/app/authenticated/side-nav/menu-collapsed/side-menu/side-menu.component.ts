/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

import { AnimationProvider } from '@dpm-providers';
import { Section } from '../../../../state/config/section';
import { Menu } from '../../menu/menu.class';
import { MenuItem } from '../../menu/menu-item.class';


@Component({
    selector: 'dpm-side-menu',
    templateUrl: './side-menu.component.html',
    styleUrls: ['./side-menu.component.scss'],
    animations: [
        AnimationProvider.getToggleUpDown()
    ]
})
export class SideMenuComponent implements OnInit, OnChanges {

    @Input() sections: Section[];
    @Input() sectionPath: string;
    @Input() showSideMenu: boolean;

    sideMenu: Menu;
    sideMenuVisibility: string;

    constructor() {
        this.sideMenuVisibility = 'unSelected';
    }

    ngOnInit() {
        this.sideMenu = new Menu(this.sections);
    }

    ngOnChanges(changes: SimpleChanges) {
        this.sideMenuVisibility = changes.showSideMenu.currentValue ? 'selected' : 'unSelected';
    }

    selectSideMenu(menuItem: MenuItem) {
        this.sideMenu.setSelected(menuItem);
    }

}
