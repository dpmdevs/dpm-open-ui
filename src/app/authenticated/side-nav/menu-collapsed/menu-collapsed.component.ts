/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, Input, OnChanges } from '@angular/core';

import { Menu } from '../menu/menu.class';
import { AnimationProvider } from '@dpm-providers';
import { MenuItem } from '../menu/menu-item.class';
import { Section } from '../../../state/config/section';


@Component({
    selector: 'dpm-menu-collapsed',
    templateUrl: './menu-collapsed.component.html',
    styleUrls: ['../menu/menu.component.scss'],
    animations: [
        AnimationProvider.getSlideIn()
    ]
})
export class MenuCollapsedComponent implements OnChanges {

    @Input() sections: Section[];

    menu: Menu;

    ngOnChanges() {
        this.loadInitialMenu();
        this.selectSubMenuBasedOnLocation();
    }

    private selectSubMenuBasedOnLocation() {
        const hashArray = location.hash.split('/');
        if (hashArray.length > 2)
            this.selectMenuItem(this.menu.findByPath(hashArray[1]));
    }

    loadInitialMenu(): void {
        this.menu = new Menu(this.sections);
    }

    selectMenuItem(menuItem: MenuItem) {
        if (menuItem.children)
            this.menu = new Menu(menuItem.children);

        this.menu.setSelected(menuItem);
    }

}
