/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { User } from '@dpm-models';
import { StateService } from 'app/state/state.service';
import { Section } from '../../state/config/section';


@Component({
    selector: 'dpm-side-nav',
    templateUrl: 'side-nav.component.html',
    styleUrls: ['side-nav.component.scss']
})
export class SideNavComponent implements OnInit, OnChanges {

    @Input() sideNavState: 'opened' | 'collapsed';

    sections$: Observable<Section[]>;
    user: User;

    constructor(private _stateService: StateService) {
    }

    ngOnInit() {
        this.sections$ = this._stateService.getSections();

        this._stateService
            .getUser()
            .subscribe(user => this.user = user);
    }

    ngOnChanges(changes: SimpleChanges) {
        this.sideNavState = changes.sideNavState.currentValue;
    }
}
