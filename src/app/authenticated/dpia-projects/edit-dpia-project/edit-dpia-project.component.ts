/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnDestroy, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { Subscription } from 'rxjs/Subscription';
import * as moment from 'moment';

import { DpiaProject, StepState } from '@dpm-models';
import { AnimationProvider } from '@dpm-providers';
import { DpiaPreAssessmentActions } from '../../../state/dpia-projects/dpia-pre-assessment-questionnaire/dpia-pre-assessment.actions';
import { StateService } from '../../../state/state.service';
import { CurrentDpiaProjectIdActions } from '../../../state/dpia-projects/current-dpia-project-id/current-dpia-project-id.actions';
import { BasicDpiaDataActions } from '../../../state/dpia-projects/basic-dpia-data/basic-dpia-data.actions';
import { dpiaProjectStepsLabels, initialDpiaCompletionState } from '../dpia-steps';
import { DpiaProjectsService } from '../dpia-projects.service';
import { initialBasicDpiaDataState } from '../../../state/dpia-projects/basic-dpia-data/basic-dpia-data';
import { DpiaPrinciplesActions } from '../../../state/dpia-projects/dpia-principles/dpia-principles.actions';


@Component({
    selector: 'dpm-edit-dpia-project',
    templateUrl: './edit-dpia-project.component.html',
    styleUrls: ['./edit-dpia-project.component.scss'],
    animations: [AnimationProvider.getFadeIn()]
})
export class EditDpiaProjectComponent implements OnInit, OnDestroy {
    dpiaProjectName: string;
    routeSubscription: Subscription;
    currentDpiaProjectId: number;
    currentStepId = 'step-0';
    steps: StepState[] = initialDpiaCompletionState;
    dpiaProjectExistsOnServer = false;
    stepsLabels = dpiaProjectStepsLabels;

    constructor(private _route: ActivatedRoute,
                private _router: Router,
                private _stateService: StateService,
                private _dpiaProjectsService: DpiaProjectsService,
                private _toastr: ToastsManager,
                vcr: ViewContainerRef) {
        _toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit() {
        this.routeSubscription = this._route.params
            .subscribe({
                next: params => {
                    const id = +params.id;
                    this.currentStepId = params.stepId;
                    this.populateDpiaData(id);
                },
                error: () => {
                    this._router.navigate(['/dpia']).then();
                }
            });
    }

    ngOnDestroy() {
        this.routeSubscription.unsubscribe();
    }

    private populateDpiaData(id: number): void {
        this._dpiaProjectsService.getEntity(id)
            .subscribe(
                (data: DpiaProject) => {
                    this.currentDpiaProjectId = data.id;
                    this._stateService.dispatch(CurrentDpiaProjectIdActions.setId(data.id));
                    this.populateStoreWithRecordData(data);
                    this.dpiaProjectName = data.name;
                },
                () => this._router.navigate(['/dpia']).then()
            );
    }

    populateStoreWithRecordData(data): void {
        const {
            id, name, goal, status, reporter, processing_activity, proportionality_evaluation, review_date, notes,
            proportionality_reason
        } = data;
        const basicData = Object.assign({}, initialBasicDpiaDataState, {
            id,
            name,
            goal,
            status,
            reporter,
            proportionality_evaluation,
            processing_activity,
            user_id: reporter.id,
            processing_activity_id: processing_activity ? processing_activity.id : null,
            review_date: moment(review_date, 'YYYY-MM-DD'),
            notes,
            proportionality_reason,
        });
        this._stateService.dispatch(BasicDpiaDataActions.setBasicDpiaData(basicData));

        const answers = data.pre_assessment_answers.map(a => ({ question_id: a.id, answer: true }));
        this._stateService.dispatch(DpiaPreAssessmentActions.setPreAssessmentAnswers(answers));

        const principles = data.principles_answers.map(
            ({ id: question_id, pivot: { answer, description } }) => ({ question_id, answer, description }));
        this._stateService.dispatch(DpiaPrinciplesActions.setPrinciplesAnswers(principles));
    }

    onChangeStep(stepId: string, dpiaProjectId: number = this.currentDpiaProjectId): void {
        if (!this.currentDpiaProjectId) {
            this.currentDpiaProjectId = dpiaProjectId;
            this.dpiaProjectExistsOnServer = true;
        }

        this._router.navigate([`/dpia/edit/${this.currentDpiaProjectId}/${stepId}`]).then();
    }

    onWizardEnd(): void {
        this._router.navigate(['/dpia']).then();
    }

}
