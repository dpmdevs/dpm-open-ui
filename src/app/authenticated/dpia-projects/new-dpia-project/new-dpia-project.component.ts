/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnDestroy, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { ToastsManager } from 'ng2-toastr';

import { AnimationProvider } from '@dpm-providers';
import { StepState } from '@dpm-models';
import { StateService } from '../../../state/state.service';
import { dpiaProjectStepsLabels, initialDpiaCompletionState } from '../dpia-steps';


@Component({
    templateUrl: './new-dpia-project.component.html',
    styleUrls: ['./new-dpia-project.component.scss'],
    animations: [AnimationProvider.getFadeIn()],
})
export class NewDpiaProjectComponent implements OnInit, OnDestroy {

    subscription: Subscription;
    currentDpiaProjectId: number;
    currentStepId = 'step-0';
    steps: StepState[] = initialDpiaCompletionState;
    dpiaProjectExistsOnServer = false;
    stepsLabels = dpiaProjectStepsLabels;

    constructor(private _router: Router,
                private _toastr: ToastsManager,
                private _stateService: StateService,
                vcr: ViewContainerRef) {
        _toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit() {
        this.subscription = this._stateService.getCurrentDpiaProjectId()
            .subscribe(id => this.currentDpiaProjectId = id);
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    onChangeStep(stepId: string, dpiaProjectId: number = this.currentDpiaProjectId): void {
        if (!this.currentDpiaProjectId) {
            this.currentDpiaProjectId = dpiaProjectId;
            this.dpiaProjectExistsOnServer = true;
        }

        this.currentStepId = stepId;
    }

    onWizardEnd(): void {
        this._router.navigate(['/dpia']).then();
    }

}
