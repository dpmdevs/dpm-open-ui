/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { StepState } from '@dpm-models';

export const dpiaProjectStepsLabels = [
    'Creazione Progetto',
    'Pre-Assessment',
    'Valutazione Proporzionalità',
    'Diritti e principi fondamentali',
    'Valutazione Del Rischio',
    'Controllo e Manutenzione',
    // 'Report'
];

export const initialDpiaCompletionState: StepState[] = [
    { id: 'step-0', visited: false, completed: false },
    { id: 'step-1', visited: false, completed: false },
    { id: 'step-2', visited: false, completed: false },
    { id: 'step-3', visited: false, completed: false },
    { id: 'step-4', visited: false, completed: false },
    { id: 'step-5', visited: false, completed: false },
];
