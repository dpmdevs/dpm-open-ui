/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { isNullOrUndefined, isUndefined } from 'util';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subscription } from 'rxjs/Subscription';
import { ToastsManager } from 'ng2-toastr';

import { DpmTagCouplerHandler, DpmTagCouplerLabels, Tag } from '@dpm-models';
import { AnimationProvider } from '@dpm-providers';
import { DpmSelectOption } from '../../../../../shared/components/dpm-select/dpm-select.component';
import { controlMitigationOptions } from '../evaluation-selector-options';
import { Control } from '../control';
import { Risk } from '../risk';
import { RiskAssessmentService } from '../risk-assessment.service';


@Component({
    selector: 'dpm-risk-controls-list',
    templateUrl: './risk-controls-list.component.html',
    styleUrls: ['./risk-controls-list.component.scss'],
    animations: [AnimationProvider.getFadeIn()]
})
export class RiskControlsListComponent implements OnInit, OnDestroy, DpmTagCouplerHandler {

    @Input() selectedRisk$: BehaviorSubject<Risk>;
    @Input() riskControls$: Observable<Control[]>;
    @Output() openModal = new EventEmitter<TemplateRef<any>>();
    @Output() hideModal = new EventEmitter<any>();
    @Output() reloadRiskControls = new EventEmitter<any>();
    @Output() reloadCurrentRisks = new EventEmitter<any>();

    selectedRiskSubscription: Subscription;
    selectedRisk: Risk;
    selectedControl: Control;
    controlForm: FormGroup;

    selectedControlTag: Tag[] = [];

    get assignedTags$(): Observable<Tag[]> {
        return Observable.of(this.selectedControlTag);
    }

    availableTags$: Observable<Tag[]>;
    lastKeyword = '';
    tagCouplerLabels: DpmTagCouplerLabels = {
        header: 'Controllo selezionato',
        searchBoxTitle: 'Controlli disponibili',
        placeholder: 'Ricerca controlli',
        noTagsFallbackText: 'Nessun controllo selezionato'
    };

    constructor(_formBuilder: FormBuilder,
                private _riskAssessmentService: RiskAssessmentService,
                private _toastr: ToastsManager) {
        this.controlForm = _formBuilder.group({
            control_id: [0, Validators.required],
            identifiability: [0, Validators.required],
            prejudicial_effect: [0, Validators.required],
            vulnerability: [0, Validators.required],
            risk_sources_capabilities: [0, Validators.required]
        });
    }

    ngOnInit() {
        this.selectedRiskSubscription = this.selectedRisk$
            .distinctUntilChanged((p, q) => p && q && p.id === q.id)
            .subscribe(risk => {
                this.selectedRisk = risk;
                this.getTagCouplerControlsTags();
            });
    }

    ngOnDestroy() {
        this.selectedRiskSubscription.unsubscribe();
    }


    protected getTagCouplerControlsTags(): void {
        this.getAvailableControls();
    }

    protected getAvailableControls(keyword: string = this.lastKeyword): void {
        if (isNullOrUndefined(this.selectedRisk))
            return;

        this.availableTags$ = this._riskAssessmentService.getAvailableControls(this.selectedRisk.id, keyword)
            .map(this.mapControlsToTags);
    }

    mapControlsToTags(controls: Control[]): Tag[] {
        return controls.map(({ id, name, description: title }) => ({ id, name, title }));
    }


    selectControl(control: Control): void {
        this.selectedControl = control;
    }

    onSearchTag(keyword: string): void {
        this.lastKeyword = keyword;
        this.getTagCouplerControlsTags();
    }

    onToggleTag(tag: Tag): void {
        if (this.selectedControlTag.length === 0 || this.selectedControlTag[0].id !== tag.id) {
            this.selectedControlTag = [tag];
            this.controlForm.controls.control_id.setValue(tag.id);
            this.controlForm.markAsTouched();
        } else {
            this.selectedControlTag = [];
            this.controlForm.controls.control_id.reset();
        }
    }


    selectControlIdentifiability({ value }): void {
        this.controlForm.controls.identifiability.setValue(value);
        this.controlForm.markAsTouched();
        this.controlForm.markAsDirty();
    }

    selectControlPrejudicialEffect({ value }): void {
        this.controlForm.controls.prejudicial_effect.setValue(value);
        this.controlForm.markAsTouched();
        this.controlForm.markAsDirty();
    }

    selectControlVulnerability({ value }): void {
        this.controlForm.controls.vulnerability.setValue(value);
        this.controlForm.markAsTouched();
        this.controlForm.markAsDirty();
    }

    selectControlCapabilities({ value }): void {
        this.controlForm.controls.risk_sources_capabilities.setValue(value);
        this.controlForm.markAsTouched();
        this.controlForm.markAsDirty();
    }

    getControlIdentifiability(): DpmSelectOption {
        return isUndefined(this.controlForm.controls.identifiability.value)
            ? controlMitigationOptions[0]
            : controlMitigationOptions.find(v => v.value === this.controlForm.controls.identifiability.value);
    }

    getControlPrejudicialEffect(): DpmSelectOption {
        return isUndefined(this.controlForm.controls.prejudicial_effect.value)
            ? controlMitigationOptions[0]
            : controlMitigationOptions.find(v => v.value === this.controlForm.controls.prejudicial_effect.value);
    }

    getControlVulnerability(): DpmSelectOption {
        return isUndefined(this.controlForm.controls.vulnerability.value)
            ? controlMitigationOptions[0]
            : controlMitigationOptions.find(v => v.value === this.controlForm.controls.vulnerability.value);
    }

    getControlCapabilities(): DpmSelectOption {
        return isUndefined(this.controlForm.controls.risk_sources_capabilities.value)
            ? controlMitigationOptions[0]
            : controlMitigationOptions.find(v => v.value === this.controlForm.controls.risk_sources_capabilities.value);
    }

    get controlMitigationOptions() {
        return controlMitigationOptions;
    }


    populateControlForm(): void {
        const { id: control_id, pivot: { identifiability, prejudicial_effect, vulnerability, risk_sources_capabilities } } = this.selectedControl;

        this.controlForm.controls.control_id.setValue(control_id);
        this.controlForm.controls.identifiability.setValue(identifiability);
        this.controlForm.controls.prejudicial_effect.setValue(prejudicial_effect);
        this.controlForm.controls.vulnerability.setValue(vulnerability);
        this.controlForm.controls.risk_sources_capabilities.setValue(risk_sources_capabilities);
    }

    resetControlForm(): void {
        this.selectedControlTag = [];
        this.controlForm.reset({
            control_id: 0,
            identifiability: 0,
            prejudicial_effect: 0,
            vulnerability: 0,
            risk_sources_capabilities: 0
        });
    }

    deleteSelectedControl(): void {
        this._riskAssessmentService.deleteControl(this.selectedRisk.id, this.selectedControl.id)
            .subscribe(
                () => {
                    this._toastr.info('Controllo rimosso con successo').then();
                    this.reloadRiskControls.emit();
                    this.hideModal.emit();
                    this.reloadCurrentRisks.emit();
                }
            );
    }

    updateControl(): void {
        const control = Object.assign({}, this.controlForm.getRawValue(), { control_id: this.selectedControl.id });

        this._riskAssessmentService.updateControl(this.selectedRisk.id, control)
            .subscribe(() => {
                this._toastr.info('Controllo aggiornato con successo').then();
                this.reloadRiskControls.emit();
                this.hideModal.emit();
                this.reloadCurrentRisks.emit();
            });
    }

    saveControl(): void {
        const control = Object.assign({}, this.controlForm.getRawValue(), { control_id: this.selectedControlTag[0].id });
        this._riskAssessmentService.addControl(this.selectedRisk.id, control)
            .subscribe(() => {
                this._toastr.success('Controllo aggiunto con successo').then();
                this.reloadRiskControls.emit();
                this.hideModal.emit();
                this.resetControlForm();
                this.reloadCurrentRisks.emit();
            });
    }


    getBadgeClass(value: number) {
        return {
            '': value === 0,
            'badge-success': value > 0 && value < 3,
            'badge-warning': value === 3,
            'badge-danger': value > 3
        };
    }

}
