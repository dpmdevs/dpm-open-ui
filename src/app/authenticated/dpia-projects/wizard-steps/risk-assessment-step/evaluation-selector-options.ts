/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { DpmSelectOption } from '../../../../shared/components/dpm-select/dpm-select.component';


export const identifiabilityOptions: DpmSelectOption[] = [
    {
        value: 1,
        name: '1 - Quasi Impossibile',
        description: 'Identificare gli interessati utilizzando i dati sembra essere praticamente impossibile',
        customClasses: 'border-left-info'
    },
    {
        value: 2,
        name: '2 - Difficile',
        description: 'Identificare gli interessati utilizzando i dati sembra essere difficile ma è possibile in alcuni casi',
        customClasses: 'border-left-success'
    },
    {
        value: 3,
        name: '3 - Facile',
        description: 'Identificare gli interessati utilizzando i dati sembra essere relativamente facile',
        customClasses: 'border-left-warning'
    },
    {
        value: 4,
        name: '4 - Facilissimo',
        description: 'Identificare gli interessati utilizzando i dati sembra essere estremamente facile',
        customClasses: 'border-left-danger'
    },
];

export const prejudicialEffectOptions: DpmSelectOption[] = [
    {
        value: 1,
        name: '1 - Lieve',
        description: 'Gli interessati non saranno coinvolti o nella peggiore delle ipotesi potrebbero incontrare' +
        ' alcuni inconvenienti, che supereranno senza alcun problema',
        customClasses: 'border-left-info'
    },
    {
        value: 2,
        name: '2 - Medio',
        description: 'Gli interessati potrebbero incontrare degli inconvenienti, che saranno in grado di superare' +
        ' nonostante alcune difficoltà',
        customClasses: 'border-left-success'
    },
    {
        value: 3,
        name: '3 - Grave',
        description: 'Gli interessati potrebbero incontrare conseguenze significative, che dovrebbero essere in' +
        ' grado di far fronte seppur con gravi difficoltà',
        customClasses: 'border-left-warning'
    },
    {
        value: 4,
        name: '4 - Gravissimo',
        description: 'Gli interessati potrebbero confrontarsi con conseguenze significative, o addirittura' +
        ' irreversibili che non possono superare',
        customClasses: 'border-left-danger'
    },
];

export const vulnerabilityOptions: DpmSelectOption[] = [
    {
        value: 1,
        name: '1 - Improbabile',
        description: 'L’avverarsi della minaccia non sembra possibile',
        customClasses: 'border-left-info'
    },
    {
        value: 2,
        name: '2 - Poco Probabile',
        description: 'L’avverarsi della minaccia sembra difficile',
        customClasses: 'border-left-success'
    },
    {
        value: 3,
        name: '3 - Probabile',
        description: 'L’avverarsi della minaccia sembra possibile',
        customClasses: 'border-left-warning'
    },
    {
        value: 4,
        name: '4 - Altamente Probabile',
        description: 'L’avverarsi della minaccia sembra probabile',
        customClasses: 'border-left-danger'
    },
];

export const capabilitiesOptions: DpmSelectOption[] = [
    {
        value: 1,
        name: '1 - Improbabile',
        description: 'Le fonti di rischio non sembrano essere in grado di realizzare la minaccia',
        customClasses: 'border-left-info'
    },
    {
        value: 2,
        name: '2 - Poco Probabile',
        description: 'Le capacità delle fonti di rischio di realizzare la minaccia sono limitate',
        customClasses: 'border-left-success'
    },
    {
        value: 3,
        name: '3 - Probabile',
        description: 'Le capacità delle fonti di rischio di realizzare la minaccia sono reali e significative',
        customClasses: 'border-left-warning'
    },
    {
        value: 4,
        name: '4 - Altamente Probabile',
        description: 'Le capacità delle fonti di rischio di realizzare la minaccia sono illimitate',
        customClasses: 'border-left-danger'
    },
];

export const controlMitigationOptions: DpmSelectOption[] = [
    {
        name: '0 - Nessuno',
        value: 0,
        customClasses: 'border-left-info'
    },
    {
        name: '1 - Lieve',
        value: 1,
        customClasses: 'border-left-info'
    },
    {
        name: '2 - Medio',
        value: 2,
        customClasses: 'border-left-success'
    },
    {
        name: '3 - Significativo',
        value: 3,
        customClasses: 'border-left-warning'
    },
    {
        name: '4 - Massimo',
        value: 4,
        customClasses: 'border-left-danger'
    }
];
