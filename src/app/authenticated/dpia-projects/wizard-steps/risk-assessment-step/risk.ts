/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { BaseRecord } from '@dpm-models';
import { RiskSource } from './risk-source';
import { ProtectionCategory } from './protection-category';
import { ResidualRisk } from './residual-risk';


export interface Risk extends BaseRecord {
    name: string;
    description: string;
    acceptance_motivation; string;
    identifiability: number;
    prejudicial_effect: number;
    vulnerability: number;
    risk_sources_capabilities: number;
    severity: number;
    likelihood: number;
    risk_level: number;
    risk_sources: RiskSource[];
    protection_category: ProtectionCategory;
    residual_risk: ResidualRisk;
    protection_goals?: string;
}
