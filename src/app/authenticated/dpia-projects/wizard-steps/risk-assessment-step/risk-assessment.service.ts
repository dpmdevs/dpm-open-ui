/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { environment } from 'environments/environment';
import { TableBaseService } from '@dpm-components';
import { TableData } from '@dpm-models';
import { Risk } from './risk';
import { ProtectionCategory } from './protection-category';
import { RiskSource } from './risk-source';
import { Control } from './control';


export declare type ThreatLevel = 1 | 2 | 3 | 4;

export interface RiskRequest {
    id?: number;
    name: string;
    description?: string;
    dpia_project_id: number;
    protection_category_id: number;
    identifiability: ThreatLevel;
    prejudicial_effect: ThreatLevel;
    vulnerability: ThreatLevel;
    risk_sources_capabilities: ThreatLevel;
}

export interface ControlRequest {
    id?: number;
    name: string;
    description?: string;
    treatment_type?: string;
    control_id?: number;
    identifiability: ThreatLevel;
    prejudicial_effect: ThreatLevel;
    vulnerability: ThreatLevel;
    risk_sources_capabilities: ThreatLevel;
}

@Injectable()
export class RiskAssessmentService extends TableBaseService {

    get baseUrl(): string {
        return environment.apiUrl;
    }

    get table(): string {
        return '';
    }

    getRisksByProtectionCategory(dpiaId: number, pgId: number): Observable<Risk[]> {
        return this.http.get<Risk[]>(`${this.baseUrl}dpia/${dpiaId}/risks/${pgId}`);
    }

    getProtectionCategories(): Observable<ProtectionCategory[]> {
        return this.http.get<TableData>(`${this.baseUrl}protection-categories`)
            .map((tableData: TableData) => <ProtectionCategory[]>tableData.data);
    }

    getRiskSources(): Observable<RiskSource[]> {
        return this.http.get<TableData>(`${this.baseUrl}risk-sources`)
            .map((tableData: TableData) => <RiskSource[]>tableData.data);
    }

    addRisk(risk: RiskRequest): Observable<Response> {
        return this.http.post<Response>(`${this.baseUrl}risks`, risk);
    }

    deleteRisk(riskId: number): Observable<Response> {
        return this.http.delete<Response>(`${this.baseUrl}risks/${riskId}`, {});
    }

    updateRisk(risk: RiskRequest): Observable<Response> {
        return this.http.put<Response>(`${this.baseUrl}risks/${risk.id}`, risk);
    }

    updateAcceptance(risk: Risk, acceptance_motivation: string): Observable<Response> {
        return this.http.put<Response>(`${this.baseUrl}risks/${risk.id}/acceptance-motivation`, { acceptance_motivation });
    }

    getRiskControls(riskId: number): Observable<Control[]> {
        return this.http.get<Control[]>(`${this.baseUrl}risks/${riskId}/controls`);
    }

    getAvailableControls(riskId: number, keyword: string): Observable<Control[]> {
        const url = `${this.baseUrl}risks/${riskId}/available-risk-controls?sortField=name&sortDirection=asc&` + (
            keyword !== ''
                ? `searchCriteria=${ JSON.stringify([
                    { columnName: 'name', keyword }
                ]) }`
                : ''
        );

        return this.http.get<Control[]>(url);
    }

    addControl(riskId: number, control: ControlRequest): Observable<Response> {
        return this.http.post<Response>(`${this.baseUrl}risks/${riskId}/controls`, control);
    }

    deleteControl(riskId: number, controlId: number): Observable<Response> {
        return this.http.delete<Response>(`${this.baseUrl}risks/${riskId}/controls/${controlId}`);
    }

    updateControl(riskId: number, control: ControlRequest): Observable<Response> {
        return this.http.put<Response>(`${this.baseUrl}risks/${riskId}/controls/${control.control_id}`, control);
    }

}
