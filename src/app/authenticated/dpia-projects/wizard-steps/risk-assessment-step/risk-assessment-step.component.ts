/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnDestroy, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastsManager } from 'ng2-toastr';
import { isNull, isNullOrUndefined, isUndefined } from 'util';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/observable/forkJoin';
import { Subject } from 'rxjs/Subject';

import { BaseWizardStep } from '@dpm-components';
import { AnimationProvider } from '@dpm-providers';
import { DpmSelectOption } from '../../../../shared/components/dpm-select/dpm-select.component';
import { StateService } from '../../../../state/state.service';
import { RiskAssessmentService } from './risk-assessment.service';
import { Risk } from './risk';
import { ProtectionCategory } from './protection-category';
import { ResidualRisk } from './residual-risk';
import { RiskSource } from './risk-source';
import { Control } from './control';

import {
    capabilitiesOptions,
    identifiabilityOptions,
    prejudicialEffectOptions,
    vulnerabilityOptions
} from './evaluation-selector-options';
import { DPMValidators } from '@dpm-helpers';


@Component({
    selector: 'dpm-risk-assessment-step',
    templateUrl: './risk-assessment-step.component.html',
    styleUrls: ['../../../../shared/components/dpm-wizard/base-wizard-step.scss', './risk-assessment-step.component.scss'],
    animations: [AnimationProvider.getFadeIn(), AnimationProvider.getToggleUpDown()],
    providers: [RiskAssessmentService]
})
export class RiskAssessmentStepComponent extends BaseWizardStep implements OnDestroy {

    public capabilitiesOptions = capabilitiesOptions;
    public identifiabilityOptions = identifiabilityOptions;
    public prejudicialEffectOptions = prejudicialEffectOptions;
    public vulnerabilityOptions = vulnerabilityOptions;

    protected formConfig = {};
    modalRef: BsModalRef;
    riskForm: FormGroup;
    acceptanceForm: FormGroup;
    selectedRisk: Risk;
    selectedRisk$ = new BehaviorSubject<Risk>(null);
    acceptanceMotivationChanges$ = new Subject();
    currentRisks$: Observable<Risk[]>;
    riskSources$: Observable<RiskSource[]>;
    riskControls$: Observable<Control[]>;
    protectionCategories: ProtectionCategory[];
    riskSources: RiskSource[];
    currentDpiaProjectId: number;
    currentProtectionCategoryId: number;

    protectionGoals = ['integrità', 'riservatezza', 'disponibilità'];

    get toastr(): ToastsManager {
        return this._toastr;
    }

    constructor(protected _formBuilder: FormBuilder,
                private _riskAssessmentService: RiskAssessmentService,
                private _stateService: StateService,
                private _modalService: BsModalService,
                private _toastr: ToastsManager) {
        super(_formBuilder);
        this.riskSources$ = this._riskAssessmentService.getRiskSources();
        this.riskSources$.subscribe(rs => this.riskSources = rs);
        this.selectedRisk$.subscribe(r => this.selectedRisk = r);
    }

    ngOnInit() {
        super.ngOnInit();

        this.stateSubscriptions.push(
            this._stateService.getCurrentDpiaProjectId()
                .do(id => this.currentDpiaProjectId = id)
                .mergeMap(() => this._riskAssessmentService.getProtectionCategories())
                .subscribe((pCategories: ProtectionCategory[]) => {
                    this.protectionCategories = pCategories;

                    this.currentRisks$ = this._riskAssessmentService.getRisksByProtectionCategory(this.currentDpiaProjectId, pCategories[0].id)
                        .shareReplay(pCategories.length - 1);

                    this.currentProtectionCategoryId = pCategories[0].id;
                }),
            this.acceptanceMotivationChanges$
                .subscribe(this.saveAcceptance.bind(this))
        );

        this.riskForm = this._formBuilder.group({
            name: ['', [DPMValidators.required, Validators.maxLength(255)]],
            description: '',
            identifiability: [1, Validators.required],
            prejudicial_effect: [1, Validators.required],
            vulnerability: [1, Validators.required],
            risk_sources_capabilities: [1, Validators.required],
            risk_sources: [[], Validators.required],
            protection_goals: ['', DPMValidators.required]
        });

        this.acceptanceForm = this._formBuilder.group({
            acceptance_motivation: ['', DPMValidators.required]
        });

    }

    onNextStep() {
        this.nextStep.emit(this.currentDpiaProjectId);
    }

    resetSelectedRisk(): void {
        this.selectedRisk$.next(null);
    }

    resetRiskForm(): void {
        this.riskForm.reset({
            name: '',
            description: '',
            identifiability: 1,
            prejudicial_effect: 1,
            vulnerability: 1,
            risk_sources_capabilities: 1,
            risk_sources: [],
            protection_goals: '',
        });
    }


    selectProtectionCategory(event, protectionCategory: ProtectionCategory): void {
        if (!event.tabset)
            return;

        this.currentRisks$ = this._riskAssessmentService.getRisksByProtectionCategory(this.currentDpiaProjectId, protectionCategory.id)
            .shareReplay(this.protectionCategories.length - 1);
        this.currentProtectionCategoryId = protectionCategory.id;

        this.resetSelectedRisk();
    }

    selectRisk(risk: Risk): void {
        this.selectedRisk$.next(risk);
        this.reloadRiskControls();
        this.acceptanceForm.patchValue({ acceptance_motivation: risk.acceptance_motivation });
    }

    openModal(template: TemplateRef<any>): void {
        this.modalRef = this._modalService.show(template);
    }

    isRiskSourceSelected(sourceRiskId): boolean {
        return !isNull(this.riskForm.controls.risk_sources.value)
            && this.riskForm.controls.risk_sources.value.indexOf(sourceRiskId) !== -1;
    }

    selectRiskSource(sourceRiskId): void {
        const currentValue = this.riskForm.controls.risk_sources.value || [];

        const newValue = this.isRiskSourceSelected(sourceRiskId)
            ? currentValue.filter(id => id !== sourceRiskId)
            : [...currentValue, sourceRiskId];

        this.riskForm.controls.risk_sources.setValue(newValue);
        this.riskForm.markAsTouched();
        this.riskForm.markAsDirty();
    }


    isProtectionGoalOptionSelected(pgOption: string): boolean {
        return this.riskForm.controls.protection_goals.value.includes(pgOption);
    }

    toggleProtectionGoalOption(pgOption: string): void {
        const currentValue = this.riskForm.controls.protection_goals.value.split(',').filter(s => s !== '');

        const newValue = (this.isProtectionGoalOptionSelected(pgOption)
            ? currentValue.filter(v => v !== pgOption)
            : [...currentValue, pgOption]).join(',');

        this.riskForm.controls.protection_goals.setValue(newValue);
        this.riskForm.controls.protection_goals.markAsTouched();
        this.riskForm.controls.protection_goals.markAsDirty();
    }


    selectRiskIdentifiability({ value }): void {
        this.riskForm.controls.identifiability.setValue(value);
        this.riskForm.markAsTouched();
        this.riskForm.markAsDirty();
    }

    selectRiskPrejudicialEffect({ value }): void {
        this.riskForm.controls.prejudicial_effect.setValue(value);
        this.riskForm.markAsTouched();
        this.riskForm.markAsDirty();
    }

    selectRiskVulnerability({ value }): void {
        this.riskForm.controls.vulnerability.setValue(value);
        this.riskForm.markAsTouched();
        this.riskForm.markAsDirty();
    }

    selectRiskCapabilities({ value }): void {
        this.riskForm.controls.risk_sources_capabilities.setValue(value);
        this.riskForm.markAsTouched();
        this.riskForm.markAsDirty();
    }

    getRiskIdentifiability(): DpmSelectOption {
        return isUndefined(this.riskForm.controls.identifiability.value)
            ? identifiabilityOptions[0]
            : identifiabilityOptions.find(v => v.value === this.riskForm.controls.identifiability.value);
    }

    getRiskPrejudicialEffect(): DpmSelectOption {
        return isUndefined(this.riskForm.controls.prejudicial_effect.value)
            ? prejudicialEffectOptions[0]
            : prejudicialEffectOptions.find(v => v.value === this.riskForm.controls.prejudicial_effect.value);
    }

    getRiskVulnerability(): DpmSelectOption {
        return isUndefined(this.riskForm.controls.vulnerability.value)
            ? vulnerabilityOptions[0]
            : vulnerabilityOptions.find(v => v.value === this.riskForm.controls.vulnerability.value);
    }

    getRiskCapabilities(): DpmSelectOption {
        return isUndefined(this.riskForm.controls.risk_sources_capabilities.value)
            ? capabilitiesOptions[0]
            : capabilitiesOptions.find(v => v.value === this.riskForm.controls.risk_sources_capabilities.value);
    }


    populateRiskForm(): void {
        Object.keys(this.selectedRisk).forEach(key => {
            if (!this.riskForm.controls.hasOwnProperty(key))
                return;
            this.riskForm.controls[key].setValue(this.selectedRisk[key]);
        });
        this.riskForm.controls.risk_sources.setValue(this.selectedRisk.risk_sources.map(rs => rs.id));

        this.riskForm.controls.name.markAsTouched();
        this.riskForm.markAsDirty();
    }

    saveRisk(): void {
        const risk = Object.assign(
            {},
            this.riskForm.value,
            {
                dpia_project_id: this.currentDpiaProjectId,
                protection_category_id: this.currentProtectionCategoryId
            }
        );

        this._riskAssessmentService.addRisk(risk)
            .subscribe({
                next: () => {
                    this.resetValidationErrors();
                    this._toastr.success('Rischio inserito con successo').then();
                    this.modalRef.hide();
                    this.resetRiskForm();
                    this.reloadCurrentRisks();
                    this.resetSelectedRisk();
                },
                error: this.catchError.bind(this)
            });
    }

    deleteSelectedRisk(): void {
        this._riskAssessmentService.deleteRisk(this.selectedRisk.id)
            .subscribe(
                () => {
                    this._toastr.info('Rischio rimosso con successo').then();
                    this.modalRef.hide();
                    this.reloadCurrentRisks();
                    this.resetSelectedRisk();
                }
            );
    }

    updateRisk(): void {
        if (this.riskForm.invalid)
            return;

        const risk = Object.assign({}, this.riskForm.value, {
            id: this.selectedRisk.id,
            dpia_project_id: this.currentDpiaProjectId,
            protection_category_id: this.currentProtectionCategoryId
        });

        this._riskAssessmentService.updateRisk(risk)
            .subscribe({
                next: () => {
                    this.resetValidationErrors();
                    this._toastr.info('Rischio aggiornato con successo').then();
                    this.modalRef.hide();
                    this.resetRiskForm();
                    this.reloadCurrentRisks();
                    this.resetSelectedRisk();
                },
                error: this.catchError.bind(this)
            });
    }


    saveAcceptance() {
        if (this.acceptanceForm.invalid)
            return;

        this._riskAssessmentService.updateAcceptance(this.selectedRisk, this.acceptanceForm.controls.acceptance_motivation.value)
            .subscribe({
                next: () => {
                    this.resetValidationErrors();
                    this.reloadCurrentRisks();
                    this._toastr.success('Motivazione aggiornata con successo').then();
                },
                error: this.catchError.bind(this)
            });
    }

    private reloadCurrentRisks() {
        this.currentRisks$ = this._riskAssessmentService.getRisksByProtectionCategory(this.currentDpiaProjectId, this.currentProtectionCategoryId)
            .shareReplay(this.protectionCategories.length - 1)
            .do(risks => {
                if (!isNullOrUndefined(this.selectedRisk && risks.find(r => r.id === this.selectedRisk.id)))
                    this.selectRisk(risks.find(r => r.id === this.selectedRisk.id));
            });
    }

    private reloadRiskControls(): void {
        this.riskControls$ = this._riskAssessmentService.getRiskControls(this.selectedRisk.id)
            .shareReplay(this.protectionCategories.length - 1);
    }

    isLowRisk(residualRisk: ResidualRisk): boolean {
        return residualRisk.likelihood <= 2 && residualRisk.severity <= 2;
    }

    isMediumRisk(residualRisk: ResidualRisk): boolean {
        return residualRisk.risk_level < 9 && (residualRisk.likelihood >= 3 || residualRisk.severity >= 3);
    }

    isMaximumRisk(residualRisk: ResidualRisk): boolean {
        return residualRisk.risk_level >= 9;
    }

    getBadgeClass(value: number) {
        return {
            '': value === 0,
            'badge-success': value > 0 && value < 3,
            'badge-warning': value === 3,
            'badge-danger': value > 3
        };
    }

    getRiskLevelBadgeClass(value: number) {
        return {
            'badge-success': value <= 4,
            'badge-warning': value > 4 && value <= 9,
            'badge-danger': value > 9
        };
    }
}
