/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { BsDatepickerConfig, BsLocaleService } from 'ngx-bootstrap';
import { ToastsManager } from 'ng2-toastr';
import { isNullOrUndefined } from 'util';
import * as moment from 'moment';

import { BaseWizardStep } from '@dpm-components';
import { AnimationProvider } from '@dpm-providers';
import { StateService } from '../../../../state/state.service';
import { ReviewMaintenanceService } from './review-maintenance.service';


@Component({
    selector: 'dpm-review-maintenance-step',
    templateUrl: './review-maintenance-step.component.html',
    styleUrls: ['../../../../shared/components/dpm-wizard/base-wizard-step.scss', './review-maintenance-step.component.scss'],
    animations: [AnimationProvider.getFadeIn()],
    providers: [ReviewMaintenanceService]
})
export class ReviewMaintenanceStepComponent extends BaseWizardStep {

    @Output() wizardEnd = new EventEmitter();

    currentDpiaProjectId: number;
    formConfig = {
        review_date: [new Date(), Validators.required],
        status: ['', Validators.required],
        notes: ''
    };

    minDate: Date = new Date();

    get review_date(): Date {
        return this.stepForm.controls.review_date.value;
    }

    set review_date(date: Date) {
        this.stepForm.controls.review_date.setValue(date);
        this.stepForm.controls.review_date.markAsDirty();
        this.stepForm.controls.review_date.markAsTouched();
    }

    bsConfig: Partial<BsDatepickerConfig> = {
        containerClass: 'theme-blue'
    };

    get toastr(): ToastsManager {
        return this._toastr;
    }

    constructor(_formBuilder: FormBuilder,
                localeService: BsLocaleService,
                private _reviewMaintenanceService: ReviewMaintenanceService,
                private _stateService: StateService,
                private _toastr: ToastsManager) {
        super(_formBuilder);
        localeService.use('it');
    }

    ngOnInit() {
        super.ngOnInit();

        let s = this._stateService.getCurrentDpiaProjectId()
            .subscribe(id => this.currentDpiaProjectId = id);
        this.stateSubscriptions.push(s);

        s = this._stateService.getBasicDpiaData()
            .subscribe((this.populateReviewMaintenanceData).bind(this));
        this.stateSubscriptions.push(s);
    }

    private populateReviewMaintenanceData(data): void {
        const { review_date: newDate, status, notes } = data;

        if (isNullOrUndefined(newDate))
            this.review_date = moment().add(1, 'years').toDate();
        else
            this.review_date = moment(newDate).toDate();

        this.stepForm.controls.status.setValue(status);
        this.stepForm.controls.notes.setValue(notes);

        this.stepForm.controls.review_date.markAsTouched();
        this.stepForm.controls.status.markAsTouched();
        this.stepForm.markAsDirty();
    }

    updateReviewDate(date: Date): void {
        if (moment(this.review_date).isSame(moment(date)))
            return;

        const ymdIsoDate = moment(date).format('YYYY-MM-DD');
        this._reviewMaintenanceService.setReviewDate(this.currentDpiaProjectId, ymdIsoDate)
            .subscribe(
                () => this._toastr.info('Data aggiornata con successo').then(),
                this.catchError.bind(this)
            );
    }

    updateReviewStatus(): void {
        this._reviewMaintenanceService.setStatus(this.currentDpiaProjectId, this.stepForm.controls.status.value)
            .subscribe(
                () => this._toastr.info('Status aggiornato con successo').then(),
                this.catchError.bind(this)
            );
    }

    updateReviewNotes(): void {
        this._reviewMaintenanceService.setNotes(this.currentDpiaProjectId, this.stepForm.controls.notes.value)
            .subscribe(
                () => this._toastr.info('Note aggiornate con successo').then(),
                this.catchError.bind(this)
            );
    }

    onNextStep(): void {
    }

    onWizardEnd(): void {
        if (this.stepForm.invalid)
            return;

        this.wizardEnd.emit();
    }

}
