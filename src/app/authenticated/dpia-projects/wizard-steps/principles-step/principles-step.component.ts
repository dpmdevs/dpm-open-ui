/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, ElementRef, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ToastsManager } from 'ng2-toastr';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/distinctUntilChanged';
import { isNull } from 'util';

import { BaseWizardStep } from '@dpm-components';
import { AnimationProvider, RendererProvider } from '@dpm-providers';
import { ProcessingActivity, QuestionnaireAnswer, SecurityMeasureCategory } from '@dpm-models';
import { DpiaProjectsService } from '../../dpia-projects.service';
import { StateService } from '../../../../state/state.service';
import { DpiaPrinciplesActions } from '../../../../state/dpia-projects/dpia-principles/dpia-principles.actions';
import { ProcessingActivitiesService } from '../../../processing-activities/services/processing-activities.service';
import { SecurityMeasuresService } from '../../../assets/security-measures';
import { DpiaPrinciplesService } from './dpia-principles.service';


@Component({
    selector: 'dpm-principles-step',
    templateUrl: './principles-step.component.html',
    styleUrls: ['../../../../shared/components/dpm-wizard/base-wizard-step.scss', './principles-step.component.scss'],
    providers: [ProcessingActivitiesService, DpiaPrinciplesService],
    animations: [AnimationProvider.getFadeIn()],
})
export class PrinciplesStepComponent extends BaseWizardStep {

    @ViewChildren('questionDescription') questionDescriptions: QueryList<ElementRef>;

    public RendererProvider = RendererProvider;
    public currentProcessingActivity: ProcessingActivity;

    protected formConfig = {};
    subscriptions: Subscription[] = [];

    currentDpiaProjectId: number;
    dpiaDataSubjectsRightsLabels: { name: string; questions: { question: string, id: number; }[] };
    dpiaPrinciplesLabels: { name: string; questions: { title: string; question: string; id: number }[] };
    selectedQuestionsIds = [];
    savedDescriptions: { [id: number]: string; } = {};

    securityMeasuresCategories: SecurityMeasureCategory[] = [];

    get toastr(): ToastsManager {
        return this._toastr;
    }

    constructor(private dpiaService: DpiaProjectsService,
                private paService: ProcessingActivitiesService,
                private dpiaPrinciplesService: DpiaPrinciplesService,
                private securityMeasuresService: SecurityMeasuresService,
                private stateService: StateService,
                private _toastr: ToastsManager,
                formBuilder: FormBuilder) {
        super(formBuilder);
    }

    ngOnInit() {
        super.ngOnInit();
        this.stateSubscriptions.push(
            this.stateService.getCurrentDpiaProjectId()
                .subscribe(id => this.currentDpiaProjectId = id),

            this.stateService.getBasicDpiaData()
                .map(state => state.processing_activity_id)
                .filter(id => !isNull(id))
                .mergeMap(id => this.paService.getEntity(id))
                .subscribe((pa: ProcessingActivity) => this.currentProcessingActivity = pa),

            this.stateService.getDpiaPrinciplesAnswers()
                .subscribe(this.saveAnswers.bind(this))
        );
        this.subscriptions.push(
            Observable.forkJoin(
                this.dpiaPrinciplesService.getDpiaDataSubjectsRightsQuestions(),
                this.dpiaPrinciplesService.getDpiaPrinciplesQuestions()
            )
                .subscribe(data => {
                    this.dpiaDataSubjectsRightsLabels = data[0][0];
                    this.dpiaPrinciplesLabels = data[1][0];
                }),

            this.securityMeasuresService.getSecurityMeasuresCategories('org')
                .subscribe(categories => this.securityMeasuresCategories = categories),
        );
    }

    ngOnDestroy() {
        this.subscriptions.forEach(s => s.unsubscribe());
        this.subscriptions = [];
    }

    private saveAnswers(data: QuestionnaireAnswer[]): void {
        this.selectedQuestionsIds = data.filter(a => a.answer === true).map(a => a.question_id);
        data.forEach(a => {
            this.savedDescriptions[a.question_id] = a.description;
        });
    }


    onNextStep() {
        this.nextStep.emit();
    }

    isQuestionSelected(id): boolean {
        return this.selectedQuestionsIds.includes(id);
    }

    toggleQuestion(id): void {
        if (this.isQuestionSelected(id))
            this.selectedQuestionsIds = this.selectedQuestionsIds.filter(qId => qId !== id);
        else
            this.selectedQuestionsIds.push(id);
    }

    renderDataSourceTypes(dataSourceTypes): string {
        return RendererProvider.dataSourceTypesRenderer(dataSourceTypes.map(d => d.id));
    }

    getSecurityMeasuresForCategory(securityMeasures, category) {
        return securityMeasures.filter(sm => sm.security_measures_categories_id === category.id);
    }

    savePrinciplesAnswers(): void {
        const answers = [
            ...this.dpiaDataSubjectsRightsLabels.questions.map(q => q.id),
            ...this.dpiaPrinciplesLabels.questions.map(q => q.id)
        ].map(id => {
            const question_id = id;
            const answer = this.isQuestionSelected(id);
            const description = this.savedDescriptions[id];
            return { question_id, answer, description: description ? description : '' };
        });

        this.dpiaService.savePrinciples(this.currentDpiaProjectId, answers)
            .subscribe(() => {
                this.stateService.dispatch(DpiaPrinciplesActions.setPrinciplesAnswers(answers));
                this._toastr.info('Dati salvati correttamente').then();
            });
    }

}
