/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastsManager } from 'ng2-toastr';
import { isNull } from 'util';

import { DataCategory, DataType, ProcessingActivity, SecurityMeasureCategory } from '@dpm-models';
import { BaseWizardStep } from '@dpm-components';
import { AnimationProvider } from '@dpm-providers';
import { StateService } from '../../../../state/state.service';
import { ProcessingActivitiesService } from '../../../processing-activities/services/processing-activities.service';
import { SecurityMeasuresService } from '../../../assets/security-measures';
import { DpiaProjectsService } from '../../dpia-projects.service';


@Component({
    selector: 'dpm-pa-assets-review-step',
    templateUrl: './pa-assets-review-step.component.html',
    styleUrls: ['../../../../shared/components/dpm-wizard/base-wizard-step.scss'],
    animations: [AnimationProvider.getFadeIn(), AnimationProvider.getToggleUpDown()],
    providers: [ProcessingActivitiesService]
})
export class PaAssetsReviewStepComponent extends BaseWizardStep {

    protected formConfig = {
        isDataCorrect: false,
        proportionality_reason: ['', Validators.maxLength(255)],
    };
    protected securityMeasuresCategories: SecurityMeasureCategory[];
    processingActivity: ProcessingActivity;
    currentDpiaProjectId = 0;

    get toastr(): ToastsManager {
        return this._toastr;
    }

    get isDataCorrect(): boolean {
        return this.stepForm.get('isDataCorrect').value;
    }

    set isDataCorrect(value: boolean) {
        const control = this.stepForm.get('isDataCorrect');
        control.setValue(value);
        control.markAsTouched();
        control.markAsDirty();
    }

    constructor(protected _formBuilder: FormBuilder,
                private _paService: ProcessingActivitiesService,
                private _dpiaService: DpiaProjectsService,
                private _securityMeasuresService: SecurityMeasuresService,
                private _stateService: StateService,
                private _toastr: ToastsManager) {
        super(_formBuilder);

        _securityMeasuresService.getSecurityMeasuresCategories('org')
            .subscribe(categories => this.securityMeasuresCategories = categories);

        const s = this._stateService.getCurrentDpiaProjectId().subscribe(id => this.currentDpiaProjectId = id);
        this.stateSubscriptions.push(s);
    }

    ngOnInit() {
        super.ngOnInit();
        this.stateSubscriptions.push(
            this._stateService.getBasicDpiaData()
                .do(data => this.stepForm.controls.isDataCorrect.setValue(data.proportionality_evaluation))
                .do(data => this.stepForm.controls.proportionality_reason.setValue(data.proportionality_reason))
                .map(state => state.processing_activity_id)
                .filter(id => !isNull(id))
                .mergeMap(id => this._paService.getEntity(id))
                .subscribe((pa: ProcessingActivity) => this.processingActivity = pa)
        );
    }

    onNextStep() {
        if (this.stepForm.valid)
            this.nextStep.emit();
    }

    hasSelectedDataTypes(dataCategory: DataCategory): boolean {
        let result = false;
        const dataTypesIds = dataCategory.data_types.map(dt => dt.id);
        const selectedDataTypesIds = this.processingActivity.data_types.map(dt => dt.id);

        dataTypesIds.forEach(id => {
            if (selectedDataTypesIds.includes(id))
                result = true;
        });

        return result;
    }

    getSelectedDataTypes(dataCategory: DataCategory): DataType[] {
        const selectedDataTypesIds = this.processingActivity.data_types.map(dt => dt.id);
        return dataCategory.data_types.filter(dt => selectedDataTypesIds.includes(dt.id));
    }

    updateProportionalityEvaluation() {
        this._dpiaService.updateProportionalityEvaluation(this.currentDpiaProjectId, this.stepForm.controls.isDataCorrect.value)
            .subscribe({
                next: () => this._toastr.info('Aggiornata valutazione di proporzionalità'),
                error: this.catchError.bind(this)
            });
    }

    updateProportionalityReason() {
        this._dpiaService.updateProportionalityReason(this.currentDpiaProjectId, this.stepForm.controls.proportionality_reason.value)
            .subscribe({
                next: () => this._toastr.info('Aggiornata motivazione di proporzionalità'),
                error: this.catchError.bind(this)
            });
    }

}
