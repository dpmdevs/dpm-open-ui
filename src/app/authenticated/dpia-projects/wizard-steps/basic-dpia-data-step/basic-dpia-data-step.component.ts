/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { AfterContentInit, Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastsManager } from 'ng2-toastr';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/observable/of';
import { isNull, isNullOrUndefined, isNumber, isUndefined } from 'util';

import { AnimationProvider } from '@dpm-providers';
import { BaseWizardStep } from '@dpm-components';
import {
    DpmTagCouplerHandler, DpmTagCouplerLabels, ProcessingActivity, QueryParams, TableData, Tag
} from '@dpm-models';
import { StateService } from '../../../../state/state.service';
import { IBasicDpiaData } from '../../../../state/dpia-projects/basic-dpia-data/basic-dpia-data';
import { BasicDpiaDataActions } from '../../../../state/dpia-projects/basic-dpia-data/basic-dpia-data.actions';
import { CurrentDpiaProjectIdActions } from '../../../../state/dpia-projects/current-dpia-project-id/current-dpia-project-id.actions';
import { DpiaProjectsService } from '../../dpia-projects.service';
import { ProcessingActivitiesService } from '../../../processing-activities/services/processing-activities.service';
import { DPMValidators } from '@dpm-helpers';


@Component({
    selector: 'dpm-basic-dpia-data-step',
    templateUrl: './basic-dpia-data-step.component.html',
    styleUrls: ['../../../../shared/components/dpm-wizard/base-wizard-step.scss'],
    animations: [AnimationProvider.getFadeIn()],
    providers: [ProcessingActivitiesService]
})
export class BasicDpiaDataStepComponent extends BaseWizardStep implements AfterContentInit, DpmTagCouplerHandler {

    currentDpiaProjectId: number;
    assignedTags$: Observable<Tag[]>;
    availableTags$: Observable<Tag[]>;
    lastKeyword = '';
    tagCouplerLabels: DpmTagCouplerLabels = {
        header: '',
        searchBoxTitle: 'Trattamenti Selezionabili',
        placeholder: 'Ricerca Trattamenti',
        noTagsFallbackText: 'Nessun trattamento selezionato'
    };

    protected formConfig = {
        name: ['', [DPMValidators.required, Validators.minLength(3), Validators.maxLength(255)]],
        goal: ['', Validators.maxLength(255)],
        status: '',
        user_id: null,
        reporter: {},
        processing_activity_id: [null, Validators.required],
        processing_activity: {}
    };

    get toastr(): ToastsManager {
        return this._toastr;
    }

    constructor(protected _formBuilder: FormBuilder,
                private _paService: ProcessingActivitiesService,
                private _dpiaProjectService: DpiaProjectsService,
                private _stateService: StateService,
                private _toastr: ToastsManager) {
        super(_formBuilder);
    }

    ngOnInit() {
        super.ngOnInit();

        const s = this._stateService.getCurrentDpiaProjectId()
            .subscribe(id => this.currentDpiaProjectId = id);

        this.stateSubscriptions.push(s);

        this.getAvailableProcessingActivities();
    }

    ngAfterContentInit() {
        const s = this._stateService.getBasicDpiaData()
            .subscribe(this.populateForm.bind(this));

        this.stateSubscriptions.push(s);
    }

    private populateForm(formData: IBasicDpiaData): void {
        Object.keys(formData).forEach(key => {
            const formControl = this.stepForm.controls[key];
            const newValue = formData[key];

            if (!isUndefined(formControl) && formControl.value !== newValue) {
                formControl.setValue(newValue);
                formControl.markAsTouched();
            }
        });

        if (!isNullOrUndefined(formData.processing_activity)) {
            const { id, name, description: title } = formData.processing_activity;
            this.assignedTags$ = Observable.of([{ id, name, title }]);
        }
    }

    protected getAvailableProcessingActivities(keyword: string = this.lastKeyword): void {
        this.availableTags$ = this._paService.getTableData(<QueryParams>{
            searchOrCriteria: [
                { columnName: 'identification_code', keyword },
                { columnName: 'name', keyword },
                { columnName: 'description', keyword }
            ]
        })
            .map((tableData: TableData) => (<ProcessingActivity[]>tableData.data))
            .map(this.mapPAsToTags);
    }

    mapPAsToTags(processingActivities: ProcessingActivity[]): Tag[] {
        return processingActivities.map(({ id, name, description: title, identification_code }) =>
            ({ id, name: `${identification_code ? identification_code + ' - ' : ''}${name}`, title }));
    }

    onSearchTag(keyword: string): void {
        this.lastKeyword = keyword;
        this.getAvailableProcessingActivities();
    }

    onToggleTag(tag?: Tag): void {
        const paIdFormControl = this.stepForm.controls.processing_activity_id;

        if (!isNull(paIdFormControl.value) && paIdFormControl.value === tag.id) {
            paIdFormControl.setValue(null);
            this.assignedTags$ = Observable.of(null);
        } else {
            paIdFormControl.setValue(tag.id);
            this.assignedTags$ = Observable.of([tag]);
        }

        this.stepForm.markAsTouched();
        this.stepForm.markAsDirty();
    }

    private updateBasicDpiaData(basicDpiaData: IBasicDpiaData): Subscription {
        return this._dpiaProjectService.updateEntity(basicDpiaData)
            .subscribe({
                next: ((dpiaData: IBasicDpiaData) => {
                    this._stateService.dispatch(BasicDpiaDataActions.setBasicDpiaData(dpiaData));
                    this._toastr.info('Dati salvati con successo').then();
                    this.nextStep.emit();
                }).bind(this),
                error: this.catchError.bind(this)
            });
    }

    onNextStep(): void {
        if (this.stepForm.invalid)
            return;

        const dpiaProjectExistsOnServer = isNumber(this.currentDpiaProjectId);
        const rawFormValue = Object.assign({}, this.stepForm.getRawValue(), { id: this.currentDpiaProjectId });

        if (!dpiaProjectExistsOnServer)
            this.createDpiaProject(rawFormValue);
        else if (this.stepForm.dirty)
            this.updateBasicDpiaData(rawFormValue).add(this.nextStep.emit());
        else
            this.nextStep.emit();
    }

    private createDpiaProject(basicDpiaData): void {
        this._dpiaProjectService.addEntity<number>(basicDpiaData)
            .mergeMap(currentDpiaProjectId => {
                this._stateService.dispatch(CurrentDpiaProjectIdActions.setId(currentDpiaProjectId));
                this._stateService.dispatch(BasicDpiaDataActions.setBasicDpiaData(basicDpiaData));
                this._toastr.success('Progetto creato con successo').then();
                return this._dpiaProjectService.getEntity(currentDpiaProjectId);
            })
            .subscribe({
                next: (dpiaData: IBasicDpiaData) => {
                    this._stateService.dispatch(BasicDpiaDataActions.setBasicDpiaData(dpiaData));
                    this.nextStep.emit(dpiaData.id);
                },
                error: this.catchError.bind(this)
            });
    }
}
