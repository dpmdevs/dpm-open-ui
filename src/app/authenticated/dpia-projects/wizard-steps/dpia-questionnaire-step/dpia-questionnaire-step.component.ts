/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { FormBuilder } from '@angular/forms';
import { AfterContentInit, Component } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';
import { Subject } from 'rxjs/Subject';

import { BaseWizardStep } from '@dpm-components';
import { AnimationProvider } from '@dpm-providers';
import { QuestionnaireAnswer } from '@dpm-models';
import { DpiaQuestionnaireService } from './dpia-questionnaire.service';
import { DpiaPreAssessmentActions } from '../../../../state/dpia-projects/dpia-pre-assessment-questionnaire/dpia-pre-assessment.actions';
import { StateService } from '../../../../state/state.service';
import { DpiaProjectsService } from '../../dpia-projects.service';


@Component({
    selector: 'dpm-dpia-questionnaire-step',
    templateUrl: './dpia-questionnaire-step.component.html',
    styleUrls: ['../../../../shared/components/dpm-wizard/base-wizard-step.scss', './dpia-questionnaire-step.component.scss'],
    providers: [DpiaQuestionnaireService],
    animations: [AnimationProvider.getFadeIn()]
})
export class DpiaQuestionnaireStepComponent extends BaseWizardStep implements AfterContentInit {

    currentDpiaProjectId: number;
    formConfig = {
        dpiaPreAssessmentQuestionnaireAnswers: [[]]
    };
    questionnaireTitle: string;
    questions = [];

    get selectedQuestions() {
        return this.stepForm.controls.dpiaPreAssessmentQuestionnaireAnswers.value;
    }

    set selectedQuestions(value) {
        this.stepForm.controls.dpiaPreAssessmentQuestionnaireAnswers.setValue(value);
    }

    private _questionsSubject: Subject<any> = new Subject();

    get toastr(): ToastsManager {
        return this._toastr;
    }

    constructor(_formBuilder: FormBuilder,
                private dpiaProjectsService: DpiaProjectsService,
                private questionnaireService: DpiaQuestionnaireService,
                private stateService: StateService,
                private _toastr: ToastsManager) {
        super(_formBuilder);

        const s = stateService.getCurrentDpiaProjectId()
            .subscribe(id => this.currentDpiaProjectId = id);

        this.stateSubscriptions.push(s);

        questionnaireService.getTableData()
            .subscribe((data: any) => {
                this.questionnaireTitle = data[0].name;
                this.questions = data[0].questions.map(q => Object.assign({}, q, { question_id: q.id }));
            });

        this._questionsSubject
            .debounceTime(1000)
            .subscribe(() => {
                this.savePreAssessmentQuestionnaire();
            });
    }

    ngAfterContentInit() {
        const s = this.stateService.getDpiaPreAssessmentQuestionnaireAnswers()
            .subscribe(this.populateForm.bind(this));
        this.stateSubscriptions.push(s);
    }

    private populateForm(answers: QuestionnaireAnswer[]): void {
        this.selectedQuestions = answers;
    }

    private savePreAssessmentQuestionnaire(): void {
        const answers: QuestionnaireAnswer[] = this.selectedQuestions.map(sq => ({
            question_id: sq.question_id,
            answer: true
        }));

        this.dpiaProjectsService.savePreAssessmentQuestionnaire(this.currentDpiaProjectId, { answers })
            .subscribe(() => {
                this.stateService.dispatch(DpiaPreAssessmentActions.setPreAssessmentAnswers(answers));
                this._toastr.info('Pre-Assessment salvato correttamente').then();
            });
    }

    isSelected(question): boolean {
        return this.selectedQuestions.some(q => q.question_id === question.id);
    }

    toggleQuestion(question): void {
        if (this.isSelected(question))
            this.selectedQuestions = this.selectedQuestions.filter(q => q.question_id !== question.id);
        else
            this.selectedQuestions = [...this.selectedQuestions, question];

        this._questionsSubject.next();
    }

    onNextStep(): void {
        this.nextStep.emit(this.currentDpiaProjectId);
    }

}
