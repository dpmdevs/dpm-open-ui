/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { DpiaProjectsComponent } from './dpia-projects.component';
import { NewDpiaProjectComponent } from './new-dpia-project/new-dpia-project.component';
import { EditDpiaProjectComponent } from './edit-dpia-project/edit-dpia-project.component';


const routes: Routes = [
    {
        path: '',
        component: DpiaProjectsComponent
    },
    {
        path: 'new',
        component: NewDpiaProjectComponent
    },
    {
        path: 'edit/:id/:stepId',
        component: EditDpiaProjectComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DpiaRoutingModule {
}
