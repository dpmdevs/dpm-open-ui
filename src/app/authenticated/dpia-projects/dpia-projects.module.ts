/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { NgModule } from '@angular/core';
import { BsDatepickerModule, ModalModule, TabsModule, TooltipModule } from 'ngx-bootstrap';
import { ChartsModule } from 'ng2-charts';

import { SharedModule } from '../../shared/shared.module';
import { DPMCrudPageModule } from '../../shared/components/dpm-crud-page/dpm-crud-page.module';
import { DpiaRoutingModule } from './dpia-routing.module';
import { DpiaProjectsService } from './dpia-projects.service';
import { DpiaProjectsComponent } from './dpia-projects.component';
import { DpiaProjectDetailsComponent } from './dpia-project-details/dpia-project-details.component';
import { NewDpiaProjectComponent } from './new-dpia-project/new-dpia-project.component';
import { EditDpiaProjectComponent } from './edit-dpia-project/edit-dpia-project.component';
import { BasicDpiaDataStepComponent } from './wizard-steps/basic-dpia-data-step/basic-dpia-data-step.component';
import { DpiaQuestionnaireStepComponent } from './wizard-steps/dpia-questionnaire-step/dpia-questionnaire-step.component';
import { PaAssetsReviewStepComponent } from './wizard-steps/pa-assets-review-step/pa-assets-review-step.component';
import { RiskAssessmentStepComponent } from './wizard-steps/risk-assessment-step/risk-assessment-step.component';
import { ReviewMaintenanceStepComponent } from './wizard-steps/review-maintenance-step/review-maintenance-step.component';
import { RiskControlsListComponent } from './wizard-steps/risk-assessment-step/risk-controls-list/risk-controls-list.component';
import { PrinciplesStepComponent } from './wizard-steps/principles-step/principles-step.component';


@NgModule({
    imports: [
        SharedModule,
        DpiaRoutingModule,
        DPMCrudPageModule,
        TabsModule,
        ModalModule.forRoot(),
        TooltipModule.forRoot(),
        BsDatepickerModule.forRoot(),
        ChartsModule,
    ],
    declarations: [
        DpiaProjectsComponent,
        DpiaProjectDetailsComponent,
        NewDpiaProjectComponent,
        EditDpiaProjectComponent,
        BasicDpiaDataStepComponent,
        DpiaQuestionnaireStepComponent,
        PaAssetsReviewStepComponent,
        RiskAssessmentStepComponent,
        ReviewMaintenanceStepComponent,
        RiskControlsListComponent,
        PrinciplesStepComponent,
    ],
    entryComponents: [
        DpiaProjectDetailsComponent
    ],
    providers: [
        DpiaProjectsService
    ]
})
export class DpiaModule {
}
