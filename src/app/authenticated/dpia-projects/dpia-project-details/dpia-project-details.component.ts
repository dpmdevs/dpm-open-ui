/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap';
import { Subscription } from 'rxjs/Subscription';

import { BaseDetailsPage } from '@dpm-components';
import { AnimationProvider, RendererProvider } from '@dpm-providers';
import { DpiaProject } from '@dpm-models';
import { StateService } from '../../../state/state.service';


@Component({
    templateUrl: './dpia-project-details.component.html',
    styleUrls: ['../../../shared/components/dpm-crud-page/base-details-page/base-details-page.scss'],
    animations: [AnimationProvider.getFadeIn()]
})
export class DpiaProjectDetailsComponent extends BaseDetailsPage {

    public RendererProvider = RendererProvider;
    records: DpiaProject[];
    private activeTabSubscriptionForRoute: Subscription;

    get modalService(): BsModalService {
        return this._modalService;
    }

    constructor(private _router: Router,
                private _modalService: BsModalService,
                private _stateService: StateService) {
        super();
    }

    protected initValidationErrors(): void {
        this.validationMessages.errors = {
            username: [],
            firstname: [],
            lastname: [],
            email: [],
            password: [],
            fiscal_code: [],
            sex: [],
            dismissal_date: [],
        };
    }

    ngOnInit() {
        this.activeTabSubscriptionForRoute = this.activeTab$
            .filter(t => t === 'nuovo' || t === 'modifica')
            .do(tabName => {
                if (tabName === 'nuovo')
                    this._stateService.resetDpiaProjectWizardForms();

                const route = tabName === 'nuovo' ? 'new' : `edit/${this.records[0].id}/step-0`;
                this._router.navigate([`/dpia/${route}`]).then();
            })
            .subscribe();

        this.activeTabSubscription = this.activeTab$
            .filter(t => t !== 'nuovo' && t !== 'modifica')
            .subscribe(t => this.activeTab = t);
    }

    editRecord(): void {
        const selectedRecord = this.selectedRecords.getAllRecords()[0];
        this._router.navigate([`/dpia/edit/${selectedRecord.id}/step-0`]).then();
    }

}
