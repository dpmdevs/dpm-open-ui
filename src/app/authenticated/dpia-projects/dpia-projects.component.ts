/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';

import { BaseCrudPageComponent, GridDecorator, TableBaseService } from '@dpm-components';
import { DpiaProject } from '@dpm-models';
import { AnimationProvider, RendererProvider } from '@dpm-providers';
import { DpiaProjectDetailsComponent } from './dpia-project-details/dpia-project-details.component';
import { DpiaProjectsService } from './dpia-projects.service';
import { StateService } from '../../state/state.service';


@GridDecorator({
    title: 'Analisi d’impatto sulla protezione dei dati',
    description: 'Una DPIA consiste in una procedura finalizzata a descrivere il trattamento, valutarne necessità  e' +
    ' proporzionalità , e facilitare la gestione dei rischi per i diritti e le libertà  delle persone fisiche' +
    ' derivanti dal trattamento dei loro dati personali. In altri termini, la DPIA è una procedura che permette di' +
    ' realizzare e dimostrare la conformità con le norme.',
    headers: [
        { displayName: 'nome', columnName: 'name' },
        { displayName: 'obiettivo', columnName: 'goal' },
        {
            displayName: 'Trattamento',
            columnName: 'processing_activity',
            renderer: RendererProvider.processingActivitiesRenderer,
            sortable: false
        },
        { displayName: 'reporter', columnName: 'reporter', renderer: RendererProvider.userRenderer, sortable: false },
        { displayName: 'status', columnName: 'status', renderer: RendererProvider.dpiaStatusRenderer },
    ],
    paginationLimit: 10,

    hasDetailPage: true
})
@Component({
    templateUrl: '../../shared/components/dpm-crud-page/base-crud-page.html',
    styleUrls: ['../../shared/components/dpm-crud-page/base-crud-page.scss'],
    animations: [
        AnimationProvider.getFadeIn()
    ],
    providers: [DpiaProjectsService]
})
export class DpiaProjectsComponent extends BaseCrudPageComponent {

    records: DpiaProject[];

    constructor(private _dpiaProjectsService: DpiaProjectsService,
                private _stateService: StateService,
                protected _resolver: ComponentFactoryResolver,
                private _toaster: ToastsManager,
                private _viewContainerRef: ViewContainerRef) {
        super();
    }

    get defaultTab(): string {
        return 'dettagli';
    }

    get dataService(): TableBaseService {
        return this._dpiaProjectsService;
    }

    protected get stateService(): StateService {
        return this._stateService;
    }

    get resolver(): ComponentFactoryResolver {
        return this._resolver;
    }

    get viewChildComponent() {
        return DpiaProjectDetailsComponent;
    }

    get viewContainerRef() {
        return this._viewContainerRef;
    }

    get toastr() {
        return this._toaster;
    }

}
