/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { AfterViewInit, Component, HostListener, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs/Observable';

import { User } from '@dpm-models';
import { Config } from '../state/config/config';
import { CommonActions } from '../state/common/common.actions';
import { StateService } from '../state/state.service';
import { StateActions } from '../state/state.actions';
import { UserActions } from '../state/user/user.actions';
import { environment } from '../../environments/environment';


@Component({
    templateUrl: 'authenticated.component.html',
    styleUrls: ['authenticated.component.scss']
})
export class AuthenticatedComponent implements AfterViewInit {

    sideNavState: 'opened' | 'collapsed';
    user$: Observable<User>;
    isUserSignedIn$: Observable<boolean>;
    config$: Observable<Config>;
    showLoading: boolean;
    showLoading$: Observable<boolean>;
    timeoutCounter = 5;

    @ViewChild('sessionExpiredModal') sessionExpiredModal: ModalDirective;
    @ViewChild('serverErrorModal') serverErrorModal: ModalDirective;
    @ViewChild('clientErrorModal') clientErrorModal: ModalDirective;

    @HostListener('window:beforeunload') beforeunloadHandler() {
        if (environment.production) this.logoutOnUnload();
    }

    constructor(private _stateService: StateService) {
        this.sideNavState = 'opened';
        this.showLoading = false;
        this.showLoading$ = Observable.of(false);
    }

    ngAfterViewInit(): void {
        this.user$ = this._stateService.getUser();
        this.isUserSignedIn$ = this._stateService.isUserSignedIn();
        this.config$ = this._stateService.getConfig();

        // Register to session expired
        // Alert profile before log out caused by session expired
        this._stateService.isSessionExpired()
            .filter(isSessionExpired => isSessionExpired === true)
            // .do(() => localStorage.removeItem(AuthService.AUTH_TOKEN_NAME))
            .subscribe(() => {
                this.sessionExpiredModal.show();

                this.timeoutCounter = 5;

                setInterval(() => {
                    if (this.timeoutCounter > 1) this.timeoutCounter--;
                }, 1000);

                setTimeout(() => {
                    this.sessionExpiredModal.hide();
                    this._stateService.dispatch(StateActions.clearSession());
                }, 5000);
            });

        // register to server error
        this._stateService.hasServerError()
            .filter(error => error === true)
            .subscribe(() => this.serverErrorModal.show());

        this._stateService.hasClientError()
            .filter(error => error === true)
            .subscribe(() => this.clientErrorModal.show());
    }

    reloadPage(): void {
        location.reload();
    }

    unsetClientError(): void {
        this._stateService.dispatch(CommonActions.unsetClientError());
    }

    closeServerErrorModal(): void {
        this.serverErrorModal.hide();
        this.unsetServerError();
    }

    unsetServerError() {
        this._stateService.dispatch(CommonActions.unsetServerError());
    }

    toggleSideNavbar() {
        this.sideNavState = this.sideNavState === 'opened' ? 'collapsed' : 'opened';
    }

    logoutOnUnload() {
        this._stateService.dispatch(UserActions.logOut());
    }
}
