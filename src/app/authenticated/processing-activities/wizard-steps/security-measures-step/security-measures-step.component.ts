/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ToastsManager } from 'ng2-toastr';

import { AnimationProvider } from '@dpm-providers';
import { BaseWizardStep } from '@dpm-components';
import { SecurityMeasure } from '@dpm-models';
import { SecurityMeasuresService } from '../../../assets/security-measures';
import { StateService } from '../../../../state/state.service';
import { SecurityMeasuresActions } from '../../../../state/processing-activities/security-measures/security-measures.actions';
import { ProcessingActivitiesService } from '../../services/processing-activities.service';


@Component({
    selector: 'dpm-security-measures-step',
    templateUrl: './security-measures-step.component.html',
    styleUrls: ['../../../../shared/components/dpm-wizard/base-wizard-step.scss', './security-measures-step.component.scss'],
    animations: [AnimationProvider.getFadeIn(), AnimationProvider.getToggleUpDown()]
})
export class SecurityMeasuresStepComponent extends BaseWizardStep {

    protected formConfig = { security_measures: [[]] };

    securityMeasuresCategories: string[] = [];
    availableSecurityMeasures: SecurityMeasure[][] = [];
    selectedSecurityMeasuresIds: number[] = [];
    accordionsVisibility = [];

    get toastr(): ToastsManager {
        return this._toastr;
    }

    constructor(protected _formBuilder: FormBuilder,
                protected _securityMeasuresService: SecurityMeasuresService,
                private _stateService: StateService,
                private _PAService: ProcessingActivitiesService,
                protected _toastr: ToastsManager) {
        super(_formBuilder);

        _securityMeasuresService.getSecurityMeasuresCategoriesWithSecurityMeasures('org')
            .subscribe(([smCategories, securityMeasures]) => {
                this.securityMeasuresCategories = smCategories.map(c => c.name);
                this.availableSecurityMeasures = securityMeasures;
                if (this.accordionsVisibility.length !== smCategories.length) {
                    this.accordionsVisibility.length = smCategories.length;
                    this.accordionsVisibility.fill('unSelected');
                }
            });
    }

    ngOnInit() {
        super.ngOnInit();

        this._stateService.getSelectedSecurityMeasuresIds()
            .subscribe(ids => this.selectedSecurityMeasuresIds = [...ids]);
    }

    reloadSecurityMeasures(): void {
        this._PAService.getSelectedSecurityMeasuresIds()
            .map(sms => sms.map(sm => sm.id))
            .subscribe(ids => this._stateService.dispatch(SecurityMeasuresActions.setSecurityMeasures(ids)));
    }

    toggleSecurityMeasure(sm) {
        this._PAService.toggleSecurityMeasures([sm.id])
            .subscribe(() => {
                this.reloadSecurityMeasures();
                this._toastr.info('Modifiche salvate correttamente').then();
            });
    }

    isSelected({ id }): boolean {
        return this.selectedSecurityMeasuresIds.includes(id);
    }

    onNextStep(): void {
        this.nextStep.emit();
    }

    toggleAccordion(index): void {
        this.accordionsVisibility[index] = this.accordionsVisibility[index] === 'selected' ? 'unSelected' : 'selected';
    }

}
