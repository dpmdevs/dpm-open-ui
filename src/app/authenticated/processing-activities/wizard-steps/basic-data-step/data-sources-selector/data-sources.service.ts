/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { QueryParams, SearchCriteria, ThirdParties } from '@dpm-models';
import { BaseSelectorService } from '@dpm-components';
import { DataSourcesActions } from 'app/state/processing-activities/basic-data/data-sources/data-sources.actions';


@Injectable()
export class DataSourcesService extends BaseSelectorService<ThirdParties> {

    get table(): string {
        return 'third-parties';
    }

    getSelectedTags(): Observable<ThirdParties[]> {
        return this._stateService.getBasicData().map(state => state.data_sources);
    }

    addItem(dataSource: ThirdParties): void {
        this._stateService.dispatch(DataSourcesActions.addDataSource(dataSource));
    }

    removeItem(id: number): void {
        this._stateService.dispatch(DataSourcesActions.removeDataSource(id));
    }

    searchItems(keyword?: string): Observable<ThirdParties> {
        let searchResults;

        if (!keyword || keyword === '')
            searchResults = this.getTableData();
        else {
            const searchCriteria: SearchCriteria[] = [{ columnName: 'company_name', keyword }];
            searchResults = this.getTableData(<QueryParams>{ searchCriteria })
        }

        return searchResults.map(tableData => tableData.data);
    }

}
