/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component } from '@angular/core';

import { BaseSelector } from '@dpm-components';
import { AnimationProvider } from '@dpm-providers';
import { Tag, ThirdParties } from '@dpm-models';
import { DataSourcesService } from './data-sources.service';


@Component({
    selector: 'dpm-data-sources-selector',
    templateUrl: '../../../../../shared/components/dpm-base-selector/base-selector.html',
    providers: [DataSourcesService],
    animations: [AnimationProvider.getSlideInTop(), AnimationProvider.getFadeIn()]
})
export class DataSourcesSelectorComponent extends BaseSelector<ThirdParties> {

    labels = {
        selectedTags: null,
        placeholder: 'Ricerca terze parti',
        description: 'Indicare quale terza parte ha cominicato i dati oggetto del trattamento '
    };

    constructor(_dataSourcesService: DataSourcesService) {
        super(_dataSourcesService);
    }

    protected mapItemsToTags(results: ThirdParties[]): Tag[] {
        return results.map(({ id, company_name: name, company_name: title }) => ({ id, name, title }));
    }

}
