/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, EventEmitter, Output } from '@angular/core';

import { BaseSelector } from '@dpm-components';
import { LegalBasis, Tag } from '@dpm-models';
import { AnimationProvider } from '@dpm-providers';
import { LegalBasisService } from './legal-basis.service';


@Component({
    selector: 'dpm-legal-basis-selector',
    templateUrl: '../../../../../shared/components/dpm-base-selector/base-selector.html',
    providers: [LegalBasisService],
    animations: [AnimationProvider.getSlideInTop(), AnimationProvider.getFadeIn()]
})
export class LegalBasisSelectorComponent extends BaseSelector<LegalBasis> {
    @Output() selectionChange = new EventEmitter<LegalBasis[]>();

    labels = {
        selectedTags: null,
        placeholder: 'Liceità',
        description: 'Il trattamento è lecito se ricorre almeno una delle seguenti condizioni'
    };

    constructor(_legalBasisService: LegalBasisService) {
        super(_legalBasisService);
    }

    protected mapItemsToTags(legalBases: LegalBasis[]): Tag[] {
        return legalBases.map(({ id, name, description: title }: LegalBasis) => ({ id, name, title }));
    }
}
