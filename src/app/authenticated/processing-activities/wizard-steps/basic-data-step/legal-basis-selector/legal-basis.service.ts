/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { BaseSelectorService } from '@dpm-components';
import { LegalBasis, QueryParams, SearchCriteria } from '@dpm-models';
import { LegalBasisActions } from 'app/state/processing-activities/basic-data/legal-basis/legal-basis.actions';


@Injectable()
export class LegalBasisService extends BaseSelectorService<LegalBasis> {

    get table(): string {
        return 'legal-basis';
    }

    getSelectedTags(): Observable<LegalBasis[]> {
        return this._stateService.getBasicData()
            .map(state => state.legal_basis);
    }

    addItem(legalBasis: LegalBasis): void {
        this._stateService.dispatch(LegalBasisActions.addLegalBasis(legalBasis));
    }

    removeItem(id: number): void {
        this._stateService.dispatch(LegalBasisActions.removeLegalBasis(id));
    }

    searchItems(keyword?: string): Observable<LegalBasis> {
        let searchResults;

        if (!keyword || keyword === '')
            searchResults = this.getTableData(<QueryParams>{ sortField: 'name', sortDirection: 'asc' });
        else {
            const searchCriteria: SearchCriteria[] = [{ columnName: 'name', keyword }];
            searchResults = this.getTableData(<QueryParams>{ searchCriteria, sortField: 'name', sortDirection: 'asc' });
        }

        return searchResults.map(tableData => tableData.data);
    }
}
