/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { AfterContentInit, Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { isNumber } from 'util';

import { DataCategory, DataType, ProcessType, SecurityMeasure } from '@dpm-models';
import { AnimationProvider } from '@dpm-providers';
import { BaseWizardStep } from '@dpm-components';
import { DpmConstants, DPMValidators, SelectedRecords } from '@dpm-helpers';
import { StateService } from '../../../../state/state.service';
import { IBasicData } from '../../../../state/processing-activities/basic-data';
import { BasicDataActions } from '../../../../state/processing-activities/basic-data/basic-data.actions';
import { CurrentPaIdActions } from '../../../../state/processing-activities/current-pa-id/current-pa-id.actions';
import { ProcessingActivitiesService } from '../../services/processing-activities.service';
import { SecurityMeasuresActions } from '../../../../state/processing-activities/security-measures/security-measures.actions';
import { DataCategoriesService } from '../../../assets/data-categories';


@Component({
    selector: 'dpm-basic-data-step',
    templateUrl: './basic-data-step.component.html',
    styleUrls: ['../../../../shared/components/dpm-wizard/base-wizard-step.scss'],
    animations: [AnimationProvider.getFadeIn()]
})
export class BasicDataStepComponent extends BaseWizardStep implements AfterContentInit {

    readonly DATA_SOURCE_SUBJECT = DpmConstants.DATA_SOURCE_SUBJECT;
    readonly DATA_SOURCE_THIRD_PARTY = DpmConstants.DATA_SOURCE_THIRD_PARTY;
    readonly PROCESS_TYPE_PAPER = DpmConstants.PROCESS_TYPE_PAPER;
    readonly PROCESS_TYPE_IT = DpmConstants.PROCESS_TYPE_IT;

    routeSubscription: Subscription;
    currentProcessingActivityId: number;
    selectedDataCategories = new SelectedRecords<DataCategory>();
    selectedDataTypes = new SelectedRecords<DataType>();
    selectedDataCategoriesRecords: DataCategory[] = [];
    selectedDataTypesRecords: DataType[] = [];
    personalDataCategories: DataCategory[] = [];
    sensitiveDataCategories: DataCategory[] = [];
    isProcessingActivityBeingEdited = false;

    protected formConfig = {
        identification_code: ['', [Validators.maxLength(255)]],
        name: ['', [DPMValidators.required, Validators.minLength(3), Validators.maxLength(255)]],
        description: ['', [DPMValidators.required, Validators.maxLength(5000)]],
        process_types: [[], Validators.required],
        data_categories: [[], Validators.required],
        purpose: ['', Validators.maxLength(5000)],
        legal_references: ['', Validators.maxLength(5000)],
        legal_basis: [[]],
        data_sources: [[]],
        data_source_types: [[]],
        is_active: [true, Validators.required],
    };

    get is_active() {
        return this.stepForm.controls.is_active.value;
    }

    set is_active(value) {
        const control = this.stepForm.get('is_active');
        control.setValue(value);
        control.markAsTouched();
        control.markAsDirty();
    }

    get toastr(): ToastsManager {
        return this._toastr;
    }

    constructor(protected formBuilder: FormBuilder,
                private PAService: ProcessingActivitiesService,
                private dataCategoriesService: DataCategoriesService,
                private stateService: StateService,
                private route: ActivatedRoute,
                private _toastr: ToastsManager) {
        super(formBuilder);
        this.formChanges$
            .debounceTime(1000)
            .subscribe(this.saveDataToStore.bind(this));
    }

    ngOnInit() {
        super.ngOnInit();
        this.routeSubscription = this.route.url
            .subscribe(url => this.isProcessingActivityBeingEdited = url[0].path === 'edit');
        this.stateService.getCurrentProcessingActivityId()
            .subscribe(id => this.currentProcessingActivityId = id);
        Observable.forkJoin(
            this.dataCategoriesService.getPersonalDataCategories(),
            this.dataCategoriesService.getSensitiveDataCategories()
        )
            .subscribe(([personalDC, sensitiveDC]) => {
                this.personalDataCategories = <DataCategory[]>personalDC;
                this.sensitiveDataCategories = <DataCategory[]>sensitiveDC;
            });
    }

    ngAfterContentInit() {
        this.stateService.getBasicData()
            .subscribe(this.populateForm.bind(this));
    }

    ngOnDestroy() {
        this.routeSubscription.unsubscribe();
    }

    private populateForm(formData: IBasicData): void {
        this.resetStepForm();
        this.selectedDataCategories.removeAllRecords();
        this.selectedDataTypes.removeAllRecords();

        Object.keys(formData).forEach(key => {
            const formControl = this.stepForm.controls[key];
            const newValue = formData[key];

            if (formControl.value !== newValue) {
                formControl.setValue(newValue);
                formControl.markAsTouched();
            }
        });

        formData.data_categories.forEach((dataCategory: any) => {
            this.selectedDataCategories.addRecords(dataCategory);
            this.selectedDataTypes.addRecords(...dataCategory.data_types);
        });

        this.refreshSelectedDataCategoriesRecords();
        this.refreshSelectedDataTypesRecords();
    }

    refreshSelectedDataCategoriesRecords(): void {
        this.selectedDataCategoriesRecords = this.selectedDataCategories.getAllRecords();
    }

    refreshSelectedDataTypesRecords(): void {
        this.selectedDataTypesRecords = this.selectedDataTypes.getAllRecords();
    }

    private resetStepForm(): void {
        this.stepForm.reset({
            name: ['', [DPMValidators.required, Validators.minLength(3), Validators.maxLength(255)]],
            description: ['', [DPMValidators.required, Validators.maxLength(5000)]],
            process_types: [[], Validators.required],
            data_categories: [[], Validators.required],
            purpose: ['', Validators.maxLength(5000)],
            legal_references: ['', Validators.maxLength(5000)],
            legal_basis: [[]],
            data_sources: [[]],
            data_source_types: [[]],
            is_active: [true, Validators.required],
        });
    }

    saveDataToStore(): void {
        const basicData: IBasicData = this.stepForm.getRawValue();

        this.stateService.dispatch(BasicDataActions.setBasicData(basicData));

        if (isNumber(this.currentProcessingActivityId))
            this.updateBasicData(basicData);
    }

    onNextStep(): void {
        if (this.stepForm.invalid)
            return;

        const PAExistsOnServer = isNumber(this.currentProcessingActivityId);
        const basicData: IBasicData = this.stepForm.getRawValue();

        if (!PAExistsOnServer)
            this.createProcessingActivity(basicData);
        else if (this.stepForm.dirty)
            this.updateBasicData(basicData).add(() => this.nextStep.emit());
        else
            this.nextStep.emit();
    }

    private createProcessingActivity(basicData: IBasicData): void {
        const serverData = this.extractDataForServer(basicData);

        this.PAService.addEntity(serverData)
            .subscribe({
                next: (currentPAId: number) => {
                    this.resetValidationErrors();
                    this.stateService.dispatch(CurrentPaIdActions.setId(currentPAId));
                    this.stateService.dispatch(BasicDataActions.setBasicData(basicData));
                    this.toastr.success('Dati essenziali creati con successo').then();
                    this.loadSecurityMeasures();
                },
                error: this.catchError.bind(this)
            });
    }

    private updateBasicData(basicData: IBasicData): Subscription {
        const serverData = this.extractDataForServer(basicData);

        return this.PAService.saveBasicDataStep(serverData)
            .subscribe({
                next: () => {
                    this.resetValidationErrors();
                    this.stateService.dispatch(BasicDataActions.setBasicData(basicData));
                    this.toastr.info('Dati essenziali aggiornati con successo').then();
                },
                error: this.catchError.bind(this)
            });
    }

    private extractDataForServer(data) {
        const data_categories = data.data_categories.map(dc => dc.id);
        const data_types = data.data_categories
            .map(dc => dc.data_types)
            .reduce((acc, cur) => acc.concat(cur), [])
            .map(dt => dt.id);
        const process_types = data.process_types.map(pt => pt.id);
        const legal_basis = data.legal_basis.map(lb => lb.id);
        const data_sources = data.data_sources.map(ds => ds.id);
        const { data_source_types, is_active } = data;

        return Object.assign({}, data, {
            data_categories,
            data_types,
            process_types,
            legal_basis,
            data_sources,
            data_source_types,
            is_active,
        });
    }

    toggleDataSourceType(dataSourceType: 1 | 2): void {
        const dataSourceTypesControl = this.stepForm.controls.data_source_types;
        const isDataSourceTypeSelected = dataSourceTypesControl.value.some(t => t === dataSourceType);
        const newValue = isDataSourceTypeSelected
            ? dataSourceTypesControl.value.filter(t => t !== dataSourceType)
            : [...dataSourceTypesControl.value, dataSourceType];

        if (dataSourceTypesControl.value.includes(DpmConstants.DATA_SOURCE_THIRD_PARTY) && dataSourceType === DpmConstants.DATA_SOURCE_THIRD_PARTY) {
            this.stepForm.controls.data_sources.setValue([]);
            this.stepForm.controls.data_sources.markAsTouched();
            this.stepForm.controls.data_sources.markAsDirty();
        }

        dataSourceTypesControl.setValue(newValue);
        dataSourceTypesControl.markAsTouched();
        dataSourceTypesControl.markAsDirty();

        this.formChanges$.next();
    }

    isSubject(): boolean {
        return this.stepForm.controls.data_source_types.value.includes(DpmConstants.DATA_SOURCE_SUBJECT);
    }

    isThirdParty(): boolean {
        return this.stepForm.controls.data_source_types.value.includes(DpmConstants.DATA_SOURCE_THIRD_PARTY);
    }

    toggleProcessType(processType: ProcessType): void {
        const processTypes = this.stepForm.controls.process_types;
        const isProcessTypeSelected = processTypes.value.some(pType => pType.id === processType.id);
        const newValue = isProcessTypeSelected
            ? processTypes.value.filter(pType => pType.id !== processType.id)
            : [...processTypes.value, processType];

        processTypes.setValue(newValue);
        processTypes.markAsTouched();
        processTypes.markAsDirty();

        this.formChanges$.next();
    }

    isProcessTypePaper(): boolean {
        return this.stepForm.controls.process_types.value.filter(pt => pt.id === this.PROCESS_TYPE_PAPER.id).length === 1;
    }

    isProcessTypeIT(): boolean {
        return this.stepForm.controls.process_types.value.filter(pt => pt.id === this.PROCESS_TYPE_IT.id).length === 1;
    }

    onDataCategoryToggled(dataCategory: DataCategory): void {
        const dataCategoriesFormControl = this.stepForm.controls.data_categories;
        const currentDataCategories = dataCategoriesFormControl.value;
        const currentDataCategoriesIds = dataCategoriesFormControl.value.map(dc => dc.id);

        let newValue;
        if (currentDataCategoriesIds.includes(dataCategory.id)) {
            newValue = currentDataCategories.filter(dc => dc.id !== dataCategory.id);
            this.selectedDataTypes.removeRecords(...dataCategory.data_types);
            dataCategory.data_types = [];
        } else
            newValue = [...currentDataCategories, dataCategory];

        dataCategoriesFormControl.setValue(newValue);
        dataCategoriesFormControl.markAsTouched();
        dataCategoriesFormControl.markAsDirty();

        this.selectedDataCategories.toggleRecord(dataCategory);
        this.refreshSelectedDataTypesRecords();
        this.refreshSelectedDataCategoriesRecords();

        this.formChanges$.next();
    }

    onDataTypeToggled([dataType, parentDataCategory]): void {
        const dataCategoriesFormControl = this.stepForm.controls.data_categories;
        const parentDCIndex = dataCategoriesFormControl.value.findIndex(dc => dc.id === parentDataCategory.id);
        const currentParentDC = Object.assign({}, dataCategoriesFormControl.value[parentDCIndex]);

        currentParentDC.data_types = currentParentDC.data_types.map(dt => dt.id).includes(dataType.id)
            ? currentParentDC.data_types.filter(dt => dt.id !== dataType.id)
            : [...currentParentDC.data_types, dataType];

        const newDataCategories = [...dataCategoriesFormControl.value];
        newDataCategories[parentDCIndex] = currentParentDC;

        dataCategoriesFormControl.setValue(newDataCategories);
        dataCategoriesFormControl.markAsTouched();
        dataCategoriesFormControl.markAsDirty();

        this.selectedDataTypes.toggleRecord(dataType);
        this.refreshSelectedDataTypesRecords();

        this.formChanges$.next();
    }

    private loadSecurityMeasures() {
        this.PAService.getSelectedSecurityMeasuresIds().subscribe((selectedSecurityMeasures: SecurityMeasure[]) => {

            const selectedSecurityMeasureIds = selectedSecurityMeasures.map(s => s.id);

            this.stateService.dispatch(SecurityMeasuresActions.setSecurityMeasures(selectedSecurityMeasureIds));
        });
    }

}
