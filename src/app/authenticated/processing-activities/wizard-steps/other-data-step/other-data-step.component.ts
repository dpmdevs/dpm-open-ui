/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';

import { AnimationProvider } from '@dpm-providers';
import { BaseWizardStep } from '@dpm-components';
import { ProcessingActivitiesService } from '../../services/processing-activities.service';
import { StateService } from '../../../../state/state.service';
import { OtherDataActions } from '../../../../state/processing-activities/other-data/other-data.actions';


@Component({
    selector: 'dpm-other-data-step',
    templateUrl: './other-data-step.component.html',
    styleUrls: ['../../../../shared/components/dpm-wizard/base-wizard-step.scss'],
    animations: [AnimationProvider.getFadeIn()]
})
export class OtherDataStepComponent extends BaseWizardStep {

    @Output() wizardEnd = new EventEmitter();

    protected formConfig = {
        retentions: [[]],
        applications: [[]],
        risk_estimate: 0,
        notes: ['', Validators.maxLength(5000)],
        retention_description: ['', Validators.maxLength(5000)],
        is_process_automated: false,
        has_profiling_activity: false,
        automated_process_description: ['', Validators.maxLength(255)]
    };

    get toastr(): ToastsManager {
        return this._toastr;
    }

    constructor(protected _formBuilder: FormBuilder,
                private processingActivitiesService: ProcessingActivitiesService,
                private stateService: StateService,
                private router: Router,
                private _toastr: ToastsManager) {
        super(_formBuilder);
        this.formChanges$
            .debounceTime(1000)
            .subscribe(this.saveOtherData.bind(this));
    }

    ngOnInit() {
        super.ngOnInit();
        this.stateService.getOtherData()
            .subscribe(this.populateFormWithServerData.bind(this));
    }

    private populateFormWithServerData(data) {
        Object.keys(data).forEach(key => {
            const formControl = this.stepForm.controls[key];
            const value = formControl.value;
            const newValue = data[key];
            if (newValue !== value) {
                formControl.setValue(newValue);
                formControl.markAsDirty();
                formControl.markAsTouched();
            }
        });
    }

    get is_process_automated(): boolean {
        return this.stepForm.get('is_process_automated').value;
    }

    set is_process_automated(value: boolean) {
        const control = this.stepForm.get('is_process_automated');
        control.setValue(value);
        control.markAsTouched();
        control.markAsDirty();
    }

    get has_profiling_activity(): boolean {
        return this.stepForm.get('has_profiling_activity').value;
    }

    set has_profiling_activity(value: boolean) {
        const control = this.stepForm.get('has_profiling_activity');
        control.setValue(value);
        control.markAsTouched();
        control.markAsDirty();
    }

    onNextStep(): void {
    }

    onWizardEnd(): void {
        this.saveOtherData();
        if (this.stepForm.valid)
            this.wizardEnd.emit();
    }

    saveOtherData(): void {
        const rawFormData = this.stepForm.getRawValue();
        const serverData = this.extractDataForServer(rawFormData);
        this.processingActivitiesService.saveOtherDataStep(serverData)
            .subscribe({
                next: () => {
                    this.resetValidationErrors();
                    this.stateService.dispatch(OtherDataActions.setOtherData(rawFormData));
                    this._toastr.success('Informazioni salvate con successo').then();
                },
                error: this.catchError.bind(this)
            });
    }

    saveAndNavigateToDpia(): void {
        const rawFormData = this.stepForm.getRawValue();
        const serverData = this.extractDataForServer(rawFormData);
        this.processingActivitiesService.saveOtherDataStep(serverData)
            .subscribe({
                next: () => {
                    this.stateService.dispatch(OtherDataActions.setOtherData(rawFormData));
                    this._toastr.success('Informazioni salvate con successo').then();
                    this.router.navigate(['/dpia']).then();
                },
                error: this.catchError.bind(this)
            });
    }

    private extractDataForServer(data) {
        const retentions = data.retentions.map(r => r.id);
        const applications = data.applications.map(a => a.id);
        const { risk_estimate, is_process_automated, has_profiling_activity, automated_process_description, notes, retention_description } = data;

        return Object.assign({}, {
            retentions,
            applications,
            risk_estimate,
            is_process_automated,
            has_profiling_activity,
            automated_process_description,
            notes,
            retention_description
        });
    }

}
