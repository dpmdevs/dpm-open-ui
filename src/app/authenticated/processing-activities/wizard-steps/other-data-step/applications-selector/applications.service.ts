/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { BaseSelectorService } from '../../../../../shared/components/dpm-base-selector/base-selector.service';
import { ApplicationsActions } from 'app/state/processing-activities/other-data/applications/applications.actions';
import { Application, QueryParams, SearchCriteria } from '../../../../../shared/models';


@Injectable()
export class ApplicationsService extends BaseSelectorService<Application> {
    get table(): string {
        return 'applications';
    }

    getSelectedTags(): Observable<Application[]> {
        return this._stateService.getOtherData()
            .map(state => state.applications);
    }

    addItem(application: Application): void {
        this._stateService.dispatch(ApplicationsActions.addApplication(application));
    }

    removeItem(id: number): void {
        this._stateService.dispatch(ApplicationsActions.removeApplication(id));
    }

    searchItems(keyword?: string): Observable<Application> {
        let searchResults;

        if (!keyword || keyword === '')
            searchResults = this.getTableData(<QueryParams>{ sortField: 'name', sortDirection: 'asc' });
        else {
            const searchCriteria: SearchCriteria[] = [{ columnName: 'name', keyword }];
            searchResults = this.getTableData(<QueryParams>{ searchCriteria, sortField: 'name', sortDirection: 'asc' });
        }

        return searchResults.map(tableData => tableData.data);
    }
}
