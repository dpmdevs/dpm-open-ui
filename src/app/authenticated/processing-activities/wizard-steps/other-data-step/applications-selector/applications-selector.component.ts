/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component } from '@angular/core';

import { BaseSelector } from '../../../../../shared/components';
import { AnimationProvider } from '../../../../../shared/providers';
import { Application, Tag } from '../../../../../shared/models';
import { ApplicationsService } from './applications.service';


@Component({
    selector: 'dpm-applications-selector',
    templateUrl: '../../../../../shared/components/dpm-base-selector/base-selector.html',
    providers: [ApplicationsService],
    animations: [AnimationProvider.getSlideInTop(), AnimationProvider.getFadeIn()]
})
export class ApplicationsSelectorComponent extends BaseSelector<Application> {
    labels = {
        selectedTags: 'Applicativi',
        placeholder: 'Ricerca applicativi',
        description: 'I software per mezzo dei quali viene svolto il trattamento'
    };

    constructor(_applicationsService: ApplicationsService) {
        super(_applicationsService);
    }

    protected mapItemsToTags(results: Application[]): Tag[] {
        return results.map(({ id, name, description: title }: Application) => ({ id, name, title }));
    }

}
