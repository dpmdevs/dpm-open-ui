/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { BaseSelectorService } from '../../../../../shared/components/dpm-base-selector/base-selector.service';
import { QueryParams, Retention, SearchCriteria } from '../../../../../shared/models';
import { RetentionsActions } from 'app/state/processing-activities/other-data/retentions/retentions.actions';


@Injectable()
export class RetentionsService extends BaseSelectorService<Retention> {

    get table(): string {
        return 'retentions';
    }

    getSelectedTags(): Observable<Retention[]> {
        return this._stateService.getOtherData()
            .map(state => state.retentions);
    }

    addItem(retention: Retention): void {
        this._stateService.dispatch(RetentionsActions.addRetention(retention));
    }

    removeItem(id: number): void {
        this._stateService.dispatch(RetentionsActions.removeRetention(id));
    }

    searchItems(keyword?: string): Observable<Retention> {
        let searchResults;

        if (!keyword || keyword === '')
            searchResults = this.getTableData(<QueryParams>{ sortField: 'name', sortDirection: 'asc' });
        else {
            const searchCriteria: SearchCriteria[] = [{ columnName: 'name', keyword }];
            searchResults = this.getTableData(<QueryParams>{ searchCriteria, sortField: 'name', sortDirection: 'asc' });
        }

        return searchResults.map(tableData => tableData.data);
    }
}
