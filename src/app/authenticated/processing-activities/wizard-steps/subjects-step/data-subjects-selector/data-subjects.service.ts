/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { DataSubject, QueryParams, SearchCriteria } from '@dpm-models';
import { BaseSelectorService } from '../../../../../shared/components/dpm-base-selector/base-selector.service';
import { DataSubjectsActions } from '../../../../../state/processing-activities/subjects/data-subjects/data-subjects.actions';


@Injectable()
export class DataSubjectsService extends BaseSelectorService<DataSubject> {

    get table(): string {
        return 'data-subjects';
    }

    getSelectedTags(): Observable<DataSubject[]> {
        return this._stateService.getSubjects().map(state => state.data_subjects);
    }

    addItem(dataSubject: DataSubject): void {
        this._stateService.dispatch(DataSubjectsActions.addDataSubject(dataSubject));
    }

    removeItem(id: number): void {
        this._stateService.dispatch(DataSubjectsActions.removeDataSubject(id));
    }

    searchItems(keyword?: string): Observable<DataSubject> {
        let searchResults;

        if (!keyword || keyword === '')
            searchResults = this.getTableData(<QueryParams>{ sortField: 'name', sortDirection: 'asc' });
        else {
            const searchCriteria: SearchCriteria[] = [{ columnName: 'name', keyword }];
            searchResults = this.getTableData(<QueryParams>{ searchCriteria, sortField: 'name', sortDirection: 'asc' })
        }

        return searchResults.map(tableData => tableData.data);
    }
}
