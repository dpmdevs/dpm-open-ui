/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component } from '@angular/core';

import { DataSubject, Tag } from '@dpm-models';
import { AnimationProvider } from '@dpm-providers';
import { BaseSelector } from '@dpm-components';
import { DataSubjectsService } from './data-subjects.service';


@Component({
    selector: 'dpm-data-subjects-selector',
    templateUrl: '../../../../../shared/components/dpm-base-selector/base-selector.html',
    providers: [DataSubjectsService],
    animations: [AnimationProvider.getSlideInTop(), AnimationProvider.getFadeIn()]
})
export class DataSubjectsSelectorComponent extends BaseSelector<DataSubject> {
    labels = {
        selectedTags: null,
        placeholder: 'Ricerca categorie di interessati',
        description: 'Qualsiasi persona fisica identificata o identificabile a cui si riferiscono i dati'
    };

    constructor(_dataSubjectsService: DataSubjectsService) {
        super(_dataSubjectsService);
    }

    protected mapItemsToTags(results: DataSubject[]): Tag[] {
        return results.map(({ id, name, description: title }: DataSubject) => ({ id, name, title }));
    }
}
