/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component } from '@angular/core';
import 'rxjs/add/observable/of';

import { AnimationProvider } from '@dpm-providers';
import { BaseSelector } from '@dpm-components';
import { Controller, Tag } from '@dpm-models';
import { ControllersService } from './controllers.service';


@Component({
    selector: 'dpm-controllers-selector',
    templateUrl: '../../../../../shared/components/dpm-base-selector/base-selector.html',
    providers: [ControllersService],
    animations: [AnimationProvider.getSlideInTop(), AnimationProvider.getFadeIn()]
})
export class ControllersSelectorComponent extends BaseSelector<Controller> {
    labels = {
        selectedTags: null,
        placeholder: 'Ricerca titolari',
        description: 'Persona fisica o giuridica, l’autorità pubblica, il servizio o altro organismo che,' +
        ' singolarmente o insieme ad altri, determina le finalità e i mezzi del trattamento di dati personali'
    };

    constructor(protected _controllersService: ControllersService) {
        super(_controllersService);
    }

    protected mapItemsToTags(items: Controller[]): Tag[] {
        return items.map(controller => {
            const tagName = `${controller.company_name}`
                + (controller.holder_firstname || controller.holder_lastname ? ': ' : '')
                + (controller.holder_title ? `${controller.holder_title}` : '')
                + (controller.holder_firstname ? ` ${controller.holder_firstname}` : '')
                + (controller.holder_lastname ? ` ${controller.holder_lastname}` : '');

            return {
                id: controller.id,
                name: tagName,
                title: tagName
            };
        });
    }

}
