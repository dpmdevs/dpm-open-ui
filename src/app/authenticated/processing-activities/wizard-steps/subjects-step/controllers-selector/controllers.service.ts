/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Controller, QueryParams, SearchCriteria } from '@dpm-models';
import { BaseSelectorService } from '../../../../../shared/components/dpm-base-selector/base-selector.service';
import { ControllersActions } from 'app/state/processing-activities/subjects/controllers/controllers.actions';


@Injectable()
export class ControllersService extends BaseSelectorService<Controller> {

    get table(): string {
        return 'controllers';
    }

    getSelectedTags(): Observable<Controller[]> {
        return this._stateService.getSubjects()
            .map(state => state.controllers);
    }

    addItem(controller: Controller): void {
        this._stateService.dispatch(ControllersActions.addController(controller));
    }

    removeItem(id: number): void {
        this._stateService.dispatch(ControllersActions.removeController(id));
    }

    searchItems(keyword?: string): Observable<Controller> {
        let searchResults;

        if (!keyword || keyword === '')
            searchResults = this.getTableData(<QueryParams>{ sortField: 'company_name', sortDirection: 'asc' });
        else {
            const searchOrCriteria: SearchCriteria[] = [
                { columnName: 'company_name', keyword },
                { columnName: 'holder_firstname', keyword },
                { columnName: 'holder_lastname', keyword }
            ];
            searchResults = this.getTableData(<QueryParams>{ searchOrCriteria, sortField: 'company_name', sortDirection: 'asc' });
        }

        return searchResults.map(tableData => tableData.data);
    }
}
