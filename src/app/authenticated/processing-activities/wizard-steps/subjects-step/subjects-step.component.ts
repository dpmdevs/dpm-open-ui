/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { AfterContentInit, Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { ToastsManager } from 'ng2-toastr';

import { AnimationProvider } from '@dpm-providers';
import { BaseWizardStep } from '@dpm-components';
import { StateService } from '../../../../state/state.service';
import { ISubjects } from '../../../../state/processing-activities/subjects';
import { ProcessingActivitiesService } from '../../services/processing-activities.service';


@Component({
    selector: 'dpm-subjects-step',
    templateUrl: './subjects-step.component.html',
    styleUrls: ['../../../../shared/components/dpm-wizard/base-wizard-step.scss'],
    animations: [AnimationProvider.getFadeIn()]
})
export class SubjectsStepComponent extends BaseWizardStep implements AfterContentInit {

    protected formConfig = {
        data_subjects: [[]],
        controllers: [[]],
        internal_processors: [[]],
        external_processors: [[]],
        dpos: [[]],
    };

    showInternalProcessor = false;

    get toastr(): ToastsManager {
        return this._toastr;
    }

    constructor(protected _formBuilder: FormBuilder,
                private _stateService: StateService,
                private _PAService: ProcessingActivitiesService,
                private _toastr: ToastsManager) {
        super(_formBuilder);
        this.formChanges$
            .debounceTime(1000)
            .subscribe(this.saveSubjects.bind(this));
    }

    ngAfterContentInit() {
        this._stateService.getSubjects()
            .subscribe(this.populateFormValues.bind(this));

        this._stateService.getConfig()
            .subscribe(config => this.showInternalProcessor = config.instanceConfig.internal_processor_active === true);
    }

    onNextStep(): void {
        if (this.stepForm.invalid)
            return;

        if (this.stepForm.dirty)
            this.saveSubjects().add(() => this.nextStep.emit());
        else
            this.nextStep.emit();
    }

    private saveSubjects(): Subscription {
        const subjectsStepData = this.prepareData(this.stepForm.getRawValue());
        return this._PAService.saveSubjectStep(subjectsStepData)
            .subscribe(() => this._toastr.info('Soggetti aggiornati con successo').then());
    }

    private populateFormValues(newState): void {
        Object.keys(newState).forEach(key => {
            const formControl = this.stepForm.controls[key];
            const value = formControl.value;
            const newValue = newState[key];

            if (value !== newValue) {
                formControl.setValue(newValue);
                formControl.markAsTouched();
            }
        });
    }

    private prepareData(data): ISubjects {
        const { data_subjects, controllers, internal_processors, external_processors, dpos } = data;

        return {
            data_subjects: data_subjects.map(d => d.id),
            controllers: controllers.map(c => c.id),
            internal_processors: internal_processors.map(i => i.id),
            external_processors: external_processors.map(e => e.id),
            dpos: dpos.map(d => d.id),
        };
    }

}
