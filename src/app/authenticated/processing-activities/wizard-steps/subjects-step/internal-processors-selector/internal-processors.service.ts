/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { QueryParams, SearchCriteria } from '@dpm-models';
import { BaseSelectorService } from '../../../../../shared/components/dpm-base-selector/base-selector.service';
import { ProcessorTableData } from './internal-processors';
import { InternalProcessorsActions } from 'app/state/processing-activities/subjects/internal-processors/internal-processors.actions';


@Injectable()
export class InternalProcessorsService extends BaseSelectorService<ProcessorTableData> {

    get table(): string {
        return 'users';
    }

    getSelectedTags(): Observable<ProcessorTableData[]> {
        return this._stateService.getSubjects()
            .map(state => state.internal_processors);
    }

    addItem(processor: ProcessorTableData): void {
        this._stateService.dispatch(InternalProcessorsActions.addProcessor(processor));
    }

    removeItem(id: number): void {
        this._stateService.dispatch(InternalProcessorsActions.removeProcessor(id));
    }

    searchItems(keyword?: string): Observable<ProcessorTableData> {
        let searchResults;

        if (!keyword || keyword === '')
            searchResults = this.getTableData(<QueryParams>{ sortField: 'firstname', sortDirection: 'asc' });
        else {
            const searchOrCriteria: SearchCriteria[] = [
                { columnName: 'firstname', keyword },
                { columnName: 'lastname', keyword }
            ];
            searchResults = this.getTableData(<QueryParams>{ searchOrCriteria, sortField: 'firstname', sortDirection: 'asc' });
        }

        return searchResults.map(tableData => tableData.data);
    }
}
