/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component } from '@angular/core';

import { AnimationProvider } from '@dpm-providers';
import { Tag } from '@dpm-models';
import { BaseSelector } from '@dpm-components';
import { InternalProcessorsService } from './internal-processors.service';
import { ProcessorTableData } from './internal-processors';


@Component({
    selector: 'dpm-internal-processors-selector',
    templateUrl: '../../../../../shared/components/dpm-base-selector/base-selector.html',
    providers: [InternalProcessorsService],
    animations: [AnimationProvider.getSlideInTop(), AnimationProvider.getFadeIn()]
})
export class InternalProcessorsSelectorComponent extends BaseSelector<ProcessorTableData> {
    labels = {
        selectedTags: null,
        placeholder: 'Ricerca referenti interni',
        description: 'Persona fisica o giuridica, l’autorità pubblica, il servizio o altro organismo che tratta dati' +
        ' personali per conto del titolare del trattamento'
    };

    constructor(_processorsService: InternalProcessorsService) {
        super(_processorsService);
    }

    protected mapItemsToTags(processors: ProcessorTableData[]): Tag[] {
        return processors.map((processor: ProcessorTableData) => {
            const fullName = `${processor.firstname} ${processor.lastname}`;

            return {
                id: processor.id,
                name: fullName,
                title: fullName
            };
        });
    }

}
