/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { DPO, QueryParams, SearchCriteria } from '@dpm-models';
import { BaseSelectorService } from '../../../../../shared/components/dpm-base-selector/base-selector.service';
import { DPOsActions } from '../../../../../state/processing-activities/subjects/DPOs/dpos.actions';


@Injectable()
export class DPOsService extends BaseSelectorService<DPO> {

    get table(): string {
        return 'dpos';
    }

    getSelectedTags(): Observable<DPO[]> {
        return this._stateService.getSubjects().map(state => state.dpos);
    }

    addItem(dpo: DPO): void {
        this._stateService.dispatch(DPOsActions.addDPO(dpo));
    }

    removeItem(id: number): void {
        this._stateService.dispatch(DPOsActions.removeDPO(id));
    }

    searchItems(keyword?: string): Observable<DPO> {
        let searchResults;

        if (!keyword || keyword === '')
            searchResults = this.getTableData(<QueryParams>{ sortField: 'company_name', sortDirection: 'asc' });
        else {
            const searchOrCriteria: SearchCriteria[] = [
                { columnName: 'company_name', keyword },
                { columnName: 'firstname', keyword },
                { columnName: 'lastname', keyword }
            ];
            searchResults = this.getTableData(<QueryParams>{ searchOrCriteria, sortField: 'company_name', sortDirection: 'asc' });
        }

        return searchResults.map(tableData => tableData.data);
    }
}
