/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component } from '@angular/core';

import { AnimationProvider } from '@dpm-providers';
import { DPO, Tag } from '@dpm-models';
import { BaseSelector } from '@dpm-components';
import { DPOsService } from './dpos.service';


@Component({
    selector: 'dpm-dpos-selector',
    templateUrl: '../../../../../shared/components/dpm-base-selector/base-selector.html',
    providers: [DPOsService],
    animations: [AnimationProvider.getSlideInTop(), AnimationProvider.getFadeIn()]
})
export class DPOsSelectorComponent extends BaseSelector<DPO> {
    labels = {
        selectedTags: null,
        placeholder: 'Ricerca DPO',
        description: '«Responsabile della protezione dei dati (DPO)»: Sezione 4, art. 37 e ss  del Regolamento 2016/679.'
    };

    constructor(_DPOsService: DPOsService) {
        super(_DPOsService);
    }

    protected mapItemsToTags(dpos: DPO[]): Tag[] {
        return dpos.map((dpo: DPO) => {
            const tagName = dpo.company_name
                ? `${dpo.company_name}`
                : `${dpo.title} ${dpo.firstname} ${dpo.lastname}`;

            return {
                id: dpo.id,
                name: tagName,
                title: tagName
            };
        });
    }

}
