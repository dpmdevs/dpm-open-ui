/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { QueryParams, SearchCriteria, ThirdParties } from '@dpm-models';
import { BaseSelectorService } from '../../../../../shared/components/dpm-base-selector/base-selector.service';
import { ExternalProcessorsActions } from 'app/state/processing-activities/subjects/external-processors/external-processors.actions';


@Injectable()
export class ExternalProcessorsService extends BaseSelectorService<ThirdParties> {

    get table(): string {
        return 'third-parties';
    }

    getSelectedTags(): Observable<ThirdParties[]> {
        return this._stateService.getSubjects()
            .map(state => state.external_processors);
    }

    addItem(processor: ThirdParties): void {
        this._stateService.dispatch(ExternalProcessorsActions.addProcessor(processor));
    }

    removeItem(id: number): void {
        this._stateService.dispatch(ExternalProcessorsActions.removeProcessor(id));
    }

    searchItems(keyword?: string): Observable<ThirdParties> {
        let searchResults;

        if (!keyword || keyword === '')
            searchResults = this.getTableData(<QueryParams>{ sortField: 'company_name', sortDirection: 'asc' });
        else {
            const searchCriteria: SearchCriteria[] = [{ columnName: 'company_name', keyword }];
            searchResults = this.getTableData(<QueryParams>{ searchCriteria, sortField: 'company_name', sortDirection: 'asc' });
        }

        return searchResults.map(tableData => tableData.data);
    }
}
