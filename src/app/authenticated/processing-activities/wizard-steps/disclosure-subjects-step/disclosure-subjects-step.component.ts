/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { ToastsManager } from 'ng2-toastr';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';

import { DisclosureSubject } from '@dpm-models';
import { AnimationProvider } from '@dpm-providers';
import { BaseWizardStep } from '@dpm-components';
import { StateService } from '../../../../state/state.service';
import { DisclosureSubjectsActions } from '../../../../state/processing-activities/disclosure-subjects/disclosure-subjects.actions';
import { ProcessingActivitiesService } from '../../services/processing-activities.service';
import { PACompletionStateService } from '../../services/pa-completion-state.service';
import { DisclosureSubjectsService } from './disclosure-subjects.service';


@Component({
    selector: 'dpm-disclosure-subjects-step',
    templateUrl: './disclosure-subjects-step.component.html',
    styleUrls: ['../../../../shared/components/dpm-wizard/base-wizard-step.scss'],
    animations: [AnimationProvider.getFadeIn()],
    providers: [DisclosureSubjectsService],
})
export class DisclosureSubjectsStepComponent extends BaseWizardStep {

    @ViewChild('deletionPromptModal') deletionPromptModal: TemplateRef<any>;

    modalRef: BsModalRef;
    disclosureSubjects$: Observable<DisclosureSubject[]>;
    disclosureSubjects: DisclosureSubject[] = [];
    transfer: boolean;
    addingDisclosureSubject = false;
    currentProcessingActivityId: number;

    protected formConfig = { disclosureSubjects: [[]] };

    get toastr(): ToastsManager {
        return this._toastr;
    }

    constructor(protected formBuilder: FormBuilder,
                private stateService: StateService,
                private disclosureSubjectsService: DisclosureSubjectsService,
                private processingActivitiesService: ProcessingActivitiesService,
                private completionStateService: PACompletionStateService,
                private modalService: BsModalService,
                private _toastr: ToastsManager) {
        super(formBuilder);
        stateService.getCurrentProcessingActivityId()
            .subscribe(id => this.currentProcessingActivityId = id);
    }

    ngOnInit() {
        super.ngOnInit();
        this.disclosureSubjects$ = this.stateService.getDisclosure();
        this.stateService.getDisclosure()
            .do(ds => this.disclosureSubjects = ds)
            .subscribe(this.populateFormWithServerData.bind(this));
    }

    private populateFormWithServerData(disclosureSubjects: DisclosureSubject[]): void {
        this.transfer = disclosureSubjects.length > 0;
        const formControl = this.stepForm.controls.disclosureSubjects;
        formControl.setValue(disclosureSubjects);
        formControl.markAsDirty();
        formControl.markAsTouched();
    }

    onSaveDisclosureSubject(disclosureSubject: DisclosureSubject): void {
        this.processingActivitiesService.saveDisclosureSubjectStep(disclosureSubject)
            .subscribe((data: any) => {
                const dsWithServerId = Object.assign({}, disclosureSubject, { id: data.id });
                this.stateService.dispatch(DisclosureSubjectsActions.addDisclosureSubject(dsWithServerId));
                this._toastr.success('Trasferimento creato con successo').then();
                this.addingDisclosureSubject = false;
            });
    }

    onUpdateDisclosureSubject(): void {
        this.completionStateService.reloadCompletionState(this.currentProcessingActivityId);
        this._toastr.info('Trasferimento aggiornato con successo').then();
    }

    onNextStep(): void {
        this.nextStep.emit();
    }

    toggleTransfer(): void {
        if (this.disclosureSubjects.length > 0 && this.transfer)
            this.modalRef = this.modalService.show(this.deletionPromptModal);
        else
            this.transfer = !this.transfer;
    }

    confirmDeletion(): void {
        const ids = this.disclosureSubjects.map(ds => ds.id);
        const requests = ids.map(id =>
            this.disclosureSubjectsService.deleteDisclosureSubject(id)
                .do(() => this.stateService.dispatch(DisclosureSubjectsActions.removeDisclosureSubject(id)))
        );
        Observable.forkJoin(...requests)
            .finally(() => {
                this.transfer = false;
                this.modalRef.hide();
            })
            .subscribe(() => {
                this.completionStateService.reloadCompletionState(this.currentProcessingActivityId);
            });
    }

}
