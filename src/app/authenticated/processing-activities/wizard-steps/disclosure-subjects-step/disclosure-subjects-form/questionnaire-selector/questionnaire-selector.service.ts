/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';

import { environment } from 'environments/environment';


@Injectable()
export class QuestionnaireSelectorService {
    constructor(private _http: HttpClient) {
    }

    getQuestions(): Observable<any[]> {
        return Observable
            .forkJoin(
                this._http.get(`${environment.apiUrl}questionnaire/disclosure-group-1`),
                this._http.get(`${environment.apiUrl}questionnaire/disclosure-group-2`),
                this._http.get(`${environment.apiUrl}questionnaire/disclosure-group-3`)
            )
            .map(this.nestedArraysToArrayOfArrays.bind(this));
    }

    private nestedArraysToArrayOfArrays(data: any[][]): any[] {
        /**
         * from
         * [ [{ questions: [a, b, c] }],
         *   [{ questions: [d, e, f] }],
         *   ... ]
         * to
         * [ [a, b, c],  [d, e, f],  ... ]
         */
        return data.map(d => d[0].questions);
    }
}
