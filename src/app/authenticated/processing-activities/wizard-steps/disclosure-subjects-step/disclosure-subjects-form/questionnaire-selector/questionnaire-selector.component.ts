/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { isUndefined } from 'util';

import { QuestionnaireSelectorService } from './questionnaire-selector.service';
import { QuestionnaireAnswer } from '../../../../../../shared/models';


@Component({
    selector: 'dpm-questionnaire-selector',
    templateUrl: './questionnaire-selector.component.html',
    styleUrls: ['./questionnaire-selector.component.scss'],
    providers: [QuestionnaireSelectorService]
})
export class CommunicationSelectorComponent implements OnInit {
    @Input() selectionChange$: Subject<QuestionnaireAnswer[]> = new Subject<QuestionnaireAnswer[]>();
    @Input() selectedAnswers: QuestionnaireAnswer[] = [];
    _answers: QuestionnaireAnswer[] = [];

    accordions = [{ isOpen: false }, { isOpen: false }, { isOpen: false }];
    questions = [];

    constructor(private _questionnaireSelectorService: QuestionnaireSelectorService) {
        _questionnaireSelectorService.getQuestions()
            .subscribe(questions => this.questions = questions);
    }

    ngOnInit() {
        this._answers = [...this.selectedAnswers];
    }

    toggleAccordion(index: number): void {
        this.accordions.forEach((acc, i) => acc.isOpen = i === index);
    }

    isSelected(questionId: number): boolean {
        const answer = this._answers.filter(a => a.question_id === questionId)[0];

        if (isUndefined(answer))
            return false;

        return answer.answer;
    }

    toggleQuestion(id: number) {
        if (this.isSelected(id))
            this._answers = this._answers.filter(answer => answer.question_id !== id);
        else
            this._answers.push({ question_id: id, answer: true });

        this.selectionChange$.next(this._answers);
    }
}
