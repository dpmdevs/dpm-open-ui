/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Subject } from 'rxjs/Subject';

import { DisclosureSubject } from '@dpm-models';
import { AnimationProvider } from '@dpm-providers';
import { StateService } from '../../../../../state/state.service';
import { DisclosureSubjectsActions } from '../../../../../state/processing-activities/disclosure-subjects/disclosure-subjects.actions';
import { ProcessingActivitiesService } from '../../../services/processing-activities.service';
import { DisclosureSubjectsService } from '../disclosure-subjects.service';


@Component({
    selector: 'dpm-disclosure-subject',
    templateUrl: './disclosure-subject.component.html',
    styleUrls: ['./disclosure-subject.component.scss'],
    animations: [AnimationProvider.getFadeIn()],
    providers: [DisclosureSubjectsService]
})
export class DisclosureSubjectComponent implements OnInit {
    @Input() disclosureSubject: DisclosureSubject;
    @Output() update = new EventEmitter();
    selectionChange$ = new Subject<number[]>();
    updatedDisclosureSubject: any = {};
    disclosureSubjectChanged = false;

    constructor(private _stateService: StateService,
                private _processingActivitiesService: ProcessingActivitiesService,
                private _disclosureSubjectsService: DisclosureSubjectsService) {
        this.selectionChange$.subscribe(answers => {
            this.updatedDisclosureSubject.answers = answers;
            this.disclosureSubjectChanged = true;
        });
    }

    ngOnInit() {
        this.updatedDisclosureSubject = Object.assign({}, this.disclosureSubject);
    }

    onEditComplete(company_name: string): void {
        this.updatedDisclosureSubject.company_name = company_name;
        this.disclosureSubjectChanged = true;
        this._processingActivitiesService.saveDisclosureSubjectStep(this.updatedDisclosureSubject)
            .subscribe({
                next: () => {
                    this._stateService.dispatch(DisclosureSubjectsActions.updateDisclosureSubject(this.updatedDisclosureSubject));
                    this.update.emit();
                },
            });
    }

    deleteDisclosureSubject(): void {
        const { id } = this.disclosureSubject;
        this._disclosureSubjectsService.deleteDisclosureSubject(id)
            .subscribe(() =>
                this._stateService.dispatch(DisclosureSubjectsActions.removeDisclosureSubject(id))
            );
    }
}
