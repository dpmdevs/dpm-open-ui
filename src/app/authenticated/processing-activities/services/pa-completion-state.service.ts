/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from 'environments/environment';
import { StepState } from '@dpm-models';
import { PaCompletionStateActions } from '../../../state/processing-activities/pa-completion-state/pa-completion-state.actions';
import { StateService } from '../../../state/state.service';


@Injectable()
export class PACompletionStateService {

    private paCompletionStateUrl = `${environment.apiUrl}pa-completion/`;

    constructor(private _http: HttpClient,
                private _stateService: StateService) {
    }

    reloadCompletionState(processingActivityId: number): void {
        this._http.get<StepState[]>(`${this.paCompletionStateUrl}${processingActivityId}`)
            .filter(stepState => stepState.length > 0)
            .map(this.mapServerStateToClientState)
            .subscribe(
                (completionState: StepState[]) => {
                        this._stateService.dispatch(PaCompletionStateActions.setStepsState(completionState));
                },
                () => this._stateService.dispatch(PaCompletionStateActions.resetStepsState())
            );
    }

    private mapServerStateToClientState(steps): StepState[] {
        return steps.map(step => {
            const id = `step-${step['step_id']}`;
            const { visited, completed } = step;

            return { id, visited, completed };
        });
    }

}
