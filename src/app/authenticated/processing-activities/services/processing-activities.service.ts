/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TableBaseService } from '@dpm-components/dpm-crud-page';
import { environment } from 'environments/environment';
import { DisclosureSubject, SecurityMeasure } from '@dpm-models';
import { IBasicData } from '../../../state/processing-activities/basic-data';
import { ISubjects } from '../../../state/processing-activities/subjects';
import { IOtherData } from '../../../state/processing-activities/other-data';
import { StateService } from '../../../state/state.service';
import { PAStatistics } from '../../dashboard/pa-widget/pa-statistics';


@Injectable()
export class ProcessingActivitiesService extends TableBaseService {

    currentPAId: number;

    constructor(http: HttpClient, stateService: StateService) {
        super(http, stateService);
        this._stateService.getCurrentProcessingActivityId().subscribe(id => this.currentPAId = id);
    }

    get baseUrl() {
        return environment.apiUrl;
    }

    get table() {
        return 'processing-activities';
    }

    get PARoute() {
        return `${this.baseUrl}${this.table}/${this.currentPAId}`;
    }

    saveBasicDataStep(data: IBasicData): Observable<Response> {
        return this.http.put<Response>(`${this.PARoute}`, data);
    }

    saveSubjectStep(data: ISubjects): Observable<Response> {
        return this.http.put<Response>(`${this.PARoute}/subjects`, data);
    }

    saveDisclosureSubjectStep(data: DisclosureSubject): Observable<Response> {
        return this.http.put<Response>(`${this.PARoute}/disclosure`, data);
    }

    saveOtherDataStep(data: IOtherData): Observable<Response> {
        return this.http.put<Response>(`${this.PARoute}/other-data`, data);
    }

    getSelectedSecurityMeasuresIds(): Observable<SecurityMeasure[]> {
        return this.http.get<SecurityMeasure[]>(`${this.PARoute}/security-measures`);
    }

    toggleSecurityMeasures(security_measures: number[]): Observable<Response> {
        return this.http.put<Response>(`${this.PARoute}/security-measures`, { security_measures });
    }

    getPAStatistics(): Observable<PAStatistics> {
        return this.http.get<PAStatistics>(`${this.baseUrl}${this.table}/statistics`);
    }

    generateProcessorReport(unit_id: number, format: 'pdf' | 'odt'): Observable<HttpResponse<Blob>> {
        return this.http.post(`${this.baseUrl}${this.table}/processor-record-of-pa/${format}`,
            { unit_id }, { observe: 'response', responseType: 'blob' }
        );
    }

    generateReport(format: 'pdf' | 'odt'): Observable<HttpResponse<Blob>> {
        return this.http.post(`${this.baseUrl}${this.table}/record-of-pa/${format}`,{}, { observe: 'response', responseType: 'blob' }
        );
    }

    toggleUnit(paId: number, unitId: number): Observable<Response> {
        return this.http.put<Response>(`${this.baseUrl}${this.table}/${paId}/units`, { units: [unitId] });
    }

}
