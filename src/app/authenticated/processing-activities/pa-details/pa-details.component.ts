/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap';
import { Subscription } from 'rxjs/Subscription';

import { DataCategory, DataType, ProcessingActivity, SecurityMeasureCategory } from '@dpm-models';
import { BaseDetailsPage } from '@dpm-components';
import { AnimationProvider, RendererProvider } from '@dpm-providers';
import { StateService } from '../../../state/state.service';
import { QuestionnaireSelectorService } from '../wizard-steps/disclosure-subjects-step/disclosure-subjects-form/questionnaire-selector/questionnaire-selector.service';
import { SecurityMeasuresService } from '../../assets/security-measures';
import { ProcessingActivitiesService } from '../services/processing-activities.service';


@Component({
    selector: 'dpm-pa-details',
    templateUrl: './pa-details.component.html',
    styleUrls: ['../../../shared/components/dpm-crud-page/base-details-page/base-details-page.scss', './pa-details.component.scss'],
    animations: [AnimationProvider.getFadeIn(), AnimationProvider.getToggleUpDown()],
    providers: [QuestionnaireSelectorService]
})
export class PaDetailsComponent extends BaseDetailsPage {

    readonly singleRecordOptions = ['nuovo', 'dettagli', 'modifica'];
    readonly multipleRecordOptions = ['nuovo', 'dettagli'];

    private activeTabSubscriptionForRoute: Subscription;
    records: ProcessingActivity[];
    accordionsVisibility = ['unSelected', 'unSelected', 'unSelected', 'unSelected', 'unSelected', 'unSelected'];
    questionGroups = [
        { code: 'disclosure-group-1', name: 'Trasferimento sulla base di una decisione di adeguatezza' },
        { code: 'disclosure-group-2', name: 'Trasferimento soggetto a garanzie adeguate' },
        { code: 'disclosure-group-3', name: 'Deroghe in specifiche situazioni' }
    ];
    securityMeasuresCategories: SecurityMeasureCategory[] = [];

    get modalService(): BsModalService {
        return this._modalService;
    }

    constructor(private router: Router,
                private stateService: StateService,
                private securityMeasuresService: SecurityMeasuresService,
                private processingActivitiesService: ProcessingActivitiesService,
                private _modalService: BsModalService) {
        super();

        this.dropdownOptions = this.singleRecordOptions;
        securityMeasuresService.getSecurityMeasuresCategories('org')
            .subscribe(categories => this.securityMeasuresCategories = categories);
    }

    protected initValidationErrors(): void {
    }

    ngOnInit() {
        this.activeTabSubscriptionForRoute = this.activeTab$
            .filter(t => t === 'nuovo' || t === 'modifica' || t === 'data protection impact assessment')
            .do(tabName => {
                if (tabName === 'nuovo')
                    this.stateService.resetWizardForms();

                let route = '';
                switch (tabName) {
                    case 'data protection impact assessment':
                        route = '/dpia-projects';
                        break;

                    case 'nuovo':
                        route = '/p-activities/new';
                        break;

                    case 'modifica':
                        route = `/p-activities/edit/${this.selectedRecords.getAllRecords()[0].id}/step-0`;
                        break;
                }

                this.router.navigate([route]).then();
            })
            .subscribe();

        this.activeTabSubscription = this.activeTab$
            .do(() => this.dropdownOptions = this.selectedRecords.length > 1 ? this.multipleRecordOptions : this.singleRecordOptions)
            .filter(t => t !== 'nuovo' && t !== 'modifica' && t !== 'data protection impact assessment')
            .subscribe(t => this.activeTab = t);
    }

    ngOnDestroy() {
        super.ngOnDestroy();
        this.activeTabSubscriptionForRoute.unsubscribe();
    }

    editRecord(): void {
        const selectedRecord = this.selectedRecords.getAllRecords()[0];
        this.router.navigate([`/p-activities/edit/${selectedRecord.id}/step-0`]).then();
    }

    toggleAccordion(index): void {
        this.accordionsVisibility[index] = this.accordionsVisibility[index] === 'selected' ? 'unSelected' : 'selected';
    }

    hasSelectedDataTypes(dataCategory: DataCategory): boolean {
        let result = false;
        const dataTypesIds = dataCategory.data_types.map(dt => dt.id);
        const selectedDataTypesIds = this.records[0].data_types.map(dt => dt.id);

        dataTypesIds.forEach(id => {
            if (selectedDataTypesIds.includes(id))
                result = true;
        });

        return result;
    }

    getSelectedDataTypes(dataCategory: DataCategory): DataType[] {
        const selectedDataTypesIds = this.records[0].data_types.map(dt => dt.id);
        return dataCategory.data_types.filter(dt => selectedDataTypesIds.includes(dt.id));
    }

    getAnswers(answers, code) {
        return answers.filter(ans => ans.code === code);
    }

    getSecurityMeasures(securityMeasures, category) {
        return securityMeasures.filter(sm => sm.security_measures_categories_id === category.id);
    }

    refreshTable(): void {
        this.updateTable.emit();
    }

    renderRiskEstimate(riskEstimate: number): string {
        return RendererProvider.riskEstimateRenderer(riskEstimate);
    }

    renderDataSourceTypes(dataSourceTypes): string {
        return RendererProvider.dataSourceTypesRenderer(dataSourceTypes.map(d => d.id));
    }

}
