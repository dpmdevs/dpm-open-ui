/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnDestroy, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/first';
import { ToastsManager } from 'ng2-toastr';

import { Controller, DataSubject, DPO, LegalBasis, ProcessingActivity, StepState, ThirdParties } from '@dpm-models';
import { AnimationProvider } from '@dpm-providers';
import { PACompletionStateService } from '../services/pa-completion-state.service';
import { StateService } from '../../../state/state.service';
import { ProcessingActivitiesService } from '../services/processing-activities.service';
import { IBasicData } from '../../../state/processing-activities/basic-data';
import { initialPaCompletionState } from '../../../state/processing-activities/pa-completion-state/pa-completion-state';
import { ProcessorTableData } from '../wizard-steps/subjects-step/internal-processors-selector/internal-processors';
import { CurrentPaIdActions } from '../../../state/processing-activities/current-pa-id/current-pa-id.actions';
import { BasicDataActions } from '../../../state/processing-activities/basic-data/basic-data.actions';
import { DataSubjectsActions } from '../../../state/processing-activities/subjects/data-subjects/data-subjects.actions';
import { ControllersActions } from '../../../state/processing-activities/subjects/controllers/controllers.actions';
import { InternalProcessorsActions } from '../../../state/processing-activities/subjects/internal-processors/internal-processors.actions';
import { ExternalProcessorsActions } from '../../../state/processing-activities/subjects/external-processors/external-processors.actions';
import { DPOsActions } from '../../../state/processing-activities/subjects/DPOs/dpos.actions';
import { DisclosureSubjectsActions } from '../../../state/processing-activities/disclosure-subjects/disclosure-subjects.actions';
import { OtherDataActions } from '../../../state/processing-activities/other-data/other-data.actions';
import { SecurityMeasuresActions } from '../../../state/processing-activities/security-measures/security-measures.actions';
import { processingActivitiesStepsLabels } from '../processing-activities-steps';


@Component({
    selector: 'dpm-edit-processing-activity',
    templateUrl: './edit-processing-activity.component.html',
    styleUrls: ['./edit-processing-activity.component.scss'],
    animations: [AnimationProvider.getFadeIn()],
})
export class EditProcessingActivityComponent implements OnInit, OnDestroy {
    processingActivityName: string;
    routeSubscription: Subscription;
    completionStateSubscription: Subscription;
    currentProcessingActivityId: number;
    currentStepId = 'step-0';
    steps: StepState[] = initialPaCompletionState;
    PAExistsOnServer = false;
    stepsLabels = processingActivitiesStepsLabels;

    constructor(private _completionStateService: PACompletionStateService,
                private _stateService: StateService,
                private _PAService: ProcessingActivitiesService,
                private _router: Router,
                private _route: ActivatedRoute,
                private toastr: ToastsManager,
                vcr: ViewContainerRef) {
        toastr.setRootViewContainerRef(vcr);
        this.completionStateSubscription = _stateService.getPACompletionState()
            .subscribe((completionState: StepState[]) => this.steps = completionState);
    }

    ngOnInit() {
        this.routeSubscription = this._route.params.subscribe({
            next: params => {
                const id = +params.id;
                this.currentStepId = params.stepId;
                this.populatePAData(id);
            },
            error: () => {
                this._router.navigate(['/p-activities']).then();
            }
        });
    }

    ngOnDestroy() {
        this.routeSubscription.unsubscribe();
        this.completionStateSubscription.unsubscribe();
    }

    private populatePAData(id: number): void {
        this._PAService.getEntity(id)
            .subscribe((data: ProcessingActivity) => {
                    this.currentProcessingActivityId = data.id;
                    this._stateService.dispatch(CurrentPaIdActions.setId(data.id));
                    this._completionStateService.reloadCompletionState(data.id);
                    this.populateStoreWithRecordData(data);
                    this.processingActivityName = data.name;
                },
                () => this._router.navigate(['/p-activities']).then()
            );
    }

    private populateStoreWithRecordData(data): void {
        const { identification_code, name, description, process_types, purpose, legal_references, deleted_at } = data;

        const selectedDataTypesIds = data.data_types.reduce((acc, cur) => acc.concat(cur.id), []);
        const data_categories = data.data_categories.map(dc => {
            const selectedDataTypes = dc.data_types.filter(dt => selectedDataTypesIds.includes(dt.id));
            return Object.assign({}, dc, { data_types: selectedDataTypes });
        });
        const legal_basis: LegalBasis[] = data.legal_basis.map(lb => {
            return {
                id: lb.id,
                name: lb.name,
                description: lb.description,
                deleted_at: lb.deleted_at
            };
        });
        const data_sources: ThirdParties[] = data.data_sources.map((ds: ThirdParties) => {
            return {
                id: ds.id,
                company_name: ds.company_name,
                description: ds.description,
                deleted_at: ds.deleted_at
            };
        });
        const data_source_types: number[] = data.data_source_types.map(dst => dst.id);

        const basicData: IBasicData = {
            identification_code,
            name,
            description,
            process_types,
            data_categories,
            purpose,
            legal_references,
            legal_basis,
            data_sources,
            data_source_types,
            is_active: deleted_at === null,
        };
        this._stateService.dispatch(BasicDataActions.setBasicData(basicData));


        const data_subjects: DataSubject[] = data.data_subjects.map(dataSubject => {
            return {
                id: dataSubject.id,
                name: dataSubject.name,
                description: dataSubject.description,
                deleted_at: dataSubject.deleted_at
            };
        });

        const controllers: Controller[] = data.controllers;
        const internal_processors: ProcessorTableData[] = data.internal_processors.map(processor => {
            return {
                id: processor.id,
                firstname: processor.firstname,
                lastname: processor.lastname,
                deleted_at: processor.deleted_at
            };
        });
        const external_processors: ThirdParties[] = data.external_processors.map(processor => {
            return {
                id: processor.id,
                company_name: processor.company_name,
                description: processor.description,
                processor_name: processor.processor_name,
                deleted_at: processor.deleted_at
            };
        });
        const dpos: DPO[] = data.dpos;

        this._stateService.dispatch(DataSubjectsActions.setDataSubjects(data_subjects));
        this._stateService.dispatch(ControllersActions.setControllers(controllers));
        this._stateService.dispatch(InternalProcessorsActions.setProcessors(internal_processors));
        this._stateService.dispatch(ExternalProcessorsActions.setProcessors(external_processors));
        this._stateService.dispatch(DPOsActions.setDPOs(dpos));


        const { disclosure_subjects } = data;
        this._stateService.dispatch(DisclosureSubjectsActions.setDisclosureSubjects(disclosure_subjects));


        const { applications, retentions, risk_estimate, notes, retention_description, is_process_automated, has_profiling_activity,
            automated_process_description } = data;
        const other_data = {
            applications,
            retentions,
            risk_estimate,
            notes,
            retention_description,
            is_process_automated,
            has_profiling_activity,
            automated_process_description
        };
        this._stateService.dispatch(OtherDataActions.setOtherData(other_data));

        const securityMeasuresIds = data.security_measures.map(sm => sm.id);
        this._stateService.dispatch(SecurityMeasuresActions.setSecurityMeasures(securityMeasuresIds));
    }

    onChangeStep(stepId: string, processingActivityId: number = this.currentProcessingActivityId): void {
        if (!this.currentProcessingActivityId) {
            this.currentProcessingActivityId = processingActivityId;
            this.PAExistsOnServer = true;
        }

        this._router.navigate([`/p-activities/edit/${this.currentProcessingActivityId}/${stepId}`]).then();
    }

    onWizardEnd(): void {
        this._router.navigate(['/p-activities']).then();
    }
}
