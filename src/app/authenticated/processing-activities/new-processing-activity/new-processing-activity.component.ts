/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnDestroy, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/mergeMap';
import { isNumber } from 'util';

import { AnimationProvider } from '@dpm-providers';
import { StepState } from '@dpm-models';
import { StateService } from '../../../state/state.service';
import { ControllersActions } from '../../../state/processing-activities/subjects/controllers/controllers.actions';
import { PACompletionStateService } from '../services/pa-completion-state.service';
import { processingActivitiesStepsLabels } from '../processing-activities-steps';


@Component({
    selector: 'dpm-new-processing-activity',
    templateUrl: './new-processing-activity.component.html',
    styleUrls: ['./new-processing-activity.component.scss'],
    animations: [AnimationProvider.getFadeIn()],
})
export class NewProcessingActivityComponent implements OnInit, OnDestroy {

    subscriptions: Subscription[] = [];
    currentProcessingActivityId: number;
    currentStepId = 'step-0';
    steps: StepState[] = [];
    PAExistsOnServer = false;
    stepsLabels = processingActivitiesStepsLabels;

    constructor(private _paCompletionStateService: PACompletionStateService,
                private _stateService: StateService,
                private _router: Router,
                private toastr: ToastsManager,
                vcr: ViewContainerRef) {
        toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit() {
        this.subscriptions.push(
            this._stateService.getCurrentProcessingActivityId()
                .mergeMap((id: number) => {
                    this.currentProcessingActivityId = id;
                    this.PAExistsOnServer = isNumber(id);
                    return this._stateService.getPACompletionState();
                })
                .subscribe(steps => this.steps = steps),
            this._stateService.getConfig()
                .subscribe(({ instanceConfig }) => {
                    this._stateService.dispatch(ControllersActions.addController(instanceConfig.controller));
                })
        );
    }

    ngOnDestroy() {
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
        this.subscriptions = [];
    }

    onChangeStep(stepId: string, processingActivityId: number = this.currentProcessingActivityId): void {
        if (!this.currentProcessingActivityId) {
            this.currentProcessingActivityId = processingActivityId;
            this.PAExistsOnServer = true;
        }

        this._paCompletionStateService.reloadCompletionState(this.currentProcessingActivityId);
        this.currentStepId = stepId;
    }

    onWizardEnd(): void {
        this._router.navigate(['/p-activities']).then();
    }

}
