/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { ProgressbarModule, TooltipModule } from 'ngx-bootstrap';

import { DPMCrudPageModule } from '@dpm-components/dpm-crud-page/dpm-crud-page.module';
import { SharedModule } from '../../shared/shared.module';
import { ProcessingActivitiesRoutingModule } from './processing-activities-routing.module';
import { ProcessingActivitiesService } from './services/processing-activities.service';
import { ProcessingActivitiesComponent } from './processing-activities.component';
import { NewProcessingActivityComponent } from './new-processing-activity/new-processing-activity.component';
import { EditProcessingActivityComponent } from './edit-processing-activity/edit-processing-activity.component';
import { PACompletionStateService } from './services/pa-completion-state.service';
import { PaDetailsComponent } from './pa-details/pa-details.component';

import {
    BasicDataStepComponent,
    DisclosureSubjectsStepComponent,
    OtherDataStepComponent,
    SecurityMeasuresStepComponent,
    SubjectsStepComponent
} from './wizard-steps';

import { LegalBasisSelectorComponent } from './wizard-steps/basic-data-step/legal-basis-selector/legal-basis-selector.component';
import { ControllersSelectorComponent } from './wizard-steps/subjects-step/controllers-selector/controllers-selector.component';
import { InternalProcessorsSelectorComponent } from './wizard-steps/subjects-step/internal-processors-selector/internal-processors-selector.component';
import { ExternalProcessorsSelectorComponent } from './wizard-steps/subjects-step/external-processors-selector/external-processors-selector.component';
import { DPOsSelectorComponent } from './wizard-steps/subjects-step/dpos-selector/dpos-selector.component';
import { DataSubjectsSelectorComponent } from './wizard-steps/subjects-step/data-subjects-selector/data-subjects-selector.component';
import { CommunicationSelectorComponent } from './wizard-steps/disclosure-subjects-step/disclosure-subjects-form/questionnaire-selector/questionnaire-selector.component';
import { ApplicationsSelectorComponent } from './wizard-steps/other-data-step/applications-selector/applications-selector.component';
import { RetentionsSelectorComponent } from './wizard-steps/other-data-step/retentions-selector/retentions-selector.component';
import { DisclosureSubjectsFormComponent } from './wizard-steps/disclosure-subjects-step/disclosure-subjects-form/disclosure-subjects-form.component';
import { DisclosureSubjectComponent } from './wizard-steps/disclosure-subjects-step/disclosure-subject/disclosure-subject.component';
import { DataSourcesSelectorComponent } from './wizard-steps/basic-data-step/data-sources-selector/data-sources-selector.component';

const wizardSteps = [
    BasicDataStepComponent,
    SubjectsStepComponent,
    DisclosureSubjectsStepComponent,
    OtherDataStepComponent,
    SecurityMeasuresStepComponent
];

const selectors = [
    LegalBasisSelectorComponent,
    ControllersSelectorComponent,
    InternalProcessorsSelectorComponent,
    ExternalProcessorsSelectorComponent,
    DPOsSelectorComponent,
    DataSubjectsSelectorComponent,
    CommunicationSelectorComponent,
    ApplicationsSelectorComponent,
    RetentionsSelectorComponent,
    DataSourcesSelectorComponent,
];


@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        DPMCrudPageModule,
        ReactiveFormsModule,
        ProcessingActivitiesRoutingModule,
        AccordionModule.forRoot(),
        ProgressbarModule.forRoot(),
        TooltipModule.forRoot()
    ],
    entryComponents: [
        PaDetailsComponent
    ],
    declarations: [
        ProcessingActivitiesComponent,
        NewProcessingActivityComponent,
        EditProcessingActivityComponent,
        ...wizardSteps,
        ...selectors,
        PaDetailsComponent,
        DisclosureSubjectsFormComponent,
        DisclosureSubjectComponent,
    ],
    providers: [
        ProcessingActivitiesService,
        PACompletionStateService,
    ]
})
export class ProcessingActivitiesModule {
}
