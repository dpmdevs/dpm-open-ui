/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { isNullOrUndefined } from "util";

import { BaseCrudPageComponent, GridDecorator, TableBaseService } from '@dpm-components';
import { CompletionState, ProcessingActivity, TableData, User } from '@dpm-models';
import { AnimationProvider, RendererProvider } from '@dpm-providers';
import { StateService } from '../../state/state.service';
import { ProcessingActivitiesService } from './services/processing-activities.service';
import { PaDetailsComponent } from './pa-details/pa-details.component';
import { QueryParamsActions } from '../../state/query-params/query-params.actions';


@GridDecorator({
    title: 'Registro dei Trattamenti',
    description: 'Ogni titolare ed ogni responsabile del trattamento, tengono un registro delle attività di trattamento' +
    ' svolte sotto la propria responsabilità.',

    headers: [
        {
            displayName: 'data di creazione', columnName: 'created_at', renderer: RendererProvider.dateRenderer,
            customClass: 'creation-date-header'
        },
        {
            displayName: 'identificativo', columnName: 'identification_code', customClass: 'identification-code-header',
            renderer: RendererProvider.identificationCodeRenderer
        },
        { displayName: 'nome', columnName: 'name' },
        {
            displayName: 'descrizione', columnName: 'description',
            renderer: RendererProvider.truncateText({ maxStringLength: 250 })
        },
        {
            displayName: 'Tipo banca',
            columnName: 'process_types',
            listFieldName: 'name',
            searchable: false,
            sortable: false,
            renderer: RendererProvider.processTypeRenderer
        },
        {
            displayName: 'unità',
            columnName: 'units',
            renderer: RendererProvider.unitRenderer,
            searchColumn: 'units.code',
            sortable: false
        },
        {
            displayName: 'Rischio',
            columnName: 'risk_estimate',
            renderer: RendererProvider.riskEstimateRenderer,
        }

    ],
    paginationLimit: 10,

    hasDetailPage: true
})
@Component({
    templateUrl: './processing-activities.component.html',
    styleUrls: ['../../shared/components/dpm-crud-page/base-crud-page.scss', './processing-activities.component.scss'],
    animations: [AnimationProvider.getFadeIn()]
})
export class ProcessingActivitiesComponent extends BaseCrudPageComponent {

    STEPS = 5;
    records: ProcessingActivity[] = [];
    currentUser: User;
    currentUserDirectedUnitsIds: number[] = [];

    constructor(private _service: ProcessingActivitiesService,
                private _stateService: StateService,
                private _resolver: ComponentFactoryResolver,
                private _toaster: ToastsManager,
                private _viewContainerRef: ViewContainerRef,
                private _router: Router) {
        super();
        this.stateService.dispatch(QueryParamsActions.updateQueryParams({ sortField: 'created_at' }));
        _stateService.getUser()
            .do((user: User) => this.currentUser = user)
            .subscribe((user: User) => this.currentUserDirectedUnitsIds = user.directed_units.map(u => u.id));
    }

    protected subscribeOnTableData(): void {
        this.tableSubscription = this.queryParams$
            .mergeMap(queryParams => this.dataService.getTableData(queryParams))
            .distinctUntilChanged()
            .subscribe((tableData: TableData) => {
                this.isLoading = false;
                this.totalRecords = tableData.metadata.totalRecords;
                this.records = tableData.data.map((record: ProcessingActivity) => {
                    const completionValue = this.calculateCompletion(record.completion_state);
                    return Object.assign({}, record, { completionValue });
                });
            });
    }

    protected get defaultTab(): string {
        return 'dettagli';
    }

    protected get dataService(): TableBaseService {
        return this._service;
    }

    protected get stateService(): StateService {
        return this._stateService;
    }

    protected get resolver(): ComponentFactoryResolver {
        return this._resolver;
    }

    protected get viewChildComponent(): any | any {
        return PaDetailsComponent;
    }

    protected get viewContainerRef(): ViewContainerRef {
        return this._viewContainerRef;
    }

    protected get toastr(): ToastsManager {
        return this._toaster;
    }

    setActiveTab(tabName: string): void {
        if (tabName === 'nuovo' || tabName === 'modifica') {
            this._stateService.resetWizardForms();
            const selectedRecord = this.selectedRecords.getAllRecords()[0];
            const route = tabName === 'nuovo' ? 'new' : `edit/${selectedRecord.id}`;
            this._router.navigate([`p-activities/${route}`]).then();
        } else
            this.detailsActiveTab$.next(tabName);
    }

    calculateCompletion(states: CompletionState[]): number {
        let completionValue = 0;

        states.forEach(status => {
            if (status.completed)
                completionValue += 100 / this.STEPS;
            else if (status.visited)
                completionValue += 50 / this.STEPS;
        });

        return completionValue;
    }

}
