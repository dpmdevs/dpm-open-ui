/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { NgModule } from '@angular/core';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule, BsDropdownModule, TooltipModule } from 'ngx-bootstrap';

import { SharedModule } from '../../shared/shared.module';
import { DPMCrudPageModule } from '../../shared/components/dpm-crud-page/dpm-crud-page.module';

import { NewUserFormComponent, UpdateUserFormComponent, UserDetailsComponent, UsersTableComponent } from './users';

import {
    NewRoleFormComponent,
    RoleDetailsComponent,
    RolesComponent,
    RoleService,
    UpdateRoleFormComponent
} from './roles';

import {
    ExternalSuppliersComponent,
    NewThirdPartiesFormComponent,
    ThirdPartiesDetailsComponent,
    ThirdPartiesService,
    UpdateThirdPartiesFormComponent
} from './third-parties';

import {
    NewUnitFormComponent,
    UnitDetailsComponent,
    UnitsComponent,
    UnitsService,
    UpdateUnitFormComponent
} from './units';

import {
    NewQualificationFormComponent,
    QualificationDetailsComponent,
    QualificationsComponent,
    QualificationsService,
    UpdateQualificationFormComponent
} from './qualifications';

import {
    ControllerDetailsComponent,
    ControllersComponent,
    ControllersService,
    NewControllerFormComponent,
    UpdateControllerFormComponent,
} from './controllers';

import { DPODetailsComponent, DPOsComponent, DPOsService, NewDPOFormComponent, UpdateDPOFormComponent, } from './dpos';
import { SubjectsComponent } from './subjects.component';
import { SubjectsRoutingModule } from './subjects.routing.module';


@NgModule({
    imports: [
        SharedModule,
        DPMCrudPageModule,
        SubjectsRoutingModule,
        TabsModule.forRoot(),
        BsDropdownModule.forRoot(),
        BsDatepickerModule.forRoot(),
        TooltipModule.forRoot(),
    ],
    entryComponents: [
        UserDetailsComponent,
        RoleDetailsComponent,
        ThirdPartiesDetailsComponent,
        QualificationDetailsComponent,
        ControllerDetailsComponent,
        DPODetailsComponent,
    ],
    declarations: [
        SubjectsComponent,

        UsersTableComponent,
        UserDetailsComponent,
        NewUserFormComponent,
        UpdateUserFormComponent,

        RolesComponent,
        RoleDetailsComponent,
        NewRoleFormComponent,
        UpdateRoleFormComponent,

        ExternalSuppliersComponent,
        ThirdPartiesDetailsComponent,
        NewThirdPartiesFormComponent,
        UpdateThirdPartiesFormComponent,

        UnitsComponent,
        UnitDetailsComponent,
        NewUnitFormComponent,
        UpdateUnitFormComponent,

        QualificationsComponent,
        QualificationDetailsComponent,
        NewQualificationFormComponent,
        UpdateQualificationFormComponent,

        ControllersComponent,
        ControllerDetailsComponent,
        NewControllerFormComponent,
        UpdateControllerFormComponent,

        DPOsComponent,
        DPODetailsComponent,
        NewDPOFormComponent,
        UpdateDPOFormComponent,
    ],
    providers: [
        RoleService,
        ThirdPartiesService,
        UnitsService,
        QualificationsService,
        ControllersService,
        DPOsService,
    ],
})
export class SubjectsModule {
}
