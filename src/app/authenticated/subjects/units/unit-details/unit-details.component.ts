/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';
import { TreeNode } from 'angular-tree-component';
import { isNullOrUndefined } from 'util';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import { ValidationMessages } from '@dpm-components/dpm-crud-page/base-details-page/validation-messages';
import { DpmTagCouplerHandler, DpmTagCouplerLabels, Tag, Unit, User } from '@dpm-models';
import { AnimationProvider } from '@dpm-providers';
import { UnitsService } from '@dpm-services';
import { HttpErrorResponse } from '@angular/common/http';


@Component({
    selector: 'dpm-units-details',
    templateUrl: './unit-details.component.html',
    styleUrls: ['./unit-details.component.scss', '../../../../shared/components/dpm-crud-page/base-details-page/base-details-page.scss'],
    animations: [AnimationProvider.getFadeIn()]
})
export class UnitDetailsComponent implements OnInit, OnDestroy, DpmTagCouplerHandler {

    @Input() toastr: ToastsManager;
    @Input() selectedUnit$: Subject<TreeNode>;
    @Input() activeTab$: BehaviorSubject<string>;
    @Input() lastParentUnit: Unit;
    @Output() updateTree = new EventEmitter();

    activeTab = 'dettagli';
    selectedUnit: TreeNode;
    subscriptions: Subscription[] = [];
    validationMessages: ValidationMessages = { errors: {}, message: '' };

    assignedTags$: Observable<Tag[]>;
    availableTags$: Observable<Tag[]>;
    lastKeyword = '';
    tagCouplerLabels: DpmTagCouplerLabels = {
        header: 'Responsabile unità',
        placeholder: 'Ricerca utenti',
        searchBoxTitle: 'Utenti selezionabili',
        noTagsFallbackText: 'Nessun utente selezionato'
    };

    constructor(protected _unitsService: UnitsService,
                private _toastr: ToastsManager) {
    }

    ngOnInit() {
        this.subscriptions.push(
            this.activeTab$
                .subscribe(tab => this.activeTab = tab),
            this.selectedUnit$
                .subscribe(unit => {
                    this.selectedUnit = unit;
                    this.getTagCouplerUnitsDirectorsTags();
                })
        );
    }

    ngOnDestroy() {
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
        this.subscriptions = [];
    }

    protected getTagCouplerUnitsDirectorsTags(): void {
        if (!this.selectedUnit)
            return;

        this.getAvailableDirectors();
        this.getAssignedDirectors();
    }

    protected getAvailableDirectors(keyword: string = this.lastKeyword): void {
        this.availableTags$ = this._unitsService.getAvailableDirectors(this.selectedUnit.data.id, keyword)
            .map(this.mapDirectorsToTags);
    }

    protected getAssignedDirectors(): void {
        this.assignedTags$ = this._unitsService.getAssignedDirectors(this.selectedUnit.data.id)
            .map(this.mapDirectorsToTags);
    }

    protected mapDirectorsToTags(directors: User[]): Tag[] {
        return directors.map(({ id, firstname, lastname, email: title, user_number }) => ({
            id,
            title,
            name: `${user_number} - ${firstname} ${lastname}`
        }));
    }

    exists(object): boolean {
        return !isNullOrUndefined(object);
    }

    onAddEntity(formData) {
        let newUnit: Unit;
        if (isNullOrUndefined(this.lastParentUnit))
            newUnit = Object.assign({}, formData);
        else
            newUnit = Object.assign({}, formData, { parent_id: this.lastParentUnit.id });

        this._unitsService.addEntity(newUnit)
            .subscribe(
                () => {
                    this.refreshNodes();
                    this.initValidationErrors();
                },
                ({ error }: HttpErrorResponse) => {
                    this.validationMessages = error;
                    this._toastr.error(error.message).then();
                }
            );
    }

    onUpdateEntity(formData) {
        this._unitsService.updateEntity(formData)
            .subscribe(
                this.refreshNodes.bind(this),
                this.catchError.bind(this),
            );
    }

    protected catchError(error: HttpErrorResponse): Observable<HttpErrorResponse> {
        if (error.status === 422) {
            this.validationMessages = error.error;

            this.toastr.error(this.validationMessages.message).then();

            for (const err in this.validationMessages.errors)
                this.toastr.error(this.validationMessages.errors[err].toString()).then();

            return Observable.throw(error);
        }

        return Observable.of(error);
    }

    onDeleteEntity() {
        this._unitsService.deleteEntity(this.selectedUnit)
            .subscribe(() => {
                this.selectedUnit.toggleActivated();
                this.refreshNodes();
            });
    }

    onDisableEntity() {
        this._unitsService.disableEntity(this.selectedUnit)
            .subscribe(() => {
                this.selectedUnit.toggleActivated();
                this.refreshNodes();
            });
    }

    onRestoreDeletedEntity() {
        this._unitsService.restoreEntity(this.selectedUnit)
            .subscribe(() => {
                this.selectedUnit.toggleActivated();
                this.refreshNodes();
            });
    }

    private refreshNodes() {
        this.restoreDefaultTab();
        this.updateTree.emit();
    }

    private initValidationErrors(): void {
        this.validationMessages.errors = { code: [] };
    }

    onSearchTag(keyword: string): void {
        this.lastKeyword = keyword;
        this.getTagCouplerUnitsDirectorsTags();
    }

    onToggleTag(tag: Tag): void {
        this._unitsService.toggleDirector(this.selectedUnit.data.id, tag.id)
            .subscribe(() => {
                this.refreshNodes();
                this.getTagCouplerUnitsDirectorsTags();
                this._toastr.info('Modifiche effettuate con successo').then();
            });
    }

    restoreDefaultTab(): void {
        this.activeTab$.next('dettagli');
    }

}
