/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';
import { TreeComponent, TreeNode } from 'angular-tree-component';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { isNull, isUndefined } from 'util';
import { Observable } from 'rxjs/Observable';

import { AnimationProvider } from '@dpm-providers';
import { UnitsService } from '@dpm-services';
import { Unit } from '@dpm-models';
import { DpmConstants } from '@dpm-helpers';


@Component({
    templateUrl: './units.component.html',
    styleUrls: ['../../../shared/components/dpm-crud-page/base-crud-page.scss', './units.component.scss'],
    animations: [AnimationProvider.getFadeIn()],
    providers: [UnitsService]
})
export class UnitsComponent implements OnInit {

    readonly userIcon = DpmConstants.USER_ICON_PATH;
    @ViewChild('tree') treeComponent: TreeComponent;

    activeTab$: BehaviorSubject<string>;
    selectedUnit$: Subject<TreeNode> = new Subject();
    lastParentUnit: Unit;
    defaultTab = 'dettagli';
    nodes$: Observable<Unit[]>;

    options = {
        animateExpand: true,
        animateAcceleration: 2.2,
        allowDrag: true,
        levelPadding: 40,
        actionMapping: {
            mouse: {
                drop: (tree, node, $event, {from, to}) => {
                    tree.moveNode(from, to);
                    tree.update();
                }
            }
        }
    };

    constructor(protected unitsService: UnitsService,
                public toastr: ToastsManager,
                vcr: ViewContainerRef) {
        toastr.setRootViewContainerRef(vcr);
        this.activeTab$ = new BehaviorSubject(this.defaultTab);
    }

    ngOnInit() {
        this.onUpdateTree();
    }

    onUpdateTree(): void {
        this.nodes$ = this.unitsService.getUnits()
            .map(this.expandFirstLevel);
    }

    private expandFirstLevel(nodes) {
        return [...nodes].map((node: Unit) => (
            !isUndefined(node.children)
                ? Object.assign({}, node, { isExpanded: true })
                : node
        ));
    }

    filterNodes(filter: string, tree: TreeComponent): void {
        const searchNameOrCode = (node: TreeNode): boolean => {
            return !isNull(node.data.name)
                && node.data.name.toLowerCase().indexOf(filter.toLowerCase()) !== -1
                || node.data.code.toLowerCase().indexOf(filter.toLowerCase()) !== -1;
        };

        // https://angular2-tree.readme.io/docs/filtering#filter-by-function
        tree.treeModel.filterNodes(searchNameOrCode);
    }

    selectNode(event): void {
        this.selectedUnit$.next(event.node);
        this.activeTab$.next('dettagli');
    }

    unSelectNode(event): void {
        const userIsOnIE11 = !event.hasOwnProperty('eventName');
        if (userIsOnIE11)
            return;
        this.selectedUnit$.next();
    }

    onMoveNode({ node, to: { parent, index: position }, from }): void {
        const parent_id = parent.virtual ? null : parent.id;
        const newNode = Object.assign({}, node, { parent_id, position });

        this.unitsService.updateEntity(newNode)
            .finally(this.onUpdateTree.bind(this))
            .subscribe(() => {
                this.toastr.info('Unità spostata correttamente').then();
            });
    }

    openCreateRootNodeDialog(): void {
        this.lastParentUnit = null;
        this.activeTab$.next('nuovo');
    }

    openAppendChildNodeDialog(node: TreeNode): void {
        this.selectedUnit$.next(node);
        this.lastParentUnit = node.data;
        this.activeTab$.next('nuovo');
    }

}
