/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { FormGroup, Validators } from '@angular/forms';
import { isNullOrUndefined } from 'util';
import * as moment from 'moment';

import { compliantPasswordValidator, DPMValidators, fiscalCodeValidator } from '@dpm-helpers';
import { RendererProvider } from '@dpm-providers';


export abstract class UserForm {

    public RendererProvider = RendererProvider;

    controlsConfig;
    isFileReaderSupported: boolean;
    recordInfo: FormGroup;
    reader;
    base64image: string;
    isPasswordValidated: boolean;
    isFiscalCodeValidated: boolean;

    setControlsConfig({
        username = '', firstname = '', lastname = '', email = '', password = '', fiscal_code = '',
        sex = '', user_number = '', dismissal_date, binding_field = '', avatar = '',
    }) {
        if (this.controlsConfig !== undefined) return;

        this.controlsConfig = {
            username: [username, [DPMValidators.required, Validators.minLength(3), Validators.maxLength(255)]],
            firstname: [firstname, [DPMValidators.required, Validators.maxLength(255)]],
            lastname: [lastname, [DPMValidators.required, Validators.maxLength(255)]],
            email: [email, [DPMValidators.email, Validators.maxLength(255)]],
            user_number: [user_number, DPMValidators.required],
            binding_field: [binding_field, Validators.maxLength(255)],
            sex: [sex, Validators.maxLength(1)],
            fiscal_code, password, avatar,
            dismissal_date: isNullOrUndefined(dismissal_date) ? '' : moment(dismissal_date).toDate(),
        };
    }

    setFiscalCodeValidation(): void {
        if (this.isFiscalCodeValidated) return;

        this.recordInfo.get('fiscal_code').setValidators(fiscalCodeValidator());
        this.isFiscalCodeValidated = true;
    }

    setPasswordValidation(): void {
        if (this.isPasswordValidated) return;

        this.recordInfo.get('password').setValidators(compliantPasswordValidator());
        this.isPasswordValidated = true;
    }

    handleInputChange(e): void {
        if (!this.isFileReaderSupported) return;

        const file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        const fileIsImage = file.type.match(/image-*/);

        if (!fileIsImage) {
            alert('invalid format');
            return;
        }

        this.reader = new FileReader();
        this.reader.onload = (event: any) => {
            const avatar = event.target.result;
            this.base64image = avatar || '';
        };

        this.reader.readAsDataURL(file);
        this.recordInfo.get('avatar').markAsDirty();
    }
}
