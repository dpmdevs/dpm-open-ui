/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { BsDatepickerConfig, BsLocaleService } from 'ngx-bootstrap';
import * as moment from 'moment';

import { User } from '@dpm-models';
import { UpdateForm } from '@dpm-components/dpm-crud-page/base-details-page/forms/update-form';
import { compliantPasswordValidator, fiscalCodeValidator, MultipleInheritanceHelper } from '@dpm-helpers';
import { RendererProvider } from '@dpm-providers';
import { UserForm } from '../user-form';


@Component({
    selector: 'dpm-update-user-form',
    templateUrl: './update-user-form.component.html',
    styleUrls: ['../../../../../shared/components/dpm-crud-page/base-details-page/forms/base-form.scss']
})
export class UpdateUserFormComponent extends UpdateForm<User> implements UserForm {

    public RendererProvider = RendererProvider;

    bsConfig: Partial<BsDatepickerConfig> = {
        containerClass: 'theme-blue'
    };

    setControlsConfig: (record) => void;
    handleInputChange: (event) => void;
    reader;

    base64image = '';
    isFileReaderSupported = true;
    isPasswordValidated = false;
    isFiscalCodeValidated = false;

    setDismissalDate(date): void {
        if (!date) return;

        const dismissalDateControl = this.recordInfo.get('dismissal_date');
        dismissalDateControl.setValue(date);
        dismissalDateControl.markAsDirty();
        dismissalDateControl.markAsTouched();
    }

    constructor(private _formBuilder: FormBuilder, private _localeService: BsLocaleService) {
        super();
        this.isFileReaderSupported = FileReader !== undefined;
        this._localeService.use('it');
    }

    get formBuilder(): FormBuilder {
        return this._formBuilder;
    }

    ngOnInit() {
        const controlValues = Object.assign(
            {},
            this.record,
            { avatar: '' } // empty string since HTML file input requires a file path as value
        );

        this.base64image = this.record.avatar;
        this.setControlsConfig(controlValues);
        super.ngOnInit();
    }

    onSubmit({ value }): void {
        const { id } = this.record;
        const { dismissal_date: dismissalDate } = value;
        const formattedDismissalDate = moment(dismissalDate).format('YYYY-MM-DD');
        const dismissal_date = formattedDismissalDate !== 'Invalid date' ? formattedDismissalDate : null;

        const updatedUser = Object.assign(
            {},
            value,
            {
                id,
                avatar: this.base64image,
                dismissal_date
            }
        );

        super.onSubmit(updatedUser);
    }

    setPasswordValidation(): void {
        const passwordControl = this.recordInfo.get('password');
        const passwordIsUntouchedOrValid = !this.isPasswordValidated && passwordControl.value === '' || this.isPasswordValidated;

        if (passwordIsUntouchedOrValid)
            return;

        if (passwordControl.value !== '')
            passwordControl.setValidators(compliantPasswordValidator());

        this.isPasswordValidated = true;
    }

    setFiscalCodeValidation(): void {
        const fiscalCodeControl = this.recordInfo.get('fiscal_code');
        const fiscalCodeIsUntouchedOrValid = !this.isFiscalCodeValidated && fiscalCodeControl.value === '' || this.isFiscalCodeValidated;

        if (fiscalCodeIsUntouchedOrValid)
            return;

        if (fiscalCodeControl.value !== '')
            fiscalCodeControl.setValidators(fiscalCodeValidator());

        this.isFiscalCodeValidated = true;
    }
}

MultipleInheritanceHelper.applyMixins(UpdateUserFormComponent, [UserForm]);
