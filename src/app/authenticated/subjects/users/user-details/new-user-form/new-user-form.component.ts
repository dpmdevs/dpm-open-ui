/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { BsDatepickerConfig, BsLocaleService } from 'ngx-bootstrap';
import { isNullOrUndefined } from 'util';
import * as moment from 'moment';

import { User } from '@dpm-models';
import { NewForm } from '@dpm-components/dpm-crud-page/base-details-page/forms/new-form';
import { MultipleInheritanceHelper } from '@dpm-helpers';
import { RendererProvider } from '@dpm-providers';
import { UserForm } from '../user-form';


@Component({
    selector: 'dpm-new-user-form',
    templateUrl: './new-user-form.component.html',
    styleUrls: ['../../../../../shared/components/dpm-crud-page/base-details-page/forms/base-form.scss']
})
export class NewUserFormComponent extends NewForm<User> implements UserForm {

    public RendererProvider = RendererProvider;

    bsConfig: Partial<BsDatepickerConfig> = {
        containerClass: 'theme-blue'
    };

    setControlsConfig: (record) => void;
    setPasswordValidation: () => void;
    setFiscalCodeValidation: () => void;
    handleInputChange: (event) => void;

    reader;
    base64image = '';
    isFileReaderSupported = true;
    isPasswordValidated = false;
    isFiscalCodeValidated = false;

    setDismissalDate(date): void {
        if (!date) return;

        const dismissalDateControl = this.recordInfo.get('dismissal_date');
        dismissalDateControl.setValue(date);
        dismissalDateControl.markAsDirty();
        dismissalDateControl.markAsTouched();
    }

    constructor(private _formBuilder: FormBuilder, private _localeService: BsLocaleService) {
        super();
        this.isFileReaderSupported = FileReader !== undefined;
        this._localeService.use('it');
    }

    get formBuilder() {
        return this._formBuilder;
    }

    onSubmit(data): void {
        const { dismissal_date: dismissalDate } = data;
        const isDateValid = date => !isNullOrUndefined(date) && date !== '';
        const dismissal_date = isDateValid(dismissalDate) ? moment(dismissalDate).format('YYYY-MM-DD'): null;

        const user = Object.assign({}, data, {
            id: null,
            sex: 'm',
            end_date: new Date(),
            created_at: new Date(),
            dismissal_date
        }, { avatar: this.base64image });

        super.onSubmit(user);
        if (this.validationMessages.message !== '')
            return;

        this.base64image = '';
        this.recordInfo.get('dismissal_date').setValue(null);
    }
}

MultipleInheritanceHelper.applyMixins(NewUserFormComponent, [UserForm]);
