/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, ViewContainerRef } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ToastsManager } from 'ng2-toastr';
import { BsModalService } from 'ngx-bootstrap';

import { AnimationProvider, RendererProvider } from '@dpm-providers';
import { BaseDetailsPage } from '@dpm-components';
import { DpmTagCouplerLabels, Qualification, Role, Tag, User } from '@dpm-models';
import { UserService } from '../../../user/user.service';


@Component({
    selector: 'dpm-user-details',
    templateUrl: './user-details.component.html',
    styleUrls: ['../../../../shared/components/dpm-crud-page/base-details-page/base-details-page.scss', './user-details.component.scss'],
    animations: [AnimationProvider.getFadeIn(), AnimationProvider.getToggleUpDown()]
})
export class UserDetailsComponent extends BaseDetailsPage {

    public RendererProvider = RendererProvider;
    readonly singleRecordOptions = ['nuovo', 'dettagli', 'modifica', 'unità'];
    readonly multipleRecordOptions = ['nuovo', 'dettagli', 'unità'];

    records: User[];
    accordionsVisibility = ['unSelected', 'unSelected'];

    assignedRolesTags$: Observable<Tag[]>;
    availableRolesTags$: Observable<Tag[]>;
    lastRolesKeyword = '';
    rolesSelectorLabels: DpmTagCouplerLabels = {
        header: 'Ruoli Assegnati',
        searchBoxTitle: 'Ruoli Selezionabili',
        placeholder: 'Ricerca Ruoli',
        noTagsFallbackText: 'Nessun ruolo assegnato',
        optionalText: 'È possibile modificare i ruoli dell’utente tenendo presente che l’applicativo' +
        ' potrebbe modificarli automaticamente in base alle azioni/assiomi impostati'
    };

    assignedQualificationsTags$: Observable<Tag[]>;
    availableQualificationsTags$: Observable<Tag[]>;
    lastQualificationsKeyword = '';
    qualificationsSelectorLabels: DpmTagCouplerLabels = {
        header: 'Qualifiche assegnate',
        searchBoxTitle: 'Qualifiche Selezionabili',
        placeholder: 'Ricerca Qualifiche',
        noTagsFallbackText: 'Nessuna qualifica assegnata'
    };

    get modalService(): BsModalService {
        return this._modalService;
    }

    constructor(protected _userService: UserService,
                private _modalService: BsModalService,
                private _toastr: ToastsManager,
                vcr: ViewContainerRef) {
        super();
        _toastr.setRootViewContainerRef(vcr);
        this.dropdownOptions = this.singleRecordOptions;
    }

    ngOnInit() {
        this.activeTabSubscription = this.activeTab$
            .do(() => this.dropdownOptions = this.selectedRecords.length > 1 ? this.multipleRecordOptions : this.singleRecordOptions)
            .subscribe(t => {
                this.activeTab = t;

                if (this.records.length === 1) {
                    this.getTagCouplerUsersRolesTags();
                    this.getTagCouplerUsersQualificationsTags();
                }
            });
    }

    protected getTagCouplerUsersRolesTags(): void {
        this.getAvailableRoles();
        this.getAssignedRoles();
    }

    private getAvailableRoles(keyword: string = this.lastRolesKeyword): void {
        this.availableRolesTags$ = this._userService.getAvailableRoles(this.records[0].id, keyword)
            .map(this.mapRolesToTags);
    }

    private getAssignedRoles(): void {
        this.assignedRolesTags$ = this._userService.getAssignedRoles(this.records[0].id)
            .map(this.mapRolesToTags);
    }

    private mapRolesToTags(roles: Role[]): Tag[] {
        return roles.map(({ id, display_name: name, description: title }) => ({ id, name, title }));
    }

    onSearchRoleTag(keyword): void {
        this.lastRolesKeyword = keyword;
        this.getTagCouplerUsersRolesTags();
    }

    onToggleRoleTag(tag: Tag): void {
        this._userService.toggleRole(this.records[0].id, tag.id)
            .subscribe(() => {
                this.getTagCouplerUsersRolesTags();
                this._toastr.info('Modifiche effettuate con successo').then();
            });
    }

    protected getTagCouplerUsersQualificationsTags(): void {
        this.getAvailableQualifications();
        this.getAssignedQualifications();
    }

    private getAvailableQualifications(keyword: string = this.lastQualificationsKeyword): void {
        this.availableQualificationsTags$ = this._userService.getAvailableQualifications(this.records[0].id, keyword)
            .map(this.mapQualificationsToTags);
    }

    private getAssignedQualifications(): void {
        this.assignedQualificationsTags$ = this._userService.getAssignedQualifications(this.records[0].id)
            .map(this.mapQualificationsToTags);
    }

    private mapQualificationsToTags(qualifications: Qualification[]): Tag[] {
        return qualifications.map(({ id, name }) => ({ id, name, title: name }));
    }

    onSearchQualificationTag(keyword: string): void {
        this.lastQualificationsKeyword = keyword;
        this.getTagCouplerUsersQualificationsTags();
    }

    onToggleQualificationTag(tag: Tag): void {
        this._userService.toggleQualification(this.records[0].id, tag.id)
            .subscribe(() => {
                this.getTagCouplerUsersQualificationsTags();
                this._toastr.info('Modifiche effettuate con successo').then();
            });
    }

    protected initValidationErrors(): void {
        this.validationMessages.errors = {
            username: [],
            firstname: [],
            lastname: [],
            email: [],
            password: [],
            fiscal_code: [],
            sex: [],
            dismissal_date: [],
        };
    }

    toggleAccordion(index): void {
        this.accordionsVisibility[index] = this.accordionsVisibility[index] === 'selected'
            ? 'unSelected' : 'selected';
    }

    refreshTable(): void {
        this.updateTable.emit();
    }
}
