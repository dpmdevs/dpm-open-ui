/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';

import { BaseCrudPageComponent, GridDecorator } from '@dpm-components';
import { AnimationProvider, RendererProvider } from '@dpm-providers';
import { User } from '@dpm-models';
import { UserService } from '../../user/user.service';
import { UserDetailsComponent } from './user-details/user-details.component';
import { StateService } from '../../../state/state.service';


@GridDecorator({
    title: 'Lista Utenti',
    description: '',

    headers: [
        { displayName: 'avatar', columnName: 'avatar', sortable: false, searchable: false },
        { displayName: 'nome', columnName: 'firstname' },
        { displayName: 'cognome', columnName: 'lastname' },
        { displayName: 'username', columnName: 'username' },
        { displayName: 'indirizzo email', columnName: 'email' },
        { displayName: 'codice fiscale', columnName: 'fiscal_code' },
        {
            displayName: 'unità', columnName: 'working_units', renderer: RendererProvider.unitRenderer, sortable: false,
            searchColumn: 'units.name'
        },
        {
            displayName: 'qualifica', columnName: 'qualifications', renderer: RendererProvider.qualificationsRenderer,
            sortable: false, searchColumn: 'qualifications.name'
        },
    ],
    paginationLimit: 10,

    hasDetailPage: true
})
@Component({
    templateUrl: './users-table.component.html',
    styleUrls: ['../../../shared/components/dpm-crud-page/base-crud-page.scss'],
    animations: [AnimationProvider.getFadeIn()]
})
export class UsersTableComponent extends BaseCrudPageComponent {
    records: User[] = [];

    constructor(private _userService: UserService,
                private _stateService: StateService,
                protected _resolver: ComponentFactoryResolver,
                private _viewContainerRef: ViewContainerRef,
                private _toaster: ToastsManager) {
        super();
    }

    get defaultTab(): string {
        return 'dettagli';
    }

    get dataService(): UserService {
        return this._userService;
    }

    protected get stateService(): StateService {
        return this._stateService;
    }

    get resolver(): ComponentFactoryResolver {
        return this._resolver;
    }

    get viewChildComponent() {
        return UserDetailsComponent;
    }

    get viewContainerRef() {
        return this._viewContainerRef;
    }

    get toastr() {
        return this._toaster;
    }
}
