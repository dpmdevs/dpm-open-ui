/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { RoleService } from './role.service';
import { RolesComponent } from './roles.component';
import { RoleDetailsComponent } from './role-details/role-details.component';
import { NewRoleFormComponent } from './role-details/new-role-form/new-role-form.component';
import { UpdateRoleFormComponent } from './role-details/update-role-form/update-role-form.component';

export {
    RoleService,
    RolesComponent,
    RoleDetailsComponent,
    NewRoleFormComponent,
    UpdateRoleFormComponent
}
