/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, ViewContainerRef } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ToastsManager } from 'ng2-toastr';
import { BsModalService } from 'ngx-bootstrap';

import { BaseDetailsPage } from '@dpm-components';
import { AnimationProvider } from '@dpm-providers';
import { DpmTagCouplerHandler, DpmTagCouplerLabels, Role, Tag } from '@dpm-models';
import { RoleService } from '../role.service';
import { Permission } from '../permission';


@Component({
    selector: 'dpm-role-details',
    templateUrl: './role-details.component.html',
    styleUrls: ['../../../../shared/components/dpm-crud-page/base-details-page/base-details-page.scss'],
    animations: [AnimationProvider.getFadeIn()]
})
export class RoleDetailsComponent extends BaseDetailsPage implements DpmTagCouplerHandler {
    records: Role[];

    availableTags$: Observable<Tag[]>;
    assignedTags$: Observable<Tag[]>;
    lastKeyword = '';
    tagCouplerLabels: DpmTagCouplerLabels = {
        header: 'Permessi Assegnati',
        searchBoxTitle: 'Permessi Selezionabili',
        placeholder: 'Ricerca Permessi',
        noTagsFallbackText: 'Nessun permesso selezionato',
    };
    readonly singleRecordOptions = ['nuovo', 'dettagli', 'permessi', 'modifica'];
    readonly multipleRecordOptions = ['nuovo', 'dettagli', 'permessi'];

    get modalService(): BsModalService {
        return this._modalService;
    }

    constructor(protected _roleService: RoleService,
                private _modalService: BsModalService,
                private _toastr: ToastsManager,
                vcr: ViewContainerRef) {
        super();
        _toastr.setRootViewContainerRef(vcr);
        this.dropdownOptions = this.singleRecordOptions;
    }

    ngOnInit() {
        this.activeTabSubscription = this.activeTab$
            .do(() => this.dropdownOptions = this.selectedRecords.length > 1 ? this.multipleRecordOptions : this.singleRecordOptions)
            .subscribe(t => {
                this.activeTab = t;

                if (this.records.length === 1)
                    this.getTagCouplerRolesPermissionsTags();
            });
    }

    protected getTagCouplerRolesPermissionsTags(): void {
        this.getAvailablePermissions();
        this.getAssignedPermissions();
    }

    protected getAvailablePermissions(keyword: string = this.lastKeyword): void {
        this.availableTags$ = this._roleService.getAvailablePermissions(this.records[0].id, keyword)
            .map(this.mapPermissionsToTags);
    }

    protected getAssignedPermissions(): void {
        this.assignedTags$ = this._roleService.getAssignedPermissions(this.records[0].id)
            .map(this.mapPermissionsToTags);
    }

    mapPermissionsToTags(entities: Permission[]): Tag[] {
        return entities.map(({ id, display_name: name, description: title }) => ({ id, name, title }));
    }

    protected initValidationErrors(): void {
        this.validationMessages.errors = {
            name: []
        };
    }

    onToggleTag(tag: Tag): void {
        this._roleService.togglePermission(this.records[0].id, tag.id)
            .subscribe(() => {
                this.getTagCouplerRolesPermissionsTags();
                this._toastr.info('Modifiche effettuate con successo').then();
            });
    }

    onSearchTag(keyword: string): void {
        this.lastKeyword = keyword;
        this.getTagCouplerRolesPermissionsTags();
    }
}
