/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { DPOsComponent } from './dpos.component';
import { DPOsService } from './dpos.service';
import { NewDPOFormComponent } from './dpo-details/new-dpo-form/new-dpo-form.component';
import { UpdateDPOFormComponent } from './dpo-details/update-dpo-form/update-dpo-form.component';
import { DPODetailsComponent } from './dpo-details/dpo-details.component';


export {
    DPOsComponent,
    DPOsService,
    DPODetailsComponent,
    NewDPOFormComponent,
    UpdateDPOFormComponent,
};
