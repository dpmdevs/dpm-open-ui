/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Validators } from '@angular/forms';

import { DPMValidators } from '@dpm-helpers';


export abstract class DPOForm {
    controlsConfig;

    setControlsConfig({
                          company_name = '', company_number = '', firstname = '', lastname = '', title = '', email = '',
                          phone = '', address = '', certified_mail = ''
                      }) {
        this.controlsConfig = {
            company_name: [company_name, Validators.maxLength(255)],
            company_number: [company_number, Validators.maxLength(255)],
            firstname: [firstname, Validators.maxLength(255)],
            lastname: [lastname, Validators.maxLength(255)],
            title: [title, Validators.maxLength(255)],
            email: [email, [DPMValidators.required, DPMValidators.email, Validators.maxLength(255)]],
            phone: [phone, Validators.maxLength(255)],
            address: [address, Validators.maxLength(255)],
            certified_mail: [certified_mail, Validators.maxLength(255)],
        };
    }

}
