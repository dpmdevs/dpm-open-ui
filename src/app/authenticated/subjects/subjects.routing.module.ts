/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PermissionGuard } from '../../core/auth/guards/permission.guard';
import { UsersTableComponent } from './users';
import { RolesComponent } from './roles';
import { ExternalSuppliersComponent } from './third-parties';
import { UnitsComponent } from './units';
import { QualificationsComponent } from './qualifications';
import { ControllersComponent } from './controllers';
import { DPOsComponent } from './dpos';


const routes: Routes = [
    { path: '', redirectTo: '/', pathMatch: 'full' },
    {
        path: 'users',
        component: UsersTableComponent,
        canActivate: [PermissionGuard]
    },
    {
        path: 'roles',
        component: RolesComponent,
        canActivate: [PermissionGuard]
    },
    {
        path: 'third-parties',
        component: ExternalSuppliersComponent,
        canActivate: [PermissionGuard]
    },
    {
        path: 'units',
        component: UnitsComponent,
        canActivate: [PermissionGuard]
    },
    {
        path: 'qualifications',
        component: QualificationsComponent,
        canActivate: [PermissionGuard]
    },
    {
        path: 'controllers',
        component: ControllersComponent,
        canActivate: [PermissionGuard]
    },
    {
        path: 'dpos',
        component: DPOsComponent,
        canActivate: [PermissionGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class SubjectsRoutingModule {
}
