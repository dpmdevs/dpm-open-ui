/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';

import { BaseCrudPageComponent, GridDecorator } from '@dpm-components';
import { AnimationProvider } from '@dpm-providers';
import { Qualification } from '@dpm-models';
import { QualificationsService } from './qualifications.service';
import { QualificationDetailsComponent } from './qualification-details/qualification-details.component';
import { StateService } from '../../../state/state.service';


@GridDecorator({
    title: 'Lista Qualifiche',
    description: 'Elenco delle qualifiche disponibili per gli utenti',

    headers: [
        { displayName: 'id', columnName: 'id' },
        { displayName: 'nome', columnName: 'name' },
        { displayName: 'Data di creazione', columnName: 'created_at' }
    ],
    paginationLimit: 10,

    hasDetailPage: true
})
@Component({
    templateUrl: '../../../shared/components/dpm-crud-page/base-crud-page.html',
    styleUrls: ['../../../shared/components/dpm-crud-page/base-crud-page.scss'],
    animations: [AnimationProvider.getFadeIn()]
})
export class QualificationsComponent extends BaseCrudPageComponent {

    records: Qualification[] = [];

    constructor(private _qualificationsService: QualificationsService,
                private _stateService: StateService,
                protected _resolver: ComponentFactoryResolver,
                private _viewContainerRef: ViewContainerRef,
                private _toaster: ToastsManager) {
        super();
    }

    get defaultTab(): string {
        return 'dettagli';
    }

    get dataService(): QualificationsService {
        return this._qualificationsService;
    }

    protected get stateService(): StateService {
        return this._stateService;
    }

    get resolver(): ComponentFactoryResolver {
        return this._resolver;
    }

    get viewChildComponent() {
        return QualificationDetailsComponent;
    }

    get viewContainerRef() {
        return this._viewContainerRef;
    }

    get toastr() {
        return this._toaster;
    }

}
