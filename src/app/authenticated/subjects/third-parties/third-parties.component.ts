/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';

import { AnimationProvider } from '@dpm-providers';
import { BaseCrudPageComponent, GridDecorator, TableBaseService } from '@dpm-components';
import { ThirdPartiesService } from './third-parties.service';
import { ThirdPartiesDetailsComponent } from './third-parties-details/third-parties-details.component';
import { StateService } from '../../../state/state.service';


@GridDecorator({
    title: 'Terze parti',
    description: 'Elenco delle organizzazioni coinvolte nelle attività di trattamento dei dati come ad esempio i' +
    ' fornitori o i soggetti terzi a cui vengono comunicati i dati.',

    headers: [
        { displayName: 'id', columnName: 'id', searchable: false },
        { displayName: 'ragione sociale', columnName: 'company_name' },
        { displayName: 'descrizione', columnName: 'description' },
        { displayName: 'responsabile', columnName: 'processor_name' },
    ],
    paginationLimit: 10,

    hasDetailPage: true
})
@Component({
    templateUrl: '../../../shared/components/dpm-crud-page/base-crud-page.html',
    styleUrls: ['../../../shared/components/dpm-crud-page/base-crud-page.scss'],
    animations: [AnimationProvider.getFadeIn()]
})
export class ExternalSuppliersComponent extends BaseCrudPageComponent {

    constructor(private _externalProcessorsService: ThirdPartiesService,
                private _stateService: StateService,
                private _resolver: ComponentFactoryResolver,
                private _viewContainerRef: ViewContainerRef,
                private _toaster: ToastsManager) {
        super();
    }

    protected get defaultTab(): string {
        return 'dettagli';
    }

    protected get dataService(): TableBaseService {
        return this._externalProcessorsService;
    }

    protected get stateService(): StateService {
        return this._stateService;
    }

    protected get resolver(): ComponentFactoryResolver {
        return this._resolver;
    }

    protected get viewChildComponent() {
        return ThirdPartiesDetailsComponent;
    }

    protected get viewContainerRef() {
        return this._viewContainerRef;
    }

    protected get toastr() {
        return this._toaster;
    }
}
