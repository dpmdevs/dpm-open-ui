/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';
import { Observable } from 'rxjs/Observable';
import { BsModalService } from 'ngx-bootstrap';

import { Application, DpmTagCouplerHandler, DpmTagCouplerLabels, Tag, ThirdParties } from '@dpm-models';
import { AnimationProvider } from '@dpm-providers';
import { BaseDetailsPage } from '@dpm-components';
import { ThirdPartiesService } from '../third-parties.service';


@Component({
    selector: 'dpm-external-processors-details',
    templateUrl: './third-parties-details.component.html',
    styleUrls: ['../../../../shared/components/dpm-crud-page/base-details-page/base-details-page.scss'],
    animations: [AnimationProvider.getFadeIn()]
})
export class ThirdPartiesDetailsComponent extends BaseDetailsPage implements DpmTagCouplerHandler {

    records: ThirdParties[];

    availableTags$: Observable<Tag[]>;
    assignedTags$: Observable<Tag[]>;
    lastKeyword = '';
    tagCouplerLabels: DpmTagCouplerLabels = {
        header: 'Applicativi Assegnati',
        searchBoxTitle: 'Applicativi Selezionabili',
        placeholder: 'Ricerca Applicativi',
        noTagsFallbackText: 'Nessun applicativo selezionato',
    };

    readonly singleRecordOptions = ['nuovo', 'dettagli', 'modifica'];
    readonly multipleRecordOptions = ['nuovo', 'dettagli'];

    get modalService(): BsModalService {
        return this._modalService;
    }

    constructor(protected _thirdPartiesService: ThirdPartiesService,
                private _modalService: BsModalService,
                private _toastr: ToastsManager,
                vcr: ViewContainerRef) {
        super();
        _toastr.setRootViewContainerRef(vcr);
        this.dropdownOptions = this.singleRecordOptions;
    }

    ngOnInit() {
        this.activeTabSubscription = this.activeTab$
            .do(() => this.dropdownOptions = this.selectedRecords.length > 1 ? this.multipleRecordOptions : this.singleRecordOptions)
            .subscribe(t => {
                this.activeTab = t;

                if (this.records.length === 1)
                    this.getTagCouplerThirdPartiesApplicationsTags();
            });
    }

    getTagCouplerThirdPartiesApplicationsTags(): void {
        this.getAvailableApplications();
        this.getAssignedApplications();
    }

    getAvailableApplications(keyword: string = this.lastKeyword): void {
        this.availableTags$ = this._thirdPartiesService.getAvailableApplications(this.records[0].id, keyword)
            .map(this.mapApplicationsToTags);
    }

    getAssignedApplications(): void {
        this.assignedTags$ = this._thirdPartiesService.getAssignedApplications(this.records[0].id)
            .map(this.mapApplicationsToTags);
    }

    mapApplicationsToTags(applications: Application[]): Tag[] {
        return applications.map(({ id, name, description: title }) => ({ id, name, title }));
    }

    onSearchTag(keyword: string): void {
        this.lastKeyword = keyword;
        this.getTagCouplerThirdPartiesApplicationsTags();
    }

    onToggleTag(tag: Tag): void {
        this._thirdPartiesService.toggleApplication(this.records[0].id, tag.id)
            .subscribe(() => {
                this.getTagCouplerThirdPartiesApplicationsTags();
                this._toastr.info('Modifiche effettuate con successo').then();
            });
    }

    protected initValidationErrors(): void {
        this.validationMessages.errors = {
            company_name: []
        };
    }

}
