/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { TableBaseService } from '@dpm-components';
import { Application } from '@dpm-models';
import { environment } from 'environments/environment';

import { ExternalProcessor } from '@dpm-models';


@Injectable()
export class ThirdPartiesService extends TableBaseService {

    get baseUrl(): string {
        return environment.apiUrl;
    }

    get table(): string {
        return 'third-parties';
    }

    toggleApplication(thirdPartyId: number, applicationId: number): Observable<Response> {
        return this.http.put<Response>(`${this.baseUrl}${this.table}/${thirdPartyId}/applications`,
            { applications: [applicationId] });
    }

    getAssignedApplications(thirdPartyId: number): Observable<Application[]> {
        return this.http.get<Application[]>(`${this.baseUrl}${this.table}/${thirdPartyId}/applications`);
    }

    getAvailableApplications(thirdPartyId: number, keyword?: string): Observable<Application[]> {
        const url = `${this.baseUrl}${this.table}/${thirdPartyId}/available-applications?sortField=name&sortDirection=asc`
            + (
                keyword !== ''
                    ? `&searchCriteria=${ JSON.stringify([
                        { columnName: 'name', keyword }
                    ]) }`
                    : ''
            );

        return this.http.get<Application[]>(url);
    }

    getExternalProcessors(keyword = ''): Observable<ExternalProcessor[]> {
        const url = `${this.baseUrl}${this.table}/external-processors`
            + (keyword !== ''
                    ? `?searchOrCriteria=${JSON.stringify([
                        { columnName: 'company_name', keyword },
                        { columnName: 'processor_name', keyword }
                    ])}`
                    : ''
            );

        return this.http.get<ExternalProcessor[]>(url);
    }

}
