/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';

import { BaseCrudPageComponent, GridDecorator } from '@dpm-components';
import { AnimationProvider } from '@dpm-providers';
import { Controller } from '@dpm-models';
import { ControllersService } from './controllers.service';
import { ControllerDetailsComponent } from './controller-details/controller-details.component';
import { StateService } from '../../../state/state.service';


@GridDecorator({
    title: 'Lista Titolari',
    description: 'Elenco dei titolari disponibili per i trattamenti',

    headers: [
        { displayName: 'Ragione sociale', columnName: 'company_name' },
        { displayName: 'Partita IVA', columnName: 'company_number' },
        { displayName: 'Nome titolare', columnName: 'holder_firstname' },
        { displayName: 'Cognome titolare', columnName: 'holder_lastname' },
        { displayName: 'Titolo del titolare', columnName: 'holder_title' },
        { displayName: 'Carica del titolare', columnName: 'holder_qualification' },
        { displayName: 'Indirizzo', columnName: 'address' },
    ],
    paginationLimit: 10,

    hasDetailPage: true
})
@Component({
    templateUrl: './controllers.component.html',
    styleUrls: ['../../../shared/components/dpm-crud-page/base-crud-page.scss'],
    animations: [AnimationProvider.getFadeIn()]
})
export class ControllersComponent extends BaseCrudPageComponent {

    records: Controller[] = [];

    constructor(private _controllersService: ControllersService,
                private _stateService: StateService,
                protected _resolver: ComponentFactoryResolver,
                private _viewContainerRef: ViewContainerRef,
                private _toaster: ToastsManager) {
        super();
    }

    get defaultTab(): string {
        return 'dettagli';
    }

    get dataService(): ControllersService {
        return this._controllersService;
    }

    protected get stateService(): StateService {
        return this._stateService;
    }

    get resolver(): ComponentFactoryResolver {
        return this._resolver;
    }

    get viewChildComponent() {
        return ControllerDetailsComponent;
    }

    get viewContainerRef() {
        return this._viewContainerRef;
    }

    get toastr() {
        return this._toaster;
    }

}
