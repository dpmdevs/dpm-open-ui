/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { FormGroup, Validators } from '@angular/forms';

import { Controller } from '@dpm-models';
import { DPMValidators } from '@dpm-helpers';


export abstract class ControllerForm {

    controlsConfig;
    recordInfo: FormGroup;

    setControlsConfig({
                          company_name = '', company_number = '', holder_firstname = '', holder_lastname = '',
                          holder_title = '', holder_qualification = '', address = '', city = '', zip_code = '',
                          phone = '', fax = '', email = '', web = '', fiscal_code = ''
                      }: Controller) {
        this.controlsConfig = {
            company_name: [company_name, DPMValidators.required],
            email: [email, [DPMValidators.email, Validators.maxLength(255)]],
            fiscal_code: [fiscal_code, Validators.maxLength(255)],
            company_number: [company_number, Validators.maxLength(255)],
            holder_firstname: [holder_firstname, Validators.maxLength(255)],
            holder_lastname: [holder_lastname, Validators.maxLength(255)],
            holder_title: [holder_title, Validators.maxLength(255)],
            holder_qualification: [holder_qualification, Validators.maxLength(255)],
            address: [address, Validators.maxLength(255)],
            city: [city, Validators.maxLength(255)],
            zip_code: [zip_code, Validators.maxLength(20)],
            phone: [phone, Validators.maxLength(255)],
            fax: [fax, Validators.maxLength(255)],
            web: [web, Validators.maxLength(255)]
        };
    }

}
