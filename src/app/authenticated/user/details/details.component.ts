/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, EventEmitter, Input, Output, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ToastsManager } from 'ng2-toastr';

import { Qualification, Unit, User } from '@dpm-models';
import { RendererProvider } from '@dpm-providers';
import { DPMValidators, matchOtherValidator } from '@dpm-helpers';
import { UserService } from '../user.service';


@Component({
    selector: 'dpm-user-details',
    templateUrl: './details.component.html',
    styleUrls: ['./details.component.scss']
})
export class DetailsComponent {

    @Input() user: User;
    @Output() requestPasswordChange = new EventEmitter<string[]>();

    public RendererProvider = RendererProvider;
    modalRef: BsModalRef;
    passwordForm: FormGroup;

    constructor(private modalService: BsModalService,
                private userService: UserService,
                private toastr: ToastsManager,
                formBuilder: FormBuilder) {
        this.passwordForm = formBuilder.group({
            current_password: ['', Validators.required],
            password: ['', [DPMValidators.password, Validators.required]],
            confirm_password: ['', [matchOtherValidator('password'), Validators.required]]
        });
    }

    changePassword(): void {
        const { valid, value: { current_password, password } } = this.passwordForm;

        if (!valid) return;

        this.userService.changePassword(this.user.id, current_password, password)
            .subscribe(
                () => {
                    this.toastr.success('Cambio password effettuato correttamente').then();
                    this.modalRef.hide();
                },
                (error: HttpErrorResponse) => {
                    this.toastr.error(error.error, 'Errore', { dismiss: 'click' }).then();
                    this.passwordForm.reset();
                }
            );
    }

    openModal(template: TemplateRef<any>): void {
        this.modalRef = this.modalService.show(template);
    }

    unitRenderer(units: Unit[]): string {
        return RendererProvider.unitRenderer(units);
    }

    qualificationsRenderer(qualifications: Qualification[]): string {
        return RendererProvider.qualificationsRenderer(qualifications);
    }

}
