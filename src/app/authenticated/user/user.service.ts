/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { environment } from 'environments/environment';
import { TableBaseService } from '@dpm-components/dpm-crud-page';
import { Qualification, Role, TableData, Unit, User } from '@dpm-models';
import { DpmConstants } from '@dpm-helpers';
import { UserStatistics } from './user-statistics';
import { PermissionResponse } from './permission-response';


@Injectable()
export class UserService extends TableBaseService {

    get baseUrl(): string {
        return environment.apiUrl;
    }

    get table(): string {
        return 'users';
    }

    get usersUrl(): string {
        return this.baseUrl + 'users/';
    }

    getLoggedUser(): Observable<User> {
        return this.http.get<User>(this.baseUrl + 'user')
            .map(user => {
                if (!user.avatar || user.avatar === '')
                    user.avatar = DpmConstants.USER_ICON_PATH;

                return user;
            });
    }

    toggleRole(userId: number, roleId: number): Observable<Response> {
        return this.http.put<Response>(`${this.usersUrl}${userId}/role/${roleId}`, {});
    }

    getAssignedRoles(userId: number): Observable<Role[]> {
        return this.http.get<Role[]>(`${this.usersUrl}${userId}/roles`);
    }

    getAvailableRoles(userId: number, keyword?: string): Observable<Role[]> {
        const url = `${this.usersUrl}${userId}/available-roles?sortField=display_name&sortDirection=asc`
            + (
                keyword !== ''
                    ? `&searchCriteria=${ JSON.stringify([
                        { columnName: 'display_name', keyword }
                    ]) }`
                    : ''
            );

        return this.http.get<Role[]>(url);
    }

    toggleQualification(userId: number, qualificationId: number): Observable<Response> {
        return this.http.put<Response>(`${this.usersUrl}${userId}/qualifications`, { qualificationIds: [qualificationId] });
    }

    getAssignedQualifications(userId: number): Observable<Qualification[]> {
        return this.http.get<Qualification[]>(`${this.usersUrl}${userId}/qualifications`);
    }

    getAvailableQualifications(userId: number, keyword?: string): Observable<Qualification[]> {
        const url = `${this.usersUrl}${userId}/available-qualifications?sortField=name&sortDirection=asc`
            + (
                keyword !== ''
                    ? `&searchCriteria=${ JSON.stringify([
                        { columnName: 'name', keyword }
                    ]) }`
                    : ``
            );

        return this.http.get<Qualification[]>(url);
    }

    getUserStatistics(): Observable<UserStatistics> {
        return this.http.get<UserStatistics>(`${this.usersUrl}statistics`);
    }

    checkPermission(route: string): Observable<boolean> {
        return this.http.post<PermissionResponse>(this.usersUrl + 'check-permission', { route })
            .map((permission: PermissionResponse) => permission.canActivate)
            .do(canActivate => {
                if (!canActivate) console.warn('Permission denied for route ' + route);
            });
    }

    getDirectedUnits(): Observable<Unit[]> {
        return this.http.get(`${this.usersUrl}directed-units`)
            .map((tableData: TableData) => <Unit[]>tableData.data);
    }

    changePassword(userId: number, current_password: string, password: string): Observable<Response> {
        return this.http.post<Response>(`${this.usersUrl}${userId}/change-pwd`, { current_password, password });
    }

}
