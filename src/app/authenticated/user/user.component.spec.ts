/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable } from 'rxjs/Observable';

import { User } from '@dpm-models';
import { SharedModule } from '../../shared/shared.module';
import { UserStub } from '../../shared/test-stubs/user.stub';
import { CoreModule } from '../../core/core.module';
import { UserComponent } from './user.component';
import { UserService } from './user.service';


const testUser$: Observable<User> = Observable.of({}).map(() => UserStub.get());

describe('UserComponent', () => {
    let component: UserComponent;
    let fixture: ComponentFixture<UserComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                CoreModule,
                SharedModule,
                RouterTestingModule
            ],
            providers: [
                UserService
            ],
            declarations: [UserComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(UserComponent);
        component = fixture.componentInstance;

        // UserService actually injected into the component
        const userService = fixture.debugElement.injector.get(UserService);

        // Setup spy on the `getQuote` method
        spyOn(userService, 'getLoggedUser')
            .and.returnValue(testUser$);

        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
