/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';

import { User } from '@dpm-models';
import { AnimationProvider } from '@dpm-providers';
import { StateService } from '../../state/state.service';
import { UserService } from './user.service';


@Component({
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.scss'],
    animations: [AnimationProvider.getFadeIn()],
})
export class UserComponent implements OnInit {

    user: User;

    constructor(private stateService: StateService,
                private userService: UserService,
                private toastr: ToastsManager,
                vcr: ViewContainerRef) {
        toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit() {
        this.stateService.getUser()
            .filter(u => u.id >= 0)
            .do(u => this.user = u)
            .subscribe();
    }

}
