/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';

import { AnimationProvider } from '@dpm-providers';
import { BaseCrudPageComponent, GridDecorator } from '@dpm-components';
import { OperatingSystem } from '@dpm-models';
import { OperatingSystemsService } from './operating-systems.service';
import { OperatingSystemDetailsComponent } from './operating-system-details/operating-system-details.component';
import { StateService } from '../../../state/state.service';


@GridDecorator({
    title: 'Sistemi Operativi',
    description: '',

    headers: [
        { displayName: 'id', columnName: 'id' },
        { displayName: 'nome', columnName: 'name' },
        { displayName: 'server/client', columnName: 'is_server', renderer: isServer => isServer ? 'Server' : 'Client' }
    ],
    paginationLimit: 10,

    hasDetailPage: true
})
@Component({
    templateUrl: '../../../shared/components/dpm-crud-page/base-crud-page.html',
    styleUrls: ['../../../shared/components/dpm-crud-page/base-crud-page.scss'],
    animations: [AnimationProvider.getFadeIn()]
})
export class OperatingSystemsComponent extends BaseCrudPageComponent {

    records: OperatingSystem[] = [];

    constructor(private operatingSystemsService: OperatingSystemsService,
                private _stateService: StateService,
                private _resolver: ComponentFactoryResolver,
                private _viewContainerRef: ViewContainerRef,
                private _toaster: ToastsManager) {
        super();
    }

    get defaultTab(): string {
        return 'dettagli';
    }

    get dataService(): OperatingSystemsService {
        return this.operatingSystemsService;
    }

    protected get stateService(): StateService {
        return this._stateService;
    }

    get resolver(): ComponentFactoryResolver {
        return this._resolver;
    }

    get viewChildComponent() {
        return OperatingSystemDetailsComponent;
    }

    get viewContainerRef() {
        return this._viewContainerRef;
    }

    get toastr() {
        return this._toaster;
    }

}
