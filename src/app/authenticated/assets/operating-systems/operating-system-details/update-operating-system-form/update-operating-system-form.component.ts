/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';

import { MultipleInheritanceHelper } from '@dpm-helpers';
import { OperatingSystem } from '@dpm-models';
import { UpdateForm } from '@dpm-components/dpm-crud-page/base-details-page/forms/update-form';
import { OperatingSystemForm } from '../operating-system-form';


@Component({
    selector: 'dpm-update-operating-system-form',
    templateUrl: './update-operating-system-form.component.html',
    styleUrls: ['../../../../../shared/components/dpm-crud-page/base-details-page/forms/base-form.scss']
})
export class UpdateOperatingSystemFormComponent extends UpdateForm<OperatingSystem> implements OperatingSystemForm {

    setControlsConfig: (record) => void;

    constructor(private _formBuilder: FormBuilder) {
        super();
    }

    get formBuilder(): FormBuilder {
        return this._formBuilder;
    }

    get is_server(): boolean {
        return this.recordInfo.controls.is_server.value;
    }

    set is_server(value: boolean) {
        const formControl = this.recordInfo.controls.is_server;
        formControl.setValue(value);
        formControl.markAsDirty();
        formControl.markAsTouched();
    }

}
MultipleInheritanceHelper.applyMixins(UpdateOperatingSystemFormComponent, [OperatingSystemForm]);
