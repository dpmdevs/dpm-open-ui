/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';

import { environment } from 'environments/environment';
import { TableBaseService } from '@dpm-components';
import { SecurityMeasure, SecurityMeasureCategory, TableData } from '@dpm-models';


@Injectable()
export class SecurityMeasuresService extends TableBaseService {

    get baseUrl(): string {
        return environment.apiUrl;
    }

    get table(): string {
        return 'sec-measures-cat';
    }

    getSecurityMeasuresCategoriesWithSecurityMeasures(family: 'tec' | 'org'): Observable<any[]> {
        /**
         * [
         *   [ category, category, category, category, ... ],
         *   [ sMeasure, sMeasure, sMeasure, sMeasure, ... ]
         * ]
         */
        return this.getSecurityMeasuresCategories(family)
            .mergeMap((categories: any[]) => {
                return Observable
                    .forkJoin(...categories.map(c => this.getSecurityMeasures(c.id)))
                    .map((securityMeasures: any[]) => [categories, securityMeasures]);
            });
    }

    getSecurityMeasuresCategories(family: 'org' | 'tec'): Observable<SecurityMeasureCategory[]> {
        return this.http.get(`${this.baseUrl}${this.table}/${family}`)
            .map((response: TableData) => <SecurityMeasureCategory[]>response.data);
    }

    getSecurityMeasures(category: number): Observable<SecurityMeasure[]> {
        return this.http.get<SecurityMeasure[]>(`${this.baseUrl}${this.table}/${category}/sec-measures`);
    }


    updateSecurityMeasure(id: number, { name, security_level, is_adopted }: SecurityMeasure): Observable<void> {
        return this.http.put<void>(
            `${this.baseUrl}${this.table}/sec-measures/${id}`,
            { name, security_level, is_adopted }
        );
    }

    deleteSecurityMeasure(id: number): Observable<void> {
        return this.http.delete<void>(`${this.baseUrl}${this.table}/sec-measures/${id}`);
    }

    restoreSecurityMeasure(id: number): Observable<void> {
        return this.http.put<void>(`${this.baseUrl}${this.table}/sec-measures/${id}/restore`, {});
    }

    addSecurityMeasure(category: number, { name, security_level, security_measures_categories_id, is_adopted }: SecurityMeasure): Observable<void> {
        return this.http.post<void>(
            `${this.baseUrl}${this.table}/${category}/sec-measures`,
            { name, security_level, security_measures_categories_id, is_adopted });
    }

    addSecurityMeasureCategory(family: 'org' | 'tec', { name, description }: SecurityMeasureCategory) {
        return this.http.post<void>(
            `${this.baseUrl}${this.table}/${family}`,
            { name, description, family });
    }

    applyAdoptedSecurityMeasures(): Observable<Response> {
        return this.http.post<Response>(`${this.baseUrl}${this.table}/apply-adopted-security-measures`, {});
    }

}
