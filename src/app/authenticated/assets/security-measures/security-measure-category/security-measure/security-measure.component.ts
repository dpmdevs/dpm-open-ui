/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { SecurityMeasure } from '@dpm-models';
import { isUndefined } from 'util';


@Component({
    selector: '[dpmSecurityMeasure]',
    templateUrl: './security-measure.component.html',
    styleUrls: ['./security-measure.component.scss']
})
export class SecurityMeasureComponent implements OnInit {


    @Input() securityMeasure: SecurityMeasure;

    @Output() onToggle = new EventEmitter<SecurityMeasure>();
    @Output() onUpdate = new EventEmitter<SecurityMeasure>();
    @Output() startedEditing = new EventEmitter<SecurityMeasure>();

    public isNameEditingComplete = true;

    public selectedSecurityLevel: number;
    public selectedSecurityAdopted: string = 'adopted';

    ngOnInit() {
        this.selectedSecurityLevel = this.securityMeasure.security_level;
        this.selectedSecurityAdopted = this.securityMeasure.is_adopted ? 'adopted' : 'non-adopted';
    }

    toggle() {
        this.onToggle.emit(this.securityMeasure);
    }

    onEditStart(): void {
        this.startedEditing.emit(this.securityMeasure);
        this.isNameEditingComplete = false;
    }

    emitUpdate(name: string): void {
        if (name !== this.securityMeasure.name && !isUndefined(name))
            this.onUpdate.emit(Object.assign(
                {}, this.securityMeasure, { name }
            ));
    }

    emitUpdateSecurityLevel() {
        this.onUpdate.emit(<SecurityMeasure>Object.assign(
            {}, this.securityMeasure, { security_level: this.selectedSecurityLevel }
        ));
    }

    emitUpdateSecurityAdopted() {
        this.onUpdate.emit(<SecurityMeasure>Object.assign(
            {}, this.securityMeasure, { is_adopted: this.selectedSecurityAdopted === 'adopted' }
        ));
    }
}
