/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { isNull } from 'util';
import { ToastsManager } from 'ng2-toastr';

import { SecurityMeasure, SecurityMeasureCategory } from '@dpm-models';
import { AnimationProvider } from '@dpm-providers';
import { SecurityMeasuresService } from '../security-measures.service';


@Component({
    selector: '[dpmSecurityMeasureCategory]',
    templateUrl: './security-measure-category.component.html',
    styleUrls: ['./security-measure-category.component.scss'],
    animations: [AnimationProvider.getFadeIn()]
})
export class SecurityMeasureCategoryComponent implements OnInit {

    @Input() securityMeasureCategory: SecurityMeasureCategory;

    securityMeasures$: Observable<SecurityMeasure[]>;
    isNewDataInsertionActive = false;
    newName = '';
    newSecurityLevel = 1;
    newIsAdopted: boolean = false;

    constructor(private _securityMeasuresService: SecurityMeasuresService, private _toasts: ToastsManager) {
    }

    ngOnInit() {
        this.createSecMeasureSubscription();
    }

    addNew() {
        if (this.newName === '') return;
        if (this.newSecurityLevel === null) return;
        if (this.newIsAdopted === null) return;

        const { newName: name, newSecurityLevel: security_level, securityMeasureCategory: { id: security_measures_categories_id }, newIsAdopted: is_adopted = false } = this;

        this._securityMeasuresService.addSecurityMeasure(
            this.securityMeasureCategory.id, { name, security_level, security_measures_categories_id, is_adopted }
        )
            .subscribe(() => {
                this.newName = '';
                this.newSecurityLevel = 1;
                this.newIsAdopted = false;
                this.toggleInsertion();
                this.createSecMeasureSubscription();
                this._toasts.success('Record inserito correttamente');
            });
    }

    private createSecMeasureSubscription() {
        this.securityMeasures$ = this._securityMeasuresService
            .getSecurityMeasures(this.securityMeasureCategory.id);
    }

    onUpdate({ id, name, security_level, security_measures_categories_id, is_adopted }: SecurityMeasure): void {
        this._securityMeasuresService.updateSecurityMeasure(id, {
            name,
            security_level,
            security_measures_categories_id,
            is_adopted
        })
            .subscribe(() => this._toasts.info('Record aggiornato correttamente'),
                () => this._toasts.error('Errore durante l’aggiornamento'),
                () => this.createSecMeasureSubscription()
            );
    }

    onToggle(securityMeasure: SecurityMeasure): void {

        if (this.isSecurityMeasureEnabled(securityMeasure))
            this.disableSecurityMeasure(securityMeasure);
        else
            this.enableSecurityMeasure(securityMeasure);
    }

    toggleInsertion() {
        this.isNewDataInsertionActive = !this.isNewDataInsertionActive;
    }

    private isSecurityMeasureEnabled(securityMeasure: SecurityMeasure): boolean {
        return isNull(securityMeasure.deleted_at);
    }

    private disableSecurityMeasure({ id }: SecurityMeasure) {
        this._securityMeasuresService.deleteSecurityMeasure(id)
            .subscribe(() => {
                this.createSecMeasureSubscription();
                this._toasts.info('Record aggiornato correttamente');
            });
    }

    private enableSecurityMeasure({ id }: SecurityMeasure) {
        this._securityMeasuresService.restoreSecurityMeasure(id)
            .subscribe(() => {
                this.createSecMeasureSubscription();
                this._toasts.info('Record aggiornato correttamente');
            });
    }
}
