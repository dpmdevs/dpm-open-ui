/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DPMValidators } from '@dpm-helpers';


@Component({
    selector: 'dpm-new-security-measure-category-form',
    templateUrl: './new-security-measure-category-form.component.html',
    styleUrls: ['../../../../../shared/components/dpm-crud-page/base-details-page/forms/base-form.scss']
})
export class NewSecurityMeasureCategoryFormComponent implements OnInit {

    @Output() onCreate = new EventEmitter();

    securityMeasureForm: FormGroup;

    constructor(private _formBuilder: FormBuilder) {
    }

    ngOnInit() {
        this.securityMeasureForm = this._formBuilder.group({
            name: ['', [DPMValidators.required, Validators.minLength(3)]],
            description: ''
        });
    }

    onSubmit({ controls }): void {
        if (this.securityMeasureForm.invalid)
            return;

        const { name, description } = controls;
        this.onCreate.emit({
            name: name.value,
            description: description.value
        });
        this.securityMeasureForm.reset();
    }
}
