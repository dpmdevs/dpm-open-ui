/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ToastsManager } from 'ng2-toastr';

import { AnimationProvider } from '@dpm-providers';
import { SecurityMeasureCategory } from '@dpm-models';
import { SecurityMeasuresService } from './security-measures.service';


@Component({
    selector: 'dpm-security-measures',
    templateUrl: './security-measures.component.html',
    styleUrls: ['./security-measures.component.scss'],
    animations: [AnimationProvider.getFadeIn()]
})
export class SecurityMeasuresComponent implements OnInit {
    tecSecurityMeasuresCategory$: Observable<SecurityMeasureCategory[]>;
    orgSecurityMeasuresCategory$: Observable<SecurityMeasureCategory[]>;

    loadedOrgCategories = false;
    loadedTecCategories = false;

    constructor(private securityMeasuresService: SecurityMeasuresService,
                private vcr: ViewContainerRef,
                private toastr: ToastsManager) {
        toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit() {
        this.initTecSecMeasureSubscription();
        this.iniOrgSecMeasureSubscription();
    }

    private iniOrgSecMeasureSubscription() {
        this.orgSecurityMeasuresCategory$ = this.securityMeasuresService
            .getSecurityMeasuresCategories('org')
            .do(() => this.loadedOrgCategories = false)
            .finally(() => this.loadedOrgCategories = true);
    }

    private initTecSecMeasureSubscription() {
        this.tecSecurityMeasuresCategory$ = this.securityMeasuresService
            .getSecurityMeasuresCategories('tec')
            .do(() => this.loadedTecCategories = false)
            .finally(() => this.loadedTecCategories = true);
    }

    createOrgSecurityMeasureCategory(smc: SecurityMeasureCategory) {
        this.securityMeasuresService
            .addSecurityMeasureCategory('org', smc)
            .subscribe(() => this.iniOrgSecMeasureSubscription());
    }

    createTecSecurityMeasureCategory(smc: SecurityMeasureCategory) {
        this.securityMeasuresService
            .addSecurityMeasureCategory('tec', smc)
            .subscribe(() => this.initTecSecMeasureSubscription());
    }

    applyAdoptedSecurityMeasures(): void {
        this.securityMeasuresService.applyAdoptedSecurityMeasures()
            .subscribe(
                () => this.toastr.success('Misure di sicurezza applicate con successo').then(),
                () => this.toastr.error('').then()
            );
    }

}
