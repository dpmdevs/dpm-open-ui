/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { NgModule } from '@angular/core';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDropdownModule, TooltipModule } from 'ngx-bootstrap';

import { AssetsRoutingModule } from './assets.routing.module';
import { SharedModule } from '../../shared/shared.module';
import { DPMCrudPageModule } from '../../shared/components/dpm-crud-page/dpm-crud-page.module';
import { AssetsComponent } from './assets.component';


import {
    DataSubjectsComponent,
    DataSubjectsDetailsComponent,
    DataSubjectsService,
    NewDataSubjectFormComponent,
    UpdateDataSubjectFormComponent
} from './data-subjects';

import {
    DataCategoriesComponent,
    DataCategoriesService,
    DataCategoryComponent,
    DataTypeComponent,
    NewDataCategoryFormComponent,
    NewDataTypeComponent
} from './data-categories';

import {
    ApplicationDetailsComponent,
    ApplicationsComponent,
    ApplicationService,
    NewApplicationFormComponent,
    UpdateApplicationFormComponent
} from './applications';

import {
    RetentionsComponent,
    RetentionsDetailsComponent,
    RetentionService,
    NewRetentionFormComponent,
    UpdateRetentionFormComponent,
} from './retentions';

import {
    NewSecurityMeasureCategoryFormComponent,
    SecurityMeasureCategoryComponent,
    SecurityMeasureComponent,
    SecurityMeasuresComponent,
    SecurityMeasuresService
} from './security-measures';

import {
    ServerComponent,
    ServerDetailsComponent,
    NewServerFormComponent,
    UpdateServerFormComponent,
    ServerService
} from './server';

import {
    ControlsComponent,
    ControlsService,
    ControlsDetailsComponent,
    NewControlFormComponent,
    UpdateControlFormComponent
} from './controls';

import {
    OperatingSystemsComponent,
    OperatingSystemsService,
    OperatingSystemDetailsComponent,
    NewOperatingSystemFormComponent,
    UpdateOperatingSystemFormComponent,
} from './operating-systems';


@NgModule({
    imports: [
        SharedModule,
        DPMCrudPageModule,
        AssetsRoutingModule,
        TabsModule.forRoot(),
        BsDropdownModule.forRoot(),
        TooltipModule.forRoot()
    ],
    entryComponents: [
        DataSubjectsDetailsComponent,
        DataCategoryComponent,
        ApplicationDetailsComponent,
        RetentionsDetailsComponent,
        ServerDetailsComponent,
        ControlsDetailsComponent,
        OperatingSystemDetailsComponent,
    ],
    declarations: [
        AssetsComponent,

        DataSubjectsComponent,
        DataSubjectsDetailsComponent,
        NewDataSubjectFormComponent,
        UpdateDataSubjectFormComponent,

        DataCategoriesComponent,
        DataCategoryComponent,
        NewDataCategoryFormComponent,
        DataTypeComponent,
        NewDataTypeComponent,

        ApplicationsComponent,
        ApplicationDetailsComponent,
        NewApplicationFormComponent,
        UpdateApplicationFormComponent,

        RetentionsComponent,
        RetentionsDetailsComponent,
        NewRetentionFormComponent,
        UpdateRetentionFormComponent,

        SecurityMeasuresComponent,
        SecurityMeasureCategoryComponent,
        SecurityMeasureComponent,
        NewSecurityMeasureCategoryFormComponent,

        ServerComponent,
        ServerDetailsComponent,
        NewServerFormComponent,
        UpdateServerFormComponent,

        ControlsComponent,
        ControlsDetailsComponent,
        NewControlFormComponent,
        UpdateControlFormComponent,

        OperatingSystemsComponent,
        OperatingSystemDetailsComponent,
        NewOperatingSystemFormComponent,
        UpdateOperatingSystemFormComponent,
    ],
    providers: [
        DataSubjectsService,
        DataCategoriesService,
        ApplicationService,
        RetentionService,
        SecurityMeasuresService,
        ServerService,
        ControlsService,
        OperatingSystemsService,
    ],
})
export class AssetsModule {
}
