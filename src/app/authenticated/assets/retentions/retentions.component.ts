/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';

import { BaseCrudPageComponent, GridDecorator, TableBaseService } from '@dpm-components';
import { AnimationProvider } from '@dpm-providers';
import { Retention } from '@dpm-models';
import { RetentionService } from './retention.service';
import { RetentionsDetailsComponent } from './retentions-details/retentions-details.component';
import { StateService } from '../../../state/state.service';


@GridDecorator({
    title: 'Periodo di conservazione',
    description: '«Principio di limitazione della conservazione»: I dati personali sono conservati in una forma che' +
    ' consenta l’identificazione degli interessati per un arco di tempo non superiore al conseguimento delle' +
    ' finalità per le quali sono trattati',

    headers: [
        { displayName: 'id', columnName: 'id', sortable: true, searchable: false },
        { displayName: 'nome', columnName: 'name' },
        { displayName: 'descrizione', columnName: 'description' }
    ],
    paginationLimit: 10,
    hasDetailPage: true
})
@Component({
    templateUrl: '../../../shared/components/dpm-crud-page/base-crud-page.html',
    styleUrls: ['../../../shared/components/dpm-crud-page/base-crud-page.scss'],
    animations: [AnimationProvider.getFadeIn()]
})
export class RetentionsComponent extends BaseCrudPageComponent {

    records: Retention[] = [];

    constructor(private _service: RetentionService,
                private _stateService: StateService,
                protected _resolver: ComponentFactoryResolver,
                private _containerRef: ViewContainerRef,
                private _toaster: ToastsManager) {
        super();
    }

    protected get defaultTab(): string {
        return 'dettagli';
    }

    protected get dataService(): TableBaseService {
        return this._service;
    }

    protected get stateService(): StateService {
        return this._stateService;
    }

    protected get resolver(): ComponentFactoryResolver {
        return this._resolver;
    }

    protected get viewChildComponent(): any {
        return RetentionsDetailsComponent;
    }

    protected get viewContainerRef(): ViewContainerRef {
        return this._containerRef;
    }

    protected get toastr(): ToastsManager {
        return this._toaster;
    }

}
