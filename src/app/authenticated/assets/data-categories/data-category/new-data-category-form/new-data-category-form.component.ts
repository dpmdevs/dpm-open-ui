/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DPMValidators } from '@dpm-helpers';


@Component({
    selector: 'dpm-new-data-category-form',
    templateUrl: './new-data-category-form.component.html',
    styleUrls: ['../../../../../shared/components/dpm-crud-page/base-details-page/forms/base-form.scss']
})
export class NewDataCategoryFormComponent implements OnInit {

    @Output() addNewDataCategory = new EventEmitter();

    dataCategoryForm: FormGroup;

    constructor(private _formBuilder: FormBuilder) {
    }

    ngOnInit() {
        this.dataCategoryForm = this._formBuilder.group({
            name: ['', [DPMValidators.required, Validators.minLength(3), Validators.maxLength(255)]],
            description: ['', Validators.maxLength(255)]
        });
    }

    onSubmit({ controls }): void {
        if (this.dataCategoryForm.invalid)
            return;

        const { name, description } = controls;
        this.addNewDataCategory.emit({
            name: name.value,
            description: description.value
        });
        this.dataCategoryForm.reset();
    }

}
