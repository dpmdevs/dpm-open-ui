/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, Input, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { isNull, isUndefined } from 'util';
import { ToastsManager } from 'ng2-toastr';
import { Observable } from 'rxjs/Observable';

import { DataCategory, DataType } from '@dpm-models';
import { AnimationProvider } from '@dpm-providers';
import { DataCategoriesService } from '../data-categories.service';
import { ViewDataType } from './data-type/view-data-type';
import { NewDataTypeComponent } from './data-type/new-data-type/new-data-type.component';


@Component({
    selector: '[dpmDataCategory]',
    templateUrl: './data-category.component.html',
    styleUrls: ['./data-category.component.scss'],
    animations: [AnimationProvider.getFadeIn()]
})
export class DataCategoryComponent implements OnInit {
    public NewDataTypeComponent = NewDataTypeComponent;

    @Input() dataCategory: DataCategory;

    dataTypes: ViewDataType[];
    areAllDataTypesEnabled = true;
    isNewDataInsertionActive = false;

    constructor(private _dataCategoriesService: DataCategoriesService, private _toastr: ToastsManager) {
    }

    ngOnInit() {
        this.getAllDataTypes();
    }

    onStartedEditing(dataType: DataType): void {
        this.dataTypes.forEach(dt => dt.doneEditing = dt.id !== dataType.id);
    }

    private getAllDataTypes(): void {
        this._dataCategoriesService.getDataTypes(this.dataCategory.id)
            .subscribe((dataTypes => {
                this.dataTypes = dataTypes.map(dt => {
                    dt.doneEditing = true;
                    return dt;
                });
                this.areAllDataTypesEnabled = this.areAllSameType(true);
            }).bind(this));
    }

    protected toggleAllDataTypes(): void {
        if (this.areAllSameType(true))
            this.dataTypes
                .map(dt => dt.id)
                .forEach(id => this.disableDataType(id));
        else
            this.dataTypes
                .map(dt => dt.id)
                .forEach(id => this.enableDataType(id));
    }

    private areAllSameType(isEnabled: boolean = this.isDataTypeEnabled(this.dataTypes[0].id)): boolean {
        return this.dataTypes
            .map(dt => this.isDataTypeEnabled(dt.id))
            .every(isDTEnabled => isDTEnabled === isEnabled);
    }

    onToggleDataType(id: number): void {
        if (this.isDataTypeEnabled(id))
            this.disableDataType(id);
        else
            this.enableDataType(id);
    }

    private isDataTypeEnabled(id: number): boolean {
        const dataType = this.dataTypes.filter(dt => dt.id === id)[0].deleted_at;

        return isNull(dataType);
    }

    private enableDataType(id: number): void {
        this._dataCategoriesService.enableDataType(id)
            .subscribe(this.getAllDataTypes.bind(this));
    }

    private disableDataType(id: number): void {
        this._dataCategoriesService.disableDataType(id)
            .subscribe(this.getAllDataTypes.bind(this));
    }

    onUpdateDataType({ id, name, description }: DataType): void {
        this._dataCategoriesService.updateDataType(id, { name, description })
            .subscribe(
                (() => {
                    this.getAllDataTypes();
                    this._toastr.info('Record aggiornato correttamente').then();
                }).bind(this),
                this.catchError.bind(this)
            );
    }

    protected toggleDataTypeInsertion(): void {
        this.isNewDataInsertionActive = !this.isNewDataInsertionActive;
    }

    protected addNewDataType(): void {
        const { dataTypeName: name, dataTypeDescription: description } = NewDataTypeComponent;

        if (!this.isNameValid(name))
            return;

        this._dataCategoriesService.addDataType(this.dataCategory.id, { name, description })
            .subscribe(
                (() => {
                    NewDataTypeComponent.resetEntries();
                    this.toggleDataTypeInsertion();
                    this.getAllDataTypes();
                    this._toastr.success('Record inserito correttamente').then();
                }).bind(this),
                this.catchError.bind(this)
            );
    }

    private isNameValid(name): boolean {
        return !isUndefined(name) && name.trim() !== '' && name.trim().length >= 3;
    }

    private catchError({ error, status }: HttpErrorResponse): Observable<HttpErrorResponse> {
        if (status === 422) {
            for (const key in error.errors)
                this._toastr.error(error.errors[key].toString(), '', { dismiss: 'click' }).then();

            return Observable.throw(error);
        }

        return Observable.of(error);
    }

}
