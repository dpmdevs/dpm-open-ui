/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { isUndefined } from 'util';

import { DataType } from '@dpm-models';
import { ViewDataType } from './view-data-type';


@Component({
    selector: '[dpmDataType]',
    templateUrl: './data-type.component.html',
    styleUrls: ['./data-type.component.scss']
})
export class DataTypeComponent {
    @Input() dataType: ViewDataType;

    @Input('doneEditingDataType')
    set doneEditingDataType(doneEditingDataType: boolean) {
        if (doneEditingDataType) {
            this.isNameEditingComplete = true;
            this.isDescriptionEditingComplete = true;
        }
    }

    @Output() toggleDataType = new EventEmitter<number>();
    @Output() updateDataType = new EventEmitter<DataType>();
    @Output() startedEditing = new EventEmitter<DataType>();

    isNameEditingComplete;
    isDescriptionEditingComplete;

    @HostListener('click', ['$event.target'])
    onClick(target) {
        const clickedNodeName = target.nodeName.toLowerCase();

        if (!/^(input|i|span)/.test(clickedNodeName))
            this.toggleDataType.emit(this.dataType.id);

        return false;
    }

    onEditNameStart(): void {
        this.startedEditing.emit(this.dataType);
        this.isNameEditingComplete = false;
        this.isDescriptionEditingComplete = true;
    }

    onEditDescriptionStart(): void {
        this.startedEditing.emit(this.dataType);
        this.isNameEditingComplete = true;
        this.isDescriptionEditingComplete = false;
    }

    updateName(name: string): void {
        if (name !== this.dataType.name && !isUndefined(name))
            this.updateDataType.emit(Object.assign(
                {}, this.dataType, { name }
            ));
    }

    updateDescription(description: string): void {
        if (description !== this.dataType.description && !isUndefined(description))
            this.updateDataType.emit(Object.assign(
                {}, this.dataType, { description }
            ));
    }
}
