/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ToastsManager } from 'ng2-toastr';

import { DataCategory } from '@dpm-models';
import { AnimationProvider } from '@dpm-providers';
import { DataCategoriesService } from './data-categories.service';


@Component({
    selector: 'dpm-data-categories',
    templateUrl: './data-categories.component.html',
    styleUrls: ['./data-categories.component.scss'],
    animations: [AnimationProvider.getFadeIn()]
})
export class DataCategoriesComponent implements OnInit {
    personalDataCategories$: Observable<DataCategory[]>;
    sensitiveDataCategories$: Observable<DataCategory[]>;
    loadedPersonalDataCategories = false;
    loadedSensitiveDataCategories = false;

    constructor(private _dataCategoriesService: DataCategoriesService,
                private _toastr: ToastsManager,
                vcr: ViewContainerRef) {
        _toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit() {
        this.initPersonalDataCategories();
        this.initSensitiveDataCategories();
    }

    initPersonalDataCategories(): void {
        this.loadedPersonalDataCategories = false;
        this.loadedSensitiveDataCategories = false;

        this.personalDataCategories$ = this._dataCategoriesService
            .getPersonalDataCategories()
            .finally(() => { this.loadedPersonalDataCategories = true });
    }

    initSensitiveDataCategories(): void {
        this.loadedPersonalDataCategories = false;
        this.loadedSensitiveDataCategories = false;

        this.sensitiveDataCategories$ = this._dataCategoriesService
            .getSensitiveDataCategories()
            .finally(() => { this.loadedSensitiveDataCategories = true });
    }

    addNewPersonalDataCategory(dataCategory): void {
        this._dataCategoriesService.addPersonalDataCategory(dataCategory).subscribe({
            next: (() => {
                this.initPersonalDataCategories();
                this._toastr.success('Record inserito correttamente').then();
            }).bind(this),
            error: this.catchError.bind(this)
        });
    }

    addNewSensitiveDataCategory(dataCategory): void {
        this._dataCategoriesService.addSensitiveDataCategory(dataCategory).subscribe({
            next: (() => {
                this.initSensitiveDataCategories();
                this._toastr.success('Record inserito correttamente').then();
            }).bind(this),
            error: this.catchError.bind(this)
        });
    }

    private catchError({ error, status }: HttpErrorResponse): Observable<HttpErrorResponse> {
        if (status === 422) {
            for (const err in error.errors)
                this._toastr.error(error.errors[err].toString(), '', { dismiss: 'click' }).then();

            return Observable.throw(error);
        }

        return Observable.of(error);
    }

}
