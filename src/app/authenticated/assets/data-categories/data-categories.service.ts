/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { environment } from 'environments/environment';
import { TableBaseService } from '@dpm-components';
import { DataCategory, DataType, TableData } from '@dpm-models';


@Injectable()
export class DataCategoriesService extends TableBaseService {

    get baseUrl(): string {
        return environment.apiUrl;
    }

    get table(): string {
        return 'data-categories';
    }

    getPersonalDataCategories(): Observable<DataCategory[]> {
        return this.http.get<TableData>(`${this.baseUrl}${this.table}/personal?sortDirection=asc`)
            .map((response: TableData) => <DataCategory[]>response.data);
    }

    addPersonalDataCategory({ name, description }): Observable<any> {
        return this.http.post(`${this.baseUrl}${this.table}`, { name, description, family: 'personal' });
    }

    getSensitiveDataCategories(): Observable<DataCategory[]> {
        return this.http.get<TableData>(`${this.baseUrl}${this.table}/sensitive?sortDirection=asc`)
            .map((response: TableData) => <DataCategory[]>response.data);
    }

    addSensitiveDataCategory({ name, description }): Observable<any> {
        return this.http.post(`${this.baseUrl}${this.table}`, { name, description, family: 'sensitive' });
    }

    getDataTypes(id: number): Observable<DataType[]> {
        return this.http.get<DataType[]>(`${this.baseUrl}${this.table}/${id}/data-types`);
    }

    enableDataType(id: number): Observable<any> {
        return this.http.put(`${this.baseUrl}${this.table}/data-types/${id}/restore`, null);
    }

    disableDataType(id: number): Observable<any> {
        return this.http.delete(`${this.baseUrl}${this.table}/data-types/${id}`);
    }

    updateDataType(id: number, { name = '', description = '' }): Observable<any> {
        return this.http.put(`${this.baseUrl}${this.table}/data-types/${id}`, { name, description });
    }

    addDataType(categoryId: number, { name = '', description = '' }): Observable<any> {
        return this.http.post(`${this.baseUrl}${this.table}/${categoryId}/data-types`, { name, description });
    }
}
