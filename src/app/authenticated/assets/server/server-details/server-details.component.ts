/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';
import { Observable } from 'rxjs/Observable';
import { BsModalService } from 'ngx-bootstrap';

import { Application, DpmTagCouplerHandler, DpmTagCouplerLabels, Tag } from '@dpm-models';
import { AnimationProvider } from '@dpm-providers';
import { BaseDetailsPage } from '@dpm-components';
import { ServerService } from '../server.service';


@Component({
    selector: 'dpm-server-details',
    templateUrl: './server-details.component.html',
    styleUrls: [
        '../../../../shared/components/dpm-crud-page/base-details-page/base-details-page.scss',
        './server-details.component.scss'
    ],
    animations: [AnimationProvider.getFadeIn(), AnimationProvider.getToggleUpDown()]
})
export class ServerDetailsComponent extends BaseDetailsPage implements DpmTagCouplerHandler {

    availableTags$: Observable<Tag[]>;
    assignedTags$: Observable<Tag[]>;
    lastKeyword = '';
    tagCouplerLabels: DpmTagCouplerLabels = {
        header: 'Applicativi Assegnati',
        searchBoxTitle: 'Applicativi Selezionabili',
        placeholder: 'Ricerca Applicativi',
        noTagsFallbackText: 'Nessun applicativo selezionato',
    };

    readonly singleRecordOptions = ['nuovo', 'dettagli', 'modifica'];
    readonly multipleRecordOptions = ['nuovo', 'dettagli'];

    protected initValidationErrors(): void {
        this.validationMessages.errors = {
            model: [],
            description: [],
            brand: [],
            supplier: [],
            ip: [],
            notes: []
        };
    }

    get modalService(): BsModalService {
        return this._modalService;
    }

    constructor(vcr: ViewContainerRef,
                protected serverService: ServerService,
                private _modalService: BsModalService,
                private _toastr: ToastsManager) {
        super();
        _toastr.setRootViewContainerRef(vcr);
        this.dropdownOptions = this.singleRecordOptions;
    }

    ngOnInit() {
        this.activeTabSubscription = this.activeTab$
            .do(() => this.dropdownOptions = this.selectedRecords.length > 1 ? this.multipleRecordOptions : this.singleRecordOptions)
            .subscribe(t => {
                this.activeTab = t;

                if (this.records.length === 1)
                    this.getApplicationsCouplerTags();
            });
    }

    protected getApplicationsCouplerTags(): void {
        this.getAvailableApplications();
        this.getAssignedApplications();
    }

    protected getAvailableApplications(keyword: string = this.lastKeyword): void {
        this.availableTags$ = this.serverService.getAvailableApplications(this.records[0].id, keyword)
            .map(this.mapApplicationsToTags);
    }

    protected getAssignedApplications(): void {
        this.assignedTags$ = this.serverService.getAssignedApplications(this.records[0].id)
            .map(this.mapApplicationsToTags);
    }

    private mapApplicationsToTags(entities: Application[]): Tag[] {
        return entities.map(({ id, name, description: title }) => ({ id, name, title }));
    }

    onToggleTag(tag: Tag): void {
        this.serverService.toggleApplication(this.records[0].id, tag.id)
            .subscribe({
                next: () => this.getApplicationsCouplerTags(),
                complete: () => this._toastr.info('Modifiche effettuate con successo').then(),
            });
    }

    onSearchTag(keyword: string): void {
        this.lastKeyword = keyword;
        this.getApplicationsCouplerTags();
    }
}
