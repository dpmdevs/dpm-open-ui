/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Validators } from '@angular/forms';
import { DPMValidators, IPAddressPattern } from '@dpm-helpers';


export abstract class ServerForm {
    controlsConfig;

    setControlsConfig({ description = '', model = '', brand = '', supplier = '', hostname = '', notes = '', ip = '',
                          operating_system_id = '' }) {
        this.controlsConfig = {
            description: [description, [DPMValidators.required, Validators.maxLength(255)]],
            ip: [ip, Validators.pattern(IPAddressPattern)],
            model: [model, Validators.maxLength(255)],
            hostname: [hostname, Validators.maxLength(255)],
            brand: [brand, Validators.maxLength(255)],
            supplier: [supplier, Validators.maxLength(255)],
            notes: [notes, Validators.maxLength(5000)],
            operating_system_id,
        };
    }
}
