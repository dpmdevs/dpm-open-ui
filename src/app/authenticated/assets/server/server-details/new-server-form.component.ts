/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

import { OperatingSystem, Server } from '@dpm-models';
import { NewForm } from '@dpm-components/dpm-crud-page/base-details-page/forms/new-form';
import { MultipleInheritanceHelper } from '@dpm-helpers';
import { ServerForm } from './server-form';
import { ServerService } from '../server.service';


@Component({
    selector: 'dpm-new-server-form',
    templateUrl: './server-form.html',
    styleUrls: ['../../../../shared/components/dpm-crud-page/base-details-page/forms/base-form.scss', './server-form.scss']
})
export class NewServerFormComponent extends NewForm<Server> implements ServerForm {

    setControlsConfig: (record) => void;
    operatingSystems$: Observable<OperatingSystem[]>;

    constructor(private _formBuilder: FormBuilder, serverService: ServerService) {
        super();
        this.operatingSystems$ = serverService.getOperatingSystems();
    }

    get formBuilder() {
        return this._formBuilder;
    }

}
MultipleInheritanceHelper.applyMixins(NewServerFormComponent, [ServerForm]);
