/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';

import { AnimationProvider, RendererProvider } from '@dpm-providers';
import { Server } from '@dpm-models';
import { BaseCrudPageComponent, GridDecorator, TableBaseService } from '@dpm-components';
import { ServerService } from './server.service';
import { ServerDetailsComponent } from './server-details/server-details.component';
import { StateService } from '../../../state/state.service';


@GridDecorator({
    title: 'Server',
    description: 'Elenco dei server in uso.',

    headers: [
        { displayName: 'id', columnName: 'id'},
        { displayName: 'descrizione', columnName: 'description' },
        { displayName: 'modello', columnName: 'model' },
        { displayName: 'marca', columnName: 'brand' },
        { displayName: 'sistema operativo', columnName: 'operating_system', renderer: RendererProvider.operatingSystemRenderer, searchColumn: 'operating_systems.name' },
        { displayName: 'fornitore', columnName: 'supplier' },
        { displayName: 'hostname', columnName: 'hostname' },
        { displayName: 'indirizzo ip', columnName: 'ip' }
    ],
    paginationLimit: 10,

    hasDetailPage: true
})
@Component({
    templateUrl: '../../../shared/components/dpm-crud-page/base-crud-page.html',
    styleUrls: ['../../../shared/components/dpm-crud-page/base-crud-page.scss'],
    animations: [AnimationProvider.getFadeIn()]
})
export class ServerComponent extends BaseCrudPageComponent {

    records: Server[];

    constructor(private _service: ServerService,
                private _stateService: StateService,
                private _resolver: ComponentFactoryResolver,
                private _viewContainerRef: ViewContainerRef,
                private _toaster: ToastsManager) {
        super();
    }

    protected get defaultTab(): string {
        return 'dettagli';
    }

    protected get dataService(): TableBaseService {
        return this._service;
    }

    protected get stateService(): StateService {
        return this._stateService;
    }

    protected get resolver(): ComponentFactoryResolver {
        return this._resolver;
    }

    protected get viewChildComponent() {
        return ServerDetailsComponent;
    }

    protected get viewContainerRef() {
        return this._viewContainerRef;
    }

    protected get toastr(): ToastsManager {
        return this._toaster;
    }

}
