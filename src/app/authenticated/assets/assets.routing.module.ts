/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PermissionGuard } from '../../core/auth/guards/permission.guard';
import { DataCategoriesComponent } from './data-categories';
import { ApplicationsComponent } from './applications';
import { DataSubjectsComponent } from './data-subjects';
import { RetentionsComponent } from './retentions';
import { SecurityMeasuresComponent } from './security-measures';
import { ServerComponent } from './server';
import { ControlsComponent } from './controls';
import { OperatingSystemsComponent } from './operating-systems';


const routes: Routes = [
    { path: '', redirectTo: '/', pathMatch: 'full' },
    {
        path: 'controls',
        component: ControlsComponent,
        canActivate: [PermissionGuard]
    },
    {
        path: 'data-categories',
        component: DataCategoriesComponent,
        canActivate: [PermissionGuard]
    },
    {
        path: 'data-subjects',
        component: DataSubjectsComponent,
        canActivate: [PermissionGuard]
    },
    {
        path: 'applications',
        component: ApplicationsComponent,
        canActivate: [PermissionGuard]
    },
    {
        path: 'servers',
        component: ServerComponent,
        canActivate: [PermissionGuard]
    },
    {
        path: 'retentions',
        component: RetentionsComponent,
        canActivate: [PermissionGuard]
    },
    {
        path: 'security-measures',
        component: SecurityMeasuresComponent,
        canActivate: [PermissionGuard]
    },
    {
        path: 'operating-systems',
        component: OperatingSystemsComponent,
        canActivate: [PermissionGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AssetsRoutingModule {
}
