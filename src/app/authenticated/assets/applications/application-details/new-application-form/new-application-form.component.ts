/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

import { ApplicationForm } from '../application-form';
import {
    Application, DpmTagCouplerLabels, QueryParams, Server, TableData, Tag, ThirdParties
} from '@dpm-models';
import { NewForm } from '@dpm-components/dpm-crud-page/base-details-page/forms/new-form';
import { MultipleInheritanceHelper } from '@dpm-helpers';
import { ThirdPartiesService } from '../../../../subjects/third-parties';
import { ServerService } from '../../../server';


@Component({
    selector: 'dpm-new-application-form',
    templateUrl: './new-application-form.component.html',
    styleUrls: ['../../../../../shared/components/dpm-crud-page/base-details-page/forms/base-form.scss'],
    providers: [ThirdPartiesService]
})
export class NewApplicationFormComponent extends NewForm<Application> implements ApplicationForm {

    setControlsConfig: (record) => void;

    assignedThirdPartiesTags: Tag[] = [];
    availableThirdPartiesTags$: Observable<Tag[]>;
    lastThirdPartiesKeyword = '';
    tagCouplerThirdPartiesLabels: DpmTagCouplerLabels = {
        header: 'Fornitori Selezionati (Terze parti)',
        searchBoxTitle: 'Fornitori Selezionabili',
        placeholder: 'Ricerca Fornitori',
        noTagsFallbackText: 'Nessun fornitore selezionato',
        optionalText: 'Selezionare un fornitore dall’elenco delle terze parti'
    };

    assignedServersTags: Tag[] = [];
    availableServersTags$: Observable<Tag[]>;
    lastServersKeyword = '';
    tagCouplerServersLabels: DpmTagCouplerLabels = {
        header: 'Server Selezionati',
        searchBoxTitle: 'Server Selezionabili',
        placeholder: 'Ricerca Server',
        noTagsFallbackText: 'Nessun server selezionato',
        optionalText: 'Selezionare un server dall’elenco dei server'
    };

    get formBuilder() {
        return this._formBuilder;
    }

    get assignedThirdPartiesTags$(): Observable<Tag[]> {
        return Observable.of(this.assignedThirdPartiesTags);
    }

    get assignedServersTags$(): Observable<Tag[]> {
        return Observable.of(this.assignedServersTags);
    }

    constructor(private _formBuilder: FormBuilder,
                private _thirdPartiesService: ThirdPartiesService,
                private _serversService: ServerService) {
        super();
    }

    ngOnInit() {
        super.ngOnInit();
        this.getAvailableThirdParties();
        this.getAvailableServers();
    }

    protected getAvailableThirdParties(keyword: string = this.lastThirdPartiesKeyword): void {
        const queryParams = keyword !== ''
            ? <QueryParams>{
                searchOrCriteria: [
                    { columnName: 'company_name', keyword },
                    { columnName: 'description', keyword },
                ]
            } : <QueryParams>{};

        this.availableThirdPartiesTags$ = this._thirdPartiesService.getTableData(queryParams)
            .map((tableData: TableData) => <ThirdParties[]>tableData.data)
            .map(this.mapThirdPartiesToTags.bind(this));
    }

    mapThirdPartiesToTags(thirdParties: ThirdParties[]): Tag[] {
        return thirdParties.map(({ id, company_name: name, description: title }) => ({ id, name, title }));
    }

    onToggleThirdPartiesTag(tag: Tag): void {
        const assignedThirdPartiesIds = this.assignedThirdPartiesTags.map(t => t.id);
        this.assignedThirdPartiesTags = assignedThirdPartiesIds.includes(tag.id)
            ? this.assignedThirdPartiesTags.filter(t => t.id !== tag.id)
            : [...this.assignedThirdPartiesTags, tag];

        this.recordInfo.controls.third_parties.setValue(this.assignedThirdPartiesTags.map(t => t.id));
        this.recordInfo.controls.third_parties.markAsDirty();
    }

    onSearchThirdPartiesTag(keyword: string): void {
        this.lastThirdPartiesKeyword = keyword;
        this.getAvailableThirdParties();
    }

    protected getAvailableServers(keyword: string = this.lastThirdPartiesKeyword): void {
        const queryParams = keyword !== ''
            ? <QueryParams>{
                searchOrCriteria: [
                    { columnName: 'description', keyword },
                    { columnName: 'hostname', keyword },
                ]
            } : <QueryParams>{};

        this.availableServersTags$ = this._serversService.getTableData(queryParams)
            .map((tableData: TableData) => <Server[]>tableData.data)
            .map(this.mapServersToTags.bind(this));
    }

    mapServersToTags(servers: Server[]): Tag[] {
        return servers.map(({ id, hostname, description }) => ({ id, name: `${description} - ${hostname}`, title: description }));
    }

    onToggleServerTag(tag: Tag): void {
        const assignedServersIds = this.assignedServersTags.map(t => t.id);
        this.assignedServersTags = assignedServersIds.includes(tag.id)
            ? this.assignedServersTags.filter(t => t.id !== tag.id)
            : [...this.assignedServersTags, tag];

        this.recordInfo.controls.servers.setValue(this.assignedServersTags.map(t => t.id));
        this.recordInfo.controls.servers.markAsDirty();
    }

    onSearchServerTag(keyword: string): void {
        this.lastServersKeyword = keyword;
        this.getAvailableServers();
    }

}
MultipleInheritanceHelper.applyMixins(NewApplicationFormComponent, [ApplicationForm]);
