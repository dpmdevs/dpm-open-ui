/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component } from '@angular/core';
import { isUndefined } from 'util';
import { Observable } from 'rxjs/Observable';
import { ToastsManager } from 'ng2-toastr';
import { BsModalService } from 'ngx-bootstrap';

import { BaseDetailsPage } from '@dpm-components';
import { AnimationProvider } from '@dpm-providers';
import {
    Application, DpmTagCouplerLabels, SecurityMeasure, SecurityMeasureCategory, Server, Tag, ThirdParties
} from '@dpm-models';
import { ApplicationService } from '../application.service';
import { SecurityMeasuresService } from '../../security-measures';


@Component({
    selector: 'dpm-application-details',
    templateUrl: './application-details.component.html',
    styleUrls: [
        '../../../../shared/components/dpm-crud-page/base-details-page/base-details-page.scss',
        './application-details.component.scss'
    ],
    animations: [AnimationProvider.getFadeIn(), AnimationProvider.getToggleUpDown()]
})
export class ApplicationDetailsComponent extends BaseDetailsPage {

    dataService: ApplicationService;
    records: Application[];
    detailsAccordionStatus = 'unSelected';
    securityMeasuresCategories: SecurityMeasureCategory[] = [];
    availableSecurityMeasures: SecurityMeasure[][] = [];
    selectedSecurityMeasures: SecurityMeasure[] = [];
    accordionsVisibility = ['unSelected', 'unSelected', 'unSelected', 'unSelected', 'unSelected', 'unSelected', 'unSelected'];

    readonly singleRecordOptions = ['nuovo', 'dettagli', 'modifica', 'misure di sicurezza'];
    readonly multipleRecordOptions = ['nuovo', 'dettagli', 'misure di sicurezza'];

    availableThirdPartiesTags$: Observable<Tag[]>;
    assignedThirdPartiesTags$: Observable<Tag[]>;
    lastThirdPartiesKeyword = '';
    thirdPartiesLabels: DpmTagCouplerLabels = {
        header: 'Fornitori assegnati (Terze parti)',
        searchBoxTitle: 'Fornitori disponibili',
        placeholder: 'Ricerca Fornitori',
        noTagsFallbackText: 'Nessun fornitore selezionato',
        optionalText: 'Selezionare un fornitore dall’elenco delle terze parti'
    };

    availableServersTags$: Observable<Tag[]>;
    assignedServersTags$: Observable<Tag[]>;
    lastServersKeyword = '';
    serversLabels: DpmTagCouplerLabels = {
        header: 'Server Assegnati',
        searchBoxTitle: 'Server Selezionabili',
        placeholder: 'Ricerca Server',
        noTagsFallbackText: 'Nessun server selezionato',
    };

    get modalService(): BsModalService {
        return this._modalService;
    }

    constructor(protected _securityMeasuresService: SecurityMeasuresService,
                private _modalService: BsModalService,
                private _toastr: ToastsManager) {
        super();
        this.dropdownOptions = this.singleRecordOptions;
    }

    ngOnInit() {
        this._securityMeasuresService.getSecurityMeasuresCategoriesWithSecurityMeasures('tec')
            .subscribe(([smCategories, securityMeasures]) => {
                this.securityMeasuresCategories = smCategories;
                this.availableSecurityMeasures = securityMeasures;
            });

        this.activeTabSubscription = this.activeTab$
            .do(this.resetAccordions.bind(this))
            .do(() => this.dropdownOptions = this.selectedRecords.length > 1 ? this.multipleRecordOptions : this.singleRecordOptions)
            .subscribe(t => {
                this.activeTab = t;
                this.getSelectedSecurityMeasures();

                if (this.records.length === 1) {
                    this.getThirdPartiesCouplerTags();
                    this.getServersCouplerTags();
                }
            });
    }

    private resetAccordions(): void {
        this.detailsAccordionStatus = 'unSelected';
        this.accordionsVisibility = ['unSelected', 'unSelected', 'unSelected', 'unSelected', 'unSelected', 'unSelected', 'unSelected'];
    }

    protected getThirdPartiesCouplerTags(): void {
        this.getAvailableThirdParties();
        this.getAssignedThirdParties();
    }

    protected getAvailableThirdParties(keyword: string = this.lastThirdPartiesKeyword): void {
        this.availableThirdPartiesTags$ = this.dataService.getAvailableThirdParties(this.records[0].id, keyword)
            .map(this.mapThirdPartiesToTags);
    }

    protected getAssignedThirdParties(): void {
        this.assignedThirdPartiesTags$ = this.dataService.getAssignedThirdParties(this.records[0].id)
            .map(this.mapThirdPartiesToTags);
    }

    private mapThirdPartiesToTags(entities: ThirdParties[]): Tag[] {
        return entities.map(({ id, company_name: name, description: title }) => ({ id, name, title }));
    }

    protected getServersCouplerTags(): void {
        this.getAvailableServers();
        this.getAssignedServers();
    }

    protected getAvailableServers(keyword: string = this.lastServersKeyword): void {
        this.availableServersTags$ = this.dataService.getAvailableServers(this.records[0].id, keyword)
            .map(this.mapServersToTags);
    }

    protected getAssignedServers(): void {
        this.assignedServersTags$ = this.dataService.getAssignedServers(this.records[0].id)
            .map(this.mapServersToTags);
    }

    private mapServersToTags(servers: Server[]): Tag[] {
        return servers.map(({ id, hostname, description }) => ({ id, name: `${description} - ${hostname}`, title: description }));
    }

    getSelectedSecurityMeasures(): void {
        if (isUndefined(this.records[0]))
            return;

        this.dataService.getSelectedSecurityMeasures(this.records[0].id)
            .subscribe(sm => {
                this.selectedSecurityMeasures = sm;
            });
    }

    protected initValidationErrors(): void {
        this.validationMessages.errors = {
            name: [],
            description: [],
            brand: [],
            supplier: []
        };
    }

    toggleAccordion(index): void {
        this.accordionsVisibility[index] = this.accordionsVisibility[index] === 'selected' ? 'unSelected' : 'selected';
    }

    getSelectedSecurityMeasuresForCategory(category: SecurityMeasureCategory): SecurityMeasure[] {
        return this.selectedSecurityMeasures.filter(sm => sm.security_measures_categories_id === category.id);
    }

    isSecurityMeasureSelected(id): boolean {
        return this.selectedSecurityMeasures.some(sm => sm.id === id);
    }

    toggleSecurityMeasure(sm): void {
        this.dataService.toggleSecurityMeasure(this.records[0].id, [sm.id])
            .finally(this.getSelectedSecurityMeasures.bind(this))
            .subscribe(
                () => this.toastr.success('Misure di sicurezza aggiornate'),
                () => this.toastr.error('Misure di sicurezza non aggiornate')
            );
    }

    onToggleThirdParty(tag: Tag): void {
        this.dataService.toggleThirdParty(this.records[0].id, tag.id)
            .subscribe({
                next: () => this.getThirdPartiesCouplerTags(),
                complete: () => this._toastr.info('Modifiche effettuate con successo').then()
            });
    }

    onSearchThirdParty(keyword: string): void {
        this.lastThirdPartiesKeyword = keyword;
        this.getThirdPartiesCouplerTags();
    }

    onToggleServer(tag: Tag): void {
        this.dataService.toggleServer(this.records[0].id, tag.id)
            .subscribe({
                next: () => this.getServersCouplerTags(),
                complete: () => this._toastr.info('Modifiche effettuate con successo').then()
            });
    }

    onSearchServer(keyword: string): void {
        this.lastServersKeyword = keyword;
        this.getServersCouplerTags();
    }

}
