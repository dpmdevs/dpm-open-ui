/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { SecurityMeasure, Server, ThirdParties } from '@dpm-models';
import { TableBaseService } from '@dpm-components';
import { environment } from 'environments/environment';


@Injectable()
export class ApplicationService extends TableBaseService {

    get baseUrl(): string {
        return environment.apiUrl;
    }

    get table(): string {
        return 'applications';
    }

    getAssignedThirdParties(applicationId: number): Observable<ThirdParties[]> {
        return this.http.get<ThirdParties[]>(`${this.baseUrl}${this.table}/${applicationId}/third-parties`);
    }

    getAvailableThirdParties(applicationId: number, keyword?: string): Observable<ThirdParties[]> {
        const url = `${this.baseUrl}${this.table}/${applicationId}/available-third-parties?sortField=company_name&sortDirection=asc`
            + (
                keyword !== ''
                    ? `&searchCriteria=${JSON.stringify([{ columnName: 'company_name', keyword }])}` : ''
            );

        return this.http.get<ThirdParties[]>(url);
    }

    toggleThirdParty(applicationId: number, thirdPartyId: number): Observable<Response> {
        return this.http.put<Response>(`${this.baseUrl}${this.table}/${applicationId}/third-parties`,
            { thirdParties: [thirdPartyId] });
    }

    getAssignedServers(applicationId: number): Observable<Server[]> {
        return this.http.get<Server[]>(`${this.baseUrl}${this.table}/${applicationId}/servers`);
    }

    getAvailableServers(applicationId: number, keyword?: string): Observable<Server[]> {
        const url = `${this.baseUrl}${this.table}/${applicationId}/available-servers?sortField=description&sortDirection=asc`
            + (
                keyword !== ''
                    ? `&searchCriteria=${JSON.stringify([
                        { columnName: 'description', keyword },
                        { columnName: 'hostname', keyword },
                    ])}` : ''
            );

        return this.http.get<Server[]>(url);
    }

    toggleServer(applicationId: number, serverId: number): Observable<Response> {
        return this.http.put<Response>(`${this.baseUrl}${this.table}/${applicationId}/servers`, { servers: [serverId] });
    }

    getSelectedSecurityMeasures(applicationId: number): Observable<SecurityMeasure[]> {
        return this.http.get<SecurityMeasure[]>(`${this.baseUrl}${this.table}/${applicationId}/security-measures`);
    }

    toggleSecurityMeasure(applicationId: number, security_measures: number[]): Observable<Response> {
        return this.http.put<Response>(`${this.baseUrl}${this.table}/${applicationId}/security-measures`, { security_measures });
    }

}
