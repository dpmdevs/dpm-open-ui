/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';

import { BaseCrudPageComponent, GridDecorator, TableBaseService } from '@dpm-components';
import { AnimationProvider } from '@dpm-providers';
import { ControlsService } from './controls.service';
import { ControlsDetailsComponent } from './controls-details/controls-details.component';
import { StateService } from '../../../state/state.service';


@GridDecorator({
    title: 'Controlli di sicurezza',
    description: 'Elenco dei controlli di sicurezza disponibili per l’analisi di impatto.',

    headers: [
        { displayName: 'id', columnName: 'id' },
        { displayName: 'nome', columnName: 'name' },
        { displayName: 'descrizione', columnName: 'description' },
        { displayName: 'tipo di controllo', columnName: 'treatment_type' },
    ],
    paginationLimit: 10,

    hasDetailPage: true
})
@Component({
    templateUrl: '../../../shared/components/dpm-crud-page/base-crud-page.html',
    styleUrls: ['../../../shared/components/dpm-crud-page/base-crud-page.scss'],
    animations: [AnimationProvider.getFadeIn()]
})
export class ControlsComponent extends BaseCrudPageComponent {

    constructor(private _service: ControlsService,
                private _stateService: StateService,
                private _resolver: ComponentFactoryResolver,
                private _viewContainerRef: ViewContainerRef,
                private _toaster: ToastsManager) {
        super();
    }

    protected get defaultTab(): string {
        return 'dettagli';
    }

    protected get dataService(): TableBaseService {
        return this._service;
    }

    protected get stateService(): StateService {
        return this._stateService;
    }

    protected get resolver(): ComponentFactoryResolver {
        return this._resolver;
    }

    protected get viewChildComponent() {
        return ControlsDetailsComponent;
    }

    protected get viewContainerRef() {
        return this._viewContainerRef;
    }

    protected get toastr(): ToastsManager {
        return this._toaster;
    }
}
