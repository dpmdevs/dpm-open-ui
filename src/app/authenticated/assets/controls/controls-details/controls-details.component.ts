/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';
import { BsModalService } from 'ngx-bootstrap';

import { BaseDetailsPage } from '@dpm-components';
import { AnimationProvider } from '@dpm-providers';


@Component({
    selector: 'dpm-controls-details',
    templateUrl: './controls-details.component.html',
    styleUrls: [
        '../../../../shared/components/dpm-crud-page/base-details-page/base-details-page.scss',
        './controls-details.component.scss'
    ],
    animations: [AnimationProvider.getFadeIn()]
})
export class ControlsDetailsComponent extends BaseDetailsPage {

    readonly singleRecordOptions = ['nuovo', 'dettagli', 'modifica'];
    readonly multipleRecordOptions = ['nuovo', 'dettagli'];

    protected initValidationErrors(): void {
        this.validationMessages.errors = {
            name: [],
            description: [],
            treatment_type: []
        };
    }

    get modalService(): BsModalService {
        return this._modalService;
    }

    constructor(vcr: ViewContainerRef,
                private _modalService: BsModalService,
                private _toastr: ToastsManager) {
        super();
        _toastr.setRootViewContainerRef(vcr);
        this.dropdownOptions = this.singleRecordOptions;
    }

    ngOnInit() {
        this.activeTabSubscription = this.activeTab$
            .do(() => this.dropdownOptions = this.selectedRecords.length > 1 ? this.multipleRecordOptions : this.singleRecordOptions)
            .subscribe(t => this.activeTab = t);
    }

}
