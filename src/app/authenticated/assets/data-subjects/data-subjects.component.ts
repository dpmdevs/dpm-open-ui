/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';

import { AnimationProvider } from '@dpm-providers';
import { BaseCrudPageComponent, GridDecorator, TableBaseService } from '@dpm-components';
import { DataSubjectsService } from './data-subjects.service';
import { DataSubjectsDetailsComponent } from './data-subjects-details/data-subjects-details.component';
import { StateService } from '../../../state/state.service';


@GridDecorator({
    title: 'Lista categorie di interessati',
    description: '«Interessato»: Qualsiasi persona fisica identificata o identificabile a cui si riferiscono i dati',

    headers: [
        { displayName: 'id', columnName: 'id', sortable: true, searchable: false },
        { displayName: 'nome', columnName: 'name' },
        { displayName: 'descrizione', columnName: 'description' }
    ],
    paginationLimit: 10,
    hasDetailPage: true
})
@Component({
    templateUrl: '../../../shared/components/dpm-crud-page/base-crud-page.html',
    styleUrls: ['../../../shared/components/dpm-crud-page/base-crud-page.scss'],
    animations: [AnimationProvider.getFadeIn()]
})
export class DataSubjectsComponent extends BaseCrudPageComponent {

    constructor(private _dsService: DataSubjectsService,
                private _stateService: StateService,
                protected _resolver: ComponentFactoryResolver,
                private _containerRef: ViewContainerRef,
                private _toaster: ToastsManager) {
        super();
    }

    get defaultTab(): string {
        return 'dettagli';
    }

    get dataService(): TableBaseService {
        return this._dsService;
    }

    protected get stateService(): StateService {
        return this._stateService;
    }

    get resolver(): ComponentFactoryResolver {
        return this._resolver;
    }

    get viewChildComponent(): any {
        return DataSubjectsDetailsComponent;
    }

    get viewContainerRef(): ViewContainerRef {
        return this._containerRef;
    }

    get toastr(): ToastsManager {
        return this._toaster;
    }

}
