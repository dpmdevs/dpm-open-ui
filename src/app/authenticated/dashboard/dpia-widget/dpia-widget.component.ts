/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component } from '@angular/core';
import { Color } from 'ng2-charts';
import { Observable } from 'rxjs/Observable';

import { DPIAStatistics } from './dpia-statistics';
import { DpiaProjectsService } from '../../dpia-projects/dpia-projects.service';


@Component({
    selector: 'dpm-dpia-widget',
    templateUrl: './dpia-widget.component.html'
})
export class DpiaWidgetComponent {

    dpiaStatistics$: Observable<DPIAStatistics>;
    chartData: any[];

    chartLabels = ['Rischio mitigato', 'Rischio residuo'];

    statistics: DPIAStatistics = {
        residual_risk: 0,
        risk_level: 0
    };

    colors: Array<Color> = [{ backgroundColor: ['#3ebf9b', '#E9573F', '#bbbbbb'] }];

    constructor(private dpiaService: DpiaProjectsService) {
        this.dpiaStatistics$ = this.dpiaService.getStatistics()
            .do((statistics: DPIAStatistics) => {
                this.statistics = statistics;

                this.chartData = [
                    statistics.risk_level - statistics.residual_risk,
                    statistics.residual_risk
                ];
            });
    }
}
