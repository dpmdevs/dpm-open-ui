/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { environment } from '../../../environments/environment';


export interface DpmStatistic {
    displayName: string;
    route: string;
    clientRoute: string;
    count: number
}

@Injectable()
export class DashboardService {

    private baseUrl = environment.apiUrl;

    constructor(private http: HttpClient) {
    }

    /*
     * Applications
     * Servers
     * Third parties
     * Units
     */
    getStatistics(): Observable<DpmStatistic[]> {
        const requests = [
            { displayName: 'Unità', route: 'units', clientRoute: '/subjects/units' },
            { displayName: 'Applicativi', route: 'applications', clientRoute: '/assets/applications' },
            { displayName: 'Server', route: 'servers', clientRoute: '/assets/servers' },
            { displayName: 'Terze parti', route: 'third-parties', clientRoute: '/subjects/third-parties' }
        ]
            .map(statistic =>
                this.http.get(`${this.baseUrl}${statistic.route}/count`)
                    .map((data: { count: number }) => ({ ...data, ...statistic }))
            );
        return Observable.forkJoin(...requests);
    }

}
