/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { UserService } from '../../user/user.service';
import { UserStatistics } from '../../user/user-statistics';


@Component({
    selector: 'dpm-user-widget',
    templateUrl: './user-widget.component.html'
})
export class UserWidgetComponent implements OnInit {

    userStatistics$: Observable<UserStatistics>;

    constructor(private _userService: UserService) {
    }

    ngOnInit() {

        this.userStatistics$ = this._userService.getUserStatistics()
            .map((statistics: UserStatistics) => {
                statistics.usersPerRolePieData = [];
                statistics.usersPerRolePieLabels = [];

                statistics.usersPerRolePieData = statistics.usersPerRole.map(role => role.users);
                statistics.usersPerRolePieLabels = statistics.usersPerRole.map(role => role.role);

                return statistics;
            });
    }

    onResize(chart): void {
        const { style } = chart.canvas;
        style.position = 'absolute';
        style.height = 'inherit';
        style.maxHeight = '100%';
        style.width = 'auto';
        style.top = '50%';
        style.left = '50%';
        style.transform = 'translate(-50%, -50%)';
    }

}
