/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component } from '@angular/core';
import { Color } from 'ng2-charts';
import { Observable } from 'rxjs/Observable';

import { ProcessingActivitiesService } from '../../processing-activities/services/processing-activities.service';
import { PAStatistics } from './pa-statistics';


@Component({
    selector: 'dpm-pa-widget',
    templateUrl: './pa-widget.component.html'
})
export class PaWidgetComponent {

    paStatistics$: Observable<PAStatistics>;
    count = 0;
    chartData = [];
    chartLabels = ['Personali', 'Particolari', 'Super sensibili'];
    chartOptions = { legend: false, cutoutPercentage: 75, maintainAspectRatio: true, onResize: this.onResize };
    colors: Array<Color> = [{ backgroundColor: ['#3ebf9b', '#434a54', '#E9573F'] }];

    constructor(private _paService: ProcessingActivitiesService) {
        this.paStatistics$ = this._paService.getPAStatistics()
            .do((statistics: PAStatistics) => {
                this.chartData = [
                    statistics.personal,
                    statistics.sensitive,
                    statistics.superSensitive
                ];

                this.count = statistics.count;
            });
    }

    onResize(chart): void {
        const { style } = chart.canvas;
        style.position = 'absolute';
        style.height = 'inherit';
        style.maxHeight = '100%';
        style.width = 'auto';
        style.top = '50%';
        style.left = '50%';
        style.transform = 'translate(-50%, -50%)';
    }

}
