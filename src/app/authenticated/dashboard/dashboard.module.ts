/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts';

import { SharedModule } from '../../shared/shared.module';
import { DashboardRoutingModule } from './dashboard.routing.module';
import { DashboardComponent } from './dashboard.component';
import { UserWidgetComponent } from './user-widget/user-widget.component';
import { PaWidgetComponent } from './pa-widget/pa-widget.component';
import { ProcessingActivitiesService } from '../processing-activities/services/processing-activities.service';
import { DpiaWidgetComponent } from './dpia-widget/dpia-widget.component';
import { DpiaProjectsService } from '../dpia-projects/dpia-projects.service';


@NgModule({
    imports: [
        SharedModule,
        DashboardRoutingModule,
        ChartsModule,
    ],
    declarations: [
        DashboardComponent,
        UserWidgetComponent,
        PaWidgetComponent,
        DpiaWidgetComponent,
    ],
    providers: [
        ProcessingActivitiesService,
        DpiaProjectsService
    ]
})
export class DashboardModule {
}
