/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ToastsManager } from 'ng2-toastr';
import { Observable } from 'rxjs/Observable';

import { Controller, DpmTagCouplerLabels, DPO, Tag } from '@dpm-models';
import { StateService } from '../../state/state.service';
import { initialInstanceConfigState, InstanceConfig } from '../../state/config/instanceConfig';
import { ConfigActions } from '../../state/config/config.actions';
import { Config } from '../../state/config/config';
import { ConfigService } from './config.service';
import { DPMValidators } from '@dpm-helpers';
import { isNullOrUndefined } from 'util';


@Component({
    templateUrl: './config.component.html',
    styleUrls: [
        // '../../shared/dpm-crud-page/base-details-page/forms/base-form.scss',
        './config.component.scss'
    ]
})
export class ConfigComponent implements OnInit {

    config: Config;
    instanceConfig$: Observable<InstanceConfig>;
    formGroup: FormGroup;

    assignedControllersTags$: Observable<Tag[]>;
    availableControllersTags$: Observable<Tag[]>;
    availableControllers: Controller[] = [];
    lastControllersKeyword = '';
    tagCouplerControllersLabels: DpmTagCouplerLabels = {
        header: 'Titolare del trattamento',
        placeholder: 'Ricerca Titolare',
        noTagsFallbackText: 'Nessun Titolare Assegnato',
        searchBoxTitle: 'Titolari Selezionabili'
    };

    assignedDPOsTags$: Observable<Tag[]>;
    availableDPOsTags$: Observable<Tag[]>;
    availableDPOs: DPO[] = [];
    lastDPOsKeyword = '';
    tagCouplerDPOsLabels: DpmTagCouplerLabels = {
        header: 'DPO del trattamento',
        placeholder: 'Ricerca DPO',
        noTagsFallbackText: 'Nessun DPO Assegnato',
        searchBoxTitle: 'DPO Selezionabili'
    };

    constructor(private _stateService: StateService,
                private _formBuilder: FormBuilder,
                private _configService: ConfigService,
                private _toaster: ToastsManager,
                private vcr: ViewContainerRef) {
        this._toaster.setRootViewContainerRef(vcr);
        this.formGroup = this._formBuilder.group({
            notification_recipient: ['', DPMValidators.required],
            controller: '',
            controller_id: '',
            internal_processor_active: false,
            dpo: '',
            dpo_id: ''
        });
    }

    ngOnInit() {
        this.instanceConfig$ = this._stateService.getInstanceConfig();
        this._stateService.getConfig()
            .filter(config => !Object.is(config.instanceConfig, initialInstanceConfigState))
            .do(config => this.config = config)
            .map(config => config.instanceConfig)
            .subscribe(data => {
                this.populateForm(data);
                this.getTagCouplerControllersTags();
                this.getTagCouplerDPOsTags();
            });
    }

    populateForm(formData): void {
        this.formGroup.controls.controller.setValue(formData.controller);
        this.formGroup.controls.controller.markAsTouched();

        this.formGroup.controls.notification_recipient.setValue(formData.notification_recipient);
        this.formGroup.controls.notification_recipient.markAsTouched();

        this.formGroup.controls.internal_processor_active.setValue(formData.internal_processor_active);
        this.formGroup.controls.internal_processor_active.markAsTouched();

        let controllerId = null;

        if (!isNullOrUndefined(formData.controller))
            controllerId = formData.controller.id;
        if (!isNullOrUndefined(formData.controller_id))
            controllerId = formData.controller_id;


        this.formGroup.controls.controller_id.setValue(controllerId);
        this.formGroup.controls.controller_id.markAsTouched();

        let dpo = null;
        let dpoId = null;
        if (!isNullOrUndefined(formData.dpo)) {
            dpo = formData.dpo;
            dpoId = formData.dpo.id;
        }

        this.formGroup.controls.dpo.setValue(dpo);
        this.formGroup.controls.dpo.markAsTouched();

        this.formGroup.controls.dpo_id.setValue(dpoId);
        this.formGroup.controls.dpo_id.markAsTouched();
    }

    protected getTagCouplerControllersTags(): void {
        this.getAvailableControllers();
        this.getAssignedController();
    }

    protected getAvailableControllers(keyword: string = this.lastControllersKeyword): void {
        this.availableControllersTags$ = this._configService.getAvailableControllers(keyword)
            .do(controllers => this.availableControllers = controllers)
            .map(this.mapControllersToTags);
    }

    protected getAssignedController(): void {
        try {
            const { id, company_name: name } = this.config.instanceConfig.controller;
            this.assignedControllersTags$ = Observable.of([{ id, name, title: name }]);
        }
        catch (e) {
            this.assignedControllersTags$ = Observable.of([]);
        }
    }

    mapControllersToTags(controllers: Controller[]): Tag[] {
        return controllers.map(({ id, company_name: name }) => ({ id, name, title: name }));
    }

    protected getTagCouplerDPOsTags(): void {
        this.getAvailableDPOs();
        this.getAssignedDPO();
    }

    protected getAvailableDPOs(keyword: string = this.lastDPOsKeyword): void {
        this.availableDPOsTags$ = this._configService.getAvailableDPOs(keyword)
            .do(dpos => this.availableDPOs = dpos)
            .map(this.mapDPOsToTags);
    }

    protected getAssignedDPO(): void {
        try {
            const { id, company_name, firstname, lastname, email } = this.config.instanceConfig.dpo;

            let name;
            if (!isNullOrUndefined(company_name) && company_name !== '')
                name = company_name;
            else if (!isNullOrUndefined(firstname) && !isNullOrUndefined(lastname))
                name = `${firstname} ${lastname}`;
            else
                name = email;

            this.assignedDPOsTags$ = Observable.of([{ id, name, title: name }]);
        }
        catch (e) {
            this.assignedDPOsTags$ = Observable.of([]);
        }
    }

    mapDPOsToTags(dpos: DPO[]): Tag[] {
        return dpos.map(({ id, company_name, firstname, lastname, email }) => {
            let name;
            if (!isNullOrUndefined(company_name) && company_name !== '')
                name = company_name;
            else if (!isNullOrUndefined(firstname) && !isNullOrUndefined(lastname))
                name = `${firstname} ${lastname}`;
            else
                name = email;

            return { id, name, title: name };
        });
    }

    updateInstanceConfig() {
        const instanceConfig = <InstanceConfig>this.formGroup.getRawValue();
        const newConfig: Config = Object.assign({}, this.config, { instanceConfig });

        if (this.formGroup.dirty)
            this._configService
                .update(newConfig.instanceConfig)
                .finally(this.getTagCouplerControllersTags.bind(this))
                .subscribe(
                    () => this._toaster.success('Configurazione aggiornata'),
                    () => this._toaster.error('Aggiornamento non riuscito'),
                    () => this._stateService.dispatch(ConfigActions.updateConfig(newConfig))
                );
    }

    onToggleControllerTag(tag: Tag): void {
        let currentControllerId: number;
        try {
            currentControllerId = this.formGroup.controls.controller.value.id;
        } catch (e) {
            currentControllerId = -1;
        }

        if (currentControllerId === tag.id)
            return;

        const currentController = this.availableControllers.find(c => c.id === tag.id);

        this._configService.toggleController(tag.id)
            .finally(this.updateInstanceConfig.bind(this))
            .subscribe(() => {
                this.getTagCouplerControllersTags();
                this.formGroup.controls.controller.setValue(currentController);
                this.formGroup.controls.controller.markAsDirty();
                this.formGroup.controls.controller_id.setValue(tag.id);
                this.formGroup.controls.controller_id.markAsDirty();
            });
    }

    onSearchControllerTag(keyword: string): void {
        this.lastControllersKeyword = keyword;
        this.getTagCouplerControllersTags();
    }

    onToggleDPOTag(tag: Tag): void {
        const currentDPOId = this.formGroup.controls.dpo.value.id;

        if (currentDPOId === tag.id)
            return;

        const currentDPO = this.availableDPOs.find(c => c.id === tag.id);

        this._configService.toggleDPO(tag.id)
            .finally(this.updateInstanceConfig.bind(this))
            .subscribe(() => {
                this.getTagCouplerDPOsTags();
                this.formGroup.controls.dpo.setValue(currentDPO);
                this.formGroup.controls.dpo.markAsDirty();
                this.formGroup.controls.dpo_id.setValue(tag.id);
                this.formGroup.controls.dpo_id.markAsDirty();
            });
    }

    onSearchDPOTag(keyword: string): void {
        this.lastDPOsKeyword = keyword;
        this.getTagCouplerDPOsTags();
    }

    toggleInternalProcessor(): void {
        const processorControl = this.formGroup.controls.internal_processor_active;
        processorControl.setValue(!processorControl.value);
        processorControl.markAsTouched();
        processorControl.markAsDirty();

        this.updateInstanceConfig();
    }

}
