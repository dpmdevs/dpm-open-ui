/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { Controller, DPO } from '@dpm-models';
import { environment } from 'environments/environment';
import { Config } from '../../state/config/config';
import { InstanceConfig } from '../../state/config/instanceConfig';


@Injectable()
export class ConfigService {

    private configUrl = `${environment.apiUrl}config`;

    constructor(private http: HttpClient) {
    }

    getUserConfig(): Observable<Config> {
        return this.http.get<Config>(`${environment.apiUrl}user/config`);
    }

    update(config: InstanceConfig): Observable<boolean> {
        return this.http.put<boolean>(this.configUrl, config);
    }

    getAvailableControllers(keyword?: string): Observable<Controller[]> {
        const url = `${this.configUrl}/available-controllers`
            + (
                keyword !== ''
                    ? `?searchCriteria=${JSON.stringify([{ columnName: 'company_name', keyword }])}` : ''
            );

        return this.http.get<Controller[]>(url);
    }

    toggleController(controller_id: number): Observable<Response> {
        return this.http.put<Response>(`${this.configUrl}/controller`, { controller_id });
    }

    getAvailableDPOs(keyword?: string): Observable<DPO[]> {
        const url = `${this.configUrl}/available-dpos?sortField=company_name&sortDirection=asc`
            + (
                keyword !== ''
                    ? `&searchOrCriteria=${JSON.stringify([
                        { columnName: 'company_name', keyword },
                        { columnName: 'firstname', keyword },
                        { columnName: 'lastname', keyword }
                    ])}` : ''
            );

        return this.http.get<DPO[]>(url);
    }

    toggleDPO(dpo_id: number): Observable<Response> {
        return this.http.put<Response>(`${this.configUrl}/dpo`, { dpo_id });
    }

}
