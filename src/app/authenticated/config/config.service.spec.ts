/*
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C) 2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { inject, TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { Headers, RequestOptionsArgs, Response, ResponseOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from './config.service';
import { Config } from '../../state/config/config';


const createFakeResponse = function (expectedConfig: Config) {

    const fakeHeaders = new Headers();
    fakeHeaders.append('Content-Type', 'application/json');

    const fakeResponseOptions = new ResponseOptions(
        <RequestOptionsArgs> {
            body: JSON.stringify(expectedConfig),
            status: 200,
            headers: fakeHeaders,
            statusText: 'OK',
            type: '',
            url: ''
        }
    );

    return new Response(fakeResponseOptions);
};


describe('ConfigService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                ConfigService
            ]
        });
    });

    it('should instantiate ConfigService', inject([ConfigService], (service: ConfigService) => {
        expect(service).toBeTruthy();
    }));


    it('should get Config from service', inject([ConfigService, HttpClient], (service: ConfigService, auth: HttpClient) => {

        const expectedConfig = <Config> {
            sections: []
        };

        const fakeResponse = createFakeResponse(expectedConfig);

        spyOn(auth, 'get').and.returnValue(
            <Observable<Response>> Observable.of(fakeResponse));

        service.getUserConfig().subscribe((config: Config) => {

            expect(config).toEqual(expectedConfig);
        });

    }));

});

