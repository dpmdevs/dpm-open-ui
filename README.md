# DpmUi

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.0-beta.32.3.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To getConfig more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


---


## Getting started
 - `$ npm install`
 - while in `dpm-ui/src/` run `$ ln -s ../assets assets`
 - create the environment files (`environment.ts` and `environment.prod.ts`) under `src/environments` following the 
 example below
 - `ng serve`

### Environment
example for `src/environments/environment.prod.ts`:
```typescript
import { Environment } from './environment.interface';

const serverPort = '';
export const serverUrl = location.protocol + '//' + location.hostname + ':' + serverPort + '/';

export const environment: Environment = {
    production: true,
    serverUrl,
    apiUrl: `${serverUrl}api/`,
    authUrl: `${serverUrl}api/auth/`,
    customerLogo: ''
};
```
